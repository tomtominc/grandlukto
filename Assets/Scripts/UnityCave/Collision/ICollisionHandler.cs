﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Collision
{
	public interface ICollisionHandler
	{
		/// <summary>
		/// A list of all triggers that currently collide with the ICollisionHandler.
		/// </summary>
		List<GameObject> ActiveTriggerCollisions { get; }
	}
}