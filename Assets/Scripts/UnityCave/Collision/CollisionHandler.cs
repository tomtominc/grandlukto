﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Collision
{
	// todo: coroutine which checks for nulls in the list?
	public class CollisionHandler : MonoBehaviour, ICollisionHandler
	{
		public List<GameObject> ActiveTriggerCollisions { get; private set; }
		private List<GameObject> temporaryTriggerCollisions;

		public void OnEnable()
		{
			ActiveTriggerCollisions = new List<GameObject>();
		}

		public void FixedUpdate()
		{
			// Delayed updating of list to avoid collection modified exception
			if (temporaryTriggerCollisions != null)
			{
				ActiveTriggerCollisions = temporaryTriggerCollisions;
			}

			temporaryTriggerCollisions = new List<GameObject>(ActiveTriggerCollisions);
		}

		public void OnTriggerEnter2D(Collider2D other)
		{
			var gameObj = other.gameObject;

			if (temporaryTriggerCollisions.Contains(gameObj)) return;

			temporaryTriggerCollisions.Add(gameObj);
		}

		public void OnTriggerExit2D(Collider2D other)
		{
			var gameObj = other.gameObject;

			temporaryTriggerCollisions.Remove(gameObj);
		}
	}
}