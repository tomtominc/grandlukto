﻿namespace UnityCave.Collision
{
	public interface ICollisionHandlerManager
	{
		ICollisionHandler Get(string collisionHandlerName);
	}
}