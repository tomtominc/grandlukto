﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Collision
{
	/// <summary>
	/// Handles a set of ICollisionHandler object and stores them in a dictionary.
	/// </summary>
	public class CollisionHandlerManager : MonoBehaviour, ICollisionHandlerManager
	{
		private Dictionary<string, ICollisionHandler> collisionHandlersByName;

		public CollisionHandlerManager()
		{
			collisionHandlersByName = new Dictionary<string, ICollisionHandler>();
		}

		/// <summary>
		/// Gets the ICollisionHandler with the given name. Throws an Exception if it was not found.
		/// </summary>
		public ICollisionHandler Get(string collisionHandlerName)
		{
			return collisionHandlersByName[collisionHandlerName];
		}

		/// <summary>
		/// Takes the name of the handler and a GameObject that must have an ICollisionHandler component as parameters.
		/// Clones the GameObject and adds it to the list of CollisionHandlers.
		/// </summary>
		public void Add(string name, GameObject collisionHandlerHolder)
		{
			GameObject newGameObject = null;

			try
			{
				newGameObject = Instantiate(collisionHandlerHolder, transform, false);

				var collisionHandler = newGameObject.GetComponent<ICollisionHandler>();
				if (collisionHandler == null)
				{
					throw new Exception("CollisionHandler not found!");
				}

				collisionHandlersByName.Add(name, collisionHandler); // throw an exception if the element already exists
			}
			catch
			{
				// remove created object
				if (newGameObject != null) Destroy(newGameObject);
				throw;
			}
		}
	}
}