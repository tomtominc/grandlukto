﻿using DG.Tweening;
using UnityEngine;

namespace UnityCave.CameraEffects
{
	public class PostProcessingWaveExplosion : MonoBehaviour
	{
		public Material material;
		
		public void Awake()
		{
			material = new Material(Shader.Find("UnityCave/WaveExplosion"));
		}

		public void Initialize(Vector2 center, float radius, float amplitude, float time)
		{
			material.SetFloat("_Radius", 0);
			material.SetFloat("_Amplitude", amplitude);

			center = Camera.main.WorldToViewportPoint(center);
			center.y = 1 - center.y; // shader takes it inverse

			material.SetFloat("_CenterX", center.x);
			material.SetFloat("_CenterY", center.y);

			DOTween.Sequence()
				.Append(material
					.DOFloat(radius, "_Radius", time)
					.SetEase(Ease.OutSine))
				.Join(material
					.DOFloat(0, "_Amplitude", time)
					.SetEase(Ease.OutSine))
				.OnComplete(TweenComplete);
		}

		protected void TweenComplete()
		{
			Destroy(this);
		}
		
		public void OnRenderImage(RenderTexture src, RenderTexture dest)
		{
			UnityEngine.Graphics.Blit(src, dest, material);
		}

		public static PostProcessingWaveExplosion Create(Vector2 center, float targetRadius, float amplitude, float time)
		{
			PostProcessingWaveExplosion postProcessing = Camera.main.gameObject.AddComponent<PostProcessingWaveExplosion>();
			postProcessing.Initialize(center, targetRadius, amplitude, time);
			return postProcessing;
		}
	}
}