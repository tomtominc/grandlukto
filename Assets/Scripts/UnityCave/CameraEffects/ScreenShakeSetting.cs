﻿using System;

// TOOD: Rename to ShakeSetting and move to DOTween extensions
namespace UnityCave.CameraEffects
{
	[Serializable]
	public class ScreenShakeSetting
	{
		public float duration;
		public float strength;
		public int vibrato;
		public float randomness = 90;
		public bool fadeOut = false;

		public ScreenShakeSetting(float duration, float strength, int vibrato)
		{
			this.duration = duration;
			this.strength = strength;
			this.vibrato = vibrato;
		}
	}
}
