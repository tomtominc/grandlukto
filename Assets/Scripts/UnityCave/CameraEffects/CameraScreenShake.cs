﻿using DG.Tweening;
using System;
using UnityEngine;

namespace UnityCave.CameraEffects
{
	public class CameraScreenShake : MonoBehaviour
	{
		// Transform of the camera to shake. Grabs the gameObject's transform if null.
		public Camera compCamera;

		[Header("DEBUG")]
		public bool testOnClick = false;
		public ScreenShakeSetting testSetting;

		private Tween activeShake;
		private ScreenShakeSetting activeSetting;

		public void Awake()
		{
			if (compCamera == null)
			{
				compCamera = GetComponent<Camera>();
			}
		}
		
		public void Shake(ScreenShakeSetting setting)
		{
			StartShake(setting);
		}

		private void StartShake(ScreenShakeSetting setting)
		{
			if(activeShake != null)
			{
				if (activeSetting.strength > setting.strength) return; // ignore if strength is less than the active one

				activeShake.Complete();
			}

			activeSetting = setting;

			activeShake = compCamera
				.DOShakePosition(setting.duration, setting.strength, setting.vibrato, setting.randomness, setting.fadeOut)
				.OnComplete(OnCompleteShake);
		}

		private void OnCompleteShake()
		{
			activeShake = null;
			activeSetting = null;
		}

#if UNITY_EDITOR
		public void Update()
		{
			if (!testOnClick || !Input.GetMouseButtonDown(0)) return;

			StartShake(testSetting);
		}
#endif
	}
}