﻿using System.Linq;
using UnityEngine;

namespace UnityCave
{
	/// <summary>
	/// Acts as base class for static manager classes, allows only one active instance of the class at the same time.
	/// </summary>
	public class ManagerBase : MonoBehaviour
	{
		public virtual void Awake()
		{
			VerifyOnlyOneActiveInstance();
		}

		private void VerifyOnlyOneActiveInstance()
		{
			var existingObjects = FindObjectsOfType(GetType());

			if (existingObjects.Count() == 1) return; // everything is fine, this is the only instance

			Debug.LogWarning(string.Format("Active object of type {0} found. Destroying the second instance.", GetType()));

			Destroy(this);

			var componentsOnGameObj = gameObject.GetComponents<MonoBehaviour>();
			if (componentsOnGameObj.Count() > 1) return; // more than 1 component left, do not destroy GameObject
			if (gameObject.GetComponent<Transform>() == null) return; // other component than transform -> do not destroy 

			Debug.LogWarning("Only Transform component left. Destroying the GameObject.");
			Destroy(gameObject);
		}
	}
}