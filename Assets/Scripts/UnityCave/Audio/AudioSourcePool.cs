﻿using GrandLukto.Database;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Audio
{
	public class AudioSourcePool : MonoBehaviour
	{
		private GameObject poolGameObject;

		private List<AudioSource> idleSources = new List<AudioSource>();
		private List<AudioSource> activeSources = new List<AudioSource>();

		private float lastActiveSourceCheck = 0;

		public void Awake()
		{
			poolGameObject = new GameObject("AudioSourcePool");
			poolGameObject.transform.parent = transform;
			poolGameObject.transform.localPosition = Vector3.zero;
		}

		public void PlaySound(DataSoundSetting soundSetting, float pitch = 1)
		{
			var source = GetIdleSourceSource();
			source.clip = soundSetting.GetRandomAudioClip();
			source.pitch = pitch;
			source.Play();
		}

		private AudioSource GetIdleSourceSource()
		{
			CheckActiveSourcesFinished();

			// check if we have an idle source available
			if (idleSources.Count > 0)
			{
				var idleSource = idleSources[0];
				idleSources.RemoveAt(0);
				activeSources.Add(idleSource);
				return idleSource;
			}

			// create new AudioSource
			var newSource = poolGameObject.AddComponent<AudioSource>();
			activeSources.Add(newSource);
			return newSource;
		}

		private void CheckActiveSourcesFinished()
		{
			if (lastActiveSourceCheck == Time.frameCount) return; // only check once per frame
			lastActiveSourceCheck = Time.frameCount;

			for (var i = 0; i < activeSources.Count; i++)
			{
				var curSource = activeSources[i];
				if (curSource.isPlaying) continue;

				// not playing anymore -> add to idle sources
				activeSources.RemoveAt(i);
				idleSources.Add(curSource);
				i--;
			}
		}

		public void OnDestroy()
		{
			Destroy(poolGameObject);
		}
	}
}