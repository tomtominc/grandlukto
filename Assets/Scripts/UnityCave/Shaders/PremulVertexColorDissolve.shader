// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "UnityCave/PremulVertexColorDissolve" 
{
	Properties 
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

		_DissolveAmount("Dissolve Amount", Range(0, 1)) = 0
		_DissolveTex("Dissolve Texture", 2D) = "white" {}
		_DissolveColor("Dissolve Color", Color) = (0, 0, 1, 1)
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend One OneMinusSrcAlpha
		LOD 110
		
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert_vct
			#pragma fragment frag_mult 
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			#include "Assets/Scripts/UnityCave/Shaders/CGIncludes/Dissolve.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _DissolveTex;
			half _DissolveAmount;
			fixed4 _DissolveColor;

			struct vin_vct 
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f_vct
			{
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 worldPos : TEXCOORD1;
			};

			v2f_vct vert_vct(vin_vct v)
			{
				v2f_vct o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord;
				o.worldPos = mul(unity_ObjectToWorld, o.vertex);
				return o;
			}

			fixed4 frag_mult(v2f_vct i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.texcoord) * i.color;
				if (_DissolveAmount > 0) col = DissolveNonTransparent(col, _DissolveTex, i.worldPos, _DissolveColor, _DissolveAmount);
				return col;
			}
		
			ENDCG
		} 
	}
}
