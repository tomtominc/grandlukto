﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// unlit, vertex colour, premultiplied alpha blend

Shader "UnityCave/Water/Underwater Ripple"
{
	Properties
	{
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_NormalTex("Normal Distortion Map", 2D) = "transparent" {}
		_TileFactor("Tile Factor", Float) = 0.2
		_NoiseFactor("Noise Factor", Float) = 0.03
		_TimeScale("Time Scale", Float) = 0.5
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite Off Lighting Off Cull Off Fog{ Mode Off } Blend One OneMinusSrcAlpha
		LOD 110

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_vct
			#pragma fragment frag_mult 
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _NormalTex;
			float4 _NormalTex_ST;

			float _TileFactor;
			float _NoiseFactor;
			float _TimeScale;

			struct vin_vct
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f_vct
			{
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			v2f_vct vert_vct(vin_vct v)
			{
				v2f_vct o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord;
				return o;
			}

			fixed4 frag_mult(v2f_vct i) : SV_Target
			{
				//float time = _Time / 300.0;
				float time = _Time * _TimeScale;
				
				float2 texCoordNormal0 = i.texcoord * _TileFactor;
				texCoordNormal0 += time;

				float2 texCoordNormal1 = i.texcoord * _TileFactor;
				texCoordNormal1.x -= time;
				texCoordNormal1.y += time;

				float3 normal0 = tex2D(_NormalTex, texCoordNormal0).rgb * 2.0 - 1.0;
				float3 normal1 = tex2D(_NormalTex, texCoordNormal1).rgb * 2.0 - 1.0;
				float3 normal = normalize(normal0 + normal1);

				fixed4 col = tex2D(_MainTex, i.texcoord + _NoiseFactor * normal.xy) * i.color;
				return col;

				//vec2 position = fragCoord.xy / iResolution.xy;
				//vec2 p = position * vec2(-1.0, -2.0);// vec2((position.x + 1.0)/2.0, position.y);


				//if (p.y >= -1.0)
				//{
				//	// On Water Surface
				//	p.y = 1.0 - p.y;
				//	float t = iGlobalTime / 300.0;

				//	vec2 waterCoord = vec2(p.x, 1.0 - position.y);
				//	vec2 texCoordNormal0 = waterCoord * tile_factor;
				//	texCoordNormal0 += t;

				//	vec2 texCoordNormal1 = waterCoord * tile_factor;
				//	texCoordNormal1.s -= t;
				//	texCoordNormal1.t += t;

				//	vec3 normal0 = texture2D(iChannel1, texCoordNormal0).rgb * 2.0 - 1.0;
				//	vec3 normal1 = texture2D(iChannel1, texCoordNormal1).rgb * 2.0 - 1.0;
				//	vec3 normal = normalize(normal0 + normal1);

				//	//the final texture cooridnate is disturbed using the normal texture with some noise factor 
				//	p += noise_factor * normal.xy;
				//}
				//fragColor = texture2D(iChannel0, p);



				
			}
			ENDCG
		}
	}
}