﻿float4 Dissolve(float4 color, sampler2D dissolveTex, float2 uv, float4 dissolveColor, half dissolveRel)
{
	float3 dissolveAmount = tex2D(dissolveTex, uv).rgb - dissolveRel;
	half minDissolve = min(dissolveAmount.r, min(dissolveAmount.g, dissolveAmount.b));

	if (minDissolve <= 0) color = 0; // discard the pixel

	if (minDissolve > 0 && minDissolve < 0.1)
	{
		color = dissolveColor.rgba * minDissolve / 0.1; // fade out the dissolveColor
	}

	return color;
}

float4 DissolveNonTransparent(float4 color, sampler2D dissolveTex, float2 uv, float4 dissolveColor, half dissolveRel)
{
	if (color.a <= 0) return color;

	float3 dissolveAmount = tex2D(dissolveTex, uv).rgb - dissolveRel;
	half minDissolve = min(dissolveAmount.r, min(dissolveAmount.g, dissolveAmount.b));

	if (minDissolve <= 0) color = 0; // discard the pixel

	if (minDissolve > 0 && minDissolve < 0.1)
	{
		color = dissolveColor.rgba * minDissolve / 0.1; // fade out the dissolveColor
	}

	return color;
}