﻿using Spine;
using Spine.Unity;
using UnityEngine;

namespace UnityCave.Spine
{
	public class SpineDestroyAfterAnimation : MonoBehaviour
	{
		private SkeletonAnimation skeletonAnimation;
		
		private void Start()
		{
			skeletonAnimation = GetComponent<SkeletonAnimation>();
			skeletonAnimation.AnimationState.Complete += AnimationState_Complete;
		}

		private void AnimationState_Complete(TrackEntry trackEntry)
		{
			skeletonAnimation.AnimationState.Complete -= AnimationState_Complete;
			Destroy(gameObject);
		}
	}
}