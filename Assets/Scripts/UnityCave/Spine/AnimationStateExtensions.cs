﻿using Spine;

namespace UnityCave.Spine
{
	public static class AnimationStateExtensions
	{
		public static TrackEntry SetAnimationIfNotPlaying(this AnimationState animState, int trackIndex, string animationName, bool loop)
		{
			var current = animState.GetCurrent(trackIndex);
			if (current != null && current.Animation.Name == animationName) return null; // already playing

			return animState.SetAnimation(trackIndex, animationName, loop);
		}
	}
}