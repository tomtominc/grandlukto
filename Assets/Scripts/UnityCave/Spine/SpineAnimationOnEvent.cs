﻿using Spine;
using Spine.Unity;
using UnityEngine;

namespace UnityCave.Spine
{
	public class SpineAnimationOnEvent : MonoBehaviour
	{
		private MeshRenderer compMeshRenderer;
		private SkeletonAnimation compSpineAnim;

		private string animName;

		public void Start()
		{
			compMeshRenderer = GetComponent<MeshRenderer>();
			compSpineAnim = GetComponent<SkeletonAnimation>();

			ToggleAnimationEnabled(false);

			animName = compSpineAnim.AnimationName;
		}
		
		public void PlayAnimation()
		{
			compSpineAnim.AnimationState.ClearTracks();
			compSpineAnim.AnimationState.Complete += OnAnimationComplete;

			compSpineAnim.skeleton.SetToSetupPose();
			compSpineAnim.AnimationState.AddAnimation(0, animName, false, 0);

			ToggleAnimationEnabled(true);
		}

		private void OnAnimationComplete(TrackEntry trackEntry)
		{
			ToggleAnimationEnabled(false);

			compSpineAnim.AnimationState.Complete -= OnAnimationComplete;
		}

		private void ToggleAnimationEnabled(bool isEnabled)
		{
			compMeshRenderer.enabled = isEnabled;
			compSpineAnim.enabled = isEnabled;
		}
	}
}