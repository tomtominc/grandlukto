﻿using Spine.Unity;

namespace UnityCave.Spine
{
	public static class SkeletonRendererExtensions
	{
		public static void ReloadSkeletonData(this SkeletonRenderer skeletonAnimator)
		{
			if (skeletonAnimator.skeletonDataAsset != null)
			{
				foreach (AtlasAsset aa in skeletonAnimator.skeletonDataAsset.atlasAssets)
				{
					if (aa != null)
						aa.Clear();
				}

				skeletonAnimator.skeletonDataAsset.Clear();
			}

			skeletonAnimator.Initialize(true);
		}
	}
}