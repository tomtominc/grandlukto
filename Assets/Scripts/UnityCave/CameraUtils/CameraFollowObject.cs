﻿using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.CameraUtils
{
	[ExecuteInEditMode]
	public class CameraFollowObject : MonoBehaviour
	{
		public GameObject targetGameObject;
		
		public void Update()
		{
			if (targetGameObject == null) return;
			FollowTarget();
		}

		private void FollowTarget()
		{
			transform.position = targetGameObject.transform.position.Clone(z: transform.position.z);
		}
	}
}