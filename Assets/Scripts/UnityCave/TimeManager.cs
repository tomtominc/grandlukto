﻿using UnityCave.Utils;
using UnityEngine;

namespace UnityCave
{
	/// <summary>
	/// Singleton that handles time.
	/// </summary>
	public class TimeManager : ManagerBase
	{
		private bool isPaused = false;

		private float curTimeScale = 1;

		public float TimeScale
		{
			get
			{
				return curTimeScale;
			}

			set
			{
				curTimeScale = value;

				if (IsPaused) return; // if paused, apply after pause finished
				ApplyCurTimeScale();
			}
		}

		public static TimeManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<TimeManager>();
			}
		}

		public override void Awake()
		{
			base.Awake();

			DontDestroyOnLoad(gameObject);
		}

		public bool IsPaused
		{
			get
			{
				return isPaused;
			}
		}

		public void TogglePause(bool doPause)
		{
			if (isPaused == doPause) return;
			isPaused = doPause;

			Time.timeScale = isPaused ? 0 : 1;

			if (!IsPaused) ApplyCurTimeScale();
		}

		public void Reset()
		{
			TogglePause(false);
			TimeScale = 1;
		}

		private void ApplyCurTimeScale()
		{
			Time.timeScale = curTimeScale;
			Time.fixedDeltaTime = 0.02f * curTimeScale;
		}
	}
}