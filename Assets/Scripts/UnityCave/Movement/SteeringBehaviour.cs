﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Movement
{
	// https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732
	public class SteeringBehaviour : MonoBehaviour
	{
		[Header("General")]
		public float velocityMax = 1;
		public float forceMax = 1;
		public float speedMax = 1;

		public bool rotateInDirection = true;

		[Header("Separation")]
		public float separationRadius = 1;
		public float separationMax = 1;

		public Vector3 Velocity { get; private set; }
		private Vector3 force = Vector3.zero;

		public void Seek(Vector3 target)
		{
			var desiredVelocity = (target - transform.position).normalized * velocityMax;
			force += desiredVelocity - Velocity;
		}

		public void Arrive(Vector3 target, float slowingRadius)
		{
			// Calculate the desired velocity
			var desiredVelocity = target - transform.position;
			var desiredVelocityNormalized = desiredVelocity.normalized;
			var distance = desiredVelocity.magnitude;

			// Check the distance to detect whether the character
			// is inside the slowing area
			if (distance < slowingRadius)
			{
				// Inside the slowing area
				desiredVelocity = desiredVelocityNormalized * velocityMax * (distance / slowingRadius);
			}
			else
			{
				// Outside the slowing area.
				desiredVelocity = desiredVelocityNormalized * velocityMax;
			}

			// Set the steering based on this
			force += desiredVelocity - Velocity;
		}

		public void Separation(List<GameObject> otherAgents)
		{
			Vector3 separationForce = Vector3.zero;
			int neighborCount = 0;

			for (int i = 0; i < otherAgents.Count; i++) {
				var other = otherAgents[i];

				if (other != gameObject && Vector3.Distance(other.transform.position, transform.position) <= separationRadius)
				{
					separationForce.x += other.transform.position.x - transform.position.x;
					separationForce.y += other.transform.position.y - transform.position.y;
					neighborCount++;
				}
			}

			if (neighborCount != 0)
			{
				separationForce.x /= neighborCount;
				separationForce.y /= neighborCount;

				separationForce *= -1;
			}

			force += separationForce.normalized * separationMax;
		}

		public void FixedUpdate()
		{
			ApplyForce();

			if (rotateInDirection)
			{
				var velocityNormalized = Velocity.normalized;
				float rotation = Mathf.Atan2(velocityNormalized.y, velocityNormalized.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.Euler(0f, 0f, rotation - 90);
			}
		}

		private void ApplyForce()
		{
			var acceleration = force * Time.deltaTime;
			Velocity += acceleration;
			Vector3.ClampMagnitude(Velocity, speedMax);
			transform.position = transform.position + Velocity * Time.deltaTime;

			force = Vector3.zero;
		}
	}
}
