﻿using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.Movement
{
	[RequireComponent(typeof(SteeringBehaviour))]
	public class FollowPath : MonoBehaviour
	{
		public List<Vector2> path;
		public float reachedTargetRadius = 1;

		public bool TargetReached { get; private set; }

		private int curNode;

		private SteeringBehaviour compSteeringBehaviour;
		
		public void Awake()
		{
			compSteeringBehaviour = GetComponent<SteeringBehaviour>();

			Restart();
		}

		[ContextMenu("Restart")]
		public void Restart()
		{
			TargetReached = false;
			curNode = 0;
		}
		
		public void FixedUpdate()
		{
			if (!path.HasItems()) return;

			var target = path[curNode];
			var distToTarget = (target - (Vector2)transform.position).magnitude;

			if (distToTarget <= reachedTargetRadius)
			{
				curNode++;

				if (curNode > path.Count - 1)
				{
					curNode = path.Count - 1;
					TargetReached = true;
				}
			}

			compSteeringBehaviour.Seek(target);
		}
	}
}
