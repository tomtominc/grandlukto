﻿using UnityEditor;
using UnityEngine;

namespace UnityCave.DepthSorting
{
	[CustomEditor(typeof(DepthSortingPivotPoint2D))]
	public class DepthSortingPivotPoint2DEditor : EditorBase<DepthSortingPivotPoint2D>
	{
		private Renderer compRenderer;

		public void OnEnable()
		{
			compRenderer = Target.GetComponent<Renderer>();
		}
		
		public void OnSceneGUI()
		{
			var yPos = Target.transform.position.y + Target.offset;
			var zPos = Target.transform.position.z;

			var xMin = compRenderer == null ? Target.transform.position.x - 1 : compRenderer.bounds.min.x;
			var xMax = compRenderer == null ? Target.transform.position.x + 1 : compRenderer.bounds.max.x;

			Handles.DrawLine(new Vector3(xMin, yPos, zPos), new Vector3(xMax, yPos, zPos));
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if(GUILayout.Button("Apply sorting to all"))
			{
				DepthSortAll();
			}
		}

		private void DepthSortAll()
		{
			var depthSortingComponents = FindObjectsOfType<DepthSortingPivotPoint2D>();

			foreach(var component in depthSortingComponents)
			{
				component.ApplyDepthSorting();
			}
		}
	}
}