﻿using UnityEngine;

namespace UnityCave.DepthSorting
{
	public class DepthSortingPivotPoint2D : MonoBehaviour
	{
		public float offset = 0f;

		public void LateUpdate()
		{
			ApplyDepthSorting();
		}

		public void ApplyDepthSorting()
		{
			var tmpPos = transform.position;
			tmpPos.z = (tmpPos.y + offset) / 1000;
			transform.position = tmpPos;
		}
	}
}