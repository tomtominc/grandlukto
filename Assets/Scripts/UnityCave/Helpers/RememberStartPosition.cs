﻿using UnityEngine;

namespace UnityCave.Helpers
{
	public class RememberStartPosition : MonoBehaviour
	{
		public bool useLocalPosition;

		private Vector3 startPosition;

		public Vector3 StartPosition
		{
			get { return useLocalPosition ? transform.parent.TransformPoint(startPosition) : startPosition; }
		}

		public Vector3 StartPositionLocal
		{
			get { return useLocalPosition ? startPosition : transform.parent.InverseTransformPoint(startPosition); }
		}

		public void Awake()
		{
			SetCurrentPositionAsStartPosition();
		}

		public void ResetToStartPosition()
		{
			if(useLocalPosition)
			{
				transform.localPosition = startPosition;
			}
			else
			{
				transform.position = startPosition;
			}
		}

		public void SetCurrentPositionAsStartPosition()
		{
			startPosition = useLocalPosition ? transform.localPosition : transform.position;
		}
	}
}