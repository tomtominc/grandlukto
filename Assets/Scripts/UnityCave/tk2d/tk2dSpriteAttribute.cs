﻿using UnityEngine;

namespace UnityCave.tk2d
{
	public class tk2dSpriteAttribute : PropertyAttribute
	{
		public string AtlasName { get; private set; }

		public tk2dSpriteAttribute(string atlasName)
		{
			AtlasName = atlasName;
		}
	}
}