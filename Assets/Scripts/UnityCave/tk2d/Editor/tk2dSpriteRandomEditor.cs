﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace UnityCave.tk2d
{
	[CustomEditor(typeof(tk2dSpriteRandom))]
	public class tk2dSpriteRandomEditor : EditorBase<tk2dSpriteRandom>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			var tk2dSprite = Target.GetComponent<tk2dSprite>();
			var markedForRemove = new List<tk2dSpriteRandomData>();

			foreach(var data in Target.randomSprites)
			{
				EditorGUILayout.LabelField("Random Sprite");
				tk2dSpriteGuiUtility.SpriteSelector(tk2dSprite.Collection, data.spriteId, onSpriteChange, data);
				if (GUILayout.Button("Remove"))
				{
					markedForRemove.Add(data);
				}
			}

			foreach(var data in markedForRemove)
			{
				Target.randomSprites.Remove(data);
			}

			if (GUILayout.Button("Add Element"))
			{
				Target.randomSprites.Add(new tk2dSpriteRandomData());
			}
		}

		private void onSpriteChange(tk2dSpriteCollectionData spriteCollection, int spriteIndex, object callbackData)
		{
			var data = (tk2dSpriteRandomData)callbackData;
			data.spriteId = spriteIndex;
		}
	}
}