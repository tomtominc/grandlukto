﻿using UnityEditor;

namespace UnityCave.tk2d.Editor
{
	public class tk2dSpriteAttributeDrawerValuePair
	{
		public string SelectedSpriteName { get; private set; }
		public SerializedProperty Property { get; private set; }

		public tk2dSpriteAttributeDrawerValuePair(string selectedSpriteName, SerializedProperty property)
		{
			SelectedSpriteName = selectedSpriteName;
			Property = property;
		}
	}
}