﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace UnityCave.tk2d.Editor
{
	[CustomPropertyDrawer(typeof(tk2dSpriteAttribute))]
	public class tk2dSpriteAttributeDrawer : PropertyDrawer
	{
		private const string TextNone = "None";

		private tk2dSpriteCollectionIndex collection;

		private tk2dSpriteAttribute Attribute { get { return attribute as tk2dSpriteAttribute; } }

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (property.propertyType != SerializedPropertyType.String)
			{
				EditorGUI.LabelField(position, "ERROR:", "May only apply to type integer!");
				return;
			}

			tk2dSpriteCollectionIndex selectedCollection;
			if (!TrySelectCollection(out selectedCollection))
			{
				EditorGUI.LabelField(position, "ERROR:", string.Format("Collection {0} not found!", Attribute.AtlasName));
				return;
			}

			position = EditorGUI.PrefixLabel(position, label);
			if (GUI.Button(position, string.IsNullOrEmpty(property.stringValue) ? TextNone : property.stringValue, EditorStyles.popup))
			{
				DisplaySpriteSelectMenu(property);
			}
		}

		private void DisplaySpriteSelectMenu(SerializedProperty property)
		{
			GenericMenu menu = new GenericMenu();

			// none item = 0
			menu.AddItem(new GUIContent(TextNone), string.IsNullOrEmpty(property.stringValue), OnSpriteSelected, new tk2dSpriteAttributeDrawerValuePair(null, property));

			for (int i = 0; i < collection.spriteNames.Length; i++)
			{
				var name = collection.spriteNames[i];
				menu.AddItem(new GUIContent(name), name == property.stringValue, OnSpriteSelected, new tk2dSpriteAttributeDrawerValuePair(name, property));
			}

			menu.ShowAsContext();
		}

		private void OnSpriteSelected(object data)
		{
			var valuePair = (tk2dSpriteAttributeDrawerValuePair)data;

			valuePair.Property.stringValue = valuePair.SelectedSpriteName;
			valuePair.Property.serializedObject.ApplyModifiedProperties();
		}

		private bool TrySelectCollection(out tk2dSpriteCollectionIndex selectedCollection)
		{
			if (collection == null)
			{
				collection = tk2dEditorUtility.GetOrCreateIndex().GetSpriteCollectionIndex().FirstOrDefault(collection => collection.name == Attribute.AtlasName);
			}

			selectedCollection = collection;
			return selectedCollection != null;
		}
	}
}