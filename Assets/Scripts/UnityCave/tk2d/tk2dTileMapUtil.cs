﻿using System.Collections.Generic;
using tk2dRuntime.TileMap;

namespace UnityCave.tk2d
{
	public static class tk2dTileMapUtil
	{
		// stolen from tk2dTileMapUtility
		public static void CleanLayers(tk2dTileMap tileMap)
		{
			if (tileMap.Layers == null) return;

			// Delete old layer render data
			foreach (Layer layer in tileMap.Layers)
			{
				layer.DestroyGameData(tileMap);
				if (layer.gameObject != null)
				{
					tk2dUtil.DestroyImmediate(layer.gameObject);
					layer.gameObject = null;
				}
			}
		}

		// stolen from tk2dTileMapUtility
		public static Layer[] CopyLayers(Layer[] layersToCopy, int width, int height, int partitionSizeX, int partitionSizeY)
		{
			if (layersToCopy == null || layersToCopy.Length < 1)
			{
				return null;
			}

			Layer[] layers = new Layer[layersToCopy.Length];
			for (int layerId = 0; layerId < layersToCopy.Length; ++layerId)
			{
				Layer srcLayer = layersToCopy[layerId];
				layers[layerId] = new Layer(srcLayer.hash, width, height, partitionSizeX, partitionSizeY);
				Layer destLayer = layers[layerId];

				if (srcLayer.IsEmpty)
					continue;

				int hcopy = height;
				int wcopy = width;

				for (int y = 0; y < hcopy; ++y)
				{
					for (int x = 0; x < wcopy; ++x)
					{
						destLayer.SetRawTile(x, y, srcLayer.GetRawTile(x, y));
					}
				}

				destLayer.Optimize();
			}

			return layers;
		}

		public static void MirrorTilesXYAxis(tk2dTileMap tileMap)
		{
			MirrorTilesXAxis(tileMap);
			MirrorTilesYAxis(tileMap);
		}

		/// <summary>
		/// Mirrors the tilemap along the X axis.
		/// Note: Build must be called to accept changes!
		/// </summary>
		public static void MirrorTilesXAxis(tk2dTileMap tileMap)
		{
			if (tileMap.Layers == null || tileMap.Layers.Length < 1) return; // no layer exists

			for (var ctWidth = 0; ctWidth < tileMap.width; ctWidth++)
			{
				// + 1 makes the equation work for 4 (5/2 = 2.5) and so on; - 1 since we start at 0
				float mirrorPoint = ((tileMap.height + 1) / 2f) - 1;
				List<int> tilesBelowHalfHeight = new List<int>();

				for (var ctHeight = 0; ctHeight < tileMap.height; ctHeight++)
				{
					if (ctHeight < mirrorPoint)
					{
						tilesBelowHalfHeight.Add(tileMap.GetTile(ctWidth, ctHeight, 0));
						continue;
					}

					if (ctHeight > mirrorPoint)
					{
						tileMap.SetTile(ctWidth, tilesBelowHalfHeight.Count - 1, 0, tileMap.GetTile(ctWidth, ctHeight, 0));
						tileMap.SetTile(ctWidth, ctHeight, 0, tilesBelowHalfHeight[tilesBelowHalfHeight.Count - 1]);
						tilesBelowHalfHeight.RemoveAt(tilesBelowHalfHeight.Count - 1);
					}
				}
			}
		}

		/// <summary>
		/// Mirrors the tilemap along the Y axis.
		/// Note: Build must be called to accept changes!
		/// </summary>
		public static void MirrorTilesYAxis(tk2dTileMap tileMap)
		{
			if (tileMap.Layers == null || tileMap.Layers.Length < 1) return; // no layer exists

			for (var ctHeight = 0; ctHeight < tileMap.height; ctHeight++)
			{
				// + 1 makes the equation work for 4 (5/2 = 2.5) and so on; - 1 since we start at 0
				float mirrorPoint = ((tileMap.width + 1) / 2f) - 1;
				List<int> tilesBelowHalfWidth = new List<int>();

				for (var ctWidth = 0; ctWidth < tileMap.width; ctWidth++)
				{
					if (ctWidth < mirrorPoint)
					{
						tilesBelowHalfWidth.Add(tileMap.GetTile(ctWidth, ctHeight, 0));
						continue;
					}

					if (ctWidth > mirrorPoint)
					{
						tileMap.SetTile(tilesBelowHalfWidth.Count - 1, ctHeight, 0, tileMap.GetTile(ctWidth, ctHeight, 0));
						tileMap.SetTile(ctWidth, ctHeight, 0, tilesBelowHalfWidth[tilesBelowHalfWidth.Count - 1]);
						tilesBelowHalfWidth.RemoveAt(tilesBelowHalfWidth.Count - 1);
					}
				}
			}
		}
	}
}