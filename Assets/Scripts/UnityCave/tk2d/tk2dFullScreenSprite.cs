﻿using UnityCave.GraphicUtils;
using UnityEngine;

namespace UnityCave.tk2d
{
	/// <summary>
	/// Scales a tk2dSprite so that it covers the full screen.
	/// Only works with centered sprites!
	/// </summary>
	public class tk2dFullScreenSprite : FullScreenGraphic
	{
		private tk2dSprite sprite;

		public void Awake()
		{
			sprite = GetComponent<tk2dSprite>();
		}

		protected override void UpdateGraphic(float width, float height, float halfWidth, float halfHeight)
		{
			if (sprite == null) sprite = GetComponent<tk2dSprite>();

			var spriteBounds = sprite.GetBounds();

			// scale back to get initial extents
			var extents = spriteBounds.extents;
			extents.Scale(new Vector3(1 / sprite.scale.x, 1 / sprite.scale.y, 1 / sprite.scale.z)); // scale to normal size
			spriteBounds.extents = extents;

			// and scale it up again
			sprite.scale = new Vector3(
				halfWidth / spriteBounds.extents.x,
				halfHeight / spriteBounds.extents.y,
				1);
		}
	}
}