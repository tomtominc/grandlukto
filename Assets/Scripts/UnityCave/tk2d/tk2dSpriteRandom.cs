﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.tk2d
{
	[RequireComponent(typeof(tk2dSprite))]
	public class tk2dSpriteRandom : MonoBehaviour
	{
		[HideInInspector]
		public List<tk2dSpriteRandomData> randomSprites;

		public void Start()
		{
			var selectedSprite = randomSprites[Random.Range(0, randomSprites.Count)];

			var tk2dSprite = GetComponent<tk2dSprite>();
			tk2dSprite.spriteId = selectedSprite.spriteId;
		}
	}
}