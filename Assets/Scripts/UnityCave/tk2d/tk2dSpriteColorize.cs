﻿using UnityEngine;

namespace UnityCave.tk2d
{
	public class tk2dSpriteColorize : MonoBehaviour
	{
		public Color color;

		[Range(0, 1)]
		public float flashAmount;

		private Renderer compRenderer;

		private void Awake()
		{
			compRenderer = GetComponent<Renderer>();
		}

		private void Start()
		{
			compRenderer.material.shader = Shader.Find("UnityCave/PremulVertexColorColorize");
		}

		private void Update()
		{
			compRenderer.material.SetColor("_FlashColor", color);
			compRenderer.material.SetFloat("_FlashAmount", flashAmount);
		}
	}
}