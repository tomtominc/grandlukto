﻿using System;
using System.Collections;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityCave
{
	public class ScreenExtendsChangeArgs : EventArgs
	{
		public Vector2 ScreenExtendsWorld { get; private set; }

		public ScreenExtendsChangeArgs(Vector2 resolution)
		{
			ScreenExtendsWorld = resolution;
		}
	}

	public class ScreenManager : ManagerBase
	{
		public static ScreenManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<ScreenManager>();
			}
		}

		private Vector2? lastExtends;

		public EventHandler<ScreenExtendsChangeArgs> ScreenExtendsChange;

		override public void Awake()
		{
			base.Awake();

			DontDestroyOnLoad(gameObject);
		}
		
		public void Update()
		{
			var curExtends = Camera.main.GetScreenExtendsWorld();
			if (!lastExtends.HasValue || lastExtends != curExtends)
			{
				lastExtends = curExtends;
				StartCoroutine(FireScreenExtendsChangeNextFrame(curExtends));
			}
		}

		private IEnumerator FireScreenExtendsChangeNextFrame(Vector2 extends)
		{
			yield return new WaitForEndOfFrame();
			FireScreenExtendsChange(extends);
		}

		private void FireScreenExtendsChange(Vector2 resolution)
		{
			if (ScreenExtendsChange == null) return;
			ScreenExtendsChange.Invoke(this, new ScreenExtendsChangeArgs(resolution));
		}
	}
}