﻿using System.Collections.Generic;

namespace UnityCave.MeshUtils
{
	public class MeshBuilderTriangle
	{
		public int IndexA { get; private set; }
		public int IndexB { get; private set; }
		public int IndexC { get; private set; }

		public MeshBuilderTriangle(int indexA, int indexB, int indexC, int indexOffset = 0)
		{
			IndexA = indexOffset + indexA;
			IndexB = indexOffset + indexB;
			IndexC = indexOffset + indexC;
		}

		public List<int> GetIndices(int startVertexCount)
		{
			var triangleIndices = new List<int> { IndexA, IndexB, IndexC };

			for(var i = 0; i < triangleIndices.Count; i++)
			{
				triangleIndices[i] = startVertexCount + triangleIndices[i];
			}

			return triangleIndices;
		}
	}
}