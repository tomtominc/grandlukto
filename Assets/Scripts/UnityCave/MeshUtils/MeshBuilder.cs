﻿using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.MeshUtils
{
	public class MeshBuilder
	{
		public List<Vector3> Vertices { get; private set; }
		public List<Vector3> Normals { get; private set; }
		public List<Vector2> UVs { get; private set; }
		public List<int> Triangles { get; private set; }

		/// <summary>
		/// Contains the last created mesh or null if none has been created yet.
		/// </summary>
		public Mesh CachedMesh { get; private set; }

		private bool HasNormals
		{
			get
			{
				return Normals.Count > 0;
			}
		}

		public int VerticesCount
		{
			get
			{
				return Vertices.Count;
			}
		}

		public MeshBuilder()
		{
			Vertices = new List<Vector3>();
			Normals = new List<Vector3>();
			UVs = new List<Vector2>();
			Triangles = new List<int>();
		}

		public Mesh CreateMesh()
		{
			var mesh = new Mesh();
			mesh.vertices = Vertices.ToArray();
			if(HasNormals) mesh.normals = Normals.ToArray();
			mesh.triangles = Triangles.ToArray();
			mesh.uv = UVs.ToArray();

			if (HasNormals) mesh.RecalculateNormals();
			mesh.RecalculateBounds();

			CachedMesh = mesh;

			return mesh;
		}

		/// <summary>
		/// Updates the mesh with the current data.
		/// </summary>
		/// <param name="doClear">
		/// If true, mesh gets cleared before assigning the new data.
		/// Set this to true if you updated triangles.
		/// </param>
		public void UpdateMesh(bool doClear = false)
		{
			if (doClear)
			{
				CachedMesh.Clear();
			}

			CachedMesh.vertices = Vertices.ToArray();
			if (HasNormals) CachedMesh.normals = Normals.ToArray();
			CachedMesh.triangles = Triangles.ToArray();
			CachedMesh.uv = UVs.ToArray();

			if (HasNormals) CachedMesh.RecalculateNormals();
			// TODO: do we need RecalculateBounds here?
		}

		/// <summary>
		/// Addes vertices, uvs and triangles. Index of triagnles gets converted to the current index.
		/// </summary>
		/// <param name="verticesToAdd">Vertices to add.</param>
		/// <param name="uvsToAdd">UV coordinates to add.</param>
		/// <param name="trianglesToAdd">Triagnles to add.</param>
		public void AddTriangles(List<Vector3> verticesToAdd, List<Vector2> uvsToAdd, List<MeshBuilderTriangle> trianglesToAdd, List<Vector3> normals = null)
		{
			var nextVertexIndex = VerticesCount;

			Vertices.AddRange(verticesToAdd);
			if(normals != null) normals.AddRange(normals);
			UVs.AddRange(uvsToAdd);

			for(int i = 0; i < trianglesToAdd.Count; i++)
			{
				Triangles.AddRange(trianglesToAdd[i].GetIndices(nextVertexIndex));
			}
		}

		public void AddTriangles(List<Vector3> verticesToAdd, List<Vector2> uvsToAdd, List<int> trianglesToAdd)
		{
			List<MeshBuilderTriangle> convertedTriangles = new List<MeshBuilderTriangle>();

			for(var i = 0; i < trianglesToAdd.Count; i += 3)
			{
				convertedTriangles.Add(new MeshBuilderTriangle(trianglesToAdd[i], trianglesToAdd[i + 1], trianglesToAdd[i + 2]));
			}

			AddTriangles(verticesToAdd, uvsToAdd, convertedTriangles);
		}

		public void AddToGameObject(GameObject go, Material material = null)
		{
			var meshFilter = go.GetOrAddComponent<MeshFilter>();
			meshFilter.mesh = CreateMesh();

			var meshRenderer = go.GetOrAddComponent<MeshRenderer>();
			if(material != null) meshRenderer.material = material;
		}
	}
}
