﻿using System.Collections.Generic;
using UnityCave.GraphicUtils;
using UnityEngine;

namespace UnityCave.MeshUtils
{
	public class FullScreenPlane : FullScreenGraphic
	{
		private MeshBuilder meshBuilder;

		protected override void UpdateGraphic(float width, float height, float halfWidth, float halfHeight)
		{
			if (meshBuilder == null)
			{
				CreateMesh();
			}

			meshBuilder.Vertices[0] = new Vector3(-halfWidth, halfHeight);
			meshBuilder.Vertices[1] = new Vector3(halfWidth, halfHeight);
			meshBuilder.Vertices[2] = new Vector3(-halfWidth, -halfHeight);
			meshBuilder.Vertices[3] = new Vector3(halfWidth, -halfHeight);

			meshBuilder.UVs[0] = new Vector2(-halfWidth, halfHeight);
			meshBuilder.UVs[1] = new Vector2(halfWidth, halfHeight);
			meshBuilder.UVs[2] = new Vector2(-halfWidth, -halfHeight);
			meshBuilder.UVs[3] = new Vector2(halfWidth, -halfHeight);

			meshBuilder.UpdateMesh();
		}

		private void CreateMesh()
		{
			meshBuilder = new MeshBuilder();

			meshBuilder.AddTriangles(
				new List<Vector3>()
				{
					new Vector3(-1, 1, 0),
					new Vector3(1, 1, 0),
					new Vector3(-1, -1, 0),
					new Vector3(1, -1, 0)
				},
				new List<Vector2>()
				{
					new Vector2(-1, 1),
					new Vector2(1, 1),
					new Vector2(-1, -1),
					new Vector2(1, -1)
				},
				new List<MeshBuilderTriangle>()
				{
					new MeshBuilderTriangle(0, 1, 2),
					new MeshBuilderTriangle(1, 3, 2)
				});

			meshBuilder.AddToGameObject(gameObject);
		}
	}
}
