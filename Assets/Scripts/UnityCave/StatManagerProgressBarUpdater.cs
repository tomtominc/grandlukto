﻿using UnityEngine;

namespace UnityCave
{
	public class StatManagerProgressBarUpdater : MonoBehaviour
	{
		public StatManager statManager;

		private tk2dUIProgressBar compProgressBar;

		public void Start()
		{
			compProgressBar = GetComponent<tk2dUIProgressBar>();

			statManager.OnChange.AddListener(OnChangeStat);
		}

		public void OnEnable()
		{
			if(compProgressBar != null) compProgressBar.Value = statManager.ValueRel;
		}

		private void OnChangeStat(StatManager statManager)
		{
			compProgressBar.Value = statManager.ValueRel;
		}
	}
}
