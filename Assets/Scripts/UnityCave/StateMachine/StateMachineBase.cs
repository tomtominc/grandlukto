﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.StateMachine
{
	public class StateMachineBase<TMachine, TState> : MonoBehaviour
		where TMachine : StateMachineBase<TMachine, TState>
		where TState : StateBase<TMachine, TState>
	{
		public TState ActiveState { get; private set; }
		public TState LastDifferentState { get; private set; }

		private Dictionary<Type, TState> cachedStates = new Dictionary<Type, TState>();

		/// <summary>
		/// Tries to change the state of the given type (cached state). Returns false if it fails to change the state (e.g. newState is already active).
		/// </summary>
		public bool TryChangeState(Type stateType)
		{
			var state = GetCachedState(stateType);
			if (state == null) return false; // error

			return TryChangeState(state);
		}

		/// <summary>
		/// Tries to change the state to the newState. Returns false if it fails to change the state (e.g. newState is already active).
		/// </summary>
		/// <param name="newState">The State to change to.</param>
		public bool TryChangeState(TState newState)
		{
			if (ActiveState == newState) return false;

			ChangeState(newState);
			return true;
		}

		public void ChangeState(Type stateType, bool ignoreActiveCheck = false)
		{
			var state = GetCachedState(stateType);
			if (state == null) return; // error

			ChangeState(state, ignoreActiveCheck);
		}

		private TState GetCachedState(Type stateType)
		{
			TState state;
			if (cachedStates.TryGetValue(stateType, out state))
			{
				return state;
			}

			state = (TState)Activator.CreateInstance(stateType);
			if (state == null)
			{
				Debug.Log("Could not create state: " + stateType);
				return null;
			}

			cachedStates[stateType] = state;
			return state;
		}

		public void ChangeState(TState newState, bool ignoreActiveCheck = false)
		{
			if(!ignoreActiveCheck && ActiveState == newState)
			{
				Debug.LogWarning("Changing to currently active state: " + newState);
			}

			if (ActiveState != null)
			{
				ActiveState.OnExit();
			}

			newState.Machine = (TMachine)this;

			if(ActiveState != newState)
			{
				LastDifferentState = ActiveState;
			}

			ActiveState = newState;
			newState.OnEnter();
		}

		public void Update()
		{
			if (ActiveState == null) return; // no state active
			ActiveState.Update();
		}

		public virtual void FixedUpdate()
		{
			if (ActiveState == null) return; // no state active
			ActiveState.FixedUpdate();
		}

		public void LateUpdate()
		{
			if (ActiveState == null) return;
			ActiveState.LateUpdate();
		}

		public virtual void Initialize()
		{
			ActiveState = null;
		}

		public bool IsStateActive(TState state)
		{
			return state == ActiveState;
		}
	}
}