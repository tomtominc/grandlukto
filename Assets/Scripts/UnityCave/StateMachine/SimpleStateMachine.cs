﻿using System;
using UnityEngine;

namespace UnityCave.StateMachine
{
	public class SimpleStateMachine<TEnum> where TEnum : struct, IConvertible, IComparable, IFormattable
	{
		private TEnum initialState;

		public bool DebugModeEnabled { get; set; }

		public TEnum CurState { get; private set; }
		public Action<TEnum> OnExitState = null;
		public Action<TEnum> OnEnterState = null;

		public SimpleStateMachine(TEnum initialState)
		{
			if(!typeof(TEnum).IsEnum)
			{
				Debug.LogError("SimpleStateMachine can only be used with Enums!");
				return;
			}

			this.initialState = initialState;
		}

		public void ChangeState(TEnum newState)
		{
			ChangeState(newState, false);
		}

		public void ChangeStateIfNotActive(TEnum newState)
		{
			if (CurState.Equals(newState)) return;
			ChangeState(newState);
		}

		private void ChangeState(TEnum newState, bool changeIfActive)
		{
			if (!changeIfActive && CurState.Equals(newState))
			{
				Debug.LogWarningFormat("Trying to change to an already active state: {0}", newState);
				return;
			}

			DebugLog(string.Format("[SimpleStateMachine] Changing to state: {0}", newState));

			if (OnExitState != null) OnExitState(CurState);
			CurState = newState;
			if (OnEnterState != null) OnEnterState(newState);
		}

		public void Reset(TEnum initialState)
		{
			DebugLog("[SimpleStateMachine] Reset");

			this.initialState = initialState;
			Reset();
		}

		public void Reset()
		{
			ChangeState(initialState, true);
		}

		private void DebugLog(string logMessage)
		{
			if (!DebugModeEnabled) return;
			Debug.Log(logMessage);
		}
	}
}