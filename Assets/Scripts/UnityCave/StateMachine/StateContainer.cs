﻿using UnityEngine;

namespace UnityCave.StateMachine
{
	public abstract class StateContainer : MonoBehaviour
	{
		private StateBase instance;

		public StateBase Instance
		{
			get
			{
				if (instance == null)
				{
					instance = CreateState();
					instance.StateContainer = this;
				}

				return instance;
			}
		}

		protected abstract StateBase CreateState();
	}
}