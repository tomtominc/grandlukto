﻿using System;
using UnityEngine;

namespace UnityCave.StateMachine
{
	public class StateBase
	{
		public event EventHandler ExitedState;

		public float StateActiveFor { get; private set; }
		public float LastActiveTimestamp { get; private set; }

		public MonoBehaviour StateContainer { get; set; }

		public virtual void OnEnter()
		{
			StateActiveFor = 0;
		}

		public virtual void Update()
		{
			StateActiveFor += Time.deltaTime;
		}

		public virtual void FixedUpdate()
		{
		}

		public virtual void LateUpdate()
		{
		}

		public virtual void OnExit()
		{
			StateActiveFor = 0;
			LastActiveTimestamp = Time.time;

			OnExitedState();
		}

		private void OnExitedState()
		{
			if (ExitedState == null) return;
			ExitedState.Invoke(this, EventArgs.Empty);
		}
	}

	public class StateBase<TMachine, TState> : StateBase
		where TMachine : StateMachineBase<TMachine, TState>
		where TState : StateBase<TMachine, TState>
	{
		public TMachine Machine { get; set; }
	}
}