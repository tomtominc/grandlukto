﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityCave.Tiles;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Tiles
{
	[TestFixture]
	public class TilePatternPositionerConfigParserTest
	{
		private TilePatternPositionerConfigParser parser;

		[SetUp]
		public void SetUp()
		{
			parser = new TilePatternPositionerConfigParser();
		}

		[Test]
		public void ReadConfig_Single_NoSettings()
		{
			var configString = File.ReadAllText(@"Assets\Scripts\UnityCave\Tests\Editor\Tiles\PositionerConfig_Single_NoSettings.txt");

			var config = parser.ParseConfig(configString);
			var patterns = config.GetAllConfigs();

			Assert.AreEqual(1, patterns.Count());

			var pattern = patterns.ElementAt(0);

			Assert.AreEqual(pattern.Width, 4);
			Assert.AreEqual(pattern.Height, 4);

			CollectionAssert.AreEquivalent(
				new List<IntVector2>
				{
					new IntVector2(1, 0),
					new IntVector2(3, 1),
					new IntVector2(0, 2),
					new IntVector2(0, 3)
				},
				pattern.SpawnPositions);
		}

		[Test]
		public void ReadConfig_Multiple_WithSettings()
		{
			var configString = File.ReadAllText(@"Assets\Scripts\UnityCave\Tests\Editor\Tiles\PositionerConfig_Multiple_WithSettings.txt");

			var config = parser.ParseConfig(configString);
			var patterns = config.GetAllConfigs();

			Assert.AreEqual(3, patterns.Count());

			var pattern = patterns.ElementAt(0);
			Assert.AreEqual(pattern.Width, 2);
			Assert.AreEqual(pattern.Height, 2);
			CollectionAssert.AreEquivalent(new List<IntVector2> { new IntVector2(1, 1) }, pattern.SpawnPositions);

			pattern = patterns.ElementAt(1);
			Assert.IsFalse(pattern.MirrorX);
			Assert.IsTrue(pattern.MirrorY);
			Assert.IsFalse(pattern.Rotate);
			Assert.AreEqual(pattern.Width, 2);
			Assert.AreEqual(pattern.Height, 2);
			CollectionAssert.AreEquivalent(new List<IntVector2> { new IntVector2(0, 0) }, pattern.SpawnPositions);

			pattern = patterns.ElementAt(2);
			Assert.IsFalse(pattern.MirrorX);
			Assert.IsFalse(pattern.MirrorY);
			Assert.AreEqual(pattern.Width, 4);
			Assert.AreEqual(pattern.Height, 1);
			CollectionAssert.AreEquivalent(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(3, 0) }, pattern.SpawnPositions);
		}
	}
}