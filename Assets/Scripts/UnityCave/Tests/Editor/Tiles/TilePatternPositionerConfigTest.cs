﻿using NUnit.Framework;
using System.Collections.Generic;
using UnityCave.Tiles;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Tiles
{
	[TestFixture]
	public class TilePatternPositionerConfigTest
	{
		[Test]
		public void AddConfig_AvoidDuplicatePatterns()
		{
			var patternConfig = new TilePatternPositionerPatternConfig
			{
				Height = 3,
				Width = 3,
				MirrorX = true,
				MirrorY = true,
				Rotate = true
			};

			patternConfig.SpawnPositions.Add(new IntVector2(0, 0));

			var config = new TilePatternPositionerConfig();
			config.AddPatternConfig(patternConfig);

			CollectionAssert.AreEquivalent(
				new List<TilePatternPositionerPattern>
				{
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 0) }, 3, 3),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 2) }, 3, 3),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 2) }, 3, 3)
				},
				config.GetAllPatterns());
		}

		[Test]
		public void GetRandomPattern()
		{
			var patternConfig3x3 = new TilePatternPositionerPatternConfig
			{
				Height = 3,
				Width = 3
			};

			patternConfig3x3.SpawnPositions.Add(new IntVector2(0, 0));

			var patternConfig2x2 = new TilePatternPositionerPatternConfig
			{
				Height = 2,
				Width = 2
			};

			patternConfig2x2.SpawnPositions.Add(new IntVector2(0, 0));

			var config = new TilePatternPositionerConfig();
			config.AddPatternConfig(patternConfig3x3);
			config.AddPatternConfig(patternConfig2x2);

			var result = config.GetRandomPattern(2, 2);
			Assert.IsNotNull(result);
		}
	}
}