﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityCave.Tiles;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Tiles
{
	[TestFixture]
	public class TilePatternPositionerPatternTest
	{
		public static IEnumerable Equals_NoNull_Source
		{
			get
			{
				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 5, 7),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 5, 7))
					.Returns(true);

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 5, 7),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 10, 7))
					.SetDescription("Same positions, different width")
					.Returns(false);

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 5, 7),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(3, 1), new IntVector2(4, 6) }, 5, 7))
					.Returns(false);

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 1), new IntVector2(4, 6) }, 5, 7),
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(4, 6), new IntVector2(0, 1) }, 5, 7))
					.Returns(true);
			}
		}

		[TestCaseSource("Equals_NoNull_Source")]
		public bool HashCode_Equal(TilePatternPositionerPattern objA, TilePatternPositionerPattern objB)
		{
			return objA.GetHashCode() == objB.GetHashCode();
		}

		[TestCaseSource("Equals_NoNull_Source")]
		public bool Equals(TilePatternPositionerPattern objA, TilePatternPositionerPattern objB)
		{
			return objA.Equals(objB);
		}
		
		public static IEnumerable Rotate_Source
		{
			get
			{
				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(1, 1) }, 3, 2),
					0,
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(1, 1) }, 3, 2));

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(1, 1) }, 3, 2),
					1,
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(1, 0), new IntVector2(0, 1) }, 2, 3));

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(1, 1) }, 3, 2),
					2,
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(1, 0), new IntVector2(2, 1) }, 3, 2));

				yield return new TestCaseData(
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(1, 1) }, 3, 2),
					3,
					new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(1, 1), new IntVector2(0, 2) }, 2, 3));
			}
		}

		[TestCaseSource("Rotate_Source")]
		public void Rotate(TilePatternPositionerPattern patternToRotate, int rotateTimes, TilePatternPositionerPattern expectedPattern)
		{
			var result = patternToRotate.Rotate(rotateTimes);
			Assert.AreEqual(expectedPattern, result);
		}

		[Test]
		public void MirrorX()
		{
			var pattern = new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 3), new IntVector2(2, 1) }, 4, 4);
			var result = pattern.MirrorX();

			Assert.AreEqual(
				new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(1, 1), new IntVector2(3, 3) }, 4, 4),
				result);
		}

		[Test]
		public void MirrorY()
		{
			var pattern = new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 3), new IntVector2(2, 1) }, 4, 4);
			var result = pattern.MirrorY();

			Assert.AreEqual(
				new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0), new IntVector2(2, 2) }, 4, 4),
				result);
		}

		[Test]
		public void Copy()
		{
			var sourcePattern = new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 3), new IntVector2(2, 1) }, 4, 4);
			var result = sourcePattern.Copy();

			Assert.AreNotSame(sourcePattern, result);
			Assert.AreEqual(sourcePattern, result);
		}
	}
}