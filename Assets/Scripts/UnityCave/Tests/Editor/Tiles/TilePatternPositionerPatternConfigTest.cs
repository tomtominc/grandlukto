﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityCave.Tiles;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Tiles
{
	[TestFixture]
	public class TilePatternPositionerPatternConfigTest
	{
		public static IEnumerable EvaluateConfig_Source
		{
			get
			{
				var config = CreateSimpleConfig();
				yield return new TestCaseData(
					config,
					new List<TilePatternPositionerPattern>
					{
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3)
					}
				);

				config = CreateSimpleConfig();
				config.MirrorX = true;
				yield return new TestCaseData(
					config,
					new List<TilePatternPositionerPattern>
					{
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 0) }, 3, 3)
					}
				);

				config = CreateSimpleConfig();
				config.MirrorY = true;
				yield return new TestCaseData(
					config,
					new List<TilePatternPositionerPattern>
					{
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 2) }, 3, 3)
					}
				);

				config = CreateSimpleConfig();
				config.MirrorX = true;
				config.MirrorY = true;
				yield return new TestCaseData(
					config,
					new List<TilePatternPositionerPattern>
					{
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 2) }, 3, 3)
					}
				);

				config = CreateSimpleConfig();
				config.Rotate = true;
				yield return new TestCaseData(
					config,
					new List<TilePatternPositionerPattern>
					{
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 0) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(2, 2) }, 3, 3),
						new TilePatternPositionerPattern(new List<IntVector2> { new IntVector2(0, 2) }, 3, 3)
					}
				);
			}
		}

		[TestCaseSource("EvaluateConfig_Source")]
		public void EvaluateConfig(TilePatternPositionerPatternConfig config, IEnumerable<TilePatternPositionerPattern> expectedPatterns)
		{
			var result = config.EvaluateConfig();
			CollectionAssert.AreEquivalent(expectedPatterns, result);
		}

		private static TilePatternPositionerPatternConfig CreateSimpleConfig()
		{
			var config = new TilePatternPositionerPatternConfig
			{
				MirrorX = false,
				MirrorY = false,
				Rotate = false,
				Width = 3,
				Height = 3
			};

			config.SpawnPositions.Add(new IntVector2(0, 0));
			return config;
		}
	}
}