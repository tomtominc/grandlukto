﻿using NUnit.Framework;
using RAIN.Memory;
using RAIN.Representation;
using System;
using UnityCave.RAIN;
using UnityEngine;

namespace UnityCave.Tests.Editor.RAIN
{
	[TestFixture]
	public class ExpressionExtensionsTest
	{
		[Test]
		public void EvaluatePosition2D_VariableNotSet()
		{
			var expression = new Expression();
			expression.SetVariable("Test");

			var memory = new BasicMemory();

			Assert.Throws<NotImplementedException>(() => expression.EvaluatePosition2D(0, memory));
		}

		[Test]
		public void EvaluatePosition2D_FromNull()
		{
			Assert.Throws<NotImplementedException>(() => EvaluatePosition2D((object)null));
		}

		[Test]
		public void EvaluatePosition2D_FromGameObject()
		{
			var givenVector = new Vector2(10, 10);

			var gameObj = new GameObject();
			gameObj.transform.position = givenVector;

			Assert.AreEqual(givenVector, EvaluatePosition2D(gameObj));
		}

		[Test]
		public void EvaluatePosition2D_FromMonoBehaviour()
		{
			var givenVector = new Vector2(10, 10);

			var gameObj = new GameObject();
			gameObj.transform.position = givenVector;

			var monoBehaviour = gameObj.AddComponent<TestMonoBehaviour>();

			Assert.AreEqual(givenVector, EvaluatePosition2D(monoBehaviour));
		}

		[Test]
		public void EvaluatePosition2D_FromVector2()
		{
			var givenVector = new Vector2(10, 10);
			Assert.AreEqual(givenVector, EvaluatePosition2D(givenVector));
		}

		[Test]
		public void EvaluatePosition2D_FromVector3()
		{
			var givenVector = new Vector3(10, 10);
			Assert.AreEqual((Vector2)givenVector, EvaluatePosition2D(givenVector));
		}
		
		private Vector2 EvaluatePosition2D<T>(T variableValue)
		{
			var expression = new Expression();
			expression.SetVariable("Test");

			var memory = new BasicMemory();
			memory.SetItem("Test", variableValue);

			return expression.EvaluatePosition2D(0, memory);
		}

		private class TestMonoBehaviour : MonoBehaviour
		{
		}
	}
}