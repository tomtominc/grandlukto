﻿using NUnit.Framework;
using System.Linq;
using UnityEngine;

namespace UnityCave.Tests.Editor
{
	[TestFixture]
	public class EntityManagerTest
	{
		private EntityManager entityManager;

		[SetUp]
		public void SetUp()
		{
			var testObj = new GameObject();
			entityManager = testObj.AddComponent<EntityManager>();
		}
		
		[Test]
		public void GetAll_ManagerContainsObjectsOfType()
		{
			FillEntityManager();
			Assert.AreEqual(3, entityManager.GetAll<TestTypeA>().Count());
		}

		[Test]
		public void GetAll_NoObjectsOfType()
		{
			FillEntityManager();
			Assert.AreEqual(0, entityManager.GetAll<TestTypeC>().Count());
		}

		[Test]
		public void Add_Remove()
		{
			FillEntityManager();

			var entity = new GameObject();
			entityManager.Add<TestTypeC>(entity);
			entityManager.Remove<TestTypeC>(entity);

			Assert.AreEqual(0, entityManager.GetAll<TestTypeC>().Count());
		}

		private void FillEntityManager()
		{
			entityManager.Add<TestTypeA>(new GameObject());
			entityManager.Add<TestTypeA>(new GameObject());
			entityManager.Add<TestTypeA>(new GameObject());

			entityManager.Add<TestTypeB>(new GameObject());
			entityManager.Add<TestTypeB>(new GameObject());
		}

		private class TestTypeA
		{
		}

		private class TestTypeB
		{
		}

		private class TestTypeC
		{
		}
	}
}