﻿using NUnit.Framework;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.Tests.Editor.ExtensionMethods
{
	[TestFixture]
	public class LayerMaskExtensionsTest
	{
		[TestCase(0, ExpectedResult = true)]
		[TestCase(1, ExpectedResult = true)]
		[TestCase(2, ExpectedResult = false)]
		[TestCase(3, ExpectedResult = true)]
		[TestCase(4, ExpectedResult = false)]
		public bool ContainsLayer(int layer)
		{
			LayerMask mask = 1 | 2 | 8;

			return mask.ContainsLayer(layer);
		}
	}
}