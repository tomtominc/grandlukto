﻿using NUnit.Framework;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.Tests.Editor.ExtensionMethods
{
	[TestFixture]
	public class BoundsExtensionsTest
	{
		[TestCase(0, ExpectedResult = 0.5f)]
		[TestCase(0.5f, ExpectedResult = 1f)]
		[TestCase(1, ExpectedResult = 1.5f)]
		public float GetRelativeXPosition(float relativeX)
		{
			var bounds = new Bounds(new Vector3(1, 1, 1), new Vector3(1, 1, 1));
			return bounds.GetRelativeXPosition(relativeX);
		}

		[TestCase(-1, 1, 1, 9, 9)]
		[TestCase(1, -1, -1, 11, 11)]
		[TestCase(0, 0, 0, 10, 10)]
		public void AddOnEachSide(float padding, float expectedMinX, float expectedMinY, float expectedMaxX, float expectedMaxY)
		{
			var bounds = new Bounds(new Vector3(5, 5, 5), new Vector3(10, 10, 1));

			bounds = bounds.AddOnEachSide(padding);

			Assert.AreEqual(expectedMinX, bounds.min.x);
			Assert.AreEqual(expectedMinY, bounds.min.y);
			Assert.AreEqual(expectedMaxX, bounds.max.x);
			Assert.AreEqual(expectedMaxY, bounds.max.y);
			Assert.AreEqual(1f, bounds.size.z); // avoid negative z size!
		}
	}
}