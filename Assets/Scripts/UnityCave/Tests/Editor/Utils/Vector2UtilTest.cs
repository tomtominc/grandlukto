﻿using NUnit.Framework;
using System.Collections;
using UnityCave.Utils;
using UnityEngine;

namespace UnityCave.Tests.Editor.Utils
{
	[TestFixture]
	public class Vector2UtilTest
	{
		public static IEnumerable GetClosestPointOnLineSegment_Source
		{
			get
			{
				yield return new TestCaseData(new Vector2(3, 0)).Returns(new Vector2(3, 2));
				yield return new TestCaseData(new Vector2(0, 3)).Returns(new Vector2(2, 2));
				yield return new TestCaseData(new Vector2(9, 1)).Returns(new Vector2(6, 2));
			}
		}

		[TestCaseSource("GetClosestPointOnLineSegment_Source")]
		public Vector2 GetClosestPointOnLineSegment(Vector2 testPoint)
		{
			return Vector2Util.GetClosestPointOnLineSegment(new Vector2(2, 2), new Vector2(6, 2), testPoint);
		}
	}
}