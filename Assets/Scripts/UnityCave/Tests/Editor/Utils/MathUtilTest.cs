﻿using NUnit.Framework;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Utils
{
	[TestFixture]
	public class MathUtilTest
	{
		[TestCase(0, 3, ExpectedResult = 0)]
		[TestCase(1, 3, ExpectedResult = 1)]
		[TestCase(2, 3, ExpectedResult = 2)]
		[TestCase(3, 3, ExpectedResult = 0)]
		[TestCase(4, 3, ExpectedResult = 1)]
		[TestCase(-1, 3, ExpectedResult = 2)]
		[TestCase(-2, 3, ExpectedResult = 1)]
		[TestCase(-3, 3, ExpectedResult = 0)]
		public int PositiveModulo(int i, int n)
		{
			return MathUtil.PositiveModulo(i, n);
		}
	}
}