﻿using NUnit.Framework;
using System.Collections;
using UnityCave.Utils;

namespace UnityCave.Tests.Editor.Utils
{
	[TestFixture]
	public class IntVector2Test
	{
		public static IEnumerable Addition_Source
		{
			get
			{
				yield return new TestCaseData(new IntVector2(0, 0), new IntVector2(2, 2), new IntVector2(2, 2));
				yield return new TestCaseData(new IntVector2(2, 2), new IntVector2(-4, 0), new IntVector2(-2, 2));
				yield return new TestCaseData(new IntVector2(6, 10), new IntVector2(4, 10), new IntVector2(10, 20));
			}
		}

		[TestCaseSource("Addition_Source")]
		public void Addition(IntVector2 vectorA, IntVector2 vectorB, IntVector2 expectedResult)
		{
			Assert.AreEqual(expectedResult, vectorA + vectorB);
		}

		[TestCaseSource("Addition_Source")]
		public void Substraction(IntVector2 expectedResult, IntVector2 vectorB, IntVector2 vectorA)
		{
			Assert.AreEqual(expectedResult, vectorA - vectorB);
		}
	}
}