﻿using RAIN.Memory;
using RAIN.Representation;
using System;
using UnityEngine;

namespace UnityCave.RAIN
{
	public static class ExpressionExtensions
	{
		public static Vector2 EvaluatePosition2D(this Expression expression, float aDeltaTime, RAINMemory aWorkingMemory)
		{
			var evaluated = expression.Evaluate<object>(aDeltaTime, aWorkingMemory);

			if (evaluated is Vector2)
			{
				return (Vector2)evaluated;
			}

			if (evaluated is GameObject)
			{
				return ((GameObject)evaluated).transform.position;
			}

			if (evaluated is MonoBehaviour)
			{
				return ((MonoBehaviour)evaluated).transform.position;
			}

			if (evaluated is Vector3)
			{
				return (Vector3)evaluated;
			}

			throw new NotImplementedException();
		}
	}
}