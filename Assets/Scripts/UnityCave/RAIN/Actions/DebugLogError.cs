﻿using RAIN.Action;
using RAIN.Core;
using UnityEngine;

namespace UnityCave.Rain.Actions
{
	/// <summary>
	/// Logs the action's name as error.
	/// </summary>
	[RAINAction]
	public class DebugLogError : RAINAction
	{
		public override ActionResult Execute(AI ai)
		{
			Debug.LogError(actionName);
			return ActionResult.SUCCESS;
		}
	}
}