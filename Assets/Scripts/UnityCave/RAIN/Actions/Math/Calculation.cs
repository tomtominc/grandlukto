﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.ExtensionMethods.RAIN;

namespace UnityCave.RAIN.Actions.Math
{
	public abstract class Calculation<TElement> : RAINAction
	{
		public Expression elementA = new Expression();
		public Expression elementB = new Expression();

		public Expression outResult = new Expression();

		public override ActionResult Execute(AI ai)
		{
			var elementAValue = elementA.Evaluate<TElement>(ai.DeltaTime, ai.WorkingMemory);
			var elementBValue = elementB.Evaluate<TElement>(ai.DeltaTime, ai.WorkingMemory);

			var result = Calculate(elementAValue, elementBValue);
			outResult.TrySetVariable(ai.WorkingMemory, result);

			return ActionResult.SUCCESS;
		}

		protected abstract TElement Calculate(TElement valueA, TElement valueB);
	}
}