﻿using RAIN.Action;

namespace UnityCave.RAIN.Actions.Math
{
	[RAINAction("Integer Addition")]
	public class IntegerAddition : Calculation<int>
	{
		protected override int Calculate(int valueA, int valueB)
		{
			return valueA + valueB;
		}
	}
}