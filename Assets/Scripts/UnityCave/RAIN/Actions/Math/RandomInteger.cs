﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.ExtensionMethods.RAIN;
using UnityEngine;

namespace UnityCave.RAIN.Actions.Math
{
	[RAINAction("Random integer")]
	public class RandomInteger : RAINAction
	{
		public Expression minValue = new Expression();
		public Expression maxValue = new Expression();

		public Expression outValue = new Expression();

		public override ActionResult Execute(AI ai)
		{
			var minValueInt = minValue.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);
			var maxValueInt = maxValue.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);

			var randomValue = Random.Range(minValueInt, maxValueInt);
			outValue.TrySetVariable(ai.WorkingMemory, randomValue);

			return ActionResult.SUCCESS;
		}
	}
}