﻿using RAIN.Action;

namespace UnityCave.RAIN.Actions.Math
{
	[RAINAction("Integer Subtraction")]
	public class IntegerSubtraction : Calculation<int>
	{
		protected override int Calculate(int valueA, int valueB)
		{
			return valueA - valueB;
		}
	}
}