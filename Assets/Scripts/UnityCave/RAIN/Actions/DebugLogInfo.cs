﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace UnityCave.Rain.Actions
{
	/// <summary>
	/// Logs the ActionName as info log.
	/// </summary>
	[RAINAction]
	public class DebugLogInfo : RAINAction
	{
		public Expression placeholderValue1 = new Expression();

		public override ActionResult Execute(AI ai)
		{
			if (placeholderValue1.IsValid)
			{
				Debug.LogFormat(actionName, placeholderValue1.Evaluate<string>(ai.DeltaTime, ai.WorkingMemory));
			}
			else
			{
				Debug.Log(actionName);
			}

			return ActionResult.SUCCESS;
		}
	}
}