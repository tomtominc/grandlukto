﻿using UnityEditor;
using UnityEngine;

namespace UnityCave
{
	public class EditorHeightCalculator
	{
		private float curHeight;

		public float Height
		{
			get
			{
				return curHeight;
			}
		}

		public EditorHeightCalculator()
		{
			Reset();
		}

		public void Reset()
		{
			curHeight = 0;
		}

		public void Add(float height)
		{
			curHeight += height;
		}

		public void AddProperty(SerializedProperty property, GUIContent label, int addVerticalSpacing = 1)
		{
			curHeight += EditorGUI.GetPropertyHeight(property, label);
			AddVerticalSpacing(addVerticalSpacing);
		}

		public void AddProperty(SerializedProperty property, int addVerticalSpacing = 1)
		{
			AddProperty(property, null, addVerticalSpacing);
		}

		public void AddLine(int addVerticalSpacing = 1)
		{
			AddLines(1, addVerticalSpacing);
		}

		public void AddLines(int numLines, int addVerticalSpacing = 1)
		{
			curHeight += EditorGUIUtility.singleLineHeight * numLines;
			AddVerticalSpacing(addVerticalSpacing);
		}

		public void AddVerticalSpacing(int multiplier = 1)
		{
			curHeight += EditorGUIUtility.standardVerticalSpacing * multiplier;
		}
	}
}