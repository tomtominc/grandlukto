﻿using UnityEngine;
using UnityEditor;

namespace UnityCave
{
	public class EditorBase<T> : Editor where T : Object
	{
		public T Target
		{
			get
			{
				return (T)target;
			}
		}
	}
}