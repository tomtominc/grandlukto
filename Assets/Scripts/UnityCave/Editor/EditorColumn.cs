﻿using UnityEditor;
using UnityEngine;

namespace UnityCave
{
	public class EditorColumn
	{
		private Rect columnRect;
		private float curYOffset = 0;

		public void Reset(Rect columnRect)
		{
			this.columnRect = columnRect;
			curYOffset = 0;
		}

		public bool DrawPropertyField(SerializedProperty prop, GUIContent label, int verticalSpacingMult = 1)
		{
			var propHeight = EditorGUI.GetPropertyHeight(prop, label);
			return EditorGUI.PropertyField(GetRect(propHeight, verticalSpacingMult), prop, label);
		}

		public bool DrawPropertyField(SerializedProperty prop, int verticalSpacingMult = 1)
		{
			return DrawPropertyField(prop, null, 1);
		}

		public void Offset(float yOffset)
		{
			curYOffset += yOffset;
		}

		public Rect GetRect(float height, int verticalSpacingMult = 0)
		{
			var rect = new Rect(columnRect.x, columnRect.y + curYOffset, columnRect.width, height);
			curYOffset += height;
			curYOffset += EditorGUIUtility.standardVerticalSpacing * verticalSpacingMult;
			return rect;
		}
		
		public Rect GetLineRect(int lines = 1, int verticalSpacingMult = 1)
		{
			return GetRect(lines * EditorGUIUtility.singleLineHeight, verticalSpacingMult);
		}
	}
}