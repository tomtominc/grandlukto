﻿using System;
using UnityEditor;

namespace UnityCave
{
	public static class SerializedPropertyExtensions
	{
		public static T GetEnum<T>(this SerializedProperty prop) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}

			return (T)Enum.GetValues(typeof(T)).GetValue(prop.enumValueIndex);
		}
	}
}