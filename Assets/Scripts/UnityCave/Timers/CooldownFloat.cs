﻿using System.Collections;
using UnityEngine;

namespace UnityCave.Timers
{
	public class CooldownFloat : MonoBehaviour, ICooldown
	{
		private float cooldownTime;
		private Coroutine cooldownRoutine;

		public bool IsOnCooldown { get { return cooldownRoutine != null; } }

		/// <summary>
		/// Initializes the cooldown with the given cooldown in seconds
		/// </summary>
		public void Initialize(float cooldownTime)
		{
			this.cooldownTime = cooldownTime;
		}

		public void StartCooldown()
		{
			if (cooldownRoutine != null) StopCoroutine(cooldownRoutine);
			cooldownRoutine = StartCoroutine(CooldownRoutine());
		}

		private IEnumerator CooldownRoutine()
		{
			yield return new WaitForSeconds(cooldownTime);
			cooldownRoutine = null;
		}
	}
}