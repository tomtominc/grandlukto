﻿using UnityCave.CustomDataTypes;
using UnityEngine;

namespace UnityCave.Timers
{
	public class CooldownFactory
	{
		public ICooldown Create(GameObject target, float cooldown)
		{
			var cooldownFloat = target.AddComponent<CooldownFloat>();
			cooldownFloat.Initialize(cooldown);
			return cooldownFloat;
		}

		public ICooldown Create(GameObject target, FloatRange cooldown)
		{
			var cooldownFloat = target.AddComponent<CooldownFloatRange>();
			cooldownFloat.Initialize(cooldown);
			return cooldownFloat;
		}
	}
}