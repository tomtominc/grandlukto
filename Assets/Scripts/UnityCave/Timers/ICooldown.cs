﻿using UnityEngine;

namespace UnityCave.Timers
{
	public interface ICooldown
	{
		/// <summary>
		/// Returns true if the cooldown is active.
		/// </summary>
		bool IsOnCooldown { get; }

		/// <summary>
		/// Starts the cooldown.
		/// </summary>
		void StartCooldown();
	}
}