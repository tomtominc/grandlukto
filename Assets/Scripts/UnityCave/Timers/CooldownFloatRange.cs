﻿using System;
using System.Collections;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace UnityCave.Timers
{
	public class CooldownFloatRange : MonoBehaviour, ICooldown
	{
		private FloatRange range;
		private Coroutine cooldownRoutine;

		public bool IsOnCooldown { get { return cooldownRoutine != null; } }

		/// <summary>
		/// Initializes the cooldown with a cooldown range in seconds.
		/// </summary>
		public void Initialize(FloatRange range)
		{
			this.range = range;
		}

		public void StartCooldown()
		{
			if (range == null) throw new ArgumentNullException();
			if (cooldownRoutine != null) StopCoroutine(cooldownRoutine);

			cooldownRoutine = StartCoroutine(CooldownRoutine());
		}

		private IEnumerator CooldownRoutine()
		{
			yield return new WaitForSeconds(range);
			cooldownRoutine = null;
		}
	}
}