﻿using System.Collections.Generic;
using UnityCave.Utils;

namespace UnityCave.Tiles
{
	public class TilePatternPositionerPatternConfig
	{
		public bool MirrorX { get; set; }
		public bool MirrorY { get; set; }
		public bool Rotate { get; set; }

		public int Height { get; set; }
		public int Width { get; set; }

		public List<IntVector2> SpawnPositions { get; private set; }

		public TilePatternPositionerPatternConfig()
		{
			MirrorX = true;
			MirrorY = true;
			Rotate = true;

			SpawnPositions = new List<IntVector2>();
		}

		/// <summary>
		/// Evalutes the config and returns all possible patterns.
		/// </summary>
		public IEnumerable<TilePatternPositionerPattern> EvaluateConfig()
		{
			var rotateTimes = Rotate ? 3 : 0;
			var basePattern = new TilePatternPositionerPattern(SpawnPositions, Width, Height);

			for (var i = 0; i <= rotateTimes; i++)
			{
				var rotated = basePattern.Rotate(i);
				yield return rotated.Copy();

				if (MirrorX) yield return rotated.MirrorX();
				if (MirrorY) yield return rotated.MirrorY();
			}
		}
	}
}