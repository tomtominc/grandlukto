﻿using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;

namespace UnityCave.Tiles
{
	public class TilePatternPositionerConfig
	{
		private SortedDictionary<int, SortedDictionary<int, List<TilePatternPositionerPatternConfig>>> patternsByWidthAndHeight = new SortedDictionary<int, SortedDictionary<int, List<TilePatternPositionerPatternConfig>>>();
		private Dictionary<IntVector2, HashSet<TilePatternPositionerPattern>> patternsBySize = new Dictionary<IntVector2, HashSet<TilePatternPositionerPattern>>();

		public void AddPatternConfig(TilePatternPositionerPatternConfig patternConfig)
		{
			SortedDictionary<int, List<TilePatternPositionerPatternConfig>> patternsByHeight;
			if (!patternsByWidthAndHeight.TryGetValue(patternConfig.Width, out patternsByHeight))
			{
				patternsByHeight = new SortedDictionary<int, List<TilePatternPositionerPatternConfig>>();
				patternsByWidthAndHeight[patternConfig.Width] = patternsByHeight;
			}

			List<TilePatternPositionerPatternConfig> patternList;
			if (!patternsByHeight.TryGetValue(patternConfig.Height, out patternList))
			{
				patternList = new List<TilePatternPositionerPatternConfig>();
				patternsByHeight[patternConfig.Height] = patternList;
			}

			patternList.Add(patternConfig);
			EvaluatePatternConfig(patternConfig);
		}

		private void EvaluatePatternConfig(TilePatternPositionerPatternConfig patternConfig)
		{
			var patterns = patternConfig.EvaluateConfig();

			foreach (var pattern in patterns)
			{
				AddPattern(pattern);
			}
		}

		private void AddPattern(TilePatternPositionerPattern pattern)
		{
			var size = new IntVector2(pattern.Width, pattern.Height);

			HashSet<TilePatternPositionerPattern> patternsForSize;
			if (!patternsBySize.TryGetValue(size, out patternsForSize))
			{
				patternsForSize = new HashSet<TilePatternPositionerPattern>();
				patternsBySize.Add(size, patternsForSize);
			}

			patternsForSize.Add(pattern);
		}

		public IEnumerable<TilePatternPositionerPatternConfig> GetAllConfigs()
		{
			foreach(var patternsByHeight in patternsByWidthAndHeight)
			{
				foreach(var patternList in patternsByHeight.Value)
				{
					foreach(var pattern in patternList.Value)
					{
						yield return pattern;
					}
				}
			}
		}

		public IEnumerable<TilePatternPositionerPattern> GetAllPatterns()
		{
			foreach (var patterns in patternsBySize)
			{
				foreach (var pattern in patterns.Value)
				{
					yield return pattern;
				}
			}
		}

		public IEnumerable<TilePatternPositionerPattern> GetPatterns(IntVector2 size)
		{
			HashSet<TilePatternPositionerPattern> patterns;
			return patternsBySize.TryGetValue(size, out patterns) ? patterns : Enumerable.Empty<TilePatternPositionerPattern>();
		}

		public TilePatternPositionerPattern GetRandomPattern(int width, int height)
		{
			return GetRandomPattern(width, height, width, height);
		}

		public TilePatternPositionerPattern GetRandomPattern(int minWidth, int minHeight, int maxWidth, int maxHeight)
		{
			var possiblePatterns = Enumerable.Empty<TilePatternPositionerPattern>();

			for (var width = minWidth; width <= maxWidth; width++)
			{
				for (var height = minHeight; height <= maxHeight; height++)
				{
					var size = new IntVector2(width, height);
					possiblePatterns = possiblePatterns.Concat(GetPatterns(size));
				}
			}

			return possiblePatterns.SelectRandom();
		}
	}
}