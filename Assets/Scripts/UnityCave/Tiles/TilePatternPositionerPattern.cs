﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityCave.Utils;

namespace UnityCave.Tiles
{
	public class TilePatternPositionerPattern : IEquatable<TilePatternPositionerPattern>
	{
		public ReadOnlyCollection<IntVector2> SpawnPositions { get; private set; }

		private int hashCode;

		public int Width { get; private set; }
		public int Height { get; private set; }

		public TilePatternPositionerPattern(IEnumerable<IntVector2> spawnPositions, int width, int height)
			: this(spawnPositions.ToList(), width, height)
		{
		}

		public TilePatternPositionerPattern(List<IntVector2> spawnPositions, int width, int height)
		{
			Width = width;
			Height = height;

			spawnPositions.Sort(SortPositionsEvaluateWidthAndHeight);
			
			SpawnPositions = spawnPositions.AsReadOnly();

			hashCode = GenerateHashCode();
		}

		private TilePatternPositionerPattern(TilePatternPositionerPattern patternToCopy)
		{
			Width = patternToCopy.Width;
			Height = patternToCopy.Height;
			SpawnPositions = patternToCopy.SpawnPositions;
			hashCode = patternToCopy.hashCode;
		}

		private int SortPositionsEvaluateWidthAndHeight(IntVector2 posA, IntVector2 posB)
		{
			var comparisonResult = posA.x.CompareTo(posB.x);
			if (comparisonResult != 0) return comparisonResult;

			return posA.y.CompareTo(posB.y);
		}
		
		public TilePatternPositionerPattern Rotate(int rotateTimes)
		{
			rotateTimes = MathUtil.PositiveModulo(rotateTimes, 4);

			switch (rotateTimes)
			{
				case 0:
					return new TilePatternPositionerPattern(SpawnPositions.ToList(), Width, Height);

				case 1:
					return new TilePatternPositionerPattern(
						SpawnPositions
							.Select(spawnPosition => new IntVector2(
								Height - 1 - spawnPosition.y,
								spawnPosition.x)),
							Height,
							Width);

				case 2:
					return new TilePatternPositionerPattern(
							SpawnPositions
								.Select(spawnPosition => new IntVector2(
									Width - 1 - spawnPosition.x,
									Height - 1 - spawnPosition.y)),
							Width,
							Height);

				case 3:
					return new TilePatternPositionerPattern(
							SpawnPositions
								.Select(spawnPosition => new IntVector2(
									spawnPosition.y,
									Width - 1 - spawnPosition.x)),
							Height,
							Width);

				default: throw new NotImplementedException();
			}
		}

		public TilePatternPositionerPattern MirrorX()
		{
			return new TilePatternPositionerPattern(
				SpawnPositions.Select(spawnPosition => new IntVector2(
					Width - 1 - spawnPosition.x,
					spawnPosition.y)),
				Width,
				Height);
		}

		public TilePatternPositionerPattern MirrorY()
		{
			return new TilePatternPositionerPattern(
				SpawnPositions.Select(spawnPosition => new IntVector2(
					spawnPosition.x,
					Height - 1 - spawnPosition.y)),
				Width,
				Height);
		}

		public TilePatternPositionerPattern Copy()
		{
			return new TilePatternPositionerPattern(this);
		}

		public bool Equals(TilePatternPositionerPattern other)
		{
			return
				Width == other.Width && Height == other.Height &&
				SpawnPositions.SequenceEqual(other.SpawnPositions);
		}

		public override bool Equals(object obj)
		{
			return obj is TilePatternPositionerPattern && Equals((TilePatternPositionerPattern)obj);
		}

		private int GenerateHashCode()
		{
			unchecked
			{
				var hash = 19;

				hash = hash * 31 + Width;
				hash = hash * 31 + Height;

				foreach (var spawnPosition in SpawnPositions)
				{
					hash = hash * 31 + spawnPosition.GetHashCode();
				}

				return hash;
			}
		}

		public override int GetHashCode()
		{
			return hashCode;
		}
	}
}