﻿using System;
using System.IO;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;

namespace UnityCave.Tiles
{
	public class TilePatternPositionerConfigParser
	{
		private const char SpawnChar = '1';
		private const char PlaceholderChar = '0';
		private readonly char[] SettingsSplitCharacters = new char[] { ',' };

		private TilePatternPositionerConfig config;

		private TilePatternPositionerPatternConfig curSpawnPattern;
		private bool isReadingSpawnPositions;
		private int curSpawnY;

		public TilePatternPositionerConfigParser()
		{
		}

		public TilePatternPositionerConfig ParseConfig(string spawnConfiguration)
		{
			config = new TilePatternPositionerConfig();

			var reader = new StringReader(spawnConfiguration);

			for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
			{
				if (line.IsNullOrWhiteSpace())
				{
					if (curSpawnPattern != null)
					{
						SaveCurConfig();
					}

					continue; // skip empty lines at beginning
				}

				ReadConfigLine(line);
			}

			SaveCurConfig();
			return config;
		}

		private void ReadConfigLine(string line)
		{
			if (curSpawnPattern == null)
			{
				StartNewPattern();
			}

			if (!isReadingSpawnPositions)
			{
				var firstChar = line[0];

				if (IsSpawnPatternCharacter(firstChar))
				{
					ReadSettingsLine(line);
					return;
				}

				isReadingSpawnPositions = true;
			}

			ReadSpawnPositionsLine(line);
		}

		private void ReadSpawnPositionsLine(string line)
		{
			curSpawnPattern.Width = Math.Max(curSpawnPattern.Width, line.Length);

			for (var i = 0; i < line.Length; i++)
			{
				var curCharacter = line[i];

				if (curCharacter == SpawnChar)
				{
					curSpawnPattern.SpawnPositions.Add(new IntVector2(i, curSpawnY));
				}
			}

			curSpawnY++;
			curSpawnPattern.Height = curSpawnY;
		}

		private void ReadSettingsLine(string line)
		{
			var settings = line.Split(SettingsSplitCharacters, StringSplitOptions.RemoveEmptyEntries);

			foreach(var setting in settings)
			{
				ReadSetting(setting);
			}
		}

		private void ReadSetting(string setting)
		{
			switch (setting.ToLower())
			{
				case "nomirror":
					curSpawnPattern.MirrorX = false;
					curSpawnPattern.MirrorY = false;
					break;

				case "nomirrorx":
					curSpawnPattern.MirrorX = false;
					break;

				case "nomirrory":
					curSpawnPattern.MirrorY = false;
					break;

				case "norotate":
					curSpawnPattern.Rotate = false;
					break;

				default:
					throw new NotImplementedException(string.Format("Setting '{0}' is not supported!", setting));
			}
		}

		private void StartNewPattern()
		{
			curSpawnPattern = new TilePatternPositionerPatternConfig();
			isReadingSpawnPositions = false;
			curSpawnY = 0;
		}

		private void SaveCurConfig()
		{
			config.AddPatternConfig(curSpawnPattern);
			curSpawnPattern = null;
		}

		/// <summary>
		/// Returns true if the character is a character that occurs in the spawn patterns.
		/// </summary>
		private static bool IsSpawnPatternCharacter(char firstChar)
		{
			return firstChar != SpawnChar && firstChar != PlaceholderChar;
		}
	}
}