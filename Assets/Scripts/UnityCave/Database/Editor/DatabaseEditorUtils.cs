﻿using System;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityCave.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace UnityCave.Database
{
	public static class DatabaseEditorUtils
	{
		public static TDatabase LoadDatabase<TDatabase>()
			where TDatabase : ScriptableObject, IDatabase
		{
			var dataName = GetDataName(typeof(TDatabase));
			return AssetDatabase.LoadAssetAtPath<TDatabase>(string.Format("Assets/Data/DB_{0}.asset", dataName));
		}

		public static TData CreateNewDataObject<TDatabase, TData>()
			where TDatabase : DatabaseBase<TData>
			where TData : ScriptableObject
		{
			var db = LoadDatabase<TDatabase>();
			var dataName = GetDataName(typeof(TDatabase));

			var newObject = ScriptableObjectUtil.Create<TData>(string.Format("Assets/Data/DB_{0}", dataName), dataName);
			Undo.RegisterCreatedObjectUndo(newObject, "Create new " + dataName);

			Undo.RecordObject(db, "Added entry to database");
			db.Add(newObject);

			EditorUtils.SetDirty(db);

			return newObject;
		}

		private static string GetDataName(Type dataType)
		{
			return dataType.Name
				.RemoveAtStart("Database")
				.RemoveAtStart("Data");
		}
	}
}