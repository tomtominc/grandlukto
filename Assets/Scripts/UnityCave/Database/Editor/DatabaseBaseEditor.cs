﻿using System;
using UnityCave.Utils;
using UnityEditor;
using UnityEngine;

namespace UnityCave.Database
{
	// todo: automatically create database!
	// todo: automatically create folder if not exists
	public abstract class DatabaseBaseEditor<TDatabase, TData> : EditorWindow
		where TData : ScriptableObject
		where TDatabase : DatabaseBase<TData>
	{
		private bool stylesInitialized = false;
		private GUIStyle styleElement;
		private GUIStyle styleElementSelected;

		private TDatabase database;

		private TData selectedElement;
		private Editor selectedElementEditor;

		private Vector2 listScrollPosition;

		private string databasePath;
		private string newObjectName;
		private Type customEditorType;

		public DatabaseBaseEditor(string databasePath, string newObjectName, Type customEditorType = null)
		{
			this.databasePath = databasePath;
			this.newObjectName = newObjectName;
			this.customEditorType = customEditorType;
		}

		public void OnEnable()
		{
			if (database == null) LoadDatabase();
		}

		private void LoadDatabase()
		{
			database = (TDatabase)AssetDatabase.LoadAssetAtPath(databasePath + ".asset", typeof(TDatabase));
		}

		public void OnGUI()
		{
			if (!stylesInitialized) InitStyles();

			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
			DisplayListArea();
			DisplayMainArea();
			EditorGUILayout.EndHorizontal();
		}

		private void InitStyles()
		{
			styleElement = new GUIStyle("box");
			styleElementSelected = new GUIStyle("box") { fontStyle = FontStyle.Bold };

			stylesInitialized = true;
		}

		private void DisplayListArea()
		{
			EditorGUILayout.BeginVertical(GUILayout.Width(250));
			EditorGUILayout.Space();

			listScrollPosition = EditorGUILayout.BeginScrollView(listScrollPosition, "box", GUILayout.ExpandHeight(true));

			for (int i = 0; i < database.Count; i++)
			{
				var curElement = database.Get(i);

				EditorGUILayout.BeginHorizontal();
				if (GUILayout.Button("-", GUILayout.Width(25)))
				{
					if (selectedElement == curElement) selectedElement = null;

					ScriptableObjectUtil.Remove(curElement);
					database.RemoveAt(i);

					SaveAllChanges();
					return;
				}

				var name = GetElementName(curElement);

				if (GUILayout.Button(name, selectedElement == curElement ? styleElementSelected : styleElement, GUILayout.ExpandWidth(true)))
				{
					selectedElement = database.Get(i);
				}

				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.EndScrollView();

			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
			EditorGUILayout.LabelField("Entries: " + database.Count, GUILayout.Width(100));

			if (GUILayout.Button("New..."))
			{
				var newObject = ScriptableObjectUtil.Create<TData>(databasePath, newObjectName);
				database.Add(newObject);

				SaveAllChanges();
			}

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
		}

		private void DisplayMainArea()
		{
			EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
			EditorGUILayout.Space();

			if (selectedElement != null) DisplayEditMainArea();

			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
		}
		
		private void DisplayEditMainArea()
		{
			Editor.CreateCachedEditor(selectedElement, customEditorType, ref selectedElementEditor);
			selectedElementEditor.OnInspectorGUI();

			if (GUILayout.Button("Select in Project window"))
			{
				EditorGUIUtility.PingObject(selectedElement);
			}
		}

		private void SaveAllChanges()
		{
			EditorUtility.SetDirty(database);
		}

		protected virtual string GetElementName(TData curElement)
		{
			return ScriptableObjectUtil.GetFileName(curElement);
		}
	}
}