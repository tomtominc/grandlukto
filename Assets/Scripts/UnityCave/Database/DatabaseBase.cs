﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityCave.Database
{
	public interface IDatabase
	{
	}

	public abstract class DatabaseBase<T> : ScriptableObject, IDatabase where T : ScriptableObject
	{
		[SerializeField]
		private List<T> data;

		protected List<T> Data
		{
			get
			{
				return data;
			}
		}

		public void OnEnable()
		{
			if (data == null)
			{
				data = new List<T>();
			}
		}

		public int Count
		{
			get
			{
				return data.Count;
			}
		}

		public void Add(T entry)
		{
			data.Add(entry);
		}

		public void Remove(T entry)
		{
			data.Remove(entry);
		}

		public void RemoveAt(int index)
		{
			data.RemoveAt(index);
		}

		public T Get(int index)
		{
			return data[index];
		}

		public T GetRandom()
		{
			return Get(UnityEngine.Random.Range(0, Count));
		}

		public List<T> GetAll()
		{
			return new List<T>(data);
		}

		public IEnumerable<T> Find(Func<T, bool> predicate)
		{
			return data.Where(predicate);
		}
	}
}