﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityCave.Utils;
using UnityEngine;

namespace UnityCave
{
	public class EntityManager : MonoBehaviour
	{
		private Dictionary<Type, List<GameObject>> entitesByType = new Dictionary<Type, List<GameObject>>();

		public static EntityManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<EntityManager>();
			}
		}

		public void Add<TEntity>(GameObject entity)
		{
			List<GameObject> entityList;
			Type entityType = typeof(TEntity);

			if (!entitesByType.TryGetValue(entityType, out entityList))
			{
				entityList = new List<GameObject>();
				entitesByType[entityType] = entityList;
			}

			entityList.Add(entity);
		}

		/// <summary>
		/// Removes the <paramref name="entity"/> of type <typeparamref name="TEntity"/>.
		/// Throws an <see cref="EntityNotFoundException"/> if the entity can not be found.
		/// </summary>
		/// <exception cref="EntityNotFoundException" />
		public void Remove<TEntity>(GameObject entity)
		{
			List<GameObject> entityList;
			if (!entitesByType.TryGetValue(typeof(TEntity), out entityList) ||
				!entityList.Remove(entity))
			{
				throw new EntityNotFoundException();
			}
		}

		/// <summary>
		/// Gets all entites that are registered for the type <typeparamref name="TEntity"/>.
		/// </summary>
		public IEnumerable<GameObject> GetAll<TEntity>()
		{
			List<GameObject> entityList;
			return entitesByType.TryGetValue(typeof(TEntity), out entityList) ? entityList : Enumerable.Empty<GameObject>();
		}
	}
}