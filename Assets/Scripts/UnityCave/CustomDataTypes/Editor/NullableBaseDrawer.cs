﻿using UnityEditor;
using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	[CustomPropertyDrawer(typeof(NullableVector2))]
	[CustomPropertyDrawer(typeof(NullableFloat))]
	public class NullableBaseDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight(property, label);
		}

		// Draw the property inside the given rect
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{

			var controlPosition = EditorGUI.PrefixLabel(position, label);

			var propHasValue = property.FindPropertyRelative("HasValue");
			propHasValue.boolValue = EditorGUI.Toggle(
				new Rect(controlPosition.x, controlPosition.y, EditorGUIUtility.singleLineHeight, controlPosition.height),
				propHasValue.boolValue);

			
			EditorGUI.BeginDisabledGroup(!propHasValue.boolValue);

			var propValue = property.FindPropertyRelative("Value");

			EditorGUI.PropertyField(
				new Rect(controlPosition.x + EditorGUIUtility.singleLineHeight, controlPosition.y, controlPosition.width - EditorGUIUtility.singleLineHeight, controlPosition.height),
				propValue,
				GUIContent.none,
				true);

			EditorGUI.EndDisabledGroup();
		}
	}
}