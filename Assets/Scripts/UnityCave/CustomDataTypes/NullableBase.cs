﻿using System;

namespace UnityCave.CustomDataTypes
{
	[Serializable]
	public class NullableBase<T> where T : struct
	{
		public T Value = default(T);
		public bool HasValue = false;
	}
}