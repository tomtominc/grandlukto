﻿using System;
using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	[Serializable]
	public class NullableVector2 : NullableBase<Vector2>
	{
	}
}