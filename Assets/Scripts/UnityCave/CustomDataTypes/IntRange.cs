﻿using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	/// <summary>
	/// Represents a random value in an int range. (min and max inclusive!)
	/// </summary>
	[System.Serializable]
	public class IntRange
	{
		public int rangeStart;
		public int rangeEnd;

		private int GetRandomValue()
		{
			return Random.Range(rangeStart, rangeEnd + 1);
		}

		public static implicit operator int (IntRange range)
		{
			return range.GetRandomValue();
		}
	}
}