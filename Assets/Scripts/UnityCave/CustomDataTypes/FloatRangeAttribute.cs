﻿using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	public class FloatRangeAttribute : PropertyAttribute
	{
		public float minLimit;
		public float maxLimit;

		public FloatRangeAttribute(float minLimit, float maxLimit)
		{
			this.minLimit = minLimit;
			this.maxLimit = maxLimit;
		}
	}
}