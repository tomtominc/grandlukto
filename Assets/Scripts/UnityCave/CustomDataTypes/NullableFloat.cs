﻿using System;

namespace UnityCave.CustomDataTypes
{
	[Serializable]
	public class NullableFloat : NullableBase<float>
	{
	}
}