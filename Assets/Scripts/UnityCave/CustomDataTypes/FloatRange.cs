﻿using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	[System.Serializable]
	public class FloatRange
	{
		public float rangeStart;
		public float rangeEnd;

		public FloatRange()
		{
		}

		public FloatRange(float start, float end)
		{
			rangeStart = start;
			rangeEnd = end;
		}

		private float GetRandomValue()
		{
			return Random.Range(rangeStart, rangeEnd);
		}

		public static implicit operator float (FloatRange range)  // implicit digit to byte conversion operator
		{
			return range.GetRandomValue();
		}

		public float Lerp(float t)
		{
			return Mathf.Lerp(rangeStart, rangeEnd, t);
		}
	}
}