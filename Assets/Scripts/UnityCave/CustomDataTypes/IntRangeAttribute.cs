﻿using UnityEngine;

namespace UnityCave.CustomDataTypes
{
	public class IntRangeAttribute : PropertyAttribute
	{
		public int minLimit;
		public int maxLimit;

		public IntRangeAttribute(int minLimit, int maxLimit)
		{
			this.minLimit = minLimit;
			this.maxLimit = maxLimit;
		}
	}
}
