﻿using UnityEngine;
using UnityEngine.Events;

namespace UnityCave
{
	public class StatManagerEvent : UnityEvent<StatManager>
	{
	}

	public class StatManager : MonoBehaviour
	{
		public float maxValue = 1f;
		public float minValue = 0f;
		public bool startFull = true;

		[HideInInspector]
		public StatManagerEvent OnChange { get; private set; }

		[HideInInspector]
		public StatManagerEvent OnFull { get; private set; }

		[HideInInspector]
		public StatManagerEvent OnEmpty { get; private set; }

		private float curValue = 0f;

		public bool IsFull
		{
			get
			{
				return curValue >= maxValue;
			}
		}

		public bool IsEmpty
		{
			get
			{
				return curValue <= minValue;
			}
		}

		public float ValueRel
		{
			get
			{
				return (curValue + minValue) / (maxValue - minValue);
			}
		}

		public void Awake()
		{
			if (OnChange == null) OnChange = new StatManagerEvent();
			if (OnFull == null) OnFull = new StatManagerEvent();
			if (OnEmpty == null) OnEmpty = new StatManagerEvent();
		}

		public void Start()
		{
			ResetValue();
		}

		private void ResetValue()
		{
			if (startFull) curValue = maxValue;
			else curValue = minValue;
		}

		public void ChangeValue(float offset)
		{
			if (offset > 0 && IsFull) return; // already full
			if (offset < 0 && IsEmpty) return; // already empty

			curValue += offset;

			if (curValue <= minValue)
			{
				curValue = minValue;
				if (OnEmpty != null) OnEmpty.Invoke(this);
			}
			
			if (curValue >= maxValue)
			{
				curValue = maxValue;
				if (OnFull != null) OnFull.Invoke(this);
			}

			if(OnChange != null) OnChange.Invoke(this);
		}

		public void Fill()
		{
			ChangeValue(maxValue - curValue);
		}

		public void Empty()
		{
			ChangeValue(minValue - curValue);
		}
	}
}
