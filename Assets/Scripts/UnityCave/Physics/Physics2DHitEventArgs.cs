﻿using System;
using UnityEngine;

namespace UnityCave.Physics
{
	public class Physics2DHitEventArgs : EventArgs
	{
		public Collision2D Collision { get; private set; }

		public Physics2DHitEventArgs(Collision2D collision)
		{
			Collision = collision;
		}
	}
}