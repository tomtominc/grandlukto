﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class MyRandom
	{
		public static int Sign()
		{
			return Random.value < .5 ? -1 : 1;
		}

		public static Vector3 Vector3InRectangle(Bounds bounds)
		{
			return new Vector3(
				Random.Range(bounds.min.x, bounds.max.x),
				Random.Range(bounds.min.y, bounds.max.y),
				Random.Range(bounds.min.z, bounds.max.z)
			);
		}

		/// <summary>
		/// Returns a random boolean value. Chance indicates to how many percent the returned value can be true.
		/// </summary>
		public static bool Bool(float chance = 0.5f)
		{
			return Random.value < chance;
		}
	}
}