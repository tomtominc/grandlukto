﻿using UnityEngine;

namespace UnityCave.Utils
{
	public class DontDestroyOnLoad : MonoBehaviour
	{
		public void Awake()
		{
			DontDestroyOnLoad(gameObject);
		}
	}
}