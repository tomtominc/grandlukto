﻿using System;
using UnityEngine;

namespace UnityCave.Utils
{
	[Serializable]
	public class SerializableTransform
	{
		public Vector3 position;
		public Quaternion rotation;
		public Vector3 scale = new Vector3(1, 1, 1);
	}
}