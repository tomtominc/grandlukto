﻿using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace UnityCave.Utils.Editor
{
	public class EditorUtils
	{
		public static void Collapse(GameObject go, bool expand)
		{
			// bail out immediately if the go doesn't have children
			if (go.transform.childCount == 0) return;
			// get a reference to the hierarchy window
			var hierarchy = GetFocusedWindow("Hierarchy");
			// select our go
			SelectObject(go);
			
			// delay call if GameObject was just created
			EditorApplication.delayCall += () =>
			{
				// create a new key event (RightArrow for expanding, LeftArrow for collapsing)
				var key = new Event { keyCode = expand ? KeyCode.RightArrow : KeyCode.LeftArrow, type = EventType.keyDown };
				// finally, send the window the event
				hierarchy.SendEvent(key);
			};
		}
		
		public static void SelectObject(UnityEngine.Object obj)
		{
			Selection.activeObject = obj;
		}

		public static EditorWindow GetFocusedWindow(string window)
		{
			FocusOnWindow(window);
			return EditorWindow.focusedWindow;
		}

		public static void FocusOnWindow(string window)
		{
			EditorApplication.ExecuteMenuItem("Window/" + window);
		}

		// stolen from tk2dUtil.cs
		public static void SetDirty(UnityEngine.Object unityObj)
		{
			EditorUtility.SetDirty(unityObj);
			if (string.IsNullOrEmpty(AssetDatabase.GetAssetPath(unityObj))) return;

			var scene = EditorSceneManager.GetSceneByPath(AssetDatabase.GetAssetOrScenePath(unityObj));
			if (!scene.IsValid()) return;

			EditorSceneManager.MarkSceneDirty(scene);
		}

		public static void ObjectConnectionField<TThis, TConnected>(string label, string undoMessage, TThis thisObj, Func<TThis, TConnected> getConnected, Action<TThis, TConnected> setConnected, Action<TConnected, TThis> setThis)
			where TThis : UnityEngine.Object
			where TConnected : UnityEngine.Object
		{
			var oldConnected = getConnected(thisObj);
			var newConnected = EditorGUILayout.ObjectField(label, oldConnected, typeof(TConnected), true) as TConnected;
			if (newConnected == oldConnected) return; // no change

			var connectedToChange = newConnected ?? oldConnected;
			var recordObjects = new UnityEngine.Object[] { thisObj, connectedToChange };
			Undo.RecordObjects(recordObjects, undoMessage);

			// change connection
			setConnected(thisObj, newConnected);
			setThis(connectedToChange, newConnected == null ? null : thisObj);

			for(var i = 0; i < recordObjects.Length; i++)
			{
				EditorUtility.SetDirty(recordObjects[i]);
			}
		}
	}
}