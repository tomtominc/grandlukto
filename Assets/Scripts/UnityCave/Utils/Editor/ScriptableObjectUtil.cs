﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnityCave.Utils
{
	public static class ScriptableObjectUtil
	{
		public static T Create<T>(string path, string name) where T : ScriptableObject
		{
			T asset = ScriptableObject.CreateInstance<T>();

			AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset"));

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			return asset;
		}

		/// <summary>
		/// Removes the given asset and returns true if it was removed successfully.
		/// </summary>
		public static bool Remove(ScriptableObject asset)
		{
			try
			{
				var path = AssetDatabase.GetAssetPath(asset);
				return AssetDatabase.DeleteAsset(path);
			}
			catch(Exception exception)
			{
				Debug.LogError("Could not remove asset.", asset);
				Debug.LogException(exception);
				return false;
			}
		}

		/// <summary>
		/// Returns the asset's filename (without extension) or null.
		/// </summary>
		public static string GetFileName(ScriptableObject asset)
		{
			var fullPath = AssetDatabase.GetAssetPath(asset);
			var fileName = Path.GetFileNameWithoutExtension(fullPath);

			return fileName;
		}
	}
}
