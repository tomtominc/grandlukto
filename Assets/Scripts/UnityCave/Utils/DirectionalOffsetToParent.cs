﻿using UnityEngine;

namespace UnityCave.Utils
{
	[ExecuteInEditMode]
	public class DirectionalOffsetToParent : MonoBehaviour
	{
		public Vector3 angle;
		public Vector3 offset;
		
		public void Start()
		{
			UpdatePosition();
		}

		public void Update()
		{
			UpdatePosition();
		}

		private void UpdatePosition()
		{
			transform.localPosition = Quaternion.Euler(angle) * offset;
		}
	}
}