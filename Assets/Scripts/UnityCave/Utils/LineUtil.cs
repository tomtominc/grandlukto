﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class LineUtil
	{
		public static Vector2 GetPointOnLineAtYCoord(Vector2 lineStart, Vector2 lineEnd, float targetY)
		{
			return Vector2.LerpUnclamped(lineStart, lineEnd, (targetY - lineStart.y) / (lineEnd.y - lineStart.y));
		}
	}
}
