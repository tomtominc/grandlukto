﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class RectUtil
	{
		public static Vector2 GetClosestPointOnRectInDir(Rect rectangle, float angleRad)
		{
			// http://stackoverflow.com/a/3197924
			// Let's say the rectangle is defined by (x1,y1,x2,y2) and let's say the ray starts at(px, py).
			float vx = Mathf.Cos(angleRad);
			float vy = Mathf.Sin(angleRad);

			float? t = null;

			// Traveling a distance of t along the ray will bring you to the point(px+tvx, py+tvy).
			// Traveling along the ray...
			if (vx != 0)
			{
				// we hit the left wall when px+t* vx = x1, or t=(x1-px)/vx
				t = GetMinPositiveValue(t, (rectangle.min.x - rectangle.center.x) / vx);

				// we hit the right wall when px+t* vx = x2, or t=(x2-px)/vx
				t = GetMinPositiveValue(t, (rectangle.max.x - rectangle.center.x) / vx);
			}

			if (vy != 0)
			{
				// we hit the top wall when py+t* vy = y1, or t=(y1-py)/vy
				t = GetMinPositiveValue(t, (rectangle.min.y - rectangle.center.y) / vy);

				// we hit the bottom wall when py+t* vy = y2, or t=(y2-py)/vy
				t = GetMinPositiveValue(t, (rectangle.max.y - rectangle.center.y) / vy);
			}

			if (!t.HasValue)
			{
				throw new System.Exception("Calculation failed!");
			}

			// So, there are four possible solutions for t.The correct value of t(among the four) is the smallest positive one. The actual intersection is at the point (px+tvx, py+tvy). Just be careful not to divide by zero!
			return new Vector2(rectangle.center.x + t.Value * vx, rectangle.center.y + t.Value * vy);
		}

		public static float? GetMinPositiveValue(float? a, float b)
		{
			if (b < 0) return a; // only positive values
			if (!a.HasValue) return b;
			return Mathf.Min(a.Value, b);
		}
	}
}
