﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class Vector2Util
	{
		public static Vector2 GetClosestPointOnLineSegment(Vector2 lineStart, Vector2 lineEnd, Vector2 fromPoint)
		{
			Vector2 ap = fromPoint - lineStart;       //Vector from A to P   
			Vector2 ab = lineEnd - lineStart;       //Vector from A to B  

			float magnitudeAB = ab.sqrMagnitude;     //Magnitude of AB vector (it's length squared)     
			float abapProduct = Vector2.Dot(ap, ab);    //The DOT product of a_to_p and a_to_b     
			float distance = abapProduct / magnitudeAB; //The normalized "distance" from a to your closest point  

			if (distance < 0)     //Check if P projection is over vectorAB     
			{
				return lineStart;

			}
			else if (distance > 1)
			{
				return lineEnd;
			}

			return lineStart + ab * distance;
		}

		// https://forum.unity.com/threads/how-do-i-find-the-closest-point-on-a-line.340058/#post-2198950
		public static Vector2 GetClosestPointOnLine(Vector2 lineStart, Vector2 lineDirection, Vector2 point)
		{
			lineDirection.Normalize();//this needs to be a unit vector
			var lineStartToPoint = point - lineStart;
			var dot = Vector2.Dot(lineStartToPoint, lineDirection);
			return lineStart + lineDirection * dot;
		}

		public static float AngleSigned(Vector2 from, Vector2 to)
		{
			float angle = Vector2.Angle(from, to);
			Vector3 cross = Vector3.Cross(from, to);

			return cross.z > 0 ? angle : -angle;
		}
	}
}
