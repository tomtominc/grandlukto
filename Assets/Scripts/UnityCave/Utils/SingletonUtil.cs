﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.Utils
{
	public static class SingletonUtil
	{
		private static Dictionary<Type, MonoBehaviour> instances = new Dictionary<Type, MonoBehaviour>();

		public static T GetInstance<T>(string defaultPrefabName = null) where T : MonoBehaviour
		{
			MonoBehaviour instance;
			if(instances.TryGetValue(typeof(T), out instance) && instance != null)
			{
				return (T)instance;
			}
			
			instance = UnityEngine.Object.FindObjectOfType<T>();

			if(instance == null)
			{
				if (string.IsNullOrEmpty(defaultPrefabName))
				{
					Debug.LogWarning(string.Format("There needs to be one active {0} script on a GameObject in your scene.", typeof(T)));
					return null;
				}

				var singletonContainer = UnityEngine.Object.Instantiate(Resources.Load<GameObject>(defaultPrefabName));
				instance = singletonContainer.GetComponent<T>();
			}

			instances[typeof(T)] = instance;
			return (T)instance;
		}
	}
}