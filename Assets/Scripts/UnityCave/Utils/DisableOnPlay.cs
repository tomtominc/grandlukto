﻿using UnityEngine;

namespace UnityCave.Utils
{
	public class DisableOnPlay : MonoBehaviour
	{
		public bool destroyImmediate = true;

		public void Awake()
		{
			if (destroyImmediate)
			{
				DestroyImmediate(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
}