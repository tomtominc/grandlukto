﻿namespace UnityCave.Utils
{
	public static class MathUtil
	{
		/// <summary>
		/// Changes curValue by delta and begins from start if new value is above max value or form beginning if new value is below min value.
		/// </summary>
		public static int CycleThrough(int curValue, int delta, int minValue, int maxValue)
		{
			int newValue = curValue + delta;
			int numTotalValues = maxValue - minValue;

			while (newValue >= maxValue)
			{
				newValue -= numTotalValues;
			}

			while (newValue < 0)
			{
				newValue += numTotalValues;
			}

			return newValue;
		}

		/// <summary>
		/// Acts like the % operator but always returns a positive number.
		/// </summary>
		public static int PositiveModulo(int i, int n)
		{
			return (i % n + n) % n;
		}
	}
}