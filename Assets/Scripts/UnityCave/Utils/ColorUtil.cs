﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class ColorUtil
	{
		public static Color MakeTransparent(this Color c, float alpha = 0)
		{
			return c * new Color(1, 1, 1, alpha);
		}
	}
}
