﻿using UnityEngine;

namespace UnityCave.Utils
{
	public static class DebugUtil
	{
		public static void DrawCross(Vector3 targetPoint, float crossSize, Color color)
		{
			var halfCrossSize = crossSize / 2;
			Debug.DrawLine(targetPoint + Vector3.left * halfCrossSize, targetPoint + Vector3.right * halfCrossSize, color);
			Debug.DrawLine(targetPoint + Vector3.up * halfCrossSize, targetPoint + Vector3.down * halfCrossSize, color);
		}
	}
}