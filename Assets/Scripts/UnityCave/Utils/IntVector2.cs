﻿using System;

namespace UnityCave.Utils
{
	public struct IntVector2 : IEquatable<IntVector2>
	{
		public int x;
		public int y;

		public IntVector2(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public bool Equals(IntVector2 other)
		{
			return this == other;
		}

		public override bool Equals(Object obj)
		{
			return obj is IntVector2 && Equals((IntVector2)obj);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public static bool operator ==(IntVector2 objA, IntVector2 objB)
		{
			return objA.x == objB.x && objA.y == objB.y;
		}

		public static bool operator !=(IntVector2 x, IntVector2 y)
		{
			return !(x == y);
		}

		public static IntVector2 operator +(IntVector2 objA, IntVector2 objB)
		{
			return new IntVector2(objA.x + objB.x, objA.y + objB.y);
		}

		public static IntVector2 operator -(IntVector2 objA, IntVector2 objB)
		{
			return new IntVector2(objA.x - objB.x, objA.y - objB.y);
		}
	}
}