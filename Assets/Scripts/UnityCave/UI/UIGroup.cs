﻿using GrandLukto.Database;
using Rewired;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCave.UI
{
	public class UIGroup : UIElement
	{
		public List<Player> ControllingPlayers { get; private set; }

		[Header("Sounds (override default)")]
		[SerializeField]
		private DataSoundSetting soundMove;

		[SerializeField]
		private DataSoundSetting soundSelect;

		public DataSoundSetting SoundMove
		{
			get
			{
				return soundMove != null ? soundMove : UIManager.Instance.config.soundMove;
			}
		}

		public DataSoundSetting SoundSelect
		{
			get
			{
				return soundSelect != null ? soundSelect : UIManager.Instance.config.soundSelect;
			}
		}

		public void AssignToPlayer(Player player)
		{
			AssignToPlayer(new List<Player> { player });
		}

		public void AssignToPlayer(List<Player> players)
		{
			ControllingPlayers = players;

			ToggleSelected(true);
		}

		public void Unassign()
		{
			ControllingPlayers = null;
		}
	}
}
