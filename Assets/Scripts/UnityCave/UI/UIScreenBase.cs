﻿using System;
using UnityEngine;

namespace UnityCave.UI
{
	public class UIScreenBase : MonoBehaviour
	{
		public EventHandler Show;

		public UIScreenManager Manager { get; private set; }

		public bool IsEnabled { get; private set; }

		public UIScreenBase()
		{
			IsEnabled = true;
		}

		public void RegisterToManager(UIScreenManager manager)
		{
			Manager = manager;
		}

		public void BackToLastScreenInStack()
		{
			if (!IsEnabled) return;
			Manager.BackToLastScreenInStack();
		}

		public void OnShow()
		{
			if (Show != null) Show(this, EventArgs.Empty);
		}
	}
}