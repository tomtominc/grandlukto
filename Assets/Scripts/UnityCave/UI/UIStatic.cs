﻿using GrandLukto.UI;
using UnityCave.MeshUtils;
using UnityEngine;

namespace UnityCave.UI
{
	public static class UIStatic
	{
		public static UIBackgroundAnimation CreateBackgroundAnim(Transform transform, Camera targetCamera = null)
		{
			var go = Object.Instantiate(Resources.Load<GameObject>("UI/Background"), transform);
			go.transform.localPosition = new Vector3(0, 0, -1);

			if(targetCamera != null)
			{
				go.GetComponent<FullScreenPlane>().targetCamera = targetCamera;
			}

			return go.GetComponent<UIBackgroundAnimation>();
		}
	}
}
