﻿using GrandLukto.Database;
using Rewired;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace UnityCave.UI
{
	public class UIManager : MonoBehaviour
	{
		public static UIManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<UIManager>();
			}
		}

		public UIConfiguration config;
		public Player LeadPlayer { get; private set; }

		public void ChangeLeadPlayer(Player newLead)
		{
			LeadPlayer = newLead;
		}

		public bool IsLeadPlayer(Player player)
		{
			return LeadPlayer == player;
		}

		public void PlaySound(DataSoundSetting soundSetting)
		{
			if (soundSetting == null) return;
			Camera.main.gameObject.GetOrAddComponent<AudioSource>().TryPlayOneShot(this, soundSetting);
		}

		public void PlaySound(DataSoundSetting[] soundSetting)
		{
			for(var i = 0; i < soundSetting.Length; i++)
			{
				PlaySound(soundSetting[i]);
			}
		}
	}
}