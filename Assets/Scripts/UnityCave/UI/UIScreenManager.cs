﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityCave.UI
{
	public class UIScreenManager : MonoBehaviour
	{
		public UIScreenBase startScreen;

		private List<UIScreenBase> screens;

		private UIScreenBase lastScreen;
		private UIScreenBase activeScreen;

		private List<UIScreenBase> screenStack;

		public void OnEnable()
		{
			InitializeScreens();

			ChangeScreen(startScreen);
		}

		private void InitializeScreens()
		{
			screenStack = new List<UIScreenBase>();
			screens = GetComponentsInChildren<UIScreenBase>(true).ToList();

			foreach (var screen in screens)
			{
				screen.RegisterToManager(this);
				screen.gameObject.SetActive(false);
			}
		}

		public void ChangeScreen<T>() where T : UIScreenBase
		{
			var nextScreen = GetScreenByType<T>();

			if(nextScreen == null)
			{
				Debug.LogError("Screen not found: " + typeof(T));
				return;
			}

			ChangeScreen(nextScreen);
		}

		public void ChangeScreen(UIScreenBase nextScreen)
		{
			nextScreen.gameObject.SetActive(true);
			nextScreen.OnShow();

			if (activeScreen != null)
			{
				activeScreen.gameObject.SetActive(false);

				lastScreen = activeScreen;

				// check if we go one level up and if we do so remove the current screen -> new screen will not get added since it is on top of stack now
				if (screenStack.Count > 1 && nextScreen == screenStack[screenStack.Count - 2])
				{
					screenStack.RemoveAt(screenStack.Count - 1);
				}
			}

			activeScreen = nextScreen;

			if(screenStack.Count == 0 || screenStack[screenStack.Count - 1] != activeScreen)
			{
				screenStack.Add(activeScreen);
			}
		}

		public T GetScreenByType<T>() where T : UIScreenBase
		{
			foreach(var screen in screens)
			{
				if (screen is T) return (T)screen;
			}

			return null;
		}

		public void BackToLastScreen()
		{
			if(lastScreen == null)
			{
				Debug.LogError("Can not change to last screen. There is no last screen.");
				return;
			}

			ChangeScreen(lastScreen);
		}

		public void BackToLastScreenInStack()
		{
			if(screenStack.Count < 2)
			{
				Debug.LogError("Can not change to last screen in stack because the stack is not filled!");
				return;
			}

			ChangeScreen(screenStack[screenStack.Count - 2]);
		}
	}
}