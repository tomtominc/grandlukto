﻿using GrandLukto.Database;
using UnityEngine;

namespace UnityCave.UI
{
	[CreateAssetMenu(menuName = "UnityCave/UIConfiguration")]
	public class UIConfiguration : ScriptableObject
	{
		public string horizontalAxisName;
		public string verticalAxisName;
		public string[] buttonEnterNames;
		public string[] buttonBackNames;

		public DataSoundSetting soundMove;
		public DataSoundSetting soundSelect;
	}
}