﻿using DG.Tweening;
using UnityEngine;

namespace UnityCave.UI
{
	public class UISelectMove : MonoBehaviour
	{
		public Vector3 angle;
		public float dist;

		public Ease easeSelect;
		public Ease easeDeselect;

		public float duration;

		private UIElement targetUIElement;

		private Tween curTween;
		private Vector3? startPosLocal;
		
		public void OnEnable()
		{
			targetUIElement = GetComponentInParent<UIElement>();
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected += OnToggleSelected;
		}

		public void OnDisable()
		{
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected -= OnToggleSelected;
		}

		private void OnToggleSelected(UIElement sender, bool isSelected)
		{
			if (curTween != null) curTween.Complete();

			if(!startPosLocal.HasValue)
			{
				startPosLocal = transform.localPosition;
			}

			if(isSelected)
			{
				curTween = gameObject.transform
					.DOLocalMove(startPosLocal.Value + Quaternion.Euler(angle) * Vector3.up * dist, duration)
					.SetEase(easeSelect)
					.SetUpdate(true);

				return;
			}

			curTween = gameObject.transform
				.DOLocalMove(startPosLocal.Value, duration)
				.SetEase(easeDeselect)
				.SetUpdate(true);
		}
	}
}