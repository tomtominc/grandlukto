﻿using GrandLukto.Characters;
using Rewired;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;

namespace UnityCave.UI
{
	public delegate void UIElementEventToggleSelected(UIElement sender, bool isSelected);

	public class SelectedChildChangedArgs : EventArgs
	{
		public UIElement PrevSelected { get; private set; }
		public UIElement Selected { get; private set; }

		public SelectedChildChangedArgs(UIElement prevSelected, UIElement selected)
		{
			PrevSelected = prevSelected;
			Selected = selected;
		}
	}

	public class UIElement : MonoBehaviour
	{
		public int selectionX;
		public int selectionY;

		public bool canGoClosestRight = false;
		public bool canGoClosestLeft = false;
		public bool canGoClosestDown = false;

		public FaceDirection[] rememberLastSelection;

		public bool rememberLastSelectedChild = false;

		public UIElement initialSelectedChild = null;

		public bool pauseOnGamePause = false;

		/// <summary>
		/// Checks if this object or any of the parents has pauseONGamePause set to true.
		/// </summary>
		private bool PauseOnGamePauseRecursive
		{
			get
			{
				if (pauseOnGamePause || ParentGroup == null || ParentGroup == this) return pauseOnGamePause;
				return ParentGroup.PauseOnGamePauseRecursive;
			}
		}

		/// <summary>
		/// Returns the controllers of the UIGroup that this UIElement belongs to.
		/// </summary>
		public List<Player> ParentControllers
		{
			get
			{
				if (ParentGroup == null) return null;
				return ParentGroup.ControllingPlayers;
			}
		}

		private Dictionary<FaceDirection, UIElement> rememberedSelections = new Dictionary<FaceDirection, UIElement>();

		public bool IsSelected { get; private set; }

		public UIGroup ParentGroup { get; private set; }
		public UIElement ParentElement { get; private set; }

		public List<UIElement> ChildElements { get; private set; }

		public UIElementEventToggleSelected EventToggleSelected;
		public UnityEvent EnterPressed;
		public UnityEvent BackPressed;
		public EventHandler<SelectedChildChangedArgs> SelectedChildChanged;

		// todo: move FaceDirection to UnityCave
		protected List<FaceDirection> AllowedNavigationDirections;

		private UIElement lastSelectedChild;
		private UIElement selectedChild;
		public UIElement SelectedChild { get { return selectedChild; } }

		/// <summary>
		/// Timestamp which is used to prevent navigating 2 times on the same frame.
		/// </summary>
		private float selectedTimestamp;

		public UIElement()
		{
			ChildElements = new List<UIElement>();
			AllowedNavigationDirections = Enum.GetValues(typeof(FaceDirection)).Cast<FaceDirection>().ToList();
		}

		public virtual void OnEnable()
		{
			if(transform.parent != null)
			{
				ParentGroup = transform.parent.GetComponentInParent<UIGroup>();
				if (ParentGroup == null) ParentGroup = this as UIGroup; // we could be a UIGroup ourself
			}

			ChangeParent(transform.parent == null ? null : transform.parent.GetComponentInParent<UIElement>());
		}

		public virtual void OnDisable()
		{
			// remove selection and let it initiate OnEnable again.
			SelectChild(null);
		}

		public void Update()
		{
			if (IsSelected)
			{
				if (Time.timeScale <= 0 && PauseOnGamePauseRecursive) return;

				UpdateSelected();

				if (selectedChild == null) UpdateCheckNavigation();
			}
		}

		private void UpdateCheckNavigation()
		{
			if (ParentControllers == null) return;
			if (Mathf.Approximately(selectedTimestamp, Time.unscaledTime)) return; // element got selected at exactly this frame

			var uiConfig = UIManager.Instance.config;

			if (ParentControllers.GetButtonDown(uiConfig.buttonBackNames))
			{
				OnBackPressed();
				return;
			}

			if (ParentControllers.GetButtonDown(uiConfig.buttonEnterNames))
			{
				OnEnterPressed();
				return;
			}

			NavigateIfCondition(FaceDirection.Up, ParentControllers.GetButtonDown(uiConfig.verticalAxisName));
			NavigateIfCondition(FaceDirection.Down, ParentControllers.GetNegativeButtonDown(uiConfig.verticalAxisName));
			NavigateIfCondition(FaceDirection.Right, ParentControllers.GetButtonDown(uiConfig.horizontalAxisName));
			NavigateIfCondition(FaceDirection.Left, ParentControllers.GetNegativeButtonDown(uiConfig.horizontalAxisName));
		}

		private void NavigateIfCondition(FaceDirection direction, bool condition)
		{
			if (!condition) return;
			if (ParentElement == null) return;
			if (!AllowedNavigationDirections.Contains(direction)) return;

			ParentElement.Navigate(direction, this);
		}

		private void Navigate(FaceDirection direction, UIElement navigationRequester)
		{
			if(ChildElements.Count <= 1)
			{
				ParentElement.Navigate(direction, navigationRequester); // send to parent since we only have 1 children
				return;
			}

			var closestChild = GetClosestChildInDirection(direction, navigationRequester);
			if (closestChild == null) return;

			UIManager.Instance.PlaySound(ParentGroup.SoundMove);

			SelectChild(closestChild, direction);
		}

		private UIElement GetClosestChildInDirection(FaceDirection direction, UIElement navigationRequester)
		{
			if (selectedChild == null) return ChildElements[0];

			var rememberedSelection = navigationRequester.GetRememberedSelection(direction);
			if (rememberedSelection != null) return rememberedSelection;

			switch(direction)
			{
				case FaceDirection.Down:
					if (navigationRequester.canGoClosestDown) return FindChildClosestX(selectedChild.selectionX, selectedChild.selectionY - 1);
					return FindChild(selectedChild.selectionX, selectedChild.selectionY - 1);
				case FaceDirection.Up:
					return FindChild(selectedChild.selectionX, selectedChild.selectionY + 1);
				case FaceDirection.Left:
					if (navigationRequester.canGoClosestLeft) return FindChildClosestY(selectedChild.selectionX - 1, selectedChild.selectionY);
					return FindChild(selectedChild.selectionX - 1, selectedChild.selectionY);
				case FaceDirection.Right:
					if (navigationRequester.canGoClosestRight) return FindChildClosestY(selectedChild.selectionX + 1, selectedChild.selectionY);
					return FindChild(selectedChild.selectionX + 1, selectedChild.selectionY);
			}

			return null;
		}

		private UIElement FindChild(int xCoord, int yCoord)
		{
			foreach(var child in ChildElements)
			{
				if (child.selectionX == xCoord && child.selectionY == yCoord) return child;
			}

			return null;
		}

		// TODO: FindChildClosestX and FindChildClosestY to same method
		private UIElement FindChildClosestX(int xCoord, int yCoord)
		{
			UIElement closest = null;
			int? closestDist = null;

			foreach (var child in ChildElements)
			{
				if (child.selectionY != yCoord) continue;

				var xDist = Math.Abs(child.selectionX - xCoord);
				if (closestDist.HasValue && xDist > closestDist) continue; // we already have a closer child

				closestDist = xDist;
				closest = child;
			}

			return closest;
		}

		private UIElement FindChildClosestY(int xCoord, int yCoord)
		{
			UIElement closest = null;
			int? closestDist = null;

			foreach (var child in ChildElements)
			{
				if (child.selectionX != xCoord) continue;

				var yDist = Math.Abs(child.selectionY - yCoord);
				if (closestDist.HasValue && yDist > closestDist) continue; // we already have a closer child

				closestDist = yDist;
				closest = child;
			}

			return closest;
		}

		private UIElement GetRememberedSelection(FaceDirection navigationDirection)
		{
			UIElement rememberedSelection = null;
			return rememberedSelections.TryGetValue(navigationDirection, out rememberedSelection) ? rememberedSelection : null;
		}

		protected virtual void UpdateSelected()
		{
		}

		public void ToggleSelected(bool isSelected)
		{
			IsSelected = isSelected;
			if(isSelected) selectedTimestamp = Time.unscaledTime;

			if (isSelected && ChildElements.HasItems())
			{
				if(rememberLastSelectedChild && selectedChild != null)
				{
					selectedChild.ToggleSelected(true); // use the child that we had selected
				}
				else if(rememberLastSelectedChild && lastSelectedChild != null)
				{
					SelectChild(lastSelectedChild); // use the child that we had selected before deselecting this element
				}
				else
				{
					SelectChild(FindInitialSelectedChild());
				}
			}

			if(!isSelected && selectedChild != null)
			{
				selectedChild.ToggleSelected(false);
				lastSelectedChild = selectedChild;
				selectedChild = null;
			}

			if (EventToggleSelected != null) EventToggleSelected(this, isSelected);

			OnToggleSelected(isSelected);
		}

		public UIElement FindInitialSelectedChild()
		{
			if (initialSelectedChild != null)
			{
				// check if we can select the child ...
				if (ChildElements.Contains(initialSelectedChild))
				{
					return initialSelectedChild;
				}

				// ... otherwise log error and continue with usual selection
				Debug.LogError("The given initialSelectedChild is no child of the current UIElement!", initialSelectedChild);
			}

			UIElement bestMatch = null;

			// find child with lowest x and y
			foreach(var child in ChildElements)
			{
				if (bestMatch == null || (child.selectionX < bestMatch.selectionX || child.selectionY > bestMatch.selectionY))
				{
					bestMatch = child;
				}
			}

			return bestMatch;
		}

		public void SelectChildByReference(UIElement newChild)
		{
			if (!ChildElements.Contains(newChild))
			{
				Debug.LogError("Not a valid child to select!", this);
				return;
			}

			SelectChild(newChild);
		}

		private void SelectChild(UIElement newChild, FaceDirection? navigationDirection = null)
		{
			if (selectedChild == newChild) return;

			if (selectedChild != null)
			{
				selectedChild.ToggleSelected(false);
			}

			if(newChild == null)
			{
				selectedChild = null;
				return;
			}

			if(navigationDirection.HasValue)
			{
				newChild.RememberLastSelection(selectedChild, navigationDirection.Value);
			}

			lastSelectedChild = newChild;
			selectedChild = newChild;
			selectedChild.ToggleSelected(true);

			if (SelectedChildChanged != null)
			{
				SelectedChildChanged(this, new SelectedChildChangedArgs(lastSelectedChild, selectedChild));
			}
		}

		protected virtual void OnToggleSelected(bool isSelected)
		{
		}

		private void ChangeParent(UIElement newParent)
		{
			if (ParentElement == newParent) return; // not changed
			if (ParentElement != null) ParentElement.ChildElements.Remove(this);

			ParentElement = newParent;
			if (ParentElement == null) return;
			
			ParentElement.ChildElements.Add(this);
		}

		private void RememberLastSelection(UIElement selectedElement, FaceDirection navigationDirection)
		{
			var directionToElement = navigationDirection.GetOpposite();
			if (!rememberLastSelection.Contains(directionToElement)) return; // remember not set

			rememberedSelections[directionToElement] = selectedElement;
		}

		private void OnEnterPressed(bool isBubbling = false)
		{
			if (EnterPressed == null) return;
			EnterPressed.Invoke();

			if (!isBubbling)
			{
				UIManager.Instance.PlaySound(ParentGroup.SoundSelect);
			}

			if (ParentElement != null) ParentElement.OnEnterPressed(true); // bubble
		}

		private void OnBackPressed()
		{
			if (BackPressed == null) return;
			BackPressed.Invoke();

			if (ParentElement != null) ParentElement.OnBackPressed(); // bubble
		}

		public void ForgetSelection()
		{
			selectedChild = null;
			lastSelectedChild = null;

			foreach (var child in ChildElements)
			{
				child.ForgetSelection(); // propagate
			}
		}
	}
}