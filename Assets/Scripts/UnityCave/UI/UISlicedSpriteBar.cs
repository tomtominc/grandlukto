﻿using UnityEngine;

namespace UnityCave.UI
{
	public class UISlicedSpriteBar : MonoBehaviour
	{
		public Vector2 dimensionsMin;
		public Vector2 dimensionsMax;

		private tk2dSlicedSprite slicedSprite;

		private void Awake()
		{
			slicedSprite = GetComponent<tk2dSlicedSprite>();

			if (slicedSprite == null)
			{
				Debug.LogError("No tk2dSlicedSprite found.");
				Destroy(this);
				return;
			}
		}

		public void Scale(float scale)
		{
			slicedSprite.dimensions = Vector2.Lerp(dimensionsMin, dimensionsMax, scale);
		}
	}
}