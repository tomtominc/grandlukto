﻿using GrandLukto.Characters;
using UnityCave.ExtensionMethods;
using UnityEngine.Events;

namespace UnityCave.UI
{
	public class UIElementLeftRightSelection : UIElement
	{
		public UIButton buttonLeft;
		public UIButton buttonRight;

		public UnityEvent OnPressLeft;
		public UnityEvent OnPressRight;

		public UIElementLeftRightSelection()
		{
			AllowedNavigationDirections.Remove(FaceDirection.Left);
			AllowedNavigationDirections.Remove(FaceDirection.Right);
		}

		public override void OnEnable()
		{
			base.OnEnable();

			buttonLeft.OnMouseDownEvent.AddListener(PressLeft);
			buttonRight.OnMouseDownEvent.AddListener(PressRight);
		}

		public override void OnDisable()
		{
			base.OnDisable();

			buttonLeft.OnMouseDownEvent.RemoveListener(PressLeft);
			buttonRight.OnMouseDownEvent.RemoveListener(PressRight);
		}

		protected override void UpdateSelected()
		{
			base.UpdateSelected();

			var axisName = UIManager.Instance.config.horizontalAxisName;

			if (string.IsNullOrEmpty(axisName)) return;
			if (ParentControllers == null) return;

			if (ParentControllers.GetButtonDown(axisName)) PressRight();
			if (ParentControllers.GetNegativeButtonDown(axisName)) PressLeft();
		}

		private void PressLeft()
		{
			OnPressLeft.Invoke();
		}

		private void PressRight()
		{
			OnPressRight.Invoke();
		}
	}
}