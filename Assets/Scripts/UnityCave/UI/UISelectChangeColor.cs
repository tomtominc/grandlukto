﻿using UnityEngine;

namespace UnityCave.UI
{
	public class UISelectChangeColor : MonoBehaviour
	{
		public Color colorNotSelected;
		public Color colorSelected;

		private UIElement targetUIElement;
		private tk2dSprite targetSprite;

		public void OnEnable()
		{
			targetSprite = GetComponent<tk2dSprite>();

			targetUIElement = GetComponentInParent<UIElement>();
			if (targetUIElement == null) return;

			OnToggleSelected(null, targetUIElement.IsSelected); // initial state

			targetUIElement.EventToggleSelected += OnToggleSelected;
		}

		public void OnDisable()
		{
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected -= OnToggleSelected;
		}

		private void OnToggleSelected(UIElement sender, bool isSelected)
		{
			targetSprite.color = isSelected ? colorSelected : colorNotSelected;
		}
	}
}
