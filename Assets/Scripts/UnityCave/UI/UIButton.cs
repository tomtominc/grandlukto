﻿using UnityEngine;
using UnityEngine.Events;

namespace UnityCave.UI
{
	public class UIButton : MonoBehaviour
	{
		public UnityEvent OnMouseDownEvent;

		public void OnMouseDown()
		{
			OnMouseDownEvent.Invoke();
		}

		public void OnDestroy()
		{
			OnMouseDownEvent.RemoveAllListeners();
		}
	}
}
