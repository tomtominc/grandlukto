﻿using UnityCave.ExtensionMethods;
using UnityEngine;

namespace UnityCave.GraphicUtils
{
	public abstract class FullScreenGraphic : MonoBehaviour
	{
		public Camera targetCamera;
		public float additionalOffset = 1;

		public virtual void Start()
		{
			if (targetCamera == null)
			{
				targetCamera = Camera.main;
			}

			UpdateGraphic();
		}

		public void OnEnable()
		{
			ScreenManager.Instance.ScreenExtendsChange += OnResolutionChanged;
		}

		public void OnDisable()
		{
			ScreenManager.Instance.ScreenExtendsChange -= OnResolutionChanged;
		}

		private void OnResolutionChanged(object sender, ScreenExtendsChangeArgs e)
		{
			UpdateGraphic();
		}

		[ContextMenu("Update Graphic")]
		protected void UpdateGraphic()
		{
			if (targetCamera == null) return;

			var cameraExtents = targetCamera.GetScreenExtendsWorld();

			var width = cameraExtents.x + additionalOffset * 2;
			var height = cameraExtents.y + additionalOffset * 2;

			UpdateGraphic(width, height, width / 2, height / 2);
		}

		protected abstract void UpdateGraphic(float width, float height, float halfWidth, float halfHeight);
	}
}
