﻿using UnityEngine;

namespace UnityCave.ParticleSystems
{
	/// <summary>
	/// Destroys the GameObject as soon as the particle system is not alive anymore.
	/// </summary>
	public class OneShotParticleSystem : MonoBehaviour
	{
		private ParticleSystem compParticleSystem;

		public void Start()
		{
			compParticleSystem = GetComponentInChildren<ParticleSystem>();
		}

		public void Update()
		{
			if (compParticleSystem == null || !compParticleSystem.IsAlive())
			{
				Destroy(gameObject);
			}
		}
	}
}