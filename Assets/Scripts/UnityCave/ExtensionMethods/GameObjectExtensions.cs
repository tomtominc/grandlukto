﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class GameObjectExtensions
	{
		public static T GetOrAddComponent<T>(this GameObject gameObj) where T : Component
		{
			T component = gameObj.GetComponent<T>();

			if(component == null)
			{
				component = gameObj.AddComponent<T>();
			}

			return component;
		}

		/// <summary>
		/// Wraps the GameObject in a new GameObject.
		/// </summary>
		public static GameObject Wrap(this GameObject gameObject, string newGameObjectName)
		{
			var newGameObject = new GameObject(newGameObjectName);

			newGameObject.transform.SetParent(gameObject.transform.parent);
			newGameObject.transform.localPosition = Vector3.zero;

			gameObject.transform.SetParent(newGameObject.transform, true);

			return newGameObject;
		}
	}
}