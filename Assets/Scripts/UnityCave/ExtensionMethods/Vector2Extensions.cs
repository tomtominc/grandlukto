﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class Vector2Extensions
	{
		public static Vector2 Clone(this Vector2 source, float? x = null, float? y = null)
		{
			var newVector = new Vector2(source.x, source.y);

			if (x.HasValue) newVector.x = x.Value;
			if (y.HasValue) newVector.y = y.Value;

			return newVector;
		}
	}
}