﻿using RAIN.Memory;
using RAIN.Representation;

namespace UnityCave.ExtensionMethods.RAIN
{
	public static class ExpressionExtensions
	{
		/// <summary>
		/// Sets the value for the variable stored in the expression in the memory if the expression is a valid variable.
		/// Returns true if the value was set, otherwise false.
		/// </summary>
		public static bool TrySetVariable<TVariable>(this Expression expression, RAINMemory memory, TVariable value)
		{
			if (expression == null || !expression.IsVariable)
			{
				return false;
			}

			memory.SetItem(expression.VariableName, value);
			return true;
		}
	}
}