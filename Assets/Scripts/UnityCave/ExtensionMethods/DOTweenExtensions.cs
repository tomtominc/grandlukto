﻿using DG.Tweening;
using UnityCave.CameraEffects;
using UnityCave.tk2d;
using UnityCave.Utils;
using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class DOTweenExtensions
	{
		#region tk2dSprite
		public static Tween DOColor(this tk2dSprite sprite, Color endValue, float duration)
		{
			return DOTween.To(() => sprite.color, c => sprite.color = c, endValue, duration);
		}

		public static Tween DOScale(this tk2dSprite sprite, Vector3 endValue, float duration)
		{
			return DOTween.To(() => sprite.scale, s => sprite.scale = s, endValue, duration);
		}
		#endregion

		#region tk2dTextMesh
		public static Tween DOColor(this tk2dTextMesh textMesh, Color endValue, float duration)
		{
			return DOTween.To(() => textMesh.color, c => textMesh.color = c, endValue, duration);
		}
		#endregion

		#region DirectionalOffsetToParent
		public static Tween DOOffset(this DirectionalOffsetToParent dirOffset, Vector3 endValue, float duration)
		{
			return DOTween.To(() => dirOffset.offset, value => dirOffset.offset = value, endValue, duration);
		}
		#endregion

		#region CircleCollider2D
		public static Tween DORadius(this CircleCollider2D obj, float endValue, float duration)
		{
			return DOTween.To(() => obj.radius, value => obj.radius = value, endValue, duration);
		}
		#endregion

		#region tk2dSpriteColorize
		public static Tween DOColor(this tk2dSpriteColorize spriteColorize, Color endValue, float duration)
		{
			return DOTween.To(() => spriteColorize.color, value => spriteColorize.color = value, endValue, duration);
		}

		public static Tween DOFlashAmount(this tk2dSpriteColorize spriteColorize, float endValue, float duration)
		{
			return DOTween.To(() => spriteColorize.flashAmount, value => spriteColorize.flashAmount = value, endValue, duration);
		}
		#endregion

		#region Transform
		public static Tweener DOShakePosition(this Transform transform, ScreenShakeSetting shakeSetting)
		{
			return transform.DOShakePosition(shakeSetting.duration, shakeSetting.strength, shakeSetting.vibrato, shakeSetting.randomness, false, shakeSetting.fadeOut);
		}
		#endregion
	}
}