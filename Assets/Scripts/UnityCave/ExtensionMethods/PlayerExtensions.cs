﻿using Rewired;
using System.Collections.Generic;

namespace UnityCave.ExtensionMethods
{
	public static class PlayerExtensions
	{
		public static bool GetButtonDown(this Player player, string[] actionNames)
		{
			for (var actionCount = 0; actionCount < actionNames.Length; actionCount++)
			{
				if (player.GetButtonDown(actionNames[actionCount])) return true;
			}

			return false;
		}

		public static bool GetButtonDown(this List<Player> playerList, string actionName)
		{
			return playerList.Find(player => player.GetButtonDown(actionName)) != null;
		}

		public static bool GetButtonDown(this List<Player> playerList, string[] actionNames)
		{
			for (var playerCount = 0; playerCount < playerList.Count; playerCount++)
			{
				for (var actionCount = 0; actionCount < actionNames.Length; actionCount++)
				{
					if (playerList[playerCount].GetButtonDown(actionNames[actionCount])) return true;
				}
			}

			return false;
		}

		public static bool GetNegativeButtonDown(this List<Player> playerList, string actionName)
		{
			return playerList.Find(player => player.GetNegativeButtonDown(actionName)) != null;
		}

		public static bool GetButton(this List<Player> playerList, string actionName)
		{
			return playerList.Find(player => player.GetButton(actionName)) != null;
		}

		public static bool GetNegativeButton(this List<Player> playerList, string actionName)
		{
			return playerList.Find(player => player.GetNegativeButton(actionName)) != null;
		}
	}
}