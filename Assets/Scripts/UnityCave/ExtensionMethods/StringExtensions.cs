﻿namespace UnityCave.ExtensionMethods
{
	public static class StringExtensions
	{
		public static string RemoveAtStart(this string source, string removeString)
		{
			return source.IndexOf(removeString) != 0 ? source : source.Remove(0, removeString.Length);
		}

		public static bool IsNullOrWhiteSpace(this string source)
		{
			return string.IsNullOrEmpty(source) || source.Trim().Length == 0;
		}
	}
}