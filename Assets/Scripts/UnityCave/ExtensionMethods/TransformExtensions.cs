﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class TransformExtensions
	{
		public static void RemoveAllChildren(this Transform transform, bool immediate = false)
		{
			while (transform.childCount > 0)
			{
				var child = transform.GetChild(0);
				child.parent = null;

				if (immediate)
				{
					Object.DestroyImmediate(child.gameObject);
				}
				else
				{
					Object.Destroy(child.gameObject);
				}
			}
		}

		public static GameObject CreateChildGameObject(this Transform transform, string name)
		{
			var gameObj = new GameObject(name);
			gameObj.transform.SetParent(transform, false);
			return gameObj;
		}

		public static GameObject CreatePrefabOrNewGameObject(this Transform transform, Transform prefab, string name)
		{
			GameObject gameObj;

			if (prefab != null)
			{
				gameObj = Object.Instantiate(prefab).gameObject;
			}
			else
			{
				gameObj = new GameObject();
			}

			gameObj.transform.SetParent(transform, false);
			gameObj.transform.localPosition = Vector3.zero;
			gameObj.name = name;
			return gameObj;
		}
	}
}