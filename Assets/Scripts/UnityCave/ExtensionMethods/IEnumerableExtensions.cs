﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnityCave.ExtensionMethods
{
	public static class IEnumerableExtensions
	{
		/// <summary>
		/// Determines whether an IEnumerable contains any item.
		/// </summary>
		public static bool HasItems(this IEnumerable enumerable)
		{
			if (enumerable == null) return false;

			try
			{
				var enumerator = enumerable.GetEnumerator();
				if (enumerator != null && enumerator.MoveNext()) return true;
			}
			catch
			{
				// swallow
			}

			return false;
		}

		/// <summary>
		/// Selects a random element form the sequence.
		/// </summary>
		public static T SelectRandom<T>(this IEnumerable<T> sequence)
		{
			if (sequence == null)
			{
				throw new ArgumentNullException();
			}

			if (!sequence.Any())
			{
				throw new ArgumentException("The sequence is empty.");
			}

			//optimization for ICollection<T>
			if (sequence is ICollection<T>)
			{
				ICollection<T> col = (ICollection<T>)sequence;
				return col.ElementAt(UnityEngine.Random.Range(0, col.Count));
			}

			int count = 1;
			T selected = default(T);

			foreach (T element in sequence)
			{
				if (UnityEngine.Random.Range(0, count++) == 0)
				{
					//Select the current element with 1/count probability
					selected = element;
				}
			}

			return selected;
		}

		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
		{
			if (source == null) throw new ArgumentNullException("source");

			return source.ShuffleIterator();
		}
		
		private static IEnumerable<T> ShuffleIterator<T>(this IEnumerable<T> source)
		{
			var buffer = source.ToList();
			for (int i = 0; i < buffer.Count; i++)
			{
				int j = UnityEngine.Random.Range(i, buffer.Count);
				yield return buffer[j];

				buffer[j] = buffer[i];
			}
		}
	}
}