﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class Vector3Extensions
	{
		public static Vector3 Clone(this Vector3 source, float? x = null, float? y = null, float? z = null)
		{
			Vector3 newVector = new Vector3(source.x, source.y, source.z);

			if (x.HasValue) newVector.x = x.Value;
			if (y.HasValue) newVector.y = y.Value;
			if (z.HasValue) newVector.z = z.Value;

			return newVector;
		}
	}
}