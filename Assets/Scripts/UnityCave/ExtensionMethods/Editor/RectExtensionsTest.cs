﻿using UnityEngine;
using NUnit.Framework;
using UnityCave.ExtensionMethods;

public class RectExtensionsTest
{
	[TestCase(0, 1, 10, 20)]
	[TestCase(0, 0.5f, 10, 10)]
	[TestCase(0.5f, 1, 20, 10)]
	[TestCase(0.25f, 0.75f, 15, 10)]
	public void DivideX(float startXRel, float endXRel, float resultX, float resultWidth)
	{
		var rect = new Rect(10, 10, 20, 40);

		var result = rect.DivideX(startXRel, endXRel);

		Assert.AreEqual(resultX, result.x);
		Assert.AreEqual(rect.y, result.y);
		Assert.AreEqual(rect.height, result.height);
		Assert.AreEqual(resultWidth, result.width);
	}
}