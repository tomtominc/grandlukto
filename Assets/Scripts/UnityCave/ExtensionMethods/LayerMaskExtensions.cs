﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class LayerMaskExtensions
	{
		public static bool ContainsLayer(this LayerMask mask, int layer)
		{
			return mask == (mask | 1 << layer);
		}
	}
}