﻿using System;
using System.ComponentModel;

namespace UnityCave.ExtensionMethods
{
	public static class EnumExtensions
	{
		private static bool TryParse<T>(string valueToParse, out T returnValue)
		{
			returnValue = default(T);
			if (Enum.IsDefined(typeof(T), valueToParse))
			{
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
				returnValue = (T)converter.ConvertFromString(valueToParse);
				return true;
			}

			return false;
		}

		public static bool ToEnum<T>(this string value, out T enumValue)
		{
			return TryParse(value, out enumValue);
		}

		public static T ToEnum<T>(this string value, T defaultValue)
		{
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}

			T result;
			return TryParse<T>(value, out result) ? result : defaultValue;
		}
	}
}