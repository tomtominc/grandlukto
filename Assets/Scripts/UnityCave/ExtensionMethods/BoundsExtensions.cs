﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class BoundsExtensions
	{
		public static Vector2 RandomInPercentage(this Bounds bounds, float minPercentX = 0, float maxPercentX = 1, float minPercentY = 0, float maxPercentY = 1)
		{
			return new Vector2(
				Random.Range(bounds.min.x + bounds.size.x * minPercentX, bounds.min.x + bounds.size.x * maxPercentX),
				Random.Range(bounds.min.y + bounds.size.y * minPercentY, bounds.min.y + bounds.size.y * maxPercentY));
		}

		public static float RandomInPercentageY(this Bounds bounds, float minPercentY = 0, float maxPercentY = 1)
		{
			return Random.Range(bounds.min.y + bounds.size.y * minPercentY, bounds.min.y + bounds.size.y * maxPercentY);
		}

		public static float GetRelativeXPosition(this Bounds bounds, float relativeX)
		{
			return Mathf.Lerp(bounds.min.x, bounds.max.x, relativeX);
		}

		public static Bounds AddOnEachSide(this Bounds bounds, float padding)
		{
			bounds.Expand(new Vector3(padding * 2, padding * 2, 0)); // avoid negative z expand value
			return bounds;
		}
	}
}