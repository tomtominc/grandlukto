﻿using GrandLukto.Database;
using System.Collections;
using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class AudioSourceExtensions
	{
		public static void PlayOneShotDelayed(this AudioSource audioSource, MonoBehaviour sender, AudioClip clip, float delay, float volumeScale = 1)
		{
			if (delay <= 0)
			{
				audioSource.PlayOneShot(clip, volumeScale);
				return;
			}

			sender.StartCoroutine(DoPlayOneShotDelayed(audioSource, clip, delay, volumeScale));
		}

		private static IEnumerator DoPlayOneShotDelayed(AudioSource audioSource, AudioClip clip, float delay, float volumeScale)
		{
			yield return new WaitForSeconds(delay);
			audioSource.PlayOneShot(clip, volumeScale);
		}

		public static void TryPlayOneShot(this AudioSource audioSource, MonoBehaviour sender, DataSoundSetting soundSetting)
		{
			if (audioSource == null || soundSetting == null) return;
			soundSetting.Play(sender, audioSource);
		}
	}
}