﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class RectExtensions
	{
		/// <summary>
		/// Divides the given rect on the x axis and returns the resulting rect.
		/// </summary>
		public static Rect DivideX(this Rect rect, float startXRel, float endXRel = 1)
		{
			rect.x = rect.x + rect.width * startXRel;
			rect.width = rect.width * (endXRel - startXRel);
			return rect;
		}
	}
}