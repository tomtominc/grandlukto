﻿using UnityEngine;

namespace UnityCave.ExtensionMethods
{
	public static class CameraExtensions
	{
		public static Bounds OrthographicBounds(this Camera camera)
		{
			if (!camera.orthographic)
			{
				Debug.Log(string.Format("The camera {0} is not Orthographic!", camera.name), camera);
				return new Bounds();
			}

			var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
			var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

			var bounds = new Bounds();
			bounds.SetMinMax(new Vector3(lowerLeft.x, lowerLeft.y, 0), new Vector3(upperRight.x, upperRight.y, 0));
			return bounds;
		}

		public static Vector2 GetScreenExtendsWorld(this Camera camera)
		{
			return camera.ViewportToWorldPoint(new Vector3(1, 1, 1)) - camera.ViewportToWorldPoint(new Vector3(0, 0, 0));
		}
	}
}