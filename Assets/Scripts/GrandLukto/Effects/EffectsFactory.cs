﻿using UnityEngine;

namespace GrandLukto.Effects
{
	public class EffectsFactory
	{
		public GameObject CreateExplosion()
		{
			return Object.Instantiate(Resources.Load<GameObject>("Effects/Explosion"));
		}
	}
}