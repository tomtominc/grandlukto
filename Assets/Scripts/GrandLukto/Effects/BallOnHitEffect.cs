﻿using DG.Tweening;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Effects
{
	// TODO: Pool effect, pool lines
	public class BallOnHitEffect : MonoBehaviour
	{
		public tk2dSprite circleInner;
		public tk2dSprite circleOuter;
		public GameObject linePrefab;

		public float circleInDuration = 0.2f;
		public float circleOutDuration = 0.4f;

		private void Start()
		{
			StartAnimation();
		}

		//private void Update()
		//{
		//	if (Input.GetMouseButtonDown(0))
		//	{
		//		StartAnimation();
		//	}
		//}

		private void StartAnimation()
		{
			circleInner.color = Color.white;
			circleOuter.color = Color.white;

			circleInner.transform.localScale = Vector3.zero;
			circleOuter.transform.localScale = Vector3.zero;

			var sequence = DOTween.Sequence()
				.Append(circleInner.transform
					.DOScale(1, circleInDuration)
					.SetEase(Ease.OutQuad))
				.Join(circleOuter.transform
					.DOScale(1, circleInDuration)
					.SetEase(Ease.OutQuad));

			for(var i = 0; i < Random.Range(3, 5); i++)
			{
				sequence.Join(CreateLineAnimation());
			}

			sequence.Append(circleInner
					.DOColor(Color.white.MakeTransparent(), circleOutDuration))
				.Join(circleOuter
					.DOColor(Color.white.MakeTransparent(), circleOutDuration))
				.AppendCallback(() => Destroy(gameObject));
		}

		private Tween CreateLineAnimation()
		{
			var line = Instantiate(linePrefab, transform, false);
			var lineSprite = line.GetComponent<tk2dSprite>();

			lineSprite.color = Color.white.MakeTransparent(Random.Range(0.5f, 1f));

			var randomRotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
			line.transform.localPosition = randomRotation * Vector3.right * Random.Range(0.3f, 0.6f);
			line.transform.localRotation = randomRotation;

			var targetScale = Random.Range(1f, 1.5f);
			line.transform.localScale = line.transform.localScale.Clone(0, targetScale, targetScale);

			return DOTween.Sequence()
				.Append(line.transform
					.DOScaleX(targetScale, Random.Range(circleInDuration * 0.66f, circleInDuration))
					.SetEase(Ease.OutQuart))
				.Append(lineSprite
					.DOColor(Color.white.MakeTransparent(), Random.Range(circleOutDuration * 0.25f, circleOutDuration * 0.5f)))
				.AppendCallback(() => Destroy(line));
		}
	}
}