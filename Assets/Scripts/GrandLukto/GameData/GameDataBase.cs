﻿using GrandLukto.UI.WinScreen;
using System;
using UnityEngine;

namespace GrandLukto.GameData
{
	// TODO: Make abstract after GameData refactoring
	public class GameDataBase
	{
		public virtual bool SpawnInitialBall
		{
			get
			{
				return true;
			}
		}

		public virtual void DoQuitGame()
		{
			GameSceneManager.LoadMenu();
		}

		public virtual void OnWinGame(Team winningTeam)
		{
		}

		public virtual bool IsHeartWallDestroyable(Team team)
		{
			return true;
		}

		/// <summary>
		/// Called after the GameManager initialized the game.
		/// </summary>
		public virtual void OnEnterGame()
		{
		}
		
		public virtual void OnBeforeCountdown(Action onComplete)
		{
			onComplete();
		}

		public virtual IWinScreenHandler CreateWinScreenHandler(Team winningTeam, Transform winScreen)
		{
			// TODO: Own GameData for Multiplayer... handle this and score there...
			var winScreenHandler = GameObject.Instantiate(Resources.Load<GameObject>("UI/WinScreenMultiplayer"), winScreen, false);
			var winScreenMultiplayer = winScreenHandler.GetComponent<WinScreenMultiplayer>();
			winScreenMultiplayer.Initialize(winningTeam, 0, 0);
			return winScreenMultiplayer;
		}
	}
}