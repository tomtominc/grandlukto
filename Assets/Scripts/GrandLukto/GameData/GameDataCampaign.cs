﻿using System;
using GrandLukto.Database;
using GrandLukto.SaveData;
using GrandLukto.UI.WinScreen;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.GameData
{
	public class GameDataCampaign : GameDataBase
	{
		private SaveDataCampaign saveData;
		private string nodeID;
		private DataCampaignLevel campaignLevelConfig;

		private bool showUnlockAnim = false;

		public GameDataCampaign(SaveDataCampaign saveData, string nodeID, DataCampaignLevel campaignLevelConfig)
		{
			this.saveData = saveData;
			this.nodeID = nodeID;
			this.campaignLevelConfig = campaignLevelConfig;
		}

		public override bool SpawnInitialBall
		{
			get
			{
				return campaignLevelConfig.spawnInitialBall;
			}
		}

		public override void DoQuitGame()
		{
			GameSceneManager.LoadOverworld(showUnlockAnim ? nodeID : null);
		}

		public override void OnWinGame(Team winningTeam)
		{
			base.OnWinGame(winningTeam);

			if (winningTeam == Team.Top) return; // TODO: Does the player always play in bottom team?

			// player did win
			var wasCompletedBefore = saveData.CompletedLevelIDs.Contains(nodeID);

			if (!wasCompletedBefore)
			{
				// player did complete level for the first time
				saveData.SetLevelCompleted(nodeID);
				saveData.Save();

				showUnlockAnim = true;
			}
		}

		public override bool IsHeartWallDestroyable(Team team)
		{
			return team != Team.Top || campaignLevelConfig.gameMode.IsHeartWallTopDestroyable();
		}

		public override void OnEnterGame()
		{
			base.OnEnterGame();

			campaignLevelConfig.gameMode.OnEnterGame();
		}

		public override void OnBeforeCountdown(Action onComplete)
		{
			if (campaignLevelConfig.dialogBeforeGameStart == null)
			{
				onComplete(); // no dialog to show
				return;
			}

			GameManager.Instance.ShowDialog(campaignLevelConfig.dialogBeforeGameStart, onComplete);
		}

		public override IWinScreenHandler CreateWinScreenHandler(Team winningTeam, Transform winScreen)
		{
			var prefabName = winningTeam == Team.Top ? "UI/LoseScreenCampaign" : "UI/WinScreenCampaign";

			var gameObj = GameObject.Instantiate(Resources.Load<GameObject>(prefabName), winScreen, false);
			return gameObj.GetComponent<IWinScreenHandler>();
		}
	}
}