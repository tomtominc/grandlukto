﻿namespace GrandLukto
{
	public class Tag
	{
		public const string Block = "Block";
		public const string Player = "Player";
		public const string Ball = "Ball";
		public const string Pad = "Pad";
	}
}