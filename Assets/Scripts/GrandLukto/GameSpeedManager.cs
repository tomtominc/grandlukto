﻿using System.Collections;
using System.Collections.Generic;
using UnityCave;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class GameSpeedManager : MonoBehaviour
	{
		public static GameSpeedManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<GameSpeedManager>();
			}
		}

		public float changeTimeScaleSpeed = 1f;
		public float timeScaleEndGame = 0.1f;

		private List<GameObject> winConditionTriggers = new List<GameObject>();

		private TimeManager compTimeManager;

		private Coroutine slowMotionTimerCoroutine;
		private bool isSlowMotionPowerUpActive = false;

		public void Start()
		{
			compTimeManager = TimeManager.Instance;
		}

		public void AddWinConditionTrigger(GameObject gameObj)
		{
			if (winConditionTriggers.Contains(gameObj)) return;
			winConditionTriggers.Add(gameObj);
		}

		public void RemoveWinConditionTrigger(GameObject gameObj)
		{
			winConditionTriggers.Remove(gameObj);
		}

		public void UseSlowMotionPowerUp()
		{
			if (isSlowMotionPowerUpActive)
			{
				StopCoroutine(slowMotionTimerCoroutine);
			}

			isSlowMotionPowerUpActive = true;
			slowMotionTimerCoroutine = StartCoroutine(SlowMotionPowerUpTimer());
		}

		private IEnumerator SlowMotionPowerUpTimer()
		{
			yield return new WaitForSeconds(GameManager.Instance.gameConfiguration.powerUpSlowMotionTime);
			isSlowMotionPowerUpActive = false;
		}

		public void Update()
		{
			var targetTimeScale = GetTargetTimeScale();
			compTimeManager.TimeScale = Mathf.MoveTowards(compTimeManager.TimeScale, targetTimeScale, changeTimeScaleSpeed * Time.deltaTime);

			if(compTimeManager.TimeScale <= 0)
			{
				compTimeManager.TogglePause(true);
			}
		}

		private float GetTargetTimeScale()
		{
			var gameManager = GameManager.Instance;
			switch (gameManager.ActiveState)
			{
				case GameState.WinScreenShown:
					return 0f;

				case GameState.RoundEnded:
					return timeScaleEndGame;
			}

			if (winConditionTriggers.HasItems())
			{
				return timeScaleEndGame;
			}

			if (isSlowMotionPowerUpActive)
			{
				return gameManager.gameConfiguration.powerUpSlowMotionTimeScale;
			}

			return gameManager.gameConfiguration.gameSpeed;
		}
	}
}