﻿using UnityEngine;

namespace GrandLukto.ParticleSystems
{
	public class ConfettiCannon : MonoBehaviour
	{
		private ParticleSystem compParticleSystem;

		public void Awake()
		{
			compParticleSystem = GetComponent<ParticleSystem>();
		}

		public void Update()
		{
			compParticleSystem.Simulate(Time.unscaledDeltaTime, true, false);
		}

		public void Shoot(Team winningTeam)
		{
			float yPos = 0;

			switch (winningTeam)
			{
				case Team.Top:
					yPos = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0)).y;
					break;

				case Team.Bottom:
					yPos = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 1)).y;
					transform.rotation = Quaternion.Euler(0, 0, 180);
					break;
			}

			transform.position = new Vector3(GameManager.Instance.BlockGroupByTeam[winningTeam.GetOpponent()].CenterX, yPos, transform.position.z);

			compParticleSystem.Play();
		}
	}
}