﻿using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class EventManager : MonoBehaviour
	{
		public static EventManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<EventManager>();
			}
		}
	}
}