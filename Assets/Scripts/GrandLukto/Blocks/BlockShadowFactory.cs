﻿using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockShadowFactory
	{
		public GameObject Create(Transform parent, bool isSizeOfHeartWall)
		{
			var shadowPos = new Vector2(-0.4592919f, -0.2165697f);
			var shadowSize = new Vector2(85, 48);

			if (isSizeOfHeartWall)
			{
				shadowSize = new Vector2(877.3f, 39.5f);
				shadowPos = new Vector2(-4.418197f, -0.155f);
			}

			var shadow = Object.Instantiate(Resources.Load<GameObject>("Blocks/Block Shadow"), parent, false);
			shadow.transform.localPosition = shadowPos;

			shadow.GetComponent<tk2dSlicedSprite>().dimensions = shadowSize;

			return shadow;
		}
	}
}