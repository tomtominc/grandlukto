﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.Blocks
{
	public abstract class BlockAnimationBase : MonoBehaviour
	{
		protected BlockBase CompBlockBase { get; private set; }

		public EventHandler AnimationComplete;

		protected virtual void Awake()
		{
			CompBlockBase = GetComponent<BlockBase>();

			Assert.IsNotNull(CompBlockBase, "SpawnBlockWithImpact only works with BlockBase objects.");
		}

		private void Start()
		{
			CompBlockBase.SetActiveBlockAnimation(this); // ended in AnimationComplete event
		}

		protected void OnAnimationComplete()
		{
			if (AnimationComplete == null) return;
			AnimationComplete(this, EventArgs.Empty);
		}
	}
}
