﻿using GrandLukto.Characters;
using GrandLukto.PowerUps;
using System.Collections;
using UnityCave.Collision;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class PowerUpSpawner : MonoBehaviour, IPowerUpSpawner
	{
		[Header("Config")]
		[Tooltip("How often the routine checks for collecting players (in seconds).")]
		public float collectRoutineTimeout = 0.1f;

		public PowerUpSpawnerIcon icon;
		public CollisionHandler collisionHandler;
		
		[Header("Routine")]
		public IPowerUpSpawnerRoutine routine;

		private Collider2D compCollider;

		private PowerUpType? curPowerUp;
		private Coroutine curRoutineWaitForCollect;

		public bool IsPowerUpSpawned { get { return curPowerUp.HasValue;  } }

		public Vector2 CenterPosition
		{
			get { return transform.localToWorldMatrix * compCollider.offset; }
		}

		private void Awake()
		{
			compCollider = GetComponent<Collider2D>();
		}

		private void Start()
		{
			StartCoroutine(routine.StartRoutine(this));
		}
		
		public void ShowPowerUp(PowerUpType type)
		{
			curPowerUp = type;
			icon.Show(type);

			curRoutineWaitForCollect = StartCoroutine(RoutineWaitForCollect());
		}

		public void HidePowerUp()
		{
			icon.Hide();
			curPowerUp = null;

			if (curRoutineWaitForCollect != null)
			{
				StopCoroutine(curRoutineWaitForCollect);
				curRoutineWaitForCollect = null;
			}
		}

		private IEnumerator RoutineWaitForCollect()
		{
			while(true)
			{
				yield return new WaitForSeconds(collectRoutineTimeout);

				if (TryHandoverPowerUp())
				{
					curPowerUp = null; // mark as collected
					HidePowerUp();
				}
			}
		}

		private bool TryHandoverPowerUp()
		{
			if (!collisionHandler.ActiveTriggerCollisions.HasItems()) return false; // not colliding with any objects

			var eventArgs = new RecievePowerUpEventArgs(curPowerUp.Value);

			foreach (var collision in collisionHandler.ActiveTriggerCollisions)
			{
				if (!collision) continue;

				// now check if somebody can collect it
				var character = collision.gameObject.GetComponent<CharacterBase>();
				if (!character) continue;

				// character stepped on us
				character.OnRecievePowerUp(this, eventArgs);

				if (eventArgs.IsCollected)
				{
					// a player collected the power up
					return true;
				}
			}

			return false;
		}
	}
}