﻿using UnityEngine;

namespace GrandLukto.Blocks
{
	public interface IDestroyableBlock
	{
		/// <summary>
		/// Destroys the block. 
		/// </summary>
		void DestroyBlock();

		/// <summary>
		/// Sets the color in which the destroyable will fade out when being destroyed.
		/// </summary>
		void SetFadeAnimationColor(Color value);
	}
}