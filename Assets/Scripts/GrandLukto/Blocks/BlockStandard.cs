﻿namespace GrandLukto.Blocks
{
	public class BlockStandard : BlockBase
	{
		public override void Start()
		{
			base.Start();

			BallHit += OnBallHit;
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			DestroyBlock();
		}
	}
}