﻿using GrandLukto.Balls;
using System;

namespace GrandLukto.Blocks
{
	public class BlockBallHitEventArgs : EventArgs
	{
		public Ball Ball { get; private set; }

		public BlockBallHitEventArgs(Ball ball)
		{
			Ball = ball;
		}
	}
}