﻿using System;
using System.Collections.Generic;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[Serializable]
	public class BallShooterConfig
	{
		public bool loop = false;

		[FloatRange(0, 10)]
		public FloatRange loopDelay;

		[SerializeField]
		public List<BallShooterConfigStep> steps = new List<BallShooterConfigStep>();
	}
}