﻿using GrandLukto.Balls;
using UnityEngine;
using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using System;

namespace GrandLukto.Blocks
{
	public class BlockKeyPad : BlockBase
	{
		public float moveKeyOffset = 0.15f;
		public float moveKeyDuration = 0.75f;

		public GameObject visualsKey;
		public GameObject visualsKeyShadow;

		public override void Start()
		{
			base.Start();

			BallHit += OnBallHit;

			StartKeyIdleTween();
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			StartKeyCollectTween();

			// destroy all connected blocks
			GetComponentInParent<BlockGroup>().DestroyAdjacentLockedBlocks(this);

			DestroyBlock();
		}

		private void StartKeyIdleTween()
		{
			visualsKey.transform
				.DOLocalMoveY(moveKeyOffset, moveKeyDuration)
				.SetRelative()
				.SetEase(Ease.InOutSine)
				.SetLoops(-1, LoopType.Yoyo);
		}

		private void StartKeyCollectTween()
		{
			var duration = Visuals.GetComponent<FadeBlockAnimation>().duration * 0.95f;

			visualsKey.transform.DOKill();

			visualsKey.transform
				.DOLocalMoveY(moveKeyOffset * 3f, duration)
				.SetRelative()
				.SetEase(Ease.InBack);

			visualsKey.GetComponent<tk2dSprite>()
				.DOColor(Color.white.MakeTransparent(), duration)
				.SetEase(Ease.InSine);

			visualsKeyShadow.GetComponent<tk2dSprite>()
				.DOColor(Color.white.MakeTransparent(), duration)
				.SetEase(Ease.InSine);
		}
		
		public void OnDestroy()
		{
			visualsKey.transform.DOKill();
		}
	}
}