﻿using UnityCave.Collision;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public static class BlockStatic
	{
		public static void ExplodeDestroyBlocks(CollisionHandler collisionHandler, Color? fadeColor)
		{
			foreach (var gameObject in collisionHandler.ActiveTriggerCollisions)
			{
				if (gameObject == null) continue;

				var destroyableBlock = gameObject.GetComponent<IDestroyableBlock>();
				if (destroyableBlock == null) continue;

				if (fadeColor.HasValue) destroyableBlock.SetFadeAnimationColor(fadeColor.Value);

				destroyableBlock.DestroyBlock();
			}
		}
	}
}