﻿using System;

namespace GrandLukto.Blocks
{
	public interface IBlockEvents
	{
		/// <summary>
		/// Dispatched if any block on the field gets destroyed.
		/// </summary>
		event EventHandler<BlockDestroyedEventArgs> BlockDestroyed;
	}
}