﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockFortified : BlockBase
	{
		private static Shader fadeShader;

		public Color fadeInColor;

		private Renderer compRenderer;

		private GameObject gameObjToReplace;
		private tk2dTileMap.TilemapPrefabInstance targetPrefab;

		private Tween activeTween;
		private Coroutine activeCoroutine;

		protected override void Awake()
		{
			base.Awake();

			if (fadeShader == null)
			{
				fadeShader = Shader.Find("UnityCave/PremulVertexColorColorize");
			}

			compRenderer = GetComponent<Renderer>();

			ToggleActive(false);
		}
		
		private Tween StartAnimation(bool forward = true)
		{
			var maxFlashAmount = 0.4f;

			ResetMaterial(forward ? maxFlashAmount : 0);

			return compRenderer.material
				.DOFloat(forward ? 0 : maxFlashAmount, "_FlashAmount", 0.3f)
				.SetEase(forward ? Ease.InOutSine : Ease.InExpo);
		}

		private void ResetMaterial(float flashAmount = 0)
		{
			compRenderer.material.shader = fadeShader;
			compRenderer.material.SetColor("_FlashColor", fadeInColor);
			compRenderer.material.SetFloat("_FlashAmount", flashAmount);
		}

		public void Init(tk2dTileMap.TilemapPrefabInstance prefabInstance, float spawnDelay, float fortifyDuration)
		{
			targetPrefab = prefabInstance;
			var tmpObjToReplace = targetPrefab.instance;
			targetPrefab.instance = gameObject; // replace with fortified block even if animation did not show up yet

			Init(tmpObjToReplace, spawnDelay, fortifyDuration);
		}

		public void Init(GameObject gameObjToReplace, float spawnDelay, float fortifyDuration)
		{
			this.gameObjToReplace = gameObjToReplace;

			transform.position = gameObjToReplace.transform.position;

			activeCoroutine = StartCoroutine(SpawnAfterDelay(spawnDelay, fortifyDuration));
		}

		private void ResetAnim()
		{
			if (activeCoroutine != null)
			{
				StopCoroutine(activeCoroutine);
				activeCoroutine = null;
			}

			activeTween.Kill();
			ResetMaterial();
		}

		// TODO: This method does not really extend the duration at the moment
		// if durationLeft > newDuration the duration gets less; should not affect gameplay at the moment
		public void ExtendDuration(float spawnDelay, float fortifyDuration)
		{
			ResetAnim();
			activeCoroutine = StartCoroutine(SpawnAfterDelay(spawnDelay, fortifyDuration));
		}

		public void ExtendDurationEndless()
		{
			ResetAnim();

			gameObjToReplace.SetActive(false); // disable old gameobject
			ToggleActive(true); // show fortified block
		}

		private void ToggleActive(bool isActive)
		{
			compRenderer.enabled = isActive;
			GetComponent<Collider2D>().enabled = isActive;
			Shadow.SetActive(isActive);
		}

		private IEnumerator SpawnAfterDelay(float spawnDelay, float fortifyDuration)
		{
			yield return new WaitForSeconds(spawnDelay);

			// check if we are still allowed to fortify since we can have a spawn delay
			if (gameObjToReplace == null || !gameObjToReplace.GetComponent<BlockBase>().CanBeModified)
			{
				EndFortify();
				yield break;
			}
			
			// disable old gameobject
			gameObjToReplace.SetActive(false);

			// show fortified block
			ToggleActive(true);
			activeTween = StartAnimation();

			yield return new WaitForSeconds(fortifyDuration);

			activeTween = DOTween.Sequence()
				.Append(StartAnimation(false))
				.AppendCallback(EndFortify);
		}

		private void EndFortify()
		{
			gameObjToReplace.SetActive(true);

			if (targetPrefab != null)
			{
				targetPrefab.instance = gameObjToReplace;
				targetPrefab = null;
			}

			ToggleActive(false);
			Destroy(gameObject);
		}
	}
}