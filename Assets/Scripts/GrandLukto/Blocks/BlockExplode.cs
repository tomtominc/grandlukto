﻿using GrandLukto.Effects;
using System.Collections;
using UnityCave.Collision;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockExplode : BlockBase
	{
		private GameManager gameManager;

		public CollisionHandler explosionCollisionHandler;

		private bool isExploding;

		protected override void Awake()
		{
			base.Awake();

			gameManager = GameManager.Instance;
		}

		public override void Start()
		{
			base.Start();

			BallHit += OnHit;
		}
		
		private void OnHit(object sender, BlockBallHitEventArgs e)
		{
			DestroyBlock();
		}
		
		protected override void StartDestroyRoutine()
		{
			StartCoroutine(ExplodeRoutine());
		}

		private IEnumerator ExplodeRoutine()
		{
			var explosion = new EffectsFactory().CreateExplosion();
			explosion.transform.parent = transform.parent;
			explosion.transform.position = transform.position;

			yield return new WaitForSeconds(gameManager.gameConfiguration.blockExplodeDestroyDelay);

			CompFadeAnimation.fadeColor = gameManager.gameConfiguration.fadeColorExplode;
			BlockStatic.ExplodeDestroyBlocks(explosionCollisionHandler, CompFadeAnimation.fadeColor);

			FadeOutAndDestroy();
		}
	}
}