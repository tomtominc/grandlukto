﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Blocks.BlockModifiers
{
	public class Fortify
	{
		private readonly BlockGroup blockGroup;

		private tk2dTileMap tileMap;
		private Dictionary<int, Dictionary<int, tk2dTileMap.TilemapPrefabInstance>> prefabsYX;

		public Fortify(BlockGroup blockGroup)
		{
			this.blockGroup = blockGroup;
		}

		/// <summary>
		/// Fortifies all blocks starting from the heart wall to the center of the map.
		/// </summary>
		public void FortifyAll(float fortifyDuration)
		{
			tileMap = blockGroup.TileMap;
			prefabsYX = blockGroup.GetTilePrefabDictionaryYX();

			FortifyHeartWall(fortifyDuration);

			List<tk2dTileMap.TilemapPrefabInstance> tilesToFortify = new List<tk2dTileMap.TilemapPrefabInstance>();

			for (var ctX = 0; ctX < tileMap.width; ctX++)
			{
				tilesToFortify.Clear();

				for (var ctY = 0; ctY < tileMap.height; ctY++)
				{
					if (prefabsYX.ContainsKey(ctY) && prefabsYX[ctY].ContainsKey(ctX))
					{
						// space occupied
						tilesToFortify.Add(prefabsYX[ctY][ctX]);
						continue;
					}

					// first unoccupied space -> fortify until here
					break;
				}

				if (tilesToFortify.Count > 0)
				{
					FortifyTiles(tilesToFortify, fortifyDuration);
				}
			}
		}

		/// <summary>
		/// Fortifies the heart wall and returns the instance of the fortified block.
		/// </summary>
		public BlockFortified FortifyHeartWall(float fortifyDuration)
		{
			var heartWall = blockGroup.HeartWall;
			if (!heartWall) return null;

			BlockFortified fortifiedHeartWall;
			var activeHeartWallGameObj = heartWall.ActiveGameObject;
			if (FortifyGameObj(activeHeartWallGameObj, 0, fortifyDuration, "Blocks/HeartWallFortified", out fortifiedHeartWall))
			{
				fortifiedHeartWall.Init(activeHeartWallGameObj, 0, fortifyDuration);
				heartWall.FortifiedOverlay = fortifiedHeartWall.gameObject; // save to extend duration next time
			}

			return fortifiedHeartWall;
		}

		private void FortifyTiles(List<tk2dTileMap.TilemapPrefabInstance> tilesToFortify, float fortifyDuration)
		{
			for (var i = 0; i < tilesToFortify.Count; i++)
			{
				FortifyTile(tilesToFortify[i], i, fortifyDuration);
			}
		}

		private void FortifyTile(tk2dTileMap.TilemapPrefabInstance tile, int index, float fortifyDuration)
		{
			float spawnDelay = index * GameManager.Instance.gameConfiguration.powerUpFortifySpawnDelay;

			BlockFortified blockFortified;
			if (FortifyGameObj(tile.instance, spawnDelay, fortifyDuration, "Blocks/BlockFortified", out blockFortified))
			{
				blockFortified.Init(tile, spawnDelay, fortifyDuration);
			}
		}

		/// <summary>
		/// Returns a boolean value if the block still requires initialization.
		/// </summary>
		private bool FortifyGameObj(GameObject gameObj, float spawnDelay, float fortifyDuration, string resourceName, out BlockFortified fortifiedBlock)
		{
			fortifiedBlock = gameObj.GetComponent<BlockFortified>();
			if (fortifiedBlock != null)
			{
				fortifiedBlock.ExtendDuration(spawnDelay, fortifyDuration);
				return false;
			}

			var fortifyGameObj = Object.Instantiate(Resources.Load<GameObject>(resourceName), gameObj.transform.parent);
			fortifiedBlock = fortifyGameObj.GetComponent<BlockFortified>();
			return true;
		}
	}
}