﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Blocks.BlockModifiers
{
	/// <summary>
	/// "Heals" the BlockGroup by filling up the last line of blocks that has missing blocks.
	/// </summary>
	public class Heal
	{
		private readonly BlockGroup blockGroup;

		private tk2dTileMap tileMap;
		private Dictionary<int, Dictionary<int, tk2dTileMap.TilemapPrefabInstance>> prefabsYX;

		public Heal(BlockGroup blockGroup)
		{
			this.blockGroup = blockGroup;
		}

		public void HealLine()
		{
			tileMap = blockGroup.TileMap;
			prefabsYX = blockGroup.GetTilePrefabDictionaryYX();

			int lineToHealY;
			if (!TryGetFirstLineWithMissingBlocks(out lineToHealY)) return;

			Dictionary<int, tk2dTileMap.TilemapPrefabInstance> lineToHeal = null;
			prefabsYX.TryGetValue(lineToHealY, out lineToHeal);

			for (var ctX = 0; ctX < tileMap.width; ctX++)
			{
				if (lineToHeal != null && lineToHeal.ContainsKey(ctX)) continue; // line already contains a block

				var tilemapPrefab = new tk2dTileMap.TilemapPrefabInstance();
				tilemapPrefab.x = ctX;
				tilemapPrefab.y = lineToHealY;
				tilemapPrefab.layer = 0;

				// instantiate block (positioning from tk2dTileMapBuilderUtil/SpawnPrefabsForChunk)
				var instance = Object.Instantiate(GameManager.Instance.gameConfiguration.powerUpBlockHealBlock, Vector3.zero, Quaternion.identity);
				tk2dUtil.SetTransformParent(instance.transform, tileMap.renderData.transform);

				instance.transform.position = tileMap.GetTilePosition(ctX, lineToHealY) - tileMap.data.tileOrigin;

				tilemapPrefab.instance = instance;
				tileMap.TilePrefabsList.Add(tilemapPrefab);
			}
		}

		private bool TryGetFirstLineWithMissingBlocks(out int lineIndex)
		{
			lineIndex = default(int);

			for (var ctY = 0; ctY < tileMap.height; ctY++)
			{
				// check if line contains no tiles or has missing tiles
				if (!prefabsYX.ContainsKey(ctY) ||
					prefabsYX[ctY].Count < tileMap.width)
				{
					lineIndex = ctY;
					return true;
				}
			}

			return false;
		}
	}
}