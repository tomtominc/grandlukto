﻿using GrandLukto.PowerUps;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[CreateAssetMenu(menuName = "Grand Lukto/PowerUpSpawner/Random")]
	public class PowerUpSpawnerRoutineRandom : PowerUpSpawnerRoutineBase
	{
		public List<PowerUpType> types;

		protected override PowerUpType GetNextPowerUp()
		{
			return types.SelectRandom();
		}
	}
}