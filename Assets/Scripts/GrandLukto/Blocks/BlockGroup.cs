﻿using GrandLukto.Blocks.BlockModifiers;
using GrandLukto.Characters;
using GrandLukto.Database;
using System.Collections.Generic;
using UnityCave.tk2d;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockGroup : MonoBehaviour
	{
		public Team team;
		public LayerMask checkSpawnPositionMask;

		[Header("Player spawn positions")]
		public Vector2 playerSpawnSingle;
		public Vector2 playerSpawnMultiple1;
		public Vector2 playerSpawnMultiple2;

		private FaceDirection faceDirection;
		private Vector3 faceDirectionVec;

		private AIAreaDescription compAIAreaDescription;
		private tk2dTileMap tileMap;

		public tk2dTileMap TileMap
		{
			get
			{
				if (!tileMap)
				{
					tileMap = GetComponentInChildren<tk2dTileMap>();
				}

				return tileMap;
			}
		}

		public BlockHeartWall HeartWall { get; private set; }

		public float CenterX
		{
			get { return playerSpawnSingle.x; } // one player currently always spawns in center of arena
		}

		public void Awake()
		{
			compAIAreaDescription = GetComponentInChildren<AIAreaDescription>();

			faceDirection = team.GetFaceDirection();
			faceDirectionVec = faceDirection.ToDirectionVector();

			HeartWall = GetComponentInChildren<BlockHeartWall>();
		}

		public void ApplyLayout(DataBlockLayout layout)
		{
			if (layout == null)
			{
				DisableBlocks();
				return;
			}

			var tileMap = TileMap;
			ChangeTileMapSize(tileMap, layout.width, layout.height);

			tk2dTileMapUtil.CleanLayers(tileMap);
			tileMap.Layers = tk2dTileMapUtil.CopyLayers(layout.layers, tileMap.width, tileMap.height, tileMap.partitionSizeX, tileMap.partitionSizeY);

			if (team == Team.Bottom)
			{
				tk2dTileMapUtil.MirrorTilesXYAxis(tileMap);
			}

			tileMap.Build();
			InitPrefabs(layout);

			if (!GameManager.Instance.GameData.IsHeartWallDestroyable(team))
			{
				var fortifiedHeartWall = new Fortify(this).FortifyHeartWall(0); // TODO: Remove hack of passing 0 by a better design
				fortifiedHeartWall.ExtendDurationEndless();
			}
		}

		public List<GameObject> SpawnTiles(DataBlockLayout layout)
		{
			var spawnedObjects = new List<GameObject>();

			var tileMap = TileMap;
			var prefabsYX = GetTilePrefabDictionaryYX();

			for (var ctLayer = 0; ctLayer < layout.layers.Length; ctLayer++)
			{
				var curLayer = layout.layers[ctLayer];

				for (var ctHeight = 0; ctHeight < curLayer.height; ctHeight++)
				{
					for (var ctWidth = 0; ctWidth < curLayer.width; ctWidth++)
					{
						var tile = curLayer.GetTile(ctWidth, ctHeight);
						if (tile < 0) continue; // no tile placed

						var prefab = tileMap.data.tilePrefabs[tile];
						if (prefab == null) continue;

						// check if position is already occupied by a prefab
						tk2dTileMap.TilemapPrefabInstance tilePrefabsInstance = null;
						if (prefabsYX.ContainsKey(ctHeight) && prefabsYX[ctHeight].ContainsKey(ctWidth))
						{
							tilePrefabsInstance = prefabsYX[ctHeight][ctWidth];

							if (tilePrefabsInstance.instance != null)
							{
								Debug.LogWarning("Did not spawn prefab. Position already occupied!");
								continue;
							}
						}

						// TODO: Spawn logic to method? Similiar logic used for spawning heal blocks.
						var newInstance = Instantiate(prefab, Vector3.zero, Quaternion.identity);

						newInstance.transform.parent = tileMap.renderData.transform;
						newInstance.transform.localPosition = new Vector2(tileMap.data.tileSize.x * ctWidth, tileMap.data.tileSize.y * ctHeight);

						spawnedObjects.Add(newInstance);

						// add to tilemap
						if (tilePrefabsInstance != null)
						{
							tilePrefabsInstance.instance = newInstance;
						}
						else
						{
							tileMap.TilePrefabsList.Add(new tk2dTileMap.TilemapPrefabInstance
							{
								instance = newInstance,
								layer = ctLayer,
								x = ctWidth,
								y = ctHeight
							});
						}
					}
				}
			}

			return spawnedObjects;
		}
		
		private void ChangeTileMapSize(tk2dTileMap tileMap, int newWidth, int newHeight)
		{
			tileMap.width = newWidth;

			var heightDifference = newHeight - tileMap.height;
			tileMap.height = newHeight;

			if (team != Team.Top) return;

			// in case of top layout we need to offset the whole tilemap to align with the top heart wall; tileMap always grows upwards
			tileMap.transform.position -= new Vector3(0, tileMap.data.tileSize.y * heightDifference);
		}
		
		private void InitPrefabs(DataBlockLayout layout)
		{
			var ballShooters = GetTilePrefabsWithComponent<BallShooter>();
			if (ballShooters.Count > 0)
			{
				var ballShooterManager = gameObject.AddComponent<BallShooterManager>();
				ballShooterManager.Init(layout.ballShooterConfig, ballShooters);
			}

			var powerUpSpawner = GetTilePrefabsWithComponent<PowerUpSpawner>();
			for (var i = 0; i < Mathf.Min(powerUpSpawner.Count, layout.powerUpSpawnerRoutines.Length); i++)
			{
				powerUpSpawner[i].routine = layout.powerUpSpawnerRoutines[i];
			}
		}

		public List<TComponent> GetTilePrefabsWithComponent<TComponent>()
		{
			var prefabsList = TileMap.TilePrefabsList;
			var foundComponents = new List<TComponent>();

			foreach (var tilePrefab in prefabsList)
			{
				var instance = tilePrefab.instance;
				if (instance == null) continue;

				var component = instance.GetComponent<TComponent>();
				if (component == null) continue;

				foundComponents.Add(component);
			}

			return foundComponents;
		}
		
		public void DisableBlocks()
		{
			var tileMap = TileMap;
			tk2dTileMapUtil.CleanLayers(tileMap);
			tileMap.gameObject.SetActive(false);

			HeartWall.gameObject.SetActive(false);
		}

		public Vector3 GetRandomPosition(float offsetToEdge)
		{
			var spawnBounds = compAIAreaDescription.GetBounds(offsetToEdge);
			Vector3? spawnPos = null;

			// max 5 tries
			for(var i = 0; i < 5; i++)
			{
				var randomPos = MyRandom.Vector3InRectangle(spawnBounds);
				var finalPos = FindValidSpawnPosition(spawnBounds, randomPos);

				if(finalPos.HasValue)
				{
					spawnPos = finalPos.Value;
					break;
				}
			}

			if (!spawnPos.HasValue)
			{
				// if after 5 runs no position was found, use a real random position
				spawnPos = MyRandom.Vector3InRectangle(spawnBounds);
			}

			return spawnPos.Value;
		}

		private Vector3? FindValidSpawnPosition(Bounds bounds, Vector3 curPos)
		{
			while(true)
			{
				var hit = Physics2D.OverlapCircle(curPos, 0.5f, checkSpawnPositionMask);
				if(hit == null) return curPos;
				
				curPos = curPos + faceDirectionVec * 1; // move away from blocks and check again

				if (!bounds.Contains(curPos)) return null; // no point found
			}
		}

		public void DestroyAdjacentLockedBlocks(BlockBase requester)
		{
			var tileMap = TileMap;
			var prefabsYX = GetTilePrefabDictionaryYX();

			int tileX;
			int tileY;
			tileMap.GetTileAtPosition(requester.transform.position, out tileX, out tileY);

			DestroyAdjacentLockedBlocksRecursive(tileMap, prefabsYX, new HashSet<string>(), tileX, tileY);
		}

		private void DestroyAdjacentLockedBlocksRecursive(tk2dTileMap tileMap, Dictionary<int, Dictionary<int, tk2dTileMap.TilemapPrefabInstance>> prefabsYX, HashSet<string> checkedFields, int curX, int curY, int step = 0)
		{
			if (curX < 0 || curY < 0 || curX > tileMap.width || curY > tileMap.height) return; // out of bounds

			var fieldKey = string.Format("{0}-{1}", curX, curY);
			if (checkedFields.Contains(fieldKey)) return; // already checked

			checkedFields.Add(fieldKey);

			Dictionary<int, tk2dTileMap.TilemapPrefabInstance> prefabsInLine;
			tk2dTileMap.TilemapPrefabInstance prefabInstance;
			if (!prefabsYX.TryGetValue(curY, out prefabsInLine) || !prefabsInLine.TryGetValue(curX, out prefabInstance) || prefabInstance.instance == null) return; // no prefab at position

			var blockLocked = prefabInstance.instance.GetComponent<BlockLocked>();
			if (blockLocked != null)
			{
				prefabsInLine.Remove(curX); // remove the entry to avoid others from checking it again
				blockLocked.Unlock(step);
			}

			if (blockLocked == null && prefabInstance.instance.GetComponent<BlockKeyPad>() == null) return; // other block

			step++;

			DestroyAdjacentLockedBlocksRecursive(tileMap, prefabsYX, checkedFields, curX + 1, curY, step);
			DestroyAdjacentLockedBlocksRecursive(tileMap, prefabsYX, checkedFields, curX - 1, curY, step);
			DestroyAdjacentLockedBlocksRecursive(tileMap, prefabsYX, checkedFields, curX, curY + 1, step);
			DestroyAdjacentLockedBlocksRecursive(tileMap, prefabsYX, checkedFields, curX, curY - 1, step);
		}

		/// <summary>
		/// Returns tiles grouped by Y and X where Y always starts at closest row to heart wall.
		/// </summary>
		public Dictionary<int, Dictionary<int, tk2dTileMap.TilemapPrefabInstance>> GetTilePrefabDictionaryYX()
		{
			tileMap = TileMap;

			// Gather tiles for every line
			var prefabsYX = new Dictionary<int, Dictionary<int, tk2dTileMap.TilemapPrefabInstance>>();
			var prefabsList = tileMap.TilePrefabsList;

			for (var i = 0; i < prefabsList.Count; i++)
			{
				var curPrefab = prefabsList[i];

				if (curPrefab.instance == null)
				{
					prefabsList.RemoveAt(i);
					i--;
					continue; // instance destroied -> no tile at position anymore; remove it to reduce loop next time
				}

				// always start with row closest to heart wall at index 0
				var yOffsetFromHeartWall = team == Team.Top ? tileMap.height - 1 - curPrefab.y : curPrefab.y;

				if (!prefabsYX.ContainsKey(yOffsetFromHeartWall))
				{
					prefabsYX[yOffsetFromHeartWall] = new Dictionary<int, tk2dTileMap.TilemapPrefabInstance>();
				}

				prefabsYX[yOffsetFromHeartWall][curPrefab.x] = curPrefab;
			}
			
			return prefabsYX;
		}
	}
}