﻿using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockHeartWall : BlockBase
	{
		public GameObject ActiveGameObject
		{
			get
			{
				return FortifiedOverlay != null ? FortifiedOverlay : gameObject;
			}
		}

		public GameObject FortifiedOverlay { get; set; }
		
		public override void Start()
		{
			base.Start();

			BallHit += OnBallHit;
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			GameManager.Instance.WinGame(GetTeam().GetOpponent(), e.Ball.transform.position);
		}
	}
}