﻿namespace GrandLukto.Blocks
{
	public class Block2Hit : BlockBase
	{
		public int health = 2;

		public override void Start()
		{
			base.Start();

			BallHit += OnHit;
		}

		private void OnHit(object sender, BlockBallHitEventArgs e)
		{
			health--;

			if (health <= 0)
			{
				DestroyBlock();
			}
		}
	}
}