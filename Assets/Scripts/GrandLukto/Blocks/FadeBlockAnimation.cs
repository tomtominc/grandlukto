﻿using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto.Blocks
{
	public class FadeBlockAnimation : MonoBehaviour
	{
		private static Shader fadeShader;

		[HideInInspector]
		public UnityEvent OnAnimationComplete;

		public float duration = 0.5f;
		public Color fadeColor = Color.white;
		public AnimationCurve alphaCurve;
		public AnimationCurve colorCurve;

		public bool IsComplete { get; private set; }

		private float curDuration = 0;
		private bool fadeStarted = false;

		private MeshRenderer compMeshRenderer;

		private tk2dSprite compSprite;
		private SkeletonAnimation compSkeletonAnimation;

		public void Start()
		{
			if(fadeShader == null)
			{
				fadeShader = Shader.Find("UnityCave/PremulVertexColorColorize");
			}

			compMeshRenderer = GetComponent<MeshRenderer>();
			compSprite = GetComponent<tk2dSprite>();
			compSkeletonAnimation = GetComponent<SkeletonAnimation>();
		}

		public void StartFade()
		{
			fadeStarted = true;

			compMeshRenderer.material.shader = fadeShader;
			compMeshRenderer.material.SetColor("_FlashColor", fadeColor);
		}

		public void Update()
		{
			if (!fadeStarted || IsComplete) return;

			curDuration += Time.deltaTime;
			curDuration = Mathf.Clamp(curDuration, 0, duration);

			if(curDuration >= duration)
			{
				curDuration = duration;
				IsComplete = true;
				OnAnimationComplete.Invoke();
			}

			float durationRel = curDuration / duration;

			compMeshRenderer.material.SetFloat("_FlashAmount", colorCurve.Evaluate(durationRel));

			UpdateAlpha(alphaCurve.Evaluate(durationRel));
			
		}

		private void UpdateAlpha(float alpha)
		{
			// TODO: An improvement would be to create an IColorizer interface and classes for colorizing tk2dSprites and Spine animations
			// Spine offers a shader that can colorize the animation properly...

			if (compSprite != null)
			{
				compSprite.color = new Color(compSprite.color.r, compSprite.color.g, compSprite.color.b, alpha);
				return;
			}

			if (compSkeletonAnimation != null)
			{
				compSkeletonAnimation.skeleton.a = alpha;
			}
		}
	}
}
