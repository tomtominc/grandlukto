﻿using GrandLukto.Characters;
using GrandLukto.PowerUps;

namespace GrandLukto.Blocks
{
	public class BlockPowerUp : BlockBase
	{
		public PowerUpType powerUpType;

		public override void Start()
		{
			base.Start();

			BallHit += OnBallHit;
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			var lastHitter = e.Ball.GetLastHitterOfTeam(GetTeam().GetOpponent());
			if (lastHitter != null) lastHitter.OnRecievePowerUp(this, new RecievePowerUpEventArgs(powerUpType));

			DestroyBlock();
		}
	}
}