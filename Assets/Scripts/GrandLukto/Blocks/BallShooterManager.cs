﻿using GrandLukto.Balls;
using System.Collections;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BallShooterManager : MonoBehaviour
	{
		private readonly List<float> ShotAngles = new List<float> { 30f, 60f };

		private BallShooterConfig config;
		private List<BallShooter> ballShooters;

		private int curStepID = 0;
		private int curStepRepeats = 0;

		public void Init(BallShooterConfig config, List<BallShooter> ballShooters)
		{
			this.config = config;
			this.ballShooters = ballShooters;

			if (config == null || !ballShooters.HasItems() || !config.steps.HasItems())
			{
				Destroy(this); // nothing to do
			}
		}

		private void Start()
		{
			GameManager.Instance.OnStateChanged.AddListener(OnGameStateChanged);
		}

		private void OnDestroy()
		{
			GameManager.Instance.OnStateChanged.RemoveListener(OnGameStateChanged);
		}

		private void OnGameStateChanged(GameState newState)
		{
			if (newState != GameState.Running) return;

			// we are running -> start to shoot
			GameManager.Instance.OnStateChanged.RemoveListener(OnGameStateChanged);
			StartCoroutine(ShootCoroutine());
		}

		private IEnumerator ShootCoroutine()
		{
			BallShooterConfigStep curStep = null;
			bool isLoopStart = false;

			while (GetNextStep(out curStep, out isLoopStart))
			{
				var ballShooters = curStep.shooterIndexRand ? GetShooterRandom() : GetShooterByIndex(curStep.shooterIndex);
				if (ballShooters == null) continue;

				var loopDelay = config.loopDelay;
				if (isLoopStart && loopDelay > 0)
				{
					yield return new WaitForSeconds(loopDelay);
				}

				var delayBefore = curStep.delayBefore;
				if (delayBefore > 0)
				{
					yield return new WaitForSeconds(delayBefore);
				}

				int shotAngleInt = curStep.GetShotAngle();
				float shotAngle = 0;
				if (shotAngleInt != 0)
				{
					shotAngle = ShotAngles[Mathf.Abs(shotAngleInt) - 1] * Mathf.Sign(shotAngleInt);
				}
				
				var createBallFunc = CreateCreateBallFunction(curStep.ballCreator);
				for (var i = 0; i < ballShooters.Count; i++)
				{
					var ballShooter = ballShooters[i];
					ballShooter.Shoot(shotAngle, curStep.GetShotStrength(), createBallFunc);
				}
			}
		}

		private System.Func<IBallController> CreateCreateBallFunction(BallCreator ballCreator)
		{
			return () => ballCreator.CreateBall();
		}

		private bool GetNextStep(out BallShooterConfigStep curStep, out bool isLoopStart)
		{
			isLoopStart = false;

			if (curStepID >= config.steps.Count)
			{
				if (!config.loop)
				{
					curStep = null;
					return false; // out of steps
				}

				isLoopStart = true;
				curStepID = 0; // loop it
			}

			curStep = config.steps[curStepID];
			curStepRepeats++;

			if (curStepRepeats >= curStep.repeat)
			{
				curStepRepeats = 0;
				curStepID++;
			}
			return true;
		}

		private List<BallShooter> GetShooterByIndex(int index)
		{
			if (index == -1)
			{
				// ALL
				return ballShooters;
			}

			if (index < 0 || index > ballShooters.Count)
			{
				Debug.LogWarning("Ball Shooter with ID " + index + " not found. Skipped...", this);
				return null;
			}

			return new List<BallShooter> { ballShooters[index] };
		}

		private List<BallShooter> GetShooterRandom()
		{
			return GetShooterByIndex(Random.Range(0, ballShooters.Count));
		}
	}
}