﻿using GrandLukto.PowerUps;
using System.Collections;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public abstract class PowerUpSpawnerRoutineBase : ScriptableObject, IPowerUpSpawnerRoutine
	{
		public FloatRange spawnDelay;
		public NullableFloat disappearAfter;

		public IEnumerator StartRoutine(PowerUpSpawner spawner)
		{
			while (true)
			{
				yield return new WaitForSeconds(spawnDelay);

				spawner.ShowPowerUp(GetNextPowerUp());

				if (disappearAfter.HasValue)
				{
					// wait for disappearAfter and then hide the power up
					yield return new WaitForSeconds(disappearAfter.Value);
					if (spawner.IsPowerUpSpawned) spawner.HidePowerUp();
				}
				else
				{
					// wait until power up is collected
					yield return new WaitUntil(() => !spawner.IsPowerUpSpawned);
				}
			}
		}

		protected abstract PowerUpType GetNextPowerUp();
	}
}