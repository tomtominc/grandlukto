﻿using GrandLukto.Balls;
using Spine.Unity;
using System;
using System.Collections;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BallShooter : MonoBehaviour, IDestroyableBlock
	{
		public GameObject visuals;
		public GameObject shadow;

		[SpineAnimation]
		public string shootAnim;
		public float shootDelay = 0.2f;

		private SkeletonAnimation[] compsAnimation;
		private FadeBlockAnimation fadeAnimation;

		private bool fadeAnimationPlaying = false;

		private void Awake()
		{
			compsAnimation = GetComponentsInChildren<SkeletonAnimation>();
			fadeAnimation = visuals.GetComponent<FadeBlockAnimation>();
		}

		/// <summary>
		/// Shoots a ball.
		/// </summary>
		/// <param name="angle">Angle relative to Vector2.down (clockwise).</param>
		/// <param name="relativeSpeed">Relative speed between 0 and 1.</param>
		/// <param name="createBallFunc">Function used to create the ball.</param>
		public void Shoot(float angle, float relativeSpeed, Func<IBallController> createBallFunc)
		{
			if (fadeAnimationPlaying) return;
			StartCoroutine(ShootAfterDelay(angle, relativeSpeed, createBallFunc));
		}

		private IEnumerator ShootAfterDelay(float angle, float relativeSpeed, Func<IBallController> createBallFunc)
		{
			// play shoot animation on all animations (shadow separate)
			for (var i = 0; i < compsAnimation.Length; i++)
			{
				compsAnimation[i].AnimationState.SetAnimation(0, shootAnim, false);
			}

			yield return new WaitForSeconds(shootDelay);

			if (fadeAnimationPlaying) yield break;

			relativeSpeed = Mathf.Clamp01(relativeSpeed);

			var ball = createBallFunc();
			ball.InitializePosition(transform.position);
			ball.ApplyForce(
				ball.Configuration.GetRelativeSpeed(relativeSpeed),
				Quaternion.Euler(0, 0, angle) * Vector2.down);
		}

		public void DestroyBlock()
		{
			if (fadeAnimationPlaying) return;
			fadeAnimationPlaying = true;

			shadow.SetActive(false);

			// play fade anim
			fadeAnimation.OnAnimationComplete.AddListener(OnFadeBlockAnimationComplete);
			fadeAnimation.StartFade();
		}

		private void OnFadeBlockAnimationComplete()
		{
			fadeAnimation.OnAnimationComplete.RemoveListener(OnFadeBlockAnimationComplete);
			Destroy(gameObject);
		}

		public void SetFadeAnimationColor(Color value)
		{
			fadeAnimation.fadeColor = value;
		}
	}
}