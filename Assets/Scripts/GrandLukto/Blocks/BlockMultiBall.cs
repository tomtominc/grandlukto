﻿using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockMultiBall : BlockBase
	{
		public float angle = 45;
		public float destroyNewBallAfter = 5;

		public override void Start()
		{
			base.Start();

			BallHit += OnBallHit;
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			var ball = e.Ball;

			// randomize which ball is going left and right
			var randomSign = MyRandom.Sign();

			var oldMoveDirection = ball.MoveDirection; // already reflected by the block
			ball.GetComponent<BallPhysics>().ApplyForce(null, Quaternion.Euler(0, 0, angle * randomSign) * oldMoveDirection);

			// create a new vanishing ball as duplicate
			var duplicateBall = new BallFactory().CreateBallVanish(destroyNewBallAfter);
			duplicateBall.CopyValuesFrom(ball);
			duplicateBall.ApplyForce(null, Quaternion.Euler(0, 0, -angle * randomSign) * oldMoveDirection);

			DestroyBlock();
		}
	}
}