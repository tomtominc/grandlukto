﻿using System;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockEvents : MonoBehaviour, IBlockEvents
	{
		public event EventHandler<BlockDestroyedEventArgs> BlockDestroyed;

		public static BlockEvents Instance
		{
			get
			{
				return SingletonUtil.GetInstance<BlockEvents>();
			}
		}

		public void OnBlockDestroyed(object sender, BlockDestroyedEventArgs e)
		{
			if (BlockDestroyed != null) BlockDestroyed(sender, e);
		}
	}
}