﻿using DG.Tweening;
using System.Collections;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class SpawnBlockWithImpact : BlockAnimationBase
	{
		public SpawnBlockWithImpactSetting setting;

		public float startDelay = 0;

		[Header("Alert")]
		public tk2dSprite alertSprite;

		private BoxCollider2D compBoxCollider;
		
		override protected void Awake()
		{
			base.Awake();

			compBoxCollider = GetComponent<BoxCollider2D>();
		}

		private void Start()
		{
			if (setting == null)
			{
				setting = Resources.Load<SpawnBlockWithImpactSetting>("Blocks/SpawnBlockNormal");
			}

			CompBlockBase.Shadow.SetActive(false);
			CompBlockBase.Visuals.SetActive(false);

			compBoxCollider = GetComponent<BoxCollider2D>();
			compBoxCollider.enabled = false;

			StartCoroutine(CheckSpawnCoroutine());
		}

		private IEnumerator CheckSpawnCoroutine()
		{
			yield return new WaitForSeconds(startDelay + setting.startCheckOffset);

			while (true)
			{
				var isClear = CheckAreaClear();
				if (isClear)
				{
					SpawnWithAnimation();
					yield break;
				}

				yield return new WaitForSeconds(setting.checkOffset);
			}
		}

		/// <summary>
		/// Checks if the position at which the block is placed is free.
		/// </summary>
		private bool CheckAreaClear()
		{
			if (compBoxCollider == null) return true; // no box collider -> spawn

			var result = Physics2D.OverlapBox(transform.position + (Vector3)compBoxCollider.offset, compBoxCollider.size - setting.areaClearCheckSizeReduce, 0, setting.areaClearCheckMask);
			return result == null;
		}

		private void SpawnWithAnimation()
		{
			compBoxCollider.enabled = true;

			CompBlockBase.Visuals.SetActive(true);
			CompBlockBase.Visuals.transform.localPosition = new Vector3(0, setting.animSpawnOffsetY);

			var jumpDuration = setting.animJumpDuration; // get random value

			var gameManager = GameManager.Instance;

			var sequence = DOTween.Sequence()
				.Append(CompBlockBase.Visuals.transform
					.DOLocalMove(Vector3.zero, setting.animDuration)
					.SetEase(setting.animEase));

			if (alertSprite != null)
			{
				sequence.Join(alertSprite
					.DOColor(Color.white.MakeTransparent(), setting.animDuration)
					.SetEase(Ease.OutSine));
			}

			sequence.AppendCallback(() =>
				{
					if (alertSprite != null) Destroy(alertSprite.gameObject);
					gameManager.ScreenShake.Shake(setting.screenShakeFirstImpact);
				})
				.Append(CompBlockBase.Visuals.transform
					.DOLocalMoveY(setting.animJumpStrength, jumpDuration / 2)
					.SetEase(Ease.OutCirc))
				.Append(CompBlockBase.Visuals.transform
					.DOLocalMoveY(0, jumpDuration / 2)
					.SetEase(Ease.InCirc))
				.AppendCallback(() =>
				{
					gameManager.ScreenShake.Shake(setting.screenShakeSecondImpact);
				})
				.AppendCallback(OnSpawnAnimComplete);
		}

		private void OnSpawnAnimComplete()
		{
			CompBlockBase.Shadow.SetActive(true);
			OnAnimationComplete();
		}
	}
}