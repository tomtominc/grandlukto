﻿using GrandLukto.Balls;
using System;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockBase : MonoBehaviour, IDestroyableBlock
	{
		public event EventHandler<BlockBallHitEventArgs> BallHit;
		public event EventHandler BlockDestroyed;

		protected FadeBlockAnimation CompFadeAnimation { get; private set; }

		public GameObject Shadow { get; private set; }

		[Tooltip("If not set, the current GameObject contains the visuals.")]
		public GameObject visualsContainer;
		public GameObject Visuals
		{
			get
			{
				return visualsContainer ?? gameObject;
			}
		}

		[Tooltip("Assign a GameObject to override the default shadow.")]
		public GameObject shadowContainer;

		public bool isSizeOfHeartWall = false;
		public bool canBeDestroyed = true;

		private BlockAnimationBase activeBlockAnimation;

		public bool CanBeDestroyedEvaluated
		{
			get
			{
				return activeBlockAnimation == null && canBeDestroyed;
			}
		}

		/// <summary>
		/// E.g. can be transformed into fortify.
		/// </summary>
		public virtual bool CanBeModified
		{
			get
			{
				return activeBlockAnimation == null && !isBeingDestroied;
			}
		}

		private bool isBeingDestroied = false;

		protected virtual void Awake()
		{
			CompFadeAnimation = GetComponent<FadeBlockAnimation>();

			if (shadowContainer != null)
			{
				Shadow = shadowContainer;
			}
			else
			{
				Shadow = new BlockShadowFactory().Create(transform, isSizeOfHeartWall);
			}
		}

		public virtual void Start()
		{
		}
		
		/// <summary>
		/// Destroys the block with the fade animation.
		/// </summary>
		public virtual void DestroyBlock()
		{
			if (isBeingDestroied) return;
			if (!CanBeModified || !CanBeDestroyedEvaluated) return;

			isBeingDestroied = true;

			StartDestroyRoutine();
		}

		protected virtual void StartDestroyRoutine()
		{
			FadeOutAndDestroy();
		}

		protected void FadeOutAndDestroy()
		{
			var fadeAnimation = CompFadeAnimation;
			if (fadeAnimation == null)
			{
				OnFadeBlockAnimationComplete(); // no fade animation for this block
				return;
			}

			Shadow.SetActive(false);

			fadeAnimation.OnAnimationComplete.AddListener(OnFadeBlockAnimationComplete);
			fadeAnimation.StartFade();
		}

		private void OnFadeBlockAnimationComplete()
		{
			if (CompFadeAnimation != null)
			{
				CompFadeAnimation.OnAnimationComplete.RemoveListener(OnFadeBlockAnimationComplete);
			}

			Destroy(gameObject);
			OnBlockDestroyed();
		}

		public void SetFadeAnimationColor(Color color)
		{
			if (CompFadeAnimation == null) return;
			CompFadeAnimation.fadeColor = color;
		}
		
		public void TryInvokeOnBallHit(Ball sender)
		{
			if (isBeingDestroied) return;
			OnBallHit(sender);
		}

		public void SetActiveBlockAnimation(BlockAnimationBase blockAnimation)
		{
			if (activeBlockAnimation != null)
			{
				Debug.LogError("There is already an active block animation for this block!");
				return;
			}

			activeBlockAnimation = blockAnimation;
			activeBlockAnimation.AnimationComplete += OnBlockAnimationComplete;
		}

		private void OnBlockAnimationComplete(object sender, EventArgs e)
		{
			activeBlockAnimation.AnimationComplete -= OnBlockAnimationComplete;
			activeBlockAnimation = null;
		}

		private void OnBlockDestroyed()
		{
			if (BlockDestroyed == null) return;
			BlockDestroyed.Invoke(this, EventArgs.Empty);

			BlockEvents.Instance.OnBlockDestroyed(this, new BlockDestroyedEventArgs(GetTeam(), this));
		}

		private void OnBallHit(Ball ball)
		{
			if (BallHit == null) return;
			BallHit.Invoke(this, new BlockBallHitEventArgs(ball));
		}

		public Team GetTeam()
		{
			return GetComponentInParent<BlockGroup>().team;
		}
	}
}