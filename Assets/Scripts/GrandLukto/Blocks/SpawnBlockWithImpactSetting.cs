﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityCave.CameraEffects;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[CreateAssetMenu(menuName = "Grand Lukto/SpawnBlockWithImpact Setting")]
	public class SpawnBlockWithImpactSetting : ScriptableObject
	{
		[Header("Space empty check")]
		public Vector2 areaClearCheckSizeReduce = new Vector2(0.02f, 0.02f);
		public LayerMask areaClearCheckMask;
		public FloatRange startCheckOffset = new FloatRange(0f, 0.1f);
		public float checkOffset = 0.05f;

		[Header("Animation")]
		public float animSpawnOffsetY = 5;
		public Ease animEase;
		public float animDuration;

		public FloatRange animJumpDuration = new FloatRange(0.2f, 0.4f);
		public FloatRange animJumpStrength = new FloatRange(0.5f, 8f);

		public ScreenShakeSetting screenShakeFirstImpact;
		public ScreenShakeSetting screenShakeSecondImpact;
	}
}
