﻿namespace GrandLukto.Blocks
{
	public class PracticeTarget : BlockBase
	{
		public override void Start()
		{
			base.Start();

			// notice PracticeTargetsManager that we are alive
			PracticeTargetsManager.Instance.RegisterTarget(this);
			BallHit += OnBallHit;
		}

		private void OnBallHit(object sender, BlockBallHitEventArgs e)
		{
			PracticeTargetsManager.Instance.DestroyTarget(this, e.Ball.transform.position);
			DestroyBlock();
		}
	}
}