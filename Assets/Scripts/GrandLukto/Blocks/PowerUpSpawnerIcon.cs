﻿using GrandLukto.PowerUps;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class PowerUpSpawnerIcon : MonoBehaviour
	{
		private tk2dSprite compSprite;

		private void Awake()
		{
			compSprite = GetComponent<tk2dSprite>();
		}

		private void Start()
		{
			Hide();
		}

		/// <summary>
		/// Shows the icon for the given type.
		/// </summary>
		public void Show(PowerUpType type)
		{
			gameObject.SetActive(true);

			compSprite.SetSprite(string.Format("powerup-spawner-{0}", type.GetIconName()));
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}
	}
}