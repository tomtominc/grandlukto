﻿using System;

namespace GrandLukto.Blocks
{
	public class BlockDestroyedEventArgs : EventArgs
	{
		public Team Team { get; private set; }
		public BlockBase Block { get; private set; }

		public BlockDestroyedEventArgs(Team team, BlockBase block)
		{
			Team = team;
			Block = block;
		}
	}
}