﻿using Rotorz.ReorderableList;
using System;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[CustomPropertyDrawer(typeof(BallShooterConfigStep))]
	public class BallShooterConfigStepDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = 0f;

			height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("delayBefore")) + EditorGUIUtility.singleLineHeight;
			height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("shooterIndex")) + EditorGUIUtility.standardVerticalSpacing;
			height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("shotStrength")) + EditorGUIUtility.standardVerticalSpacing;
			height += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
			height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("repeat")) + EditorGUIUtility.standardVerticalSpacing;
			height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("ballCreator")) + EditorGUIUtility.standardVerticalSpacing;

			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var randCol = new Rect(position.x + position.width - 15, position.y, 15, EditorGUIUtility.singleLineHeight);
			var mainCol = new Rect(position.x, position.y, position.width - 20, EditorGUIUtility.singleLineHeight);

			var propDelayBefore = property.FindPropertyRelative("delayBefore");
			mainCol.height = EditorGUI.GetPropertyHeight(propDelayBefore);
			EditorGUI.PropertyField(mainCol, propDelayBefore);
			mainCol.y += mainCol.height + EditorGUIUtility.standardVerticalSpacing;

			mainCol.height = EditorGUIUtility.singleLineHeight;

			// shooter Index
			EditorGUI.BeginDisabledGroup(DrawRandCheckbox(property.FindPropertyRelative("shooterIndexRand"), mainCol, randCol));

			EditorGUI.PropertyField(mainCol, property.FindPropertyRelative("shooterIndex"));
			mainCol.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			EditorGUI.EndDisabledGroup();

			// shot Strength
			EditorGUI.BeginDisabledGroup(DrawRandCheckbox(property.FindPropertyRelative("shotStrengthRand"), mainCol, randCol));

			EditorGUI.PropertyField(mainCol, property.FindPropertyRelative("shotStrength"));
			mainCol.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			EditorGUI.EndDisabledGroup();

			// shot Angle
			EditorGUI.BeginDisabledGroup(DrawRandCheckbox(property.FindPropertyRelative("shotAngleRand"), mainCol, randCol));

			EditorGUI.IntSlider(mainCol, property.FindPropertyRelative("shotAngle"), -2, 2);
			mainCol.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			EditorGUI.EndDisabledGroup();
			
			EditorGUI.IntSlider(mainCol, property.FindPropertyRelative("repeat"), 1, 20);
			mainCol.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			var propertyBallCreator = property.FindPropertyRelative("ballCreator");
			mainCol.height = EditorGUI.GetPropertyHeight(propertyBallCreator);
			EditorGUI.PropertyField(mainCol, propertyBallCreator);

			//mainCol.height = EditorGUIUtility.singleLineHeight;
			//EditorGUI.Toggle(mainCol, false);
		}

		private bool DrawRandCheckbox(SerializedProperty property, Rect mainCol, Rect randCol)
		{
			property.boolValue = EditorGUI.Toggle(new Rect(randCol.x, mainCol.y, randCol.width, mainCol.height), property.boolValue);
			return property.boolValue;
		}
	}
}