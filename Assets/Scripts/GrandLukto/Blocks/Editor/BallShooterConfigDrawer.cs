﻿using Rotorz.ReorderableList;
using System;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[CustomPropertyDrawer(typeof(BallShooterConfig))]
	public class BallShooterConfigDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = EditorGUIUtility.standardVerticalSpacing * 2; // offset
			height += EditorGUIUtility.singleLineHeight; // label

			var propLoop = property.FindPropertyRelative("loop");
			height += EditorGUI.GetPropertyHeight(propLoop);

			if (propLoop.boolValue)
			{
				height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("loopDelay")) + EditorGUIUtility.standardVerticalSpacing;
			}

			height += EditorGUIUtility.singleLineHeight - 1; // title
			height += ReorderableListGUI.CalculateListFieldHeight(property.FindPropertyRelative("steps"));

			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var mainCol = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

			mainCol.y += EditorGUIUtility.standardVerticalSpacing * 2; // offset

			EditorGUI.LabelField(mainCol, "Ball Shooter Config", EditorStyles.label);
			mainCol.y += EditorGUIUtility.singleLineHeight;

			var propLoop = property.FindPropertyRelative("loop");
			EditorGUI.PropertyField(mainCol, propLoop);
			mainCol.y += EditorGUIUtility.singleLineHeight;

			if (propLoop.boolValue)
			{
				mainCol.height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("loopDelay"));
				EditorGUI.PropertyField(mainCol, property.FindPropertyRelative("loopDelay"), true);
				mainCol.y += mainCol.height + EditorGUIUtility.standardVerticalSpacing;
			}

			mainCol.height = EditorGUIUtility.singleLineHeight;
			ReorderableListGUI.Title(mainCol, "Steps");
			mainCol.y += EditorGUIUtility.singleLineHeight - 1;

			mainCol.height = ReorderableListGUI.CalculateListFieldHeight(property.FindPropertyRelative("steps"));
			ReorderableListGUI.ListFieldAbsolute(mainCol, property.FindPropertyRelative("steps"));
			mainCol.y += mainCol.height;
		}
	}
}