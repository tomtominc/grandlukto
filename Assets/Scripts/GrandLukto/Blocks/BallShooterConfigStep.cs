﻿using GrandLukto.Balls;
using System;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Blocks
{
	[Serializable]
	public class BallShooterConfigStep
	{
		[FloatRange(0, 60)]
		public FloatRange delayBefore;

		public bool shooterIndexRand = false;
		public int shooterIndex;

		public bool shotStrengthRand = false;
		[Range(0, 1)]
		public float shotStrength = 0.5f;

		public bool shotAngleRand = false;
		public int shotAngle = 0;

		public int repeat = 1;

		public BallCreator ballCreator;

		public float GetShotStrength()
		{
			if (shotStrengthRand)
			{
				return UnityEngine.Random.value;
			}

			return shotStrength;
		}

		public int GetShotAngle()
		{
			if (shotAngleRand)
			{
				// can be any value from -2 to 2 (including)
				return UnityEngine.Random.Range(-2, 3);
			}

			return shotAngle;
		}
	}
}
