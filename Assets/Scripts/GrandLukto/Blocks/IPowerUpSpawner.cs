﻿using UnityEngine;

namespace GrandLukto.Blocks
{
	public interface IPowerUpSpawner
	{
		bool IsPowerUpSpawned { get; }

		/// <summary>
		/// Returns the PowerUpSpawner's visual center in world coordinates.
		/// </summary>
		Vector2 CenterPosition { get; }
	}
}