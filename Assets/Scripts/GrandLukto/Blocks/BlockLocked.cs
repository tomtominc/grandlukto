﻿using System.Collections;
using UnityEngine;

namespace GrandLukto.Blocks
{
	public class BlockLocked : BlockBase
	{
		public float destroyDelay = 0.1f;

		private bool isUnlockQueued = false;

		public override bool CanBeModified
		{
			get
			{
				if (isUnlockQueued) return false;
				return base.CanBeModified;
			}
		}

		public override void Start()
		{
			base.Start();

			canBeDestroyed = false; // will get destroied after key is collected
		}

		public void Unlock(int step)
		{
			isUnlockQueued = true;
			StartCoroutine(UnlockAfterDelay(destroyDelay * step));
		}

		private IEnumerator UnlockAfterDelay(float delay)
		{
			yield return new WaitForSeconds(delay);

			canBeDestroyed = true;
			DestroyBlock();
		}
	}
}