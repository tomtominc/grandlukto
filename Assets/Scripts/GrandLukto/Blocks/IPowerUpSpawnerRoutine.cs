﻿using System.Collections;

namespace GrandLukto.Blocks
{
	public interface IPowerUpSpawnerRoutine
	{
		IEnumerator StartRoutine(PowerUpSpawner spawner);
	}
}