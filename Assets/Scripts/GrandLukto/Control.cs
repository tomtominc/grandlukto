﻿namespace GrandLukto
{
	public class Control
	{
		public const string MoveHorizontal = "Move Horizontal";
		public const string MoveVertical = "Move Vertical";
		public const string Shoot = "Shoot";
		public const string PowerUp = "Power Up";
		public const string Dash = "Dash";
		public const string Pause = "Pause";
	}
}