﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityCave;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.SaveData
{
	[Serializable]
	public class SaveDataCampaign
	{
		public DateTime createdAt;
		public int SlotID;
		public bool IsCoop = false;

		public List<string> CompletedLevelIDs = new List<string>();
		public string playerPosLevelID;
		public List<string> serializedFlags;

		[NonSerialized]
		private HashSet<string> flags = new HashSet<string>();

		/// <summary>
		/// Contains the selected character names for every player ID.
		/// </summary>
		public List<string> SelectedCharacterNames = new List<string>();

		private SaveDataCampaign() { } // Not allowed! Use the static Load method instead

		public void SetLevelCompleted(string levelID)
		{
			if (CompletedLevelIDs.Contains(levelID)) return; // already completed
			CompletedLevelIDs.Add(levelID);
		}

		public string GetSelectedCharacterByPlayerID(int playerID)
		{
			if (SelectedCharacterNames == null) return null;
			return SelectedCharacterNames.ElementAtOrDefault(playerID);
		}

		public void SetSelectedCharacterByPlayerID(int playerID, string charName)
		{
			if (SelectedCharacterNames == null) SelectedCharacterNames = new List<string>();

			if(SelectedCharacterNames.Count < playerID + 1)
			{
				SelectedCharacterNames.AddRange(new string[(playerID + 1) - SelectedCharacterNames.Count]); // make list grow
			}

			SelectedCharacterNames[playerID] = charName;
		}

		public bool GetFlag(string flagName)
		{
			if (flags == null) return false;
			return flags.Contains(flagName);
		}

		public void SetFlag(string flagName, bool value)
		{
			if (flags == null) flags = new HashSet<string>();

			if (value)
			{
				flags.Add(flagName);
				return;
			}

			flags.Remove(flagName);
		}

		public void Clear()
		{
			CompletedLevelIDs = new List<string>();
			flags = new HashSet<string>();
			playerPosLevelID = null;

			Save();
		}

		public void Save()
		{
			SaveGame.SaveData(GetFileName(SlotID, IsCoop), this, obfName: true);
		}

		public static SaveDataCampaign Load(int slotID, bool isCoop)
		{
			var fileName = GetFileName(slotID, isCoop);

			if (SaveGame.Exists(fileName, obfName: true))
			{
				var loadedSaveGame = SaveGame.LoadData<SaveDataCampaign>(fileName, obfName: true);
				if (loadedSaveGame != null) return loadedSaveGame;

				Debug.LogError("Could not load savegame!");
			}

			// does not exist, recreate
			var saveData = new SaveDataCampaign()
			{
				SlotID = slotID,
				IsCoop = isCoop,
				createdAt = DateTime.Now
			};

			saveData.Save(); // save creation time
			return saveData;
		}

		private static string GetFileName(int slotID, bool isCoop)
		{
			return string.Format("campaign_{0}_{1}_v0.1", isCoop ? "coop" : "single", slotID);
		}

		[OnSerializing]
		private void OnSerializing(StreamingContext context)
		{
			serializedFlags = flags.ToList();
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			flags = new HashSet<string>(serializedFlags.HasItems() ? serializedFlags : new List<string>());
		}
	}
}