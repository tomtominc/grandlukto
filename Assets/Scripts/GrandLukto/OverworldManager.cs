﻿using DG.Tweening;
using GrandLukto.Characters;
using GrandLukto.GameData;
using GrandLukto.Overworld;
using GrandLukto.SaveData;
using GrandLukto.UI;
using Rewired;
using UnityCave.UI;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class OverworldManager : GameManagerBase
	{
		public static new OverworldManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<OverworldManager>();
			}
		}

		[Header("Global Objects")]
		public RosterMenu rosterMenu;

		[Header("Config")]
		public float unlockNodeDelay = 0.5f;

		public SaveDataCampaign SaveData { get; private set; }

		public bool IsAnimating { get; private set; }

		private UIBackgroundAnimation curBackgroundAnim;
		public OverworldPlayer Player { get; private set; }

		public bool PlayerHasControl
		{
			get
			{
				return !rosterMenu.IsOpen;
			}
		}
		
		public void Awake()
		{
			// TODO: Load by real slotID (do this before Awake since OverworldObjects access it in Awake
			SaveData = SaveDataCampaign.Load(0, false);

			// todo: support for coop, dynamic character
			PlayerSettings.Add(new PlayerSetting
			{
				controller = ReInput.players.AllPlayers[1],
				isHuman = true,
				playerId = 0,
				selectedCharacter = DatabaseManager.Instance.dbCharacter.Get(0),
				team = Team.Bottom
			});

			IsAnimating = true;

			Player = GameObject.FindGameObjectWithTag(Tag.Player).GetComponent<OverworldPlayer>();
		}

		public override void Start()
		{
			base.Start();

			curBackgroundAnim = UIStatic.CreateBackgroundAnim(uiContainer.transform, uiCamera);

			curBackgroundAnim
				.Hide()
				.OnComplete(() =>
				{
					IsAnimating = false;
				});
		}

		public override void Update()
		{
			base.Update();

			PlayerSetting playerSetting;
			if (IsButtonPressed(Control.Dash, out playerSetting))
			{
				rosterMenu.Show();
			}
		}

		protected override void CheckDebugKeys(Keyboard keyboard)
		{
			base.CheckDebugKeys(keyboard);

			if (keyboard.GetKeyDown(KeyCode.F1))
			{
				// set current node completed
				Player.CurNode.SetNodeCompleted();
			}

			if (keyboard.GetKeyDown(KeyCode.F2))
			{
				// clear savegame
				SaveData.Clear();
			}
		}

		public void StartLevel(OverworldObjectBase node)
		{
			if (IsAnimating) return; // already animating

			if (node.levelData == null)
			{
				Debug.LogWarning("Can not start level. Node has no levelData attached.");
				return;
			}

			IsAnimating = true;

			curBackgroundAnim
				.Show()
				.OnComplete(() =>
				{
					GameSceneManager.LoadGame(node.levelData.arena, node.levelData.levelObj, node.levelData.GetPlayerSettings(PlayerSettings[0]), new GameDataCampaign(SaveData, node.nodeID, node.levelData));
				});
		}

		public void QuitToMenu()
		{
			SaveData.Save();
			GameSceneManager.LoadMenu();
		}
	}
}