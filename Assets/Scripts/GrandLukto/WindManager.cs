﻿using DG.Tweening;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto
{
	public class WindManager : MonoBehaviour
	{
		public float WindStrength { get; private set; }

		/// <summary>
		/// The strength at which the wind currently pushes players.
		/// </summary>
		public float PushPlayerStrength { get { return WindStrength * pushPlayerStrength; } }

		[SerializeField]
		private float pushPlayerStrength = 1;

		[Tooltip("Duration between wind bursts in seconds.")]
		public FloatRange windBurstOffset;
		public FloatRange windBurstDuration;
		public FloatRange windBurstStrength;

		/// <summary>
		/// Strength change per second.
		/// </summary>
		public float deltaStrength;

		private Sequence curTween;
		
		public void StartBurstTween()
		{
			if(curTween != null) curTween.Complete();

			var targetStrength = windBurstStrength;
			var changeDuration = targetStrength / deltaStrength;

			curTween = DOTween.Sequence()
				.AppendInterval(windBurstOffset)
				.Append(DOTween
					.To(() => WindStrength, value => WindStrength = value, windBurstStrength, changeDuration)
					.SetEase(Ease.InOutSine))
				.AppendInterval(windBurstDuration)
				.Append(DOTween
					.To(() => WindStrength, value => WindStrength = value, 0, changeDuration)
					.SetEase(Ease.InOutSine))
				.AppendCallback(StartBurstTween);
		}

		private void OnDisable()
		{
			curTween.Complete();
		}
	}
}