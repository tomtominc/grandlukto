﻿using System;
using GrandLukto.UI.ArenaSelection;
using UnityCave.UI;
using UnityEngine;
using Rewired;
using DG.Tweening;
using GrandLukto.Characters;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using GrandLukto.Database;

namespace GrandLukto.UI
{
	public class ScreenArenaSelection : UIScreenBase
	{
		public ArenaSelect arenaSelection;
		public BlockLayoutGroupSelection blockLayoutGroupSelection;

		public DataSoundSetting soundOnSelectArena;
		public DataSoundSetting soundOnSelectBlockLayoutGroup;

		private List<PlayerSetting> playerSettings;

		private Player ControllingPlayer
		{
			get
			{
				return UIManager.Instance.LeadPlayer;
			}
		}

		public void Initialize(List<PlayerSetting> playerSettings)
		{
			this.playerSettings = playerSettings;

			var controllingPlayer = ControllingPlayer;
			var controllingPlayerSetting = playerSettings.Find(setting => setting.controller == controllingPlayer);

			if(controllingPlayerSetting == null)
			{
				Debug.LogError("PlayerSetting for controlling player not found!");
				return;
			}

			arenaSelection.SetFrameColor(controllingPlayerSetting.PlayerColor);
		}

		public void Awake()
		{
			Show += OnShowScreen;
		}

		public void OnDestroy()
		{
			Show -= OnShowScreen;
		}

		private void OnShowScreen(object sender, EventArgs e)
		{
			arenaSelection.Show(ControllingPlayer); // todo: set lead player
			AddArenaSelectListeners();
		}

		private void RemoveArenaSelectListeners()
		{
			arenaSelection.CompUIGroup.EnterPressed.RemoveListener(OnArenaSelected);
			arenaSelection.CompUIGroup.BackPressed.RemoveListener(OnArenaSelectBack);
			arenaSelection.CompUIGroup.Unassign();
		}

		private void AddArenaSelectListeners()
		{
			arenaSelection.CompUIGroup.EnterPressed.AddListener(OnArenaSelected);
			arenaSelection.CompUIGroup.BackPressed.AddListener(OnArenaSelectBack);
		}

		private void OnArenaSelectBack()
		{
			RemoveArenaSelectListeners();

			DOTween.Sequence()
				.Append(arenaSelection.Hide())
				.AppendCallback(() =>
				{
					Manager.BackToLastScreenInStack();
				});
		}

		private void OnArenaSelected()
		{
			RemoveArenaSelectListeners();

			blockLayoutGroupSelection.gameObject.SetActive(true);

			UIManager.Instance.PlaySound(soundOnSelectArena);

			DOTween.Sequence()
				.Append(blockLayoutGroupSelection.Show(ControllingPlayer)) // todo: set lead player
				.AppendCallback(() =>
				{
					blockLayoutGroupSelection.EnterPressed.AddListener(OnBlockLayoutGroupSelected);
					blockLayoutGroupSelection.BackPressed.AddListener(OnBlockLayoutGroupSelectBack);
				});
		}

		private void RemoveBlockLayoutSelectListeners()
		{
			blockLayoutGroupSelection.EnterPressed.RemoveListener(OnBlockLayoutGroupSelected);
			blockLayoutGroupSelection.BackPressed.RemoveListener(OnBlockLayoutGroupSelectBack);
		}

		private void OnBlockLayoutGroupSelectBack()
		{
			RemoveBlockLayoutSelectListeners();

			DOTween.Sequence()
				.Append(blockLayoutGroupSelection.Hide())
				.AppendCallback(() =>
				{
					blockLayoutGroupSelection.gameObject.SetActive(false);
					AddArenaSelectListeners();
					arenaSelection.CompUIGroup.AssignToPlayer(ControllingPlayer);
				});
		}

		private void OnBlockLayoutGroupSelected()
		{
			UIManager.Instance.PlaySound(soundOnSelectBlockLayoutGroup);

			RemoveBlockLayoutSelectListeners();

			DOTween.Sequence()
				.Join(blockLayoutGroupSelection.Hide(true))
				.Join(arenaSelection.Hide())
				.AppendCallback(OnSelectionFinished);
		}

		private void OnSelectionFinished()
		{
			var level = blockLayoutGroupSelection.GetSelectedItem().assignedLevels.SelectRandom();
			GameSceneManager.LoadGame(arenaSelection.GetSelectedArena(), level, playerSettings);
		}
	}
}