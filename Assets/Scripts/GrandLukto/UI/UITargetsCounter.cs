﻿using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI
{
	public class UITargetsCounter : MonoBehaviour
	{
		public UISlicedSpriteBar slicedSpriteBar;

		public float magicalOffset = -0.02f;

		private int? lastSeparatorCount;
		private List<GameObject> separators = new List<GameObject>();

		private void Awake()
		{
			var gameManager = GameManager.Instance;
			GetComponent<tk2dCameraAnchor>().AnchorCamera = gameManager.uiCamera;

			PracticeTargetsManager.Instance.OnTargetsChanged += OnPracticeTargetsChanged;
		}

		private void Start()
		{
			slicedSpriteBar.Scale(0);
		}

		private void OnPracticeTargetsChanged(object sender, PracticeTargetsManager.TargetsChangedArgs e)
		{
			UpdateBar(e.DeadTargets, e.SpawnedTargets);
		}
		
		private void UpdateBar(int deadTargets, int spawnedTargets)
		{
			if (!lastSeparatorCount.HasValue || lastSeparatorCount.Value != spawnedTargets)
			{
				// number of required separators changed
				UpdateSeparators(spawnedTargets);
			}

			slicedSpriteBar.Scale((float)deadTargets / spawnedTargets);
		}

		private void UpdateSeparators(int numFields)
		{
			lastSeparatorCount = numFields;

			var requiredSeparators = numFields - 1;
			var maxSeparators = Mathf.Max(requiredSeparators, separators.Count);
			
			for (var i = 0; i < maxSeparators; i++)
			{
				GameObject separator;

				if (i >= separators.Count)
				{
					// create new one
					separator = CreateSeparator();
					separators.Add(separator);
				}
				else
				{
					separator = separators[i];
				}

				separator.gameObject.SetActive(i < requiredSeparators);
				if (i >= requiredSeparators) continue; // only set inactive

				separator.transform.localPosition = separator.transform.localPosition.Clone(Mathf.Lerp(slicedSpriteBar.dimensionsMin.x, slicedSpriteBar.dimensionsMax.x, (i + 1) / (float)numFields) / 100 + magicalOffset);
			}
		}
		
		private GameObject CreateSeparator()
		{
			return Instantiate(Resources.Load<GameObject>("UI/TargetsCounterLine"), slicedSpriteBar.transform, false);
		}

		private void OnDestroy()
		{
			PracticeTargetsManager.Instance.OnTargetsChanged -= OnPracticeTargetsChanged;
		}
	}
}