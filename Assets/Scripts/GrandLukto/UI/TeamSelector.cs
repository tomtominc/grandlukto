﻿using System;
using System.Linq;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI
{
	// todo: reduce to one: ArenaSelector, TeamSelector, LevelSelector
	public class TeamSelector : MonoBehaviour
	{
		public tk2dTextMesh teamLabel;

		public Team SelectedTeam
		{
			get
			{
				return possibleTeams[selectedIndex];
			}
		}

		private Team[] possibleTeams = Enum.GetValues(typeof(Team)).Cast<Team>().ToArray();
		private int selectedIndex = 0;

		public void Start()
		{
			UpdateSelectedTeam();
		}

		public void ChangeSelectedTeam(int change)
		{
			selectedIndex = MathUtil.CycleThrough(selectedIndex, change, 0, possibleTeams.Length);
			UpdateSelectedTeam();
		}

		private void UpdateSelectedTeam()
		{
			teamLabel.text = possibleTeams[selectedIndex].ToString();
		}
	}
}
