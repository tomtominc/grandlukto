﻿using DG.Tweening;
using UnityCave;
using UnityCave.ExtensionMethods;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace GrandLukto.UI
{
	public enum PauseMenuAction
	{
		None,
		Resume,
		Restart,
		Options,
		Quit
	}

	public class PauseMenu : MonoBehaviour
	{
		public UnityEvent Quit;

		public Vector3 tweenOffset;
		public GameObject banner;

		public GameObject background;
		public Color backgroundColor;
		public tk2dSprite backgroundSprite;

		public float tweenDuration = 0.4f;
		public Ease easeIn;
		public Ease easeOut;

		private UIGroup compUIGroup;

		private Tween curTween;
		private PauseMenuAction selectedAction;

		public void Awake()
		{
			compUIGroup = GetComponentInChildren<UIGroup>();

			backgroundSprite = background.GetComponent<tk2dSprite>();
			backgroundColor = backgroundSprite.color;
		}
		
		public void Start()
		{
			GameManagerBase.Instance.GamePaused += OnGamePaused;

			gameObject.SetActive(false);
		}

		private void OnGamePaused(object sender, GamePausedArgs args)
		{
			gameObject.SetActive(true);

			compUIGroup.AssignToPlayer(args.Player.controller);

			PlayAnimation();
		}

		public void Update()
		{
			if(compUIGroup.ControllingPlayers.GetButtonDown(Control.Pause))
			{
				// pause pressed again -> resume
				SelectAction(PauseMenuAction.Resume);
				return;
			}
		}

		private Tween PlayAnimation(bool playBackwards = false)
		{
			if (curTween != null) curTween.Complete();

			var bannerStartPos = banner.GetComponent<RememberStartPosition>();
			bannerStartPos.ResetToStartPosition();

			var ease = playBackwards ? easeOut : easeIn;

			var targetColor = backgroundColor;
			backgroundSprite.color = backgroundColor * new Color(1, 1, 1, 0);

			curTween = DOTween.Sequence()
				.Append(banner.transform
					.DOMove(tweenOffset, tweenDuration)
					.From(true)
					.SetEase(ease))
				.Join(backgroundSprite
					.DOColor(targetColor, tweenDuration * 0.5f))
				.SetUpdate(true);

			if (playBackwards)
			{
				curTween.SetAutoKill(false);
				curTween.Complete();
				curTween.PlayBackwards();
			}

			return curTween;
		}

		public void SelectAction(string action)
		{
			SelectAction(action.ToEnum(PauseMenuAction.None));
		}

		public void SelectAction(PauseMenuAction action)
		{
			if (action == PauseMenuAction.None) return;
			if (selectedAction != PauseMenuAction.None) return; // action already queued

			selectedAction = action;

			if (selectedAction == PauseMenuAction.Quit)
			{
				// background animation
				UIStatic.CreateBackgroundAnim(transform, GameManagerBase.Instance.uiCamera).GetComponent<UIBackgroundAnimation>()
					.Show()
					.OnComplete(() =>
					{
						if (Quit != null) Quit.Invoke();
						else Debug.LogError("No action for Quit defined!");
					});
				return;
			}

			compUIGroup.ToggleSelected(false);
			compUIGroup.ForgetSelection();

			PlayAnimation(true)
				.OnRewind(HandleSelectedAction);
		}

		private void HandleSelectedAction()
		{
			switch(selectedAction)
			{
				case PauseMenuAction.Resume:
					TimeManager.Instance.TogglePause(false);
					break;
				default:
					Debug.Log("Action not implemented!");
					selectedAction = PauseMenuAction.None;
					return;
			}

			selectedAction = PauseMenuAction.None;
			gameObject.SetActive(false);
		}

		private void OnBackgroundAnimationComplete()
		{
			SceneManager.sceneLoaded += OnMenuLoaded;
			SceneManager.LoadScene(SceneName.Menu.ToString(), LoadSceneMode.Single);
		}

		private void OnMenuLoaded(Scene scene, LoadSceneMode mode)
		{
			SceneManager.sceneLoaded -= OnMenuLoaded;
			TimeManager.Instance.Reset();
		}
	}
}
