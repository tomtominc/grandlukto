﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	public class DialogLeftRightAnimation : MonoBehaviour
	{
		private DirectionalOffsetToParent compOffset;
		private UIDialog targetDialog;

		public float leftXPos;
		public float rightXPos;

		private void Awake()
		{
			compOffset = GetComponent<DirectionalOffsetToParent>();

			targetDialog = GetComponentInParent<UIDialog>();
			targetDialog.ShowPage += Dialog_ShowPage;
			targetDialog.Initialize += Dialog_Initialize;
		}

		private void Dialog_Initialize(object sender, DialogShowPageEventArgs e)
		{
			compOffset.offset.x = GetXOffset(e.Page);
		}

		private void Dialog_ShowPage(object sender, DialogShowPageEventArgs e)
		{
			compOffset
				.DOOffset(new Vector3(GetXOffset(e.Page), 0, 0), 0.3f)
				.SetEase(Ease.OutExpo)
				.SetUpdate(true);
		}
		
		private float GetXOffset(DialogDataPage page)
		{
			return page.avatarPosition == DialogAvatarPosition.Left ? leftXPos : rightXPos;
		}

		private void OnDestroy()
		{
			targetDialog.Initialize -= Dialog_Initialize;
			targetDialog.ShowPage -= Dialog_ShowPage;
		}
	}
}