﻿using System;
using UnityCave.tk2d;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[Serializable]
	public class DialogAvatarData
	{
		[tk2dSpriteAttribute("UIDialog")]
		public string spriteName;

		public Vector3 offset;
	}
}
