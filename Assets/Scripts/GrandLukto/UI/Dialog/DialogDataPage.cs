﻿using GrandLukto.Database;
using System;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[Serializable]
	public class DialogDataPage
	{
		[TextArea]
		public string text;

		public DataCharacter avatarCharacter;
		public DialogAvatarPosition avatarPosition;
	}
}