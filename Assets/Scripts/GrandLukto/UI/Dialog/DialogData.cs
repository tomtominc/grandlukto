﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[System.Serializable]
	public class DialogData
	{
		[SerializeField]
		public List<DialogDataPage> pages = new List<DialogDataPage>();
	}
}