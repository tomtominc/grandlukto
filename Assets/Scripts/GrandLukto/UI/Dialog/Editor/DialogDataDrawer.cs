﻿using Rotorz.ReorderableList;
using System;
using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[CustomPropertyDrawer(typeof(DialogData))]
	public class DialogDataDrawer : PropertyDrawer
	{
		private EditorHeightCalculator heightCalculator = new EditorHeightCalculator();
		private EditorColumn colMain = new EditorColumn();

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			heightCalculator.Reset();

			heightCalculator.AddLine();

			heightCalculator.Add(EditorGUIUtility.singleLineHeight - 1); // title
			heightCalculator.Add(ReorderableListGUI.CalculateListFieldHeight(property.FindPropertyRelative("pages")));

			return heightCalculator.Height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			colMain.Reset(position);

			EditorGUI.LabelField(colMain.GetRect(EditorGUIUtility.singleLineHeight, 1), "Dialog Data", EditorStyles.boldLabel);

			ReorderableListGUI.Title(colMain.GetLineRect(1, 0), "Steps");
			colMain.Offset(-1);

			var propPages = property.FindPropertyRelative("pages");
			ReorderableListGUI.ListFieldAbsolute(colMain.GetRect(ReorderableListGUI.CalculateListFieldHeight(propPages), 1), propPages);
		}
	}
}