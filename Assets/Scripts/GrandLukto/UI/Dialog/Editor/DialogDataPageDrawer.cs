﻿using UnityCave;
using UnityCave.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[CustomPropertyDrawer(typeof(DialogDataPage))]
	public class DialogDataPageDrawer : PropertyDrawer
	{
		private EditorHeightCalculator heightCalculator = new EditorHeightCalculator();
		private EditorColumn colMain = new EditorColumn();

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			heightCalculator.Reset();

			heightCalculator.AddLine();
			heightCalculator.AddProperty(property.FindPropertyRelative("text"));

			return heightCalculator.Height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			colMain.Reset(position);

			var avatarRect = colMain.GetLineRect();
			EditorGUI.PropertyField(avatarRect.DivideX(0, 0.5f), property.FindPropertyRelative("avatarCharacter"), GUIContent.none);
			EditorGUI.PropertyField(avatarRect.DivideX(0.5f), property.FindPropertyRelative("avatarPosition"), GUIContent.none);

			colMain.DrawPropertyField(property.FindPropertyRelative("text"));
		}
	}
}