﻿using DG.Tweening;
using Rewired;
using System;
using System.Collections;
using System.Linq;
using UnityCave;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	public class UIDialog : MonoBehaviour
	{
		/// <summary>
		/// Thrown before the dialog is shown. The event contains a reference to the first page.
		/// Can be used e.g. to set initial position of visuals.
		/// </summary>
		public event EventHandler<DialogShowPageEventArgs> Initialize;
		public event EventHandler<DialogShowPageEventArgs> ShowPage;

		public float textAnimationDelay = 0.1f;

		public tk2dTextMesh text;
		public GameObject visualsContainer;

		public DialogDataWrapper dialogData;

		private int pageIndex;

		private Coroutine activeAnimation;

		private UIGroup compUIGroup;

		private Action onDialogFinished;

		private void Awake()
		{
			var cameraAnchor = GetComponent<tk2dCameraAnchor>();
			cameraAnchor.AnchorCamera = GameManagerBase.Instance.uiCamera;
			cameraAnchor.ForceUpdateTransform();

			compUIGroup = GetComponent<UIGroup>();

			gameObject.SetActive(false);
		}

		private void OnEnable()
		{
			compUIGroup.EnterPressed.AddListener(OnEnterPressed);
		}

		private void OnDisable()
		{
			compUIGroup.EnterPressed.RemoveListener(OnEnterPressed);
		}

		private void OnEnterPressed()
		{
			if (activeAnimation != null)
			{
				// animation is running -> end it and display full text
				EndAnimation();
				return;
			}

			ShowNextPage();
		}

		[ContextMenu("Start conversation")]
		private void StartConversationFromEditor()
		{
			if (dialogData == null)
			{
				Debug.LogError("No dialogData set.");
				return;
			}

			StartConversation(dialogData);
		}

		public void StartConversation(DialogDataWrapper data, Action onDialogFinished = null)
		{
			if (!data.dialogData.pages.HasItems())
			{
				Debug.LogError("Can not show empty dialog!");
				return;
			}

			this.onDialogFinished = onDialogFinished;

			gameObject.SetActive(true);
			OnInitialize(data.dialogData.pages[0]);

			TimeManager.Instance.TogglePause(true);

			dialogData = data;

			// reset
			pageIndex = 0;
			text.text = "";

			AnimateShow();
		}

		private void AnimateShow()
		{
			visualsContainer.transform.position = visualsContainer.transform.position.Clone(y: -8);

			DOTween.Sequence()
				.Append(visualsContainer.transform
					.DOLocalMoveY(0, 1f)
					.SetEase(Ease.OutQuint))
				.SetUpdate(true)
				.OnComplete(OnAnimateShowComplete);
		}

		private void OnAnimateShowComplete()
		{
			ShowNextPage();
			compUIGroup.AssignToPlayer(ReInput.players.AllPlayers.ToList());
		}

		private void AnimateHide()
		{
			DOTween.Sequence()
				.Append(visualsContainer.transform
					.DOLocalMoveY(-8, 0.5f)
					.SetEase(Ease.InBack))
				.SetUpdate(true)
				.OnComplete(OnAnimateHideComplete);
		}

		private void OnAnimateHideComplete()
		{
			gameObject.SetActive(false);
			TimeManager.Instance.TogglePause(false);

			if (onDialogFinished != null) onDialogFinished();
		}

		private void ShowNextPage()
		{
			if (pageIndex >= dialogData.dialogData.pages.Count)
			{
				OnEndDialog();
				return;
			}

			text.text = "";

			var nextPage = dialogData.dialogData.pages[pageIndex];
			OnShowPage(nextPage);

			activeAnimation = StartCoroutine(AnimateShowText(nextPage.text));
		}

		private IEnumerator AnimateShowText(string textToShow)
		{
			var showChars = 0;

			while (showChars < textToShow.Length)
			{
				showChars++;
				text.text = textToShow.Substring(0, showChars);
				yield return new WaitForSecondsRealtime(textAnimationDelay);
			}

			EndAnimation();
		}

		private void EndAnimation()
		{
			if (activeAnimation == null) return;

			text.text = dialogData.dialogData.pages[pageIndex].text;

			StopCoroutine(activeAnimation);
			activeAnimation = null;

			pageIndex++;
		}

		private void OnEndDialog()
		{
			compUIGroup.Unassign();

			AnimateHide();
		}

		private void OnShowPage(DialogDataPage page)
		{
			if (ShowPage != null) ShowPage(this, new DialogShowPageEventArgs(page));
		}

		private void OnInitialize(DialogDataPage page)
		{
			if (Initialize != null) Initialize(this, new DialogShowPageEventArgs(page));
		}
	}
}