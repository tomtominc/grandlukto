﻿using System;

namespace GrandLukto.UI.Dialog
{
	public class DialogShowPageEventArgs : EventArgs
	{
		public DialogDataPage Page { get; private set; }

		public DialogShowPageEventArgs(DialogDataPage page)
		{
			Page = page;
		}
	}
}