﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	public class DialogAvatarAnimation : MonoBehaviour
	{
		private tk2dSprite compSprite;
		private UIDialog targetDialog;

		private bool isShown;
		private DialogAvatarData curAvatar;

		public DialogAvatarPosition showForAvatarPosition;

		private void Awake()
		{
			compSprite = GetComponent<tk2dSprite>();
			compSprite.color = Color.white.MakeTransparent();

			targetDialog = GetComponentInParent<UIDialog>();
			targetDialog.ShowPage += Dialog_ShowPage;
			targetDialog.Initialize += Dialog_Initialize;
		}

		private void Dialog_Initialize(object sender, DialogShowPageEventArgs e)
		{
			var page = e.Page;

			if (!GetShowAvatar(page))
			{
				compSprite.color = Color.white.MakeTransparent();
				isShown = false;
				return;
			}

			DialogAvatarData avatarData;
			if (!TryGetAvatarData(out avatarData, page)) return;

			UpdateAvatar(avatarData);
			compSprite.color = Color.white;
			isShown = true;
		}

		private void Dialog_ShowPage(object sender, DialogShowPageEventArgs e)
		{
			var page = e.Page;

			if (!GetShowAvatar(page))
			{
				// fade out
				compSprite
					.DOColor(Color.white.MakeTransparent(), 0.6f)
					.SetEase(Ease.OutExpo)
					.SetUpdate(true);

				isShown = false;
				return;
			}

			DialogAvatarData avatarData;
			if (!TryGetAvatarData(out avatarData, page))
			{
				return;
			}

			if (isShown)
			{
				if (avatarData == curAvatar) return; // avatar was already shown on this side

				// avatar data changed -> fade out and in
				DOTween.Sequence()
					.Append(compSprite
						.DOColor(Color.white.MakeTransparent(), 0.2f)
						.SetEase(Ease.InExpo))
					.AppendCallback(() => UpdateAvatar(avatarData)) // change avatar
					.Append(compSprite
						.DOColor(Color.white, 0.2f)
						.SetEase(Ease.OutExpo))
					.SetUpdate(true);
				return;
			}

			// avatar was not shown
			UpdateAvatar(avatarData);
			compSprite
				.DOColor(Color.white, 0.6f)
				.SetEase(Ease.OutExpo)
				.SetUpdate(true);
			isShown = true;
		}

		private bool TryGetAvatarData(out DialogAvatarData avatarData, DialogDataPage page)
		{
			avatarData = null;

			if (page.avatarCharacter == null)
			{
				Debug.LogError("No avatar for this dialog page set!");
				return false;
			}

			// change avatar and show
			avatarData = page.avatarCharacter.GetDialogAvatar(page.avatarPosition);
			if (string.IsNullOrEmpty(avatarData.spriteName))
			{
				Debug.LogErrorFormat("Avatar spriteName for character {0} and position {1} not set.", page.avatarCharacter.name, page.avatarPosition);
				avatarData = null;
				return false;
			}

			return true;
		}

		private void UpdateAvatar(DialogAvatarData avatarData)
		{
			curAvatar = avatarData;

			compSprite.SetSprite(avatarData.spriteName);
			compSprite.transform.localPosition = avatarData.offset;
		}

		private bool GetShowAvatar(DialogDataPage page)
		{
			return page.avatarPosition == showForAvatarPosition;
		}

		private void OnDestroy()
		{
			targetDialog.ShowPage -= Dialog_ShowPage;
			targetDialog.Initialize -= Dialog_Initialize;
		}
	}
}