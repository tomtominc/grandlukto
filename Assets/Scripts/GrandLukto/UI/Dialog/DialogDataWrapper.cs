﻿using UnityEngine;

namespace GrandLukto.UI.Dialog
{
	[CreateAssetMenu(menuName = "Grand Lukto/Dialog Data")]
	public class DialogDataWrapper : ScriptableObject
	{
		public DialogData dialogData;
	}
}
