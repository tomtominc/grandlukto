﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.UI
{
	[CustomEditor(typeof(Countdown))]
	public class CountdownEditor : EditorBase<Countdown>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (Application.isPlaying) return;
			Target.EditorUpdateText(); // set text to first available
		}
	}
}