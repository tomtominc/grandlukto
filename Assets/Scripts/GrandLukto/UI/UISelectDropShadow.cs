﻿using DG.Tweening;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI
{
	public class UISelectDropShadow : MonoBehaviour
	{
		private UIElement targetUIElement;

		private DropShadow compDropShadow;

		private Sequence curTween;
		private Vector3 targetShadowOffset;
		private Color targetShadowColor;

		private bool curSelectedState = false;

		public void Awake()
		{
			compDropShadow = GetComponent<DropShadow>();

			targetShadowOffset = compDropShadow.shadowOffset;
			targetShadowColor = compDropShadow.shadowColor;
		}

		public void OnEnable()
		{
			compDropShadow.enabled = false;

			targetUIElement = GetComponentInParent<UIElement>();
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected += OnToggleSelected;
		}

		public void OnDisable()
		{
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected -= OnToggleSelected;

			if (curTween != null) curTween.Complete();
		}

		private void OnToggleSelected(UIElement sender, bool isSelected)
		{
			curSelectedState = isSelected;

			if (curTween != null) curTween.Complete();

			if(curSelectedState)
			{
				compDropShadow.enabled = true;
				compDropShadow.shadowOffset = Vector3.zero;
				compDropShadow.shadowColor = targetShadowColor * new Color(1, 1, 1, 0);
				compDropShadow.UpdateValues();

				curTween = DOTween.Sequence()
					.Append(DOTween
						.To(() => compDropShadow.shadowOffset, value => compDropShadow.shadowOffset = value, targetShadowOffset, 0.15f)
						.SetEase(Ease.OutCubic))
					.Join(DOTween
						.To(() => compDropShadow.shadowColor, value => compDropShadow.shadowColor = value, targetShadowColor, 0.15f)
						.SetEase(Ease.Linear));
				return;
			}

			curTween = DOTween.Sequence()
				.Append(DOTween
					.To(() => compDropShadow.shadowOffset, value => compDropShadow.shadowOffset = value, Vector3.zero, 0.15f)
					.SetEase(Ease.InCubic))
				.Join(DOTween
					.To(() => compDropShadow.shadowColor, value => compDropShadow.shadowColor = value, targetShadowColor * new Color(1, 1, 1, 0), 0.15f)
					.SetEase(Ease.Linear))
				.OnComplete(() => compDropShadow.enabled = false);
		}
	}
}
