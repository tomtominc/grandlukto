﻿using GrandLukto.UI.CharacterSelection;
using UnityCave.UI;

namespace GrandLukto.UI
{
	public class ScreenCharacterSelectionNew : UIScreenBase
	{
		public CharacterSelectionSlotManagerNew slotManager;

		public void Reset()
		{
			slotManager.Reset();
		}
	}
}
