﻿using GrandLukto.Characters;
using GrandLukto.Database;
using Spine.Unity;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI
{
	public class RosterMenuCharacterButton : MonoBehaviour
	{
		public tk2dSprite spriteBackground;
		public tk2dTextMesh textDescription;
		public SkeletonAnimation animCharacter;
		public GameObject selectedDot;

		private UIElement compUIElement;

		public DataCharacter Data { get; private set; }
		
		public void Awake()
		{
			SetSelected(false);

			compUIElement = GetComponent<UIElement>();
		}

		public void Setup(int index, DataCharacter data)
		{
			Data = data;

			spriteBackground.SetSprite("bg-" + data.characterName.ToLower());
			textDescription.text = data.descriptionRoster;
			data.ApplyAnimationToGameObject(animCharacter, FaceDirection.Down);

			compUIElement.selectionY = index;
		}

		public void SetSelected(bool isSelected)
		{
			selectedDot.SetActive(isSelected);
		}
	}
}