﻿using GrandLukto.Database;
using System.Collections.Generic;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI
{
	// todo: reduce to one: ArenaSelector, TeamSelector, LevelSelector
	public class LevelSelector : MonoBehaviour
	{
		public tk2dTextMesh label;

		public DataLevel SelectedValue
		{
			get
			{
				return possibleValues[selectedIndex];
			}
		}

		private List<DataLevel> possibleValues;
		private int selectedIndex = 0;

		public void Start()
		{
			possibleValues = GetValues();
			UpdateSelectedValue();
		}

		private List<DataLevel> GetValues()
		{
			return DatabaseManager.Instance.dbLevel.GetAll();
		}

		public void ChangeSelectedValue(int change)
		{
			selectedIndex = MathUtil.CycleThrough(selectedIndex, change, 0, possibleValues.Count);
			UpdateSelectedValue();
		}

		private void UpdateSelectedValue()
		{
			label.text = possibleValues[selectedIndex].levelName;
		}
	}
}