﻿using DG.Tweening;
using GrandLukto.Database;
using System.Linq;
using UnityCave;
using UnityCave.ExtensionMethods;
using UnityCave.Helpers;
using UnityEngine;
using System;
using Rewired;
using System.Collections.Generic;
using UnityCave.UI;

namespace GrandLukto.UI
{
	public class RosterMenuCharacters : MonoBehaviour
	{
		public Vector3 offsetInitial;
		public Vector3 offsetBetweenItems;

		public float minOffsetTop = 0.2019515f;
		public float minOffsetBottom = 2f;

		public float scrollDuration = 0.2f;

		public GameObject rosterTop;
		public GameObject rosterBottom;

		public GameObject scrollArrowTop;
		public GameObject scrollArrowBottom;

		private GameObject characterContainer;
		private List<RosterMenuCharacterButton> buttons;

		private RememberStartPosition rememberStartPos;

		private float charactersHeight;
		private float totalHeight;

		private int visibleItems;

		private UIGroup compUIGroup;

		private Tween activeScrollTween;
		private bool isHidden = true;

		private int curScrollIndex = 0;

		private Player ControllingPlayer
		{
			get
			{
				return ReInput.players.AllPlayers[1];
			}
		}

		private float TopPos
		{
			get
			{
				return GameManagerBase.Instance.uiCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y + minOffsetTop;
			}
		}

		private float BottomPos
		{
			get
			{
				return GameManagerBase.Instance.uiCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + minOffsetBottom;
			}
		}

		public float MenuShownY
		{
			get
			{
				return rememberStartPos.StartPosition.y - totalHeight - minOffsetTop;
			}
		}

		public void Awake()
		{
			buttons = new List<RosterMenuCharacterButton>();

			compUIGroup = GetComponent<UIGroup>();
			rememberStartPos = GetComponent<RememberStartPosition>();
		}

		public void OnEnable()
		{
			if (characterContainer == null)
			{
				characterContainer = new GameObject("Characters");
				characterContainer.transform.SetParent(transform);
				characterContainer.transform.localPosition = offsetInitial;

				CreateCharacterButtons(); // create all and cache them, we will disable not required ones

				characterContainer.SetActive(false);
			}

			ScreenManager.Instance.ScreenExtendsChange += OnResolutionChanged;
			compUIGroup.SelectedChildChanged += OnSelectedChildChanged;

			Reposition();
		}

		private void OnSelectedChildChanged(object sender, SelectedChildChangedArgs e)
		{
			var selectedButton = e.Selected.GetComponent<RosterMenuCharacterButton>();
			OverworldManager.Instance.Player.LoadCharacter(selectedButton.Data);

			ScrollToSelectedElement();
		}

		public void OnDisable()
		{
			compUIGroup.SelectedChildChanged -= OnSelectedChildChanged;
			ScreenManager.Instance.ScreenExtendsChange -= OnResolutionChanged;
		}

		private void OnResolutionChanged(object sender, ScreenExtendsChangeArgs e)
		{
			Reposition();
		}

		public void Reposition()
		{
			var topCharacterPos = TopPos - offsetBetweenItems.y;
			var bottomCharacterPos = BottomPos + offsetInitial.y;

			var availableHeight = topCharacterPos - bottomCharacterPos;
			characterContainer.transform.localPosition = offsetInitial; // TODO: Offset so we can see selected

			visibleItems = Mathf.CeilToInt(availableHeight / offsetBetweenItems.y); // round up since we only measure offset
			visibleItems = Math.Min(buttons.Count, visibleItems);

			if (visibleItems >= buttons.Count)
			{
				scrollArrowTop.SetActive(false);
				scrollArrowBottom.SetActive(false);
			}

			charactersHeight = (visibleItems - 1) * offsetBetweenItems.y;

			totalHeight = offsetInitial.y + charactersHeight + offsetBetweenItems.y;
			rosterTop.transform.localPosition = rosterTop.transform.localPosition.Clone(y: totalHeight);

			// move menu to target position and target scroll instant
			if (!isHidden)
			{
				transform.position = transform.position.Clone(y: MenuShownY);
			}

			ScrollToSelectedElement(true);
		}

		private void CreateCharacterButtons()
		{
			var allCharacters = DatabaseManager.Instance.dbCharacter.GetPlayable().ToList();
			var curPos = Vector3.zero;

			for (var i = 0; i < allCharacters.Count; i++)
			{
				var curCharacter = allCharacters[i];

				CreateCharacterButton(i, curPos, curCharacter);
				curPos += offsetBetweenItems;
			}
		}

		private void CreateCharacterButton(int index, Vector3 position, DataCharacter character)
		{
			var newObject = Instantiate(Resources.Load<GameObject>("UI/RosterCharacter"), characterContainer.transform);
			newObject.transform.localPosition = position;

			var button = newObject.GetComponent<RosterMenuCharacterButton>();
			button.Setup(index, character);

			buttons.Add(button);
		}
		
		public void Show(string selectCharName)
		{
			isHidden = false;
			characterContainer.SetActive(true);

			SetSelectedCharacterByName(selectCharName);
			ScrollToSelectedElement(true);

			DOTween.Sequence()
				.Append(transform
					.DOMoveY(MenuShownY, 0.4f)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					compUIGroup.AssignToPlayer(ControllingPlayer);
				});
		}

		public Tween Hide()
		{
			isHidden = true;

			compUIGroup.Unassign();

			return DOTween.Sequence()
				.Append(transform
					.DOMoveY(rememberStartPos.StartPosition.y, 0.4f)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					characterContainer.SetActive(false);
				});
		}

		public DataCharacter GetSelectedCharacter()
		{
			var selectedButton = compUIGroup.SelectedChild.GetComponent<RosterMenuCharacterButton>();
			if (selectedButton == null) return null;

			return selectedButton.Data;
		}

		public void SetCurrentButtonAsSelected()
		{
			var selectedButton = compUIGroup.SelectedChild.GetComponent<RosterMenuCharacterButton>();

			for (var i = 0; i < buttons.Count; i++)
			{
				var curButton = buttons[i];
				curButton.SetSelected(curButton == selectedButton);
			}
		}

		public void SetSelectedCharacterByName(string charName)
		{
			RosterMenuCharacterButton buttonToSelect = null;

			for (var i = 0; i < buttons.Count; i++)
			{
				var curButton = buttons[i];

				curButton.SetSelected(false);

				if (curButton.Data.characterName == charName)
				{
					buttonToSelect = curButton;
				}
			}

			if (buttonToSelect == null) return; // button not found

			buttonToSelect.SetSelected(true);
			compUIGroup.initialSelectedChild = buttonToSelect.GetComponent<UIElement>();
		}

		private void ScrollToSelectedElement(bool instant = false)
		{
			activeScrollTween.Kill(false);

			if (buttons.Count <= visibleItems)
			{
				characterContainer.transform.localPosition = offsetInitial;
				return;
			}

			var targetElement = compUIGroup.SelectedChild ?? compUIGroup.initialSelectedChild;
			if (targetElement == null)
			{
				return;
			}

			var selectedButton = targetElement.GetComponent<RosterMenuCharacterButton>();
			var selectedIndex = buttons.IndexOf(selectedButton);

			var maxIndex = buttons.Count - visibleItems;

			var targetIndex = selectedIndex;

			if (targetIndex <= curScrollIndex + 1)
			{
				targetIndex = targetIndex - 1;
			}
			else if (targetIndex >= curScrollIndex + visibleItems - 1)
			{
				targetIndex = targetIndex + 1;
			}
			else
			{
				return; // selected button is already in view
			}

			targetIndex = Mathf.Max(0, Mathf.Min(maxIndex, targetIndex));
			curScrollIndex = targetIndex;

			scrollArrowTop.SetActive(targetIndex < maxIndex);
			scrollArrowBottom.SetActive(targetIndex > 0);

			var targetPosition = offsetInitial - offsetBetweenItems * targetIndex;

			if (instant)
			{
				characterContainer.transform.localPosition = targetPosition;
				return;
			}
			
			activeScrollTween = characterContainer.transform.DOLocalMove(targetPosition, scrollDuration);
		}
	}
}