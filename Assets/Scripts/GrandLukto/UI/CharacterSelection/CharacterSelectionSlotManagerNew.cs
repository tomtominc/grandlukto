﻿using DG.Tweening;
using GrandLukto.Characters;
using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;
using System;
using GrandLukto.Database;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSlotManagerNew : MonoBehaviour
	{
		public Vector2[] slotPositions;

		public DataSoundSetting soundIn;
		public DataSoundSetting soundOut;

		private List<CharacterSelectionSlotNew> slots = new List<CharacterSelectionSlotNew>();

		private bool slotsHidden = false;

		private UIScreenBase compUIScreen;

		public int SlotsTotal { get { return slots.Count; } }
		public int PlayerSlotsOccupied { get { return slots.Where(slot => slot.IsAssigned).Count(); } }
		public int PlayerSlotsReady { get { return slots.Where(slot => slot.IsAssigned && slot.IsReady).Count(); } }

		public void Awake()
		{
			compUIScreen = GetComponentInParent<UIScreenBase>();
			compUIScreen.Show += OnShowScreen;

			if(slots == null)
			{
				CreateSlots();
			}
		}
		
		private void RemoveAllSlots()
		{
			while(slots.Count > 0)
			{
				var slot = slots[0];
				Destroy(slot.gameObject);
				slots.Remove(slot);
			}
		}

		private void CreateSlots()
		{
			slots = new List<CharacterSelectionSlotNew>();

			for(var i = 0; i < slotPositions.Length; i++)
			{
				slots.Add(CreateSlot(i));
			}
		}

		public void RecreateSlot(int slotID)
		{
			var oldSlot = slots[slotID];
			Destroy(oldSlot.gameObject);

			var newSlot = CreateSlot(slotID);
			slots[slotID] = newSlot;
			newSlot.TweenToNextState(); // init slot
		}

		private CharacterSelectionSlotNew CreateSlot(int slotNum)
		{
			var go = Instantiate(Resources.Load<GameObject>("CharacterSelection/Slot"), transform);

			var slot = go.GetComponent<CharacterSelectionSlotNew>();
			slot.transform.localPosition = slotPositions[slotNum];
			slot.slotID = slotNum;
			slot.gameObject.name = "Slot " + slotNum;

			return slot;
		}

		public void Update()
		{
			if (slotsHidden) return; // all players are unassigned and the hide animation is playing

			foreach (var player in ReInput.players.GetPlayers())
			{
				if (!player.GetAnyButtonDown()) continue;
				if (IsPlayerAssigned(player)) continue;

				if (player.GetButtonDown(UIManager.Instance.config.buttonBackNames))
				{
					HideScreen();
					return;
				}

				var slot = FindFirstUnassignedSlot();
				if (slot == null) return; // no unassigned slot found -> no need to check other players

				if (!IsAnySlotAssigned())
				{
					// make first joining player the lead player
					UIManager.Instance.ChangeLeadPlayer(player);
				}

				slot.AssignTo(player);
			}
		}

		private bool IsPlayerAssigned(Player player)
		{
			foreach(var slot in slots)
			{
				if (slot.Player == player) return true;
			}

			return false;
		}

		private bool IsAnySlotAssigned()
		{
			return slots.Find(slot => slot.IsAssigned);
		}

		private CharacterSelectionSlotNew FindFirstUnassignedSlot()
		{
			for(var i = 0; i < slots.Count; i++)
			{
				var slot = slots[i];
				if (slot.IsAssigned) continue;
				return slot;
			}

			return null;
		}

		private List<PlayerSetting> GetPlayerSettings()
		{
			var playerSettings = new List<PlayerSetting>();

			foreach(var slot in slots)
			{
				if (!slot.IsAssigned) continue;
				playerSettings.Add(slot.ToPlayerSetting());
			}

			return playerSettings;
		}

		public bool AllSlotsReady()
		{
			return !slots.Find(slot => !slot.IsReady);
		}

		private void HideScreen()
		{
			CreateHideSequence()
				.AppendCallback(() =>
				{
					compUIScreen.BackToLastScreenInStack();
				});
		}

		private Sequence CreateHideSequence()
		{
			slotsHidden = true;

			var sequence = DOTween.Sequence();

			foreach (var slot in slots)
			{
				sequence.Join(slot.UnassignHideAndReset());
			}

			return sequence;
		}

		public void OnShowScreen(object sender, EventArgs e)
		{
			UIManager.Instance.PlaySound(soundIn);

			if (slotsHidden)
			{
				UnhideSlots();
				return;
			}

			foreach(var slot in slots)
			{
				slot.TweenToNextState(); // slots are in initial state -> simply tween to next state
			}
		}

		private void UnhideSlots()
		{
			var sequence = DOTween.Sequence();

			foreach (var slot in slots)
			{
				sequence.Join(slot.Show()); // show slots in old state
			}

			sequence.AppendCallback(() =>
			{
				slotsHidden = false; // unhide to go through update again
			});
		}

		public void Reset()
		{
			RemoveAllSlots();
			CreateSlots();

			slotsHidden = false; // we will tween them to the next state on the next show
		}

		/// <summary>
		/// Hides all slots and transitions to the next screen.
		/// </summary>
		public void MoveToNextScreen()
		{
			UIManager.Instance.PlaySound(soundOut);

			var playerSettings = GetPlayerSettings();

			CreateHideSequence()
				.AppendCallback(() =>
				{
					var arenaSelection = compUIScreen.Manager.GetScreenByType<ScreenArenaSelection>();
					arenaSelection.Initialize(playerSettings);
					compUIScreen.Manager.ChangeScreen(arenaSelection);
				});
		}

		public CharacterSelectionSlotNew GetSlot(int slotID)
		{
			return slots[slotID];
		}

		public void OnPlayerReady(CharacterSelectionSlotNew sender)
		{
			// check if we have a current master player slot
			var masterSlot = slots.Find(slot => slot.IsMasterSlot);
			if (masterSlot != null) return; // we already have a master slot

			// make player lead player and set master slot
			UIManager.Instance.ChangeLeadPlayer(sender.Player);
			sender.TweenToNextState(); 
		}

		public void OnUnassignMasterPlayer(CharacterSelectionSlotNew sender)
		{
			// try to find a new slot that can be master player
			CharacterSelectionSlotNew newMaster = slots.Find(slot => slot != sender && slot.IsAssigned && !slot.NPC && slot.IsReady);
			if (newMaster == null) return; // no slot found that can be master

			// make player lead player and set master slot
			UIManager.Instance.ChangeLeadPlayer(newMaster.Player);
			newMaster.TweenToNextState();
		}
	}
}