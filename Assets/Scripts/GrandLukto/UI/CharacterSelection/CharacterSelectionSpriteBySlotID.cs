﻿using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSpriteBySlotID : MonoBehaviour
	{
		[Tooltip("Use [id] as placeholder.")]
		public string spriteName;

		public void Start()
		{
			var slot = GetComponentInParent<CharacterSelectionSlotNew>();

			var sprite = GetComponent<tk2dSprite>();
			sprite.SetSprite(spriteName.Replace("[id]", (slot.slotID + 1).ToString()));
		}
	}
}