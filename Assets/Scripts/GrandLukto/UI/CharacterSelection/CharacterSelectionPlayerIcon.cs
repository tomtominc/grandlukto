﻿using System.Collections;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionPlayerIcon : MonoBehaviour
	{
		public int times = 3;
		public float timeBlink = 0.1f;
		public float timeNormal = 0.3f;

		public tk2dSprite overlaySprite;
		public tk2dTextMesh textMesh;

		private Color initialOverlayColor;
		private Color textColor;

		private CharacterSelectionSlotNew compSlot;

		public void Awake()
		{
			compSlot = GetComponentInParent<CharacterSelectionSlotNew>();

			initialOverlayColor = overlaySprite.color;
			overlaySprite.color = ColorUtil.MakeTransparent(initialOverlayColor);
		}

		public void Start()
		{
			textColor = DatabaseManager.Instance.playerColors.colorsDark[compSlot.slotID];
		}

		public void OnEnable()
		{
			// update text depending on AI or Player
			textMesh.text = compSlot.NPC ? "AI" : "P" + (compSlot.slotID + 1);
		}

		public void Blink()
		{
			StartCoroutine(BlinkRoutine());
		}

		private IEnumerator BlinkRoutine()
		{
			for(var i = 0; i < times; i++)
			{
				overlaySprite.color = initialOverlayColor;
				textMesh.color = initialOverlayColor;
				yield return new WaitForSeconds(timeBlink);
				overlaySprite.color = ColorUtil.MakeTransparent(initialOverlayColor);
				textMesh.color = textColor;
				yield return new WaitForSeconds(timeNormal);
			}
		}
	}
}