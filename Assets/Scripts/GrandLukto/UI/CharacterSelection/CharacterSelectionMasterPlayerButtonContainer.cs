﻿using DG.Tweening;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionMasterPlayerButtonContainer : MonoBehaviour
	{
		public UIGroup CompUIGroup { get; private set; }

		private RememberStartPosition compRememberStartPosition;

		public void Awake()
		{
			compRememberStartPosition = GetComponent<RememberStartPosition>();
			CompUIGroup = GetComponent<UIGroup>();

			var slotManager = GetComponentInParent<CharacterSelectionSlotManagerNew>();
			var slot = GetComponentInParent<CharacterSelectionSlotNew>();

			var addAIButtons = GetComponentsInChildren<CharacterSelectionButtonAddAI>();
			var buttonCount = 0;

			for(var i = 0; i < slotManager.SlotsTotal; i++)
			{
				if (i == slot.slotID) continue; // this is the current slot
				if (buttonCount >= addAIButtons.Length)
				{
					Debug.LogError("Not enough buttons for all slots!");
					break;
				}

				addAIButtons[buttonCount].targetSlotID = i;
				buttonCount++;
			}

			gameObject.SetActive(false);
		}

		public Tween Show()
		{
			gameObject.SetActive(true);

			return DOTween.Sequence()
				.Append(transform
					.DOLocalMoveY(-0.4f, 0.4f)
					.SetEase(Ease.OutBack));
		}

		public Tween Hide()
		{
			return DOTween.Sequence()
				.Append(transform
					.DOLocalMoveY(compRememberStartPosition.StartPositionLocal.y, 0.4f)
					.SetEase(Ease.InBack))
					.AppendCallback(() =>
					{
						gameObject.SetActive(false);
					});
		}
	}
}