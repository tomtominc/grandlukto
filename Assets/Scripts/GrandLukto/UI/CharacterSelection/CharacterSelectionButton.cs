﻿using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	[RequireComponent(typeof(UIElement))]
	public class CharacterSelectionButton : MonoBehaviour
	{
		public Color colorInactive = new Color(0.7f, 0.7f, 0.7f, 1);

		private tk2dTextMesh textMesh;
		private tk2dSprite background;

		private Color colorActive = new Color(1, 1, 1, 1);
		public bool ButtonActive { get; private set; }

		public virtual void Awake()
		{
			ButtonActive = true; // default

			textMesh = GetComponentInChildren<tk2dTextMesh>();
			background = GetComponentInChildren<tk2dSprite>();
		}

		public void SetColorActive(Color c)
		{
			if (colorActive == c) return;

			colorActive = c;
			UpdateBackgroundColor();
		}

		public void SetActive(bool isActive)
		{
			if (this.ButtonActive == isActive) return;

			this.ButtonActive = isActive;
			UpdateBackgroundColor();
		}

		private void UpdateBackgroundColor()
		{
			background.color = ButtonActive ? colorActive : colorInactive;
		}

		public void SetText(string text)
		{
			textMesh.text = text;
		}
	}
}