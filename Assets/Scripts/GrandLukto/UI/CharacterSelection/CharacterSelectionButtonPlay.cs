﻿using System;
using UnityCave.UI;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionButtonPlay : CharacterSelectionButton
	{
		private CharacterSelectionSlotManagerNew compSlotManager;
		private CharacterSelectionSlotNew compSlot;

		override public void Awake()
		{
			base.Awake();

			compSlotManager = GetComponentInParent<CharacterSelectionSlotManagerNew>();

			compSlot = GetComponentInParent<CharacterSelectionSlotNew>();
			SetColorActive(DatabaseManager.Instance.playerColors.colors[compSlot.slotID]); // color in current player's color

			GetComponent<UIElement>().EnterPressed.AddListener(OnEnterPressed);
		}

		private void OnEnterPressed()
		{
			if (!ButtonActive) return;

			compSlot.StartGamePressed();
		}

		public void Update()
		{
			var slotsReady = compSlotManager.PlayerSlotsReady;
			var slotsOccupied = compSlotManager.PlayerSlotsOccupied;

			if(slotsReady == slotsOccupied)
			{
				// all occupied slots are ready
				SetActive(true);
				SetText("PLAY");
			}
			else
			{
				// waiting for players to ready up
				SetActive(false);
				SetText(compSlotManager.PlayerSlotsReady + "/" + compSlotManager.PlayerSlotsOccupied + " READY");
			}
		}
	}
}