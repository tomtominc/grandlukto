﻿using GrandLukto.Characters;
using GrandLukto.Database;
using Rewired;
using Spine.Unity;
using UnityCave.Spine;
using UnityCave.UI;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSlot : MonoBehaviour
	{
		public int slotNumber = 0;

		public tk2dTextMesh heading;
		public GameObject stateUnassigned;
		public GameObject stateAssigned;

		public Player Player { get; private set; }
		public bool NPC { get; private set; }

		private DatabaseCharacter dbCharacter;
		private int selectedCharacterIndex = 0;
		private DataCharacter selectedCharacterData = null;

		public bool IsAssigned
		{
			get
			{
				return Player != null || NPC;
			}
		}
		
		public void Start()
		{
			dbCharacter = DatabaseManager.Instance.dbCharacter;

			heading.text = "P" + (slotNumber + 1);
			heading.color = DatabaseManager.Instance.playerColors.colors[slotNumber];

			UpdateVisuals();
		}		

		public void AssignToNPC()
		{
			if (IsAssigned)
			{
				Debug.LogError("Slot is already assigned to a Player or NPC.");
				return;
			}

			NPC = true;

			UpdateVisuals();
			UpdateSelectedCharacter();
		}

		public void AssignTo(Player player)
		{
			if(IsAssigned)
			{
				Debug.LogError("Slot is already assigned to a Player or NPC.");
				return;
			}

			Player = player;

			UpdateVisuals();
			UpdateSelectedCharacter();

			GetComponent<UIGroup>().AssignToPlayer(player);
		}

		private void UpdateVisuals()
		{
			stateUnassigned.SetActive(!IsAssigned);
			stateAssigned.SetActive(IsAssigned);
		}

		private void UpdateSelectedCharacter()
		{
			selectedCharacterData = dbCharacter.Get(selectedCharacterIndex);
			selectedCharacterData.ApplyAnimationToGameObject(stateAssigned, FaceDirection.Down);
		}

		public void Update()
		{
			if (!IsAssigned) return;
		}

		public void ChangeCharacterSelection(int change)
		{
			selectedCharacterIndex = MathUtil.CycleThrough(selectedCharacterIndex, change, 0, dbCharacter.Count);
			UpdateSelectedCharacter();
		}

		public PlayerSetting ToPlayerSetting()
		{
			var setting = new PlayerSetting();
			setting.isHuman = !NPC;
			setting.playerId = slotNumber;
			setting.selectedCharacter = selectedCharacterData;
			setting.controller = NPC ? null : Player;
			setting.team = GetComponentInChildren<TeamSelector>().SelectedTeam;
			return setting;
		}
	}
}