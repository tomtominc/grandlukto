﻿using DG.Tweening;
using Spine.Unity;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSelectSkinSlotOver : MonoBehaviour
	{
		[SerializeField]
		private tk2dSprite[] sprites;

		[SerializeField]
		private SkeletonAnimation spineAnim;

		private UIElement targetUIElement;
		private Color playerColor;

		public void Awake()
		{
			var slot = GetComponentInParent<CharacterSelectionSlotNew>();
			playerColor = DatabaseManager.Instance.playerColors.colors[slot.slotID];
		}

		public void OnEnable()
		{
			targetUIElement = GetComponentInParent<UIElement>();
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected += OnToggleSelected;
			OnToggleSelected(null, targetUIElement.IsSelected);
		}

		public void OnDisable()
		{
			if (targetUIElement == null) return;

			targetUIElement.EventToggleSelected -= OnToggleSelected;
		}

		private void OnToggleSelected(UIElement sender, bool isSelected)
		{
			var targetColor = isSelected ? playerColor : Color.white;
			var tween = DOTween.Sequence();

			if(spineAnim.AnimationState != null)
			{
				spineAnim.AnimationState.TimeScale = isSelected ? 1 : 0; // can be null if animation is not created yet
			}

			for (var i = 0; i < sprites.Length; i++)
			{
				var curSprite = sprites[i];

				tween.Join(curSprite
					.DOColor(targetColor, 0.2f)
					.SetEase(Ease.OutExpo));
			}
		}
	}
}