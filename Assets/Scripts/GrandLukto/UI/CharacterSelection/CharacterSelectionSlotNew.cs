﻿using System;
using DG.Tweening;
using GrandLukto.Characters;
using Rewired;
using UnityCave.ExtensionMethods;
using UnityEngine;
using UnityCave.Helpers;
using UnityCave.UI;
using GrandLukto.Database;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSlotNew : MonoBehaviour
	{
		private enum State
		{
			None,
			PressToJoin,
			SelectCharacter,
			SelectTeam,
			Ready,
			MasterPlayerButtons,
			AddAI
		}

		public int slotID = 0;

		public Player Player { get; private set; }
		public bool NPC { get; private set; }

		/// <summary>
		/// If the player adds an NPC we store his main slot to assign him back.
		/// </summary>
		private CharacterSelectionSlotNew playerMainSlot;

		private State lastState = State.None;
		private State curState = State.None;

		public GameObject goBackground;
		private CharacterSelectionBackground goBackgroundBackground;

		public GameObject goPressToJoin;
		private tk2dSprite goPressToJoinSprite;

		public GameObject goPlayerIcon;
		private RememberStartPosition goPlayerIconRememberPos;

		public GameObject goCharacterSelect;
		private CharacterSelectionSelectSkin goCharacterSelectSkin;
		private UIGroup goCharacterSelectUIGroup;

		public GameObject goTeamSelect;
		private CharacterSelectionSelectTeam goTeamSelectTeam;
		private UIGroup goTeamSelectUIGroup;

		public GameObject goCharacterContainer;
		public GameObject goCharacterOffsetContainer;

		public GameObject goSelectedCharacter;
		private tk2dSprite goSelectedCharacterSprite;

		public GameObject goSelectedCharacterShadow;
		private tk2dSprite goSelectedCharacterShadowSprite;

		public CharacterSelectionMasterPlayerButtonContainer goMasterPlayerButtons;

		[Header("Sounds")]
		public DataSoundSetting[] soundJoinNext;
		public DataSoundSetting soundCharacterSelectNext;
		public DataSoundSetting soundTeamSelectNext;

		private RememberStartPosition compRememberStartPos;

		private bool isTransitionActive = false;

		private DataCharacter selectedCharacter;

		public bool IsAssigned
		{
			get
			{
				return Player != null || NPC;
			}
		}

		/// <summary>
		/// Returns true if the slot is not assigned or if the slot's state is ready.
		/// </summary>
		public bool IsReady
		{
			get
			{
				return (int)curState >= (int)State.Ready || !IsAssigned;
			}
		}

		public bool IsMasterSlot
		{
			get
			{
				return (int)curState >= (int)State.MasterPlayerButtons;
			}
		}

		public void Awake()
		{
			goBackgroundBackground = goBackground.GetComponent<CharacterSelectionBackground>();

			goPressToJoinSprite = goPressToJoin.GetComponent<tk2dSprite>();
			goPlayerIconRememberPos = goPlayerIcon.GetComponent<RememberStartPosition>();

			goCharacterSelectSkin = goCharacterSelect.GetComponent<CharacterSelectionSelectSkin>();
			goCharacterSelectUIGroup = goCharacterSelectSkin.GetComponent<UIGroup>();

			goTeamSelectUIGroup = goTeamSelect.GetComponent<UIGroup>();
			goTeamSelectTeam = goTeamSelect.GetComponent<CharacterSelectionSelectTeam>();
			goTeamSelect.SetActive(false);

			goSelectedCharacterSprite = goSelectedCharacter.GetComponent<tk2dSprite>();
			goSelectedCharacter.SetActive(false);

			goSelectedCharacterShadowSprite = goSelectedCharacterShadow.GetComponent<tk2dSprite>();
			goSelectedCharacterShadow.SetActive(false);

			compRememberStartPos = GetComponent<RememberStartPosition>();
		}

		public void Start()
		{
			goPlayerIcon.transform.localPosition = goPlayerIcon.transform.localPosition.Clone(y: 2);
			goPlayerIcon.SetActive(false);
		}
		
		public void TweenToNextState()
		{
			TweenToState(curState + 1);
		}

		public void TweenToLastState()
		{
			TweenToState(curState - 1);
		}

		private void TweenToState(State nextState)
		{
			if (isTransitionActive)
			{
				Debug.Log("Transition already active!");
				return;
			}

			isTransitionActive = true;

			UnassignListeners(curState);

			if (nextState > curState) TweenForward(nextState);
			else TweenBackward(curState);

			lastState = curState;
			curState = nextState;
		}

		private void UnassignListeners(State state)
		{
			switch (curState)
			{
				case State.SelectCharacter:
					goCharacterSelectUIGroup.Unassign();
					goCharacterSelectUIGroup.EnterPressed.RemoveListener(OnCharacterSelected);
					break;

				case State.SelectTeam:
					goTeamSelectUIGroup.Unassign();
					goTeamSelectUIGroup.EnterPressed.RemoveListener(OnTeamSelected);
					break;

				case State.Ready:
					goBackgroundBackground.SelectedTeamUIGroup.Unassign();
					break;

				case State.MasterPlayerButtons:
					goMasterPlayerButtons.CompUIGroup.ToggleSelected(false);
					goMasterPlayerButtons.CompUIGroup.Unassign();
					break;
			}
		}

		private void AssignListeners(State state)
		{
			switch (state)
			{
				case State.PressToJoin:
					Unassign();
					break;

				case State.SelectCharacter:
					goCharacterSelectUIGroup.AssignToPlayer(Player);
					goCharacterSelectUIGroup.EnterPressed.AddListener(OnCharacterSelected);
					break;

				case State.SelectTeam:
					goTeamSelectUIGroup.AssignToPlayer(Player);
					goTeamSelectUIGroup.EnterPressed.AddListener(OnTeamSelected);
					break;

				case State.Ready:
					if(lastState == State.MasterPlayerButtons)
					{
						goMasterPlayerButtons.CompUIGroup.ForgetSelection(); // forget selection because it is not a real setting for the player

						GetComponentInParent<CharacterSelectionSlotManagerNew>().OnUnassignMasterPlayer(this);
						TweenToLastState(); // player pressed B in MasterButton state -> go back to team selection
						break;
					}

					if (NPC)
					{
						// Player added an npc -> let him control his old slot again
						playerMainSlot.OnNPCSlotActionComplete();

						Player = null;
						playerMainSlot = null;
						break;
					}
					
					goBackgroundBackground.SelectedTeamUIGroup.AssignToPlayer(Player); // allow other players to press back
					GetComponentInParent<CharacterSelectionSlotManagerNew>().OnPlayerReady(this);
					break;

				case State.MasterPlayerButtons:
					goMasterPlayerButtons.CompUIGroup.AssignToPlayer(Player);
					break;
			}
		}

		private void EndTransition()
		{
			isTransitionActive = false;

			AssignListeners(curState);
		}

		private void TweenBackward(State fromState)
		{
			switch(fromState)
			{
				case State.SelectCharacter:
					// fade out character selection
					DOTween.Sequence()
						.Append(goCharacterSelectSkin.TweenOut())
						.AppendCallback(() =>
						{
							goCharacterSelect.SetActive(false);
						})
						.Append(goBackground.transform
							.DOLocalMoveY(-1.6f, 0.4f)
							.SetEase(Ease.OutExpo))
						.Join(goPlayerIcon.transform
							.DOLocalMoveY(2, 0.4f)
							.SetEase(Ease.OutExpo))
						.Append(goPressToJoinSprite
							.DOColor(new Color(1, 1, 1, 1), 0.2f))
						.AppendCallback(() =>
						{
							goPlayerIcon.SetActive(false);

							EndTransition();
						});
					break;

				case State.SelectTeam:
					DOTween.Sequence()
						.Append(goTeamSelectTeam.Hide())
						// character
						.Append(goSelectedCharacterShadowSprite
							.DOColor(new Color(1, 1, 1, 0), 0.2f))
						.Join(goSelectedCharacterSprite
							.DOColor(new Color(1, 1, 1, 0), 0.2f))
						.AppendCallback(() =>
						{
							goSelectedCharacter.SetActive(false);
							goSelectedCharacterShadow.SetActive(false);

							goCharacterSelect.SetActive(true);
						})
						// playericon
						.Append(goPlayerIcon.transform
							.DOLocalMoveY(goPlayerIconRememberPos.StartPositionLocal.y, 0.4f)
							.SetEase(Ease.OutSine))
						.Join(goPlayerIcon.transform
							.DOScale(1, 0.4f))
						// background
						.Join(goBackground.transform
							.DOLocalMoveY(-4.5f, 0.4f)
							.SetEase(Ease.OutExpo))
						.Append(goCharacterSelectSkin.TweenIn())
						.AppendCallback(() =>
						{
							EndTransition();
						});
					break;

				case State.Ready:
					DOTween.Sequence()
						.Append(goBackgroundBackground.HideSelectedTeam())
						.Join(goBackgroundBackground.TweenToScreenBottom(3.1f))
						.Append(goTeamSelectTeam.Show())
						.AppendCallback(EndTransition);
					break;

				case State.MasterPlayerButtons:
					DOTween.Sequence()
						.Append(goMasterPlayerButtons.Hide())
						.Join(goBackgroundBackground.TweenToScreenBottom(.7f))
						.Join(goCharacterContainer.transform
							.DOScale(0.7f, 0.4f)
							.SetEase(Ease.OutExpo))
						.Join(goCharacterContainer.transform
							.DOLocalMoveY(-6.2f, 0.4f)
							.SetEase(Ease.InOutSine))
						.Append(goBackgroundBackground.ShowBars())
						.AppendCallback(() =>
						{
							EndTransition();
						});
					break;
			}
		}

		private void TweenForward(State toState)
		{
			switch (toState)
			{
				case State.PressToJoin:
					goPressToJoinSprite.color = new Color(1, 1, 1, 0);

					DOTween.Sequence()
						.Append(goBackground.transform
							.DOLocalMoveY(-1.6f, 0.4f)
							.SetEase(Ease.OutExpo))
						.Append(goPressToJoinSprite
							.DOColor(new Color(1, 1, 1, 1), 0.2f))
						.SetDelay(slotID * 0.1f)
						.OnComplete(EndTransition);
					break;

				case State.SelectCharacter:
					UIManager.Instance.PlaySound(soundJoinNext);

					DOTween.Sequence()
						.Append(goPressToJoinSprite
							.DOColor(new Color(1, 1, 1, 0), 0.2f))
						.AppendCallback(() =>
						{
							goPlayerIcon.SetActive(true);
						})
						.Append(goBackground.transform
							.DOLocalMoveY(-4.5f, 0.4f)
							.SetEase(Ease.OutExpo))
						.Join(goPlayerIcon.transform
							.DOLocalMoveY(goPlayerIconRememberPos.StartPositionLocal.y, 0.4f)
							.SetEase(Ease.OutExpo))
						.AppendCallback(() =>
						{
							goPlayerIcon.GetComponentInChildren<CharacterSelectionPlayerIcon>().Blink();

							goCharacterSelect.SetActive(true);
						})
						.Append(goCharacterSelectSkin.TweenIn())
						.AppendCallback(() =>
						{
							EndTransition();
						});
					break;

				case State.SelectTeam:
					UIManager.Instance.PlaySound(soundCharacterSelectNext);

					DOTween.Sequence()
						.Append(goCharacterSelectSkin
							.TweenOut()
							.OnComplete(() =>
							{
								selectedCharacter = goCharacterSelectSkin.GetSelectedCharacter();
								goCharacterSelect.SetActive(false);

								// show selected character
								goSelectedCharacter.SetActive(true);
								goSelectedCharacterSprite.SetSprite(selectedCharacter.characterSelectionSpriteName);
								goSelectedCharacterSprite.color = new Color(1, 1, 1, 0);

								goSelectedCharacterShadow.SetActive(true);
								goSelectedCharacterShadow.transform.localPosition = ((Vector3)selectedCharacter.characterSelectionShadowOffset).Clone(z: goSelectedCharacterShadow.transform.localPosition.z);

								goSelectedCharacterShadowSprite.SetSprite(selectedCharacter.characterSelectionSpriteName + "_shadow");
								goSelectedCharacterShadowSprite.color = new Color(1, 1, 1, 0);

								goCharacterOffsetContainer.transform.localPosition = ((Vector3)selectedCharacter.characterSelectionOffset).Clone(z: goCharacterOffsetContainer.transform.localPosition.z);

								DOTween.Sequence()
									.Append(goSelectedCharacterShadowSprite
										.DOColor(new Color(1, 1, 1, 1), 0.2f))
									.Join(goSelectedCharacterSprite
										.DOColor(new Color(1, 1, 1, 1), 0.2f));
							}))
						.Append(goPlayerIcon.transform
							.DOLocalMoveY(-1.4f, 0.4f)
							.SetEase(Ease.OutSine))
						.Join(goPlayerIcon.transform
							.DOScale(0.65f, 0.4f))
						.Join(goBackgroundBackground.TweenToScreenBottom(3.1f))
						.Append(goTeamSelectTeam.Show())
						.AppendCallback(EndTransition);
					break;

				case State.Ready:
					UIManager.Instance.PlaySound(soundTeamSelectNext);

					DOTween.Sequence()
						.Append(goTeamSelectTeam.Hide())
						.Append(goBackgroundBackground.TweenToScreenBottom(.7f))
						.Join(goBackgroundBackground.ShowSelectedTeam(goTeamSelectTeam.SelectedTeam))
						.AppendCallback(EndTransition);

					break;

				case State.MasterPlayerButtons:
					DOTween.Sequence()
						.Append(goBackgroundBackground.HideBars())
						.Append(goCharacterContainer.transform
							.DOScale(0.5f, 0.4f)
							.SetEase(Ease.InOutSine))
						.Join(goCharacterContainer.transform
							.DOLocalMoveY(-5.2f, 0.4f)
							.SetEase(Ease.InOutSine))
						.Join(goBackgroundBackground
							.TweenToY(-8.2f))
						.Join(goMasterPlayerButtons
							.Show())
						.AppendCallback(() =>
						{
							EndTransition();
						});
					break;

				case State.AddAI:
					EndTransition(); // simply end the transition to remove listeners etc.
					break;
			}
		}

		public bool AssignToNPC(CharacterSelectionSlotNew fromSlot)
		{
			if (IsAssigned)
			{
				Debug.LogError("Slot is already assigned to a Player or NPC.");
				return false;
			}

			if (isTransitionActive || curState != State.PressToJoin) return false;

			playerMainSlot = fromSlot;
			Player = fromSlot.Player; // this player will set up the npc
			NPC = true;
			TweenToNextState();

			return true;
		}

		public void AssignTo(Player player)
		{
			if (IsAssigned)
			{
				Debug.LogError("Slot is already assigned to a Player or NPC.");
				return;
			}

			if (isTransitionActive || curState != State.PressToJoin) return;

			playerMainSlot = null;
			Player = player;
			NPC = false;
			TweenToNextState();
		}

		private void Unassign()
		{
			if (NPC)
			{
				// player was adding an npc
				playerMainSlot.OnNPCSlotActionComplete();
			}

			NPC = false;
			Player = null;
			playerMainSlot = null;

			// reset remembered selection
			goCharacterSelectUIGroup.ForgetSelection();
			goTeamSelectUIGroup.ForgetSelection();
			goMasterPlayerButtons.CompUIGroup.ForgetSelection();
		}

		public PlayerSetting ToPlayerSetting()
		{
			var setting = new PlayerSetting();
			setting.isHuman = !NPC;
			setting.playerId = slotID;
			setting.selectedCharacter = selectedCharacter;
			setting.controller = NPC ? null : Player;
			setting.team = goTeamSelectTeam.SelectedTeam;
			return setting;
		}

		private void OnCharacterSelected()
		{
			TweenToNextState();
		}

		private void OnTeamSelected()
		{
			TweenToNextState();
		}

		public Tween Show()
		{
			return DOTween.Sequence()
				.Append(transform
					.DOMoveY(compRememberStartPos.StartPosition.y, 0.6f)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					AssignListeners(curState);
					isTransitionActive = false;
				});
		}

		public Tween UnassignHideAndReset()
		{
			if (isTransitionActive) return null;

			isTransitionActive = true;
			UnassignListeners(curState); // remove all listeners etc.

			// move object out of screen
			return DOTween.Sequence()
				.Append(transform
					.DOMoveY(compRememberStartPos.StartPosition.y + Camera.main.GetScreenExtendsWorld().y * 1.5f, 0.6f)
					.SetEase(Ease.InBack));
		}

		public void AddOrRemoveAIPressed(CharacterSelectionSlotNew targetSlot)
		{
			if (targetSlot.IsAssigned)
			{
				var targetSlotID = targetSlot.slotID;
				var sequence = targetSlot.UnassignHideAndReset();
				if (sequence == null) return; // slot already hiding?

				sequence.OnComplete(() =>
				{
					GetComponentInParent<CharacterSelectionSlotManagerNew>().RecreateSlot(targetSlotID);
				});

				return;
			}

			if (!targetSlot.AssignToNPC(this))
			{
				return; // something went wrong
			}

			ChangeStateWithoutTransition(State.AddAI);
		}

		public void OnNPCSlotActionComplete()
		{
			if(curState != State.AddAI)
			{
				Debug.LogWarning("Player was not adding an NPC!");
				return;
			}

			ChangeStateWithoutTransition(State.MasterPlayerButtons);
		}

		public void StartGamePressed()
		{
			var slotManager = GetComponentInParent<CharacterSelectionSlotManagerNew>();
			if (!slotManager.AllSlotsReady()) return; // not all slots ready yet

			slotManager.MoveToNextScreen();
		}

		private void ChangeStateWithoutTransition(State newState)
		{
			UnassignListeners(curState);
			lastState = curState;
			curState = newState;
			AssignListeners(curState);
		}
	}
}