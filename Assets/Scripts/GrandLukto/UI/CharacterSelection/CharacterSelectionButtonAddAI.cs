﻿using UnityCave.UI;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionButtonAddAI : CharacterSelectionButton
	{
		public int targetSlotID;

		private CharacterSelectionSlotNew compSlot;
		private CharacterSelectionSlotManagerNew compSlotManager;

		private CharacterSelectionSlotNew TargetSlot
		{
			get
			{
				return compSlotManager.GetSlot(targetSlotID);
			}
		}

		public override void Awake()
		{
			base.Awake();

			compSlotManager = GetComponentInParent<CharacterSelectionSlotManagerNew>();
		}

		public void Start()
		{
			compSlot = GetComponentInParent<CharacterSelectionSlotNew>();
			SetColorActive(DatabaseManager.Instance.playerColors.colors[targetSlotID]); // color in target slot's color

			var uiElement = GetComponent<UIElement>();
			uiElement.EnterPressed.AddListener(EnterPressed);
		}

		private void EnterPressed()
		{
			if (!ButtonActive) return;
			if (TargetSlot.IsAssigned && !TargetSlot.NPC) return; // player joined this slot between Update and EnterPressed

			compSlot.AddOrRemoveAIPressed(TargetSlot);
		}

		public void Update()
		{
			if (TargetSlot.IsAssigned)
			{
				if (TargetSlot.NPC)
				{
					// assigned to npc
					SetText(TargetSlot.IsReady ? "AI-" : "AI+");
					SetActive(true);
				}
				else
				{
					// assigned to player
					SetText("P" + (targetSlotID + 1));
					SetActive(false);
				}

				return;
			}

			// not assigned
			SetText("AI+");
			SetActive(true);
		}
	}
}
