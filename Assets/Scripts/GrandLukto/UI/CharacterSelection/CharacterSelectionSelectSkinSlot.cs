﻿using GrandLukto.Characters;
using GrandLukto.Database;
using Spine.Unity;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSelectSkinSlot : MonoBehaviour
	{
		private UIElement compUIElement;
		private SkeletonAnimation compSkeletonAnim;

		public DataCharacter Data { get; private set; }

		public void Awake()
		{
			compUIElement = GetComponent<UIElement>();
			compSkeletonAnim = GetComponentInChildren<SkeletonAnimation>();
		}

		public void Setup(int slotX, int slotY, DataCharacter charData)
		{
			compUIElement.selectionX = slotX;
			compUIElement.selectionY = slotY;

			Data = charData;

			charData.ApplyAnimationToGameObject(compSkeletonAnim, FaceDirection.Down);

			compSkeletonAnim.AnimationState.AddAnimation(0, "idle", true, 0);
			compSkeletonAnim.AnimationState.TimeScale = 0; // TimeScale is set to 1 on over
		}
	}
}
