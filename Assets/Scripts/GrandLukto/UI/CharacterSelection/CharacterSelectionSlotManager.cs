﻿using GrandLukto.Characters;
using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSlotManager : MonoBehaviour
	{
		private List<CharacterSelectionSlot> slots;

		public void Awake()
		{
			slots = GetComponentsInChildren<CharacterSelectionSlot>().ToList();
		}

		public void Update()
		{
			foreach (var player in ReInput.players.GetPlayers())
			{
				if (!player.GetAnyButtonDown()) continue;
				if (IsPlayerAssigned(player)) continue;

				var slot = FindFirstUnassignedSlot();
				if (slot == null) return; // no unassigned slot found -> no need to check other players

				slot.AssignTo(player);
			}
		}

		private bool IsPlayerAssigned(Player player)
		{
			foreach(var slot in slots)
			{
				if (slot.Player == player) return true;
			}

			return false;
		}

		private CharacterSelectionSlot FindFirstUnassignedSlot()
		{
			foreach(var slot in slots)
			{
				if (slot.IsAssigned) continue;
				return slot;
			}

			return null;
		}

		public List<PlayerSetting> GetPlayerSettings()
		{
			var playerSettings = new List<PlayerSetting>();

			foreach(var slot in slots)
			{
				if (!slot.IsAssigned) continue;
				playerSettings.Add(slot.ToPlayerSetting());
			}

			return playerSettings;
		}
	}
}