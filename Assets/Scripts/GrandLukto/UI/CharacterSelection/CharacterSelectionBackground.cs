﻿using DG.Tweening;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	/// <summary>
	/// Renders the bars beneath the background image.
	/// </summary>
	public class CharacterSelectionBackground : MonoBehaviour
	{
		public float initialOffset = 0.075f;
		public float offsetBetween = -0.35f;

		public float selectedTeamBGOffset = 2.2f;

		public Vector3 LastBarPosLocal { get; private set; }

		[SerializeField]
		private tk2dSprite compSelectedTeamSprite;

		[SerializeField]
		private GameObject background;
		private RememberStartPosition backgroundRememberStartPos;

		public UIGroup SelectedTeamUIGroup { get; private set; }

		private List<RememberStartPosition> bars;

		public void Awake()
		{
			SelectedTeamUIGroup = compSelectedTeamSprite.gameObject.GetComponent<UIGroup>();
			backgroundRememberStartPos = background.GetComponent<RememberStartPosition>();

			compSelectedTeamSprite.gameObject.SetActive(false);
		}

		public void Start()
		{
			var slotID = GetComponentInParent<CharacterSelectionSlotNew>().slotID;

			bars = new List<RememberStartPosition>();
			GameObject curBar = null;

			for (var i = 0; i < slotID + 1; i++)
			{
				curBar = Instantiate(Resources.Load<GameObject>("CharacterSelection/BackgroundBar"), transform, false);
				curBar.transform.position += new Vector3(0, initialOffset + offsetBetween * i);

				var rememberStartPos = curBar.AddComponent<RememberStartPosition>();
				rememberStartPos.useLocalPosition = true;
				rememberStartPos.SetCurrentPositionAsStartPosition();

				bars.Add(rememberStartPos);
			}

			LastBarPosLocal = transform.InverseTransformPoint(curBar.transform.position);
		}

		public Tween ShowSelectedTeam(Team selectedTeam)
		{
			compSelectedTeamSprite.gameObject.SetActive(true);
			compSelectedTeamSprite.SetSprite(string.Format("team {0}", selectedTeam.GetTeamColorName()));

			return DOTween.Sequence()
				.Append(background.transform
					.DOLocalMoveY(selectedTeamBGOffset, 0.4f)
					.SetRelative()
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					compSelectedTeamSprite.transform.position = compSelectedTeamSprite.transform.position.Clone(z: -0.1f); // move in front of background
				});
		}

		public Tween HideSelectedTeam()
		{
			return DOTween.Sequence()
				.AppendCallback(() =>
				{
					compSelectedTeamSprite.transform.position = compSelectedTeamSprite.transform.position.Clone(z: 0.1f); // move behind background
				})
				.Append(background.transform
					.DOLocalMoveY(backgroundRememberStartPos.StartPositionLocal.y, 0.4f)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					compSelectedTeamSprite.gameObject.SetActive(false);
				});
		}

		public Tween TweenToScreenBottom(float yOffset, float duration = 0.4f)
		{
			return transform
				// screenheight + offset
				.DOLocalMoveY(-10.8f - LastBarPosLocal.y + yOffset, 0.4f)
				.SetEase(Ease.OutExpo);
		}

		public Tween TweenToY(float y, float duration = 0.4f)
		{
			return transform
				// screenheight + offset
				.DOLocalMoveY(y, 0.4f)
				.SetEase(Ease.OutExpo);
		}

		public Tween HideBars()
		{
			var sequence = DOTween.Sequence();

			for(var i = 0; i < bars.Count; i++)
			{
				var curBar = bars[i];

				sequence.Insert(.05f * i, curBar.gameObject.transform
					.DOLocalMoveY(.5f, 0.2f)
					.SetEase(Ease.InBack));
			}

			return sequence;
		}

		public Tween ShowBars()
		{
			var sequence = DOTween.Sequence();

			for (var i = 0; i < bars.Count; i++)
			{
				var curBar = bars[i];

				sequence.Insert(.05f * (bars.Count - 1 - i), curBar.gameObject.transform
					.DOLocalMoveY(curBar.StartPositionLocal.y, 0.2f)
					.SetEase(Ease.OutBack));
			}

			return sequence;
		}
	}
}