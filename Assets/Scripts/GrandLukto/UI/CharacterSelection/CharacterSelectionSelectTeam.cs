﻿using UnityEngine;
using UnityCave.ExtensionMethods;
using DG.Tweening;
using UnityCave.UI;
using UnityCave.Helpers;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSelectTeam : MonoBehaviour
	{
		public Team SelectedTeam { get; private set; }

		private UIGroup compUIGroup;
		private CharacterSelectionBackground compBackground;
		private RememberStartPosition compRememberStartPos;
		
		public void Awake()
		{
			compUIGroup = GetComponent<UIGroup>();
			compBackground = GetComponentInParent<CharacterSelectionBackground>();
			compRememberStartPos = GetComponent<RememberStartPosition>();
		}

		public void SetSelectedTeam(string teamName)
		{
			Team parsedValue;
			if(!teamName.ToEnum(out parsedValue))
			{
				Debug.Log("Can not convert value '" + teamName + "' to enum Team.");
				return;
			}

			SelectedTeam = parsedValue;
		}

		public Tween Show()
		{
			gameObject.SetActive(true);

			return transform
				.DOLocalMoveY(compBackground.LastBarPosLocal.y - 0.79499981f, 0.3f)
				.SetEase(Ease.OutExpo);
		}

		public Tween Hide()
		{
			return DOTween.Sequence()
				.AppendCallback(() =>
				{
					compUIGroup.ToggleSelected(false);
				})
				.AppendInterval(0.2f)
				.Append(transform
					.DOLocalMoveY(compRememberStartPos.StartPositionLocal.y, 0.4f)
					.SetEase(Ease.InBack))
				.AppendCallback(() =>
				{
					gameObject.SetActive(false);
				});
		}
	}
}