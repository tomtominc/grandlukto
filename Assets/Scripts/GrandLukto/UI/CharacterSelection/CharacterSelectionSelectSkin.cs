﻿using DG.Tweening;
using GrandLukto.Database;
using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.CharacterSelection
{
	public class CharacterSelectionSelectSkin : MonoBehaviour
	{
		public float maxOffsetX = 1.58f;
		public float offsetY = -1.55f;

		private Vector3 slotTweenOffset;

		private UIGroup compUIGroup;

		private List<CharacterSelectionSelectSkinSlot> slots = new List<CharacterSelectionSelectSkinSlot>();

		private bool isInitialized = false;

		public void Awake()
		{
			compUIGroup = GetComponent<UIGroup>();
		}

		private void Initialize()
		{
			if (isInitialized) return;

			isInitialized = true;

			var background = GetComponentInParent<CharacterSelectionBackground>();
			slotTweenOffset = new Vector3(0, background.LastBarPosLocal.y - (transform.localPosition.y) - .58f, 0);

			CreateSlots();
		}

		private void CreateSlots()
		{
			var selectableCharacters = DatabaseManager.Instance.dbCharacter.GetPlayable().ToList();
			var offsetXWithAngle = maxOffsetX / Mathf.Cos(Mathf.Deg2Rad * 8.5f);

			var charCount = 0;
			int ctWidth = 0;
			int ctHeight = 0;

			while(selectableCharacters.Count > charCount)
			{
				var curChar = selectableCharacters[charCount];

				var skinSlot = Instantiate(Resources.Load<GameObject>("CharacterSelection/SkinSlot"), transform, false);

				var xOffset = -offsetXWithAngle + offsetXWithAngle * ctWidth;
				skinSlot.transform.localPosition = new Vector3(0, offsetY * ctHeight, 0) + Quaternion.Euler(0, 0, 8.5f) * new Vector3(xOffset, 0, 0);

				var slot = skinSlot.GetComponent<CharacterSelectionSelectSkinSlot>();
				slot.Setup(ctWidth, -ctHeight, curChar);

				slot.GetComponent<RememberStartPosition>().SetCurrentPositionAsStartPosition(); // update position for animation
				slot.gameObject.SetActive(false);

				slots.Add(slot);

				ctWidth++;
				if (ctWidth > 2)
				{
					ctHeight++;
					ctWidth = 0;
				}

				charCount++;
			}
		}

		private void AssignToPlayer(Player player)
		{
			compUIGroup.AssignToPlayer(player);
		}

		public Tween TweenIn()
		{
			return DoTween(false);
		}

		public Tween TweenOut()
		{
			return DoTween(true);
		}

		private Tween DoTween(bool backwards)
		{
			Initialize();

			var sequence = DOTween.Sequence();

			var ease = backwards ? Ease.InBack : Ease.OutQuint;

			for (var i = 0; i < slots.Count; i++)
			{
				var curSlot = slots[backwards ? i : slots.Count - 1 - i]; // first to last or last to first

				var rememberStartPos = curSlot.GetComponent<RememberStartPosition>();
				var targetPos = rememberStartPos.StartPositionLocal;

				if (!backwards)
				{
					curSlot.gameObject.SetActive(true);
					targetPos += slotTweenOffset;
				}

				var slotSequence = DOTween.Sequence()
					.Append(curSlot.transform
						.DOLocalMove(targetPos, 0.4f)
						.SetEase(ease));

				if (backwards)
				{
					slotSequence.AppendCallback(() =>
					{
						curSlot.gameObject.SetActive(false);
					});
				}

				sequence.Insert(i * 0.05f, slotSequence);
			}

			return sequence;
		}

		public DataCharacter GetSelectedCharacter()
		{
			return compUIGroup.SelectedChild.GetComponent<CharacterSelectionSelectSkinSlot>().Data;
		}
	}
}
