﻿using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI
{
	public class UISurviveCountdown : MonoBehaviour
	{
		public UISlicedSpriteBar compSlicedSpriteBar;

		private float secondsUntilWin;
		private Team winningTeam;

		private bool winConditionAdded;
		private bool timerStarted = false;
		private float maxSecondsUntilWin;

		private void Awake()
		{
			if (compSlicedSpriteBar == null)
			{
				Debug.LogError("compSlicedSpriteBar is not set!");
				Destroy(this);
				return;
			}

			var gameManager = GameManager.Instance;

			GetComponent<tk2dCameraAnchor>().AnchorCamera = gameManager.uiCamera;

			gameManager.OnStateChanged.AddListener(OnGameStateChanged);
		}

		private void OnGameStateChanged(GameState state)
		{
			if (state != GameState.Running) return;

			timerStarted = true;
			GameManager.Instance.OnStateChanged.RemoveListener(OnGameStateChanged);
		}

		public void Update()
		{
			UpdateTime();
			UpdateBar();
		}

		private void UpdateTime()
		{
			if (!timerStarted) return;

			secondsUntilWin -= Time.deltaTime;

			if (secondsUntilWin > 0.1f) return;

			if (!winConditionAdded)
			{
				// start slow motion
				GameSpeedManager.Instance.AddWinConditionTrigger(gameObject);
				winConditionAdded = true;
			}

			if (secondsUntilWin <= 0)
			{
				GameManager.Instance.WinGame(winningTeam);
			}
		}

		private void UpdateBar()
		{
			compSlicedSpriteBar.Scale(1 - (secondsUntilWin / maxSecondsUntilWin));
		}
		
		public void Init(float secondsUntilWin, Team winningTeam)
		{
			maxSecondsUntilWin = secondsUntilWin;
			this.secondsUntilWin = secondsUntilWin;
			this.winningTeam = winningTeam;
		}

		private void OnDestroy()
		{
			GameManager.Instance.OnStateChanged.RemoveListener(OnGameStateChanged);
		}
	}
}