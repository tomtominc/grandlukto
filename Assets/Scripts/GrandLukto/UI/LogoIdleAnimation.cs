﻿using DG.Tweening;
using Spine.Unity;
using UnityEngine;

namespace GrandLukto.UI
{
	public class LogoIdleAnimation : MonoBehaviour
	{
		public Vector3 tweenOffset;
		public float tweenDuration = 0.7f;
		public Ease tweenEase;

		private SkeletonAnimation skeletonAnim;

		public void Awake()
		{
			skeletonAnim = GetComponent<SkeletonAnimation>();
		}

		[ContextMenu("Stop")]
		public void Stop()
		{
			transform.DOKill();
		}

		public void Start()
		{
			skeletonAnim.AnimationState.SetAnimation(0, "title_idle", true);

			transform
					.DOMove(tweenOffset, tweenDuration)
					.SetEase(tweenEase)
					.SetRelative()
					.SetLoops(-1, LoopType.Yoyo);
		}
	}
}