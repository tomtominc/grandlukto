﻿using UnityEngine;

namespace GrandLukto.UI
{
	[RequireComponent(typeof(tk2dTextMesh))]
	public class Countdown : CountdownBase
	{
		private tk2dTextMesh compTextMesh;

		public string[] words;

		public float scaleMin = 0;
		public float scaleMax = 1;

		public void Awake()
		{
			compTextMesh = GetComponent<tk2dTextMesh>();
			maxValue = words.Length;
		}
		
		public override void Update()
		{
			base.Update();

			UpdateAnimation(CurValueTimeRel);
		}

		protected override void OnChangeValue(int newValue)
		{
			base.OnChangeValue(newValue);

			UpdateText(newValue);
			ResetAnimation();
		}

		public void UpdateAnimation(float timeRel)
		{
			UpdateScale(Mathf.Lerp(scaleMin, scaleMax, 1 - timeRel));
		}

		public void ResetAnimation()
		{
			UpdateScale(scaleMax);
		}

		private void UpdateScale(float value)
		{
			compTextMesh.scale = new Vector3(value, value, 0);
		}

		private void UpdateText(int curValue)
		{
			compTextMesh.text = words[maxValue - 1 - curValue]; // inverse order
		}

		public void EditorUpdateText()
		{
			GetComponent<tk2dTextMesh>().text = words.Length > 0 ? words[0] : "n/a";
		}
	}
}