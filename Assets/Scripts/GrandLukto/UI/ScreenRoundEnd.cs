﻿using UnityEngine;

namespace GrandLukto.UI
{
	public class ScreenRoundEnd : MonoBehaviour
	{
		public tk2dTextMesh winText;

		public void Start()
		{
			gameObject.SetActive(false);
		}

		public void Show(Team winningTeam)
		{
			winText.text = string.Format("Team \"{0}\" wins!", winningTeam.ToString().ToUpper());

			gameObject.SetActive(true);
		}
	}
}
