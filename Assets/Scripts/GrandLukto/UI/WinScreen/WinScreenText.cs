﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreenText : MonoBehaviour
	{
		[SerializeField] private tk2dSprite leftPlant;
		[SerializeField] private tk2dSprite rightPlant;
		[SerializeField] private tk2dSprite text;

		public void Awake()
		{
			gameObject.SetActive(false);
		}

		public Tween Show()
		{
			gameObject.SetActive(true);

			leftPlant.color = rightPlant.color = text.color = new Color(1, 1, 1, 0);

			return DOTween.Sequence()
				.Append(text
					.DOColor(new Color(1, 1, 1, 1), 0.6f)
					.SetEase(Ease.OutSine))
				.Insert(0.3f, leftPlant
					.DOColor(new Color(1, 1, 1, 1), 0.6f)
					.SetEase(Ease.OutSine))
				.Insert(0.3f, rightPlant
					.DOColor(new Color(1, 1, 1, 1), 0.6f)
					.SetEase(Ease.OutSine))
				.SetUpdate(true);
		}

		public Tween Hide()
		{
			return DOTween.Sequence()
				.Append(text
					.DOColor(new Color(1, 1, 1, 0), 0.3f)
					.SetEase(Ease.InSine))
				.Join(leftPlant
					.DOColor(new Color(1, 1, 1, 0), 0.3f)
					.SetEase(Ease.InSine))
				.Join(rightPlant
					.DOColor(new Color(1, 1, 1, 0), 0.3f)
					.SetEase(Ease.InSine))
				.SetUpdate(true);
		}
	}
}
