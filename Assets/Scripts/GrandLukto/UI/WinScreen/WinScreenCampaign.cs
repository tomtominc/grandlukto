﻿using DG.Tweening;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.tk2d;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreenCampaign : MonoBehaviour, IWinScreenHandler
	{
		public GameObject characterContainer;
		public tk2dSprite character;
		public tk2dSprite characterShadow;

		public WinScreenText winText;

		public ParticleSystem confettiCannon;

		private bool playConfettiCannon = false;

		public List<WinScreenButtonDefinition> CreateButtons()
		{
			return new List<WinScreenButtonDefinition>
			{
				new WinScreenButtonDefinition("map", () => GameManager.Instance.QuitGame()),
				new WinScreenButtonDefinition("replay", () => GameManager.Instance.RestartGame()),
				new WinScreenButtonDefinition("options", () => Debug.LogError("Not implemented!")) // TODO
			};
		}
		
		private void Update()
		{
			if (!playConfettiCannon) return;
			confettiCannon.Simulate(Time.unscaledDeltaTime, true, false);
		}

		public Tween PlayShowAnimation(WinScreen winScreen)
		{
			var colorize = character.gameObject.AddComponent<tk2dSpriteColorize>();
			colorize.color = Color.white;
			colorize.flashAmount = 0;

			character.color = Color.white.MakeTransparent();
			characterShadow.color = Color.white.MakeTransparent();

			// TODO: Pick the character that the player uses
			var characterData = DatabaseManager.Instance.dbCharacter.Get(0);
			character.SetSprite(characterData.characterSelectionSpriteName);
			
			characterShadow.transform.localPosition = ((Vector3)characterData.characterSelectionShadowOffset).Clone(z: characterShadow.transform.localPosition.z);
			
			return DOTween.Sequence()
				.AppendInterval(0.3f)
				.Append(winScreen.CreateTweenBarsToCenter())
				.AppendInterval(0.1f)
				.Append(winScreen.CreateTweenBarsShowSingle(Team.Bottom))

				.AppendCallback(() =>
				{
					playConfettiCannon = true;
					confettiCannon.Play();
				})

				.AppendInterval(0.1f)

				// show character
				.Append(character
					.DOColor(Color.white, 0.1f)
					.SetEase(Ease.OutExpo))
				.Join(characterContainer.transform
					.DOLocalMoveX(-1, 0.2f)
					.From(true)
					.SetEase(Ease.OutSine))
				.Join(characterShadow
					.DOColor(Color.white, 0.2f)
					.SetEase(Ease.OutExpo))
				
				// blink
				.Append(colorize
					.DOFlashAmount(1, 0.2f)
					.SetEase(Ease.InOutSine))
				.Append(colorize
					.DOFlashAmount(0, 0.2f)
					.SetEase(Ease.InOutSine))
				.AppendCallback(() =>
				{
					winText.Show(); // do not wait
				})
				
				.Append(winScreen.CreateTweenShowButtons());
		}

		public Tween PlayHideAnimation(WinScreen winScreen)
		{
			return DOTween.Sequence()
				.Append(winScreen.CreateTweenHideButtons())
				.Append(winScreen.CreateTweenHideBars())

				// and fade out other elements
				.Join(winText.Hide())
				.Join(characterShadow
					.DOColor(Color.white.MakeTransparent(), 0.4f)
					.SetEase(Ease.OutExpo))
				.Join(character
					.DOColor(Color.white.MakeTransparent(), 0.6f)
					.SetEase(Ease.OutExpo));
		}
	}
}