﻿using DG.Tweening;
using System.Collections.Generic;

namespace GrandLukto.UI.WinScreen
{
	public interface IWinScreenHandler
	{
		List<WinScreenButtonDefinition> CreateButtons();

		Tween PlayShowAnimation(WinScreen winScreen);
		Tween PlayHideAnimation(WinScreen winScreen);
	}
}
