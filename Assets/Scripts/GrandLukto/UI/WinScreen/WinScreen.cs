﻿using DG.Tweening;
using GrandLukto.Database;
using Rewired;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreen : MonoBehaviour
	{
		[SerializeField] private GameObject barBottom;
		private DirectionalOffsetToParent barBottomDirOffset;

		[SerializeField] private GameObject barTop;
		private DirectionalOffsetToParent barTopDirOffset;

		[SerializeField] private GameObject buttonContainer;

		public WinScreenButton[] buttons;
		private WinScreenButton selectedButton;

		public DataSoundSetting soundShow;
		public DataSoundSetting soundShowButtons;

		private UIGroup compUIGroup;

		private Tween curTween;

		private float barOffsetWin = 4.4f;
		private float barOffsetLose = 15.5f;
		private float barOffsetOutOfScreen = 19.9f;

		private float barStartOffset = 20;

		private IWinScreenHandler screenHandler;

		public void Awake()
		{
			barBottomDirOffset = barBottom.GetComponent<DirectionalOffsetToParent>();
			barTopDirOffset = barTop.GetComponent<DirectionalOffsetToParent>();

			compUIGroup = GetComponent<UIGroup>();

			gameObject.SetActive(false);
		}

		public void Show(IWinScreenHandler screenHandler)
		{
			this.screenHandler = screenHandler;

			gameObject.SetActive(true);

			// initialize
			InitButtons();
			
			buttonContainer.SetActive(false);

			barBottomDirOffset.offset = new Vector3(-barStartOffset, 0, 0);
			barTopDirOffset.offset = new Vector3(barStartOffset, 0, 0);

			screenHandler
				.PlayShowAnimation(this)
				.SetUpdate(true);
		}

		public Tween CreateTweenBarsToWinPosition(Team winningTeam)
		{
			var barBottomOffset = winningTeam == Team.Bottom ? barOffsetWin : barOffsetLose;
			var barTopOffset = winningTeam == Team.Top ? barOffsetWin : barOffsetLose;

			return DOTween.Sequence()
				.Append(barBottomDirOffset
					.DOOffset(new Vector3(-barBottomOffset, 0, 0), 0.4f)
					.SetEase(Ease.InBack))
				.Join(barTopDirOffset
					.DOOffset(new Vector3(barTopOffset, 0, 0), 0.4f)
					.SetEase(Ease.InBack));
		}

		public Tween CreateTweenBarsToCenter()
		{
			// tween bars to center
			return DOTween.Sequence()
				.AppendCallback(() => UIManager.Instance.PlaySound(soundShow))
				.Append(barBottomDirOffset
					.DOOffset(new Vector3(-9.95f, 0, 0), 0.6f)
					.SetEase(Ease.OutCubic))
				.Join(barTopDirOffset
					.DOOffset(new Vector3(9.95f, 0, 0), 0.6f)
					.SetEase(Ease.OutCubic));
		}

		public Tween CreateTweenBarsShowSingle(Team showBar)
		{
			var barBottomOffset = showBar == Team.Bottom ? 0 : barOffsetOutOfScreen;
			var barTopOffset = showBar == Team.Top ? 0 : barOffsetOutOfScreen;

			return DOTween.Sequence()
				.Append(barBottomDirOffset
					.DOOffset(new Vector3(-barBottomOffset, 0, 0), 0.6f)
					.SetEase(Ease.InBack))
				.Join(barTopDirOffset
					.DOOffset(new Vector3(barTopOffset, 0, 0), 0.6f)
					.SetEase(Ease.InBack));
		}

		public Tween CreateTweenShowButtons()
		{
			// show buttons
			return DOTween.Sequence()
				.AppendCallback(() =>
					{
						UIManager.Instance.PlaySound(soundShowButtons);
						buttonContainer.SetActive(true);
						buttonContainer.transform.localPosition = buttonContainer.transform.localPosition.Clone(z: 1);
					})
				.Append(buttonContainer.transform
					.DOLocalMoveY(-1.22f, 0.3f)
					.From()
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					buttonContainer.transform.localPosition = buttonContainer.transform.localPosition.Clone(z: -1);

					compUIGroup.AssignToPlayer(ReInput.players.AllPlayers.ToList()); // activate buttons
				});
		}

		private void InitButtons()
		{
			var buttonDefinitions = screenHandler.CreateButtons();
			Assert.AreEqual(buttons.Length, buttonDefinitions.Count, "Invalid number of buttons created!");

			for (var i = 0; i < buttons.Length; i++)
			{
				var button = buttons[i];
				var buttonDefinition = buttonDefinitions[i];

				button.Initialize(buttonDefinition);
			}
		}

		private void Hide()
		{
			screenHandler.PlayHideAnimation(this)
				.SetUpdate(true)
				.OnComplete(OnHideComplete);
		}

		public Tween CreateTweenHideBars()
		{
			return DOTween.Sequence()
				.Append(barBottomDirOffset
					.DOOffset(new Vector3(-barStartOffset, 0, 0), 0.5f)
					.SetEase(Ease.InCubic))
				.Join(barTopDirOffset
					.DOOffset(new Vector3(barStartOffset, 0, 0), 0.5f)
					.SetEase(Ease.InCubic));
		}

		public Tween CreateTweenHideButtons()
		{
			return DOTween.Sequence()
				.AppendCallback(() =>
				{
					buttonContainer.transform.localPosition = buttonContainer.transform.localPosition.Clone(z: 1);
				})
				.Append(buttonContainer.transform
					.DOLocalMoveY(-1.22f, 0.3f)
					.SetEase(Ease.InExpo))
				.AppendCallback(() =>
				{
					buttonContainer.SetActive(false);
				});
		}
		
		public void SelectButtonByIndex(int index)
		{
			if (selectedButton != null) return; // button already selected

			selectedButton = buttons.ElementAtOrDefault(index);
			if (selectedButton == null)
			{
				Debug.LogErrorFormat("Button with index {0} does not exist.", index);
				return;
			}

			Hide();
		}

		private void OnHideComplete()
		{
			gameObject.SetActive(false);
			selectedButton.ExecuteButtonAction();
		}
	}
}