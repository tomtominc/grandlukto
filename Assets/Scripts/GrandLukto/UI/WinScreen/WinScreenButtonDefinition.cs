﻿using System;
namespace GrandLukto.UI.WinScreen
{
	public class WinScreenButtonDefinition
	{
		public string SpriteName { get; private set; }
		public Action OnSelect { get; private set; }

		public WinScreenButtonDefinition(string spriteName, Action onSelect)
		{
			SpriteName = spriteName;
			OnSelect = onSelect;
		}
	}
}