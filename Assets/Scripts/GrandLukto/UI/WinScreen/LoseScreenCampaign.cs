﻿using DG.Tweening;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class LoseScreenCampaign : MonoBehaviour, IWinScreenHandler
	{
		public GameObject shield;
		public GameObject shieldShadow;
		public GameObject loseText;

		private bool playDustParticles;
		public ParticleSystem shieldDustParticles;

		public List<WinScreenButtonDefinition> CreateButtons()
		{
			return new List<WinScreenButtonDefinition>
			{
				new WinScreenButtonDefinition("map", () => GameManager.Instance.QuitGame()),
				new WinScreenButtonDefinition("replay", () => GameManager.Instance.RestartGame()),
				new WinScreenButtonDefinition("options", () => Debug.LogError("Not implemented!")) // TODO
			};
		}

		private void Update()
		{
			if (!playDustParticles) return;
			shieldDustParticles.Simulate(Time.unscaledDeltaTime, true, false);
		}

		public Tween PlayShowAnimation(WinScreen winScreen)
		{
			shield.transform.position = shield.transform.position.Clone(y: 20);

			var shieldShadowSprite = shieldShadow.GetComponent<tk2dSprite>();
			shieldShadowSprite.color = Color.white.MakeTransparent();
			shieldShadow.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

			var loseTextText = loseText.GetComponent<tk2dTextMesh>();
			loseTextText.color = Color.white.MakeTransparent();

			return DOTween.Sequence()
				.AppendInterval(0.3f)
				.Append(winScreen.CreateTweenBarsToCenter())
				.AppendInterval(0.1f)
				.Append(winScreen.CreateTweenBarsShowSingle(Team.Top))
				.AppendInterval(0.1f)

				// make shadow appear
				.Append(shieldShadowSprite
					.DOColor(Color.white, 0.3f)
					.SetEase(Ease.OutExpo))
				.Join(shieldShadow.transform
					.DOScale(1, 0.6f)
					.SetEase(Ease.OutSine))
				// make shield fly down after a slight delay
				.Join(shield.transform
					.DOMoveY(0, 0.6f)
					.SetEase(Ease.Linear)
					.SetDelay(0.2f))

				// dust
				.AppendCallback(() =>
				{
					shieldDustParticles.Play();
					playDustParticles = true;

					// animate lose text without taking care of other anims
					DOTween.Sequence()
						.Append(loseTextText
							.DOColor(Color.white, 1f)
							.SetEase(Ease.OutExpo))
						.Join(loseText.transform
							.DOLocalMoveX(2, 1f)
							.From()
							.SetEase(Ease.OutSine))
						.SetUpdate(true);
				})
				// shield jump
				.Append(shield.transform
					.DOLocalJump(shield.transform.localPosition, 1, 1, 0.3f)
					.SetEase(Ease.Linear))

				.Append(winScreen.CreateTweenShowButtons());
		}

		public Tween PlayHideAnimation(WinScreen winScreen)
		{
			return DOTween.Sequence()
				.Append(winScreen.CreateTweenHideButtons())
				.Append(winScreen.CreateTweenHideBars())
				// and fade out other elements
				.Join(shieldShadow.GetComponent<tk2dSprite>()
					.DOColor(Color.white.MakeTransparent(), 0.3f)
					.SetEase(Ease.OutExpo))
				.Join(shield.GetComponent<tk2dSprite>()
					.DOColor(Color.white.MakeTransparent(), 0.6f)
					.SetEase(Ease.OutExpo))
				.Join(loseText.GetComponent<tk2dTextMesh>()
					.DOColor(Color.white.MakeTransparent(), 0.6f)
					.SetEase(Ease.OutExpo));
		}
	}
}