﻿using DG.Tweening;
using GrandLukto.Database;
using System.Collections.Generic;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreenMultiplayer : MonoBehaviour, IWinScreenHandler
	{
		public WinScreenText winText;

		public WinScreenNumber scoreBottom;
		public WinScreenNumber scoreTop;

		public DataSoundSetting soundScoreAnim;

		private Team winningTeam;
		private int scoreTeamBottom;
		private int scoreTeamTop;
		
		public void Initialize(Team winningTeam, int scoreTeamBottom, int scoreTeamTop)
		{
			this.winningTeam = winningTeam;
			this.scoreTeamBottom = scoreTeamBottom;
			this.scoreTeamTop = scoreTeamTop;
		}

		public List<WinScreenButtonDefinition> CreateButtons()
		{
			return new List<WinScreenButtonDefinition>
			{
				new WinScreenButtonDefinition("rematch", () => GameManager.Instance.RestartGame()),
				new WinScreenButtonDefinition("quit", () => GameManager.Instance.QuitGame()),
				new WinScreenButtonDefinition("options", () => Debug.LogError("Not implemented!")) // TODO
			};
		}

		public Tween PlayShowAnimation(WinScreen winScreen)
		{
			var winningTeamScore = winningTeam == Team.Top ? scoreTop : scoreBottom;

			return DOTween.Sequence()
				.AppendInterval(0.3f)
				.AppendCallback(() =>
				{
					scoreBottom.ShowScore(scoreTeamBottom);
					scoreTop.ShowScore(scoreTeamTop);
				})
				.Append(winScreen.CreateTweenBarsToCenter())
				.AppendCallback(() =>
				{
					UIManager.Instance.PlaySound(soundScoreAnim);
					winningTeamScore.AddScore();
				})
				.AppendInterval(0.1f)
				.Append(winScreen.CreateTweenBarsToWinPosition(winningTeam))
				.Append(winText.Show())
				.Append(winScreen.CreateTweenShowButtons());
		}

		public Tween PlayHideAnimation(WinScreen winScreen)
		{
			return DOTween.Sequence()
				.Append(winScreen.CreateTweenHideButtons())
				.Append(winScreen.CreateTweenHideBars())
				// and fade out other elements
				.Join(winText.Hide())
				.Join(scoreTop.Hide())
				.Join(scoreBottom.Hide());
		}
	}
}