﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreenNumber : MonoBehaviour
	{
		private tk2dSprite compSprite;

		private int curScore = 0;

		public void Awake()
		{
			compSprite = GetComponent<tk2dSprite>();
			compSprite.color = new Color(1, 1, 1, 0);

			Destroy(GetComponent<DirectionalOffsetToParent>()); // destroy it so we can move independent
		}

		public void ShowScore(int score)
		{
			curScore = score;
			UpdateSprite();

			compSprite.color = new Color(1, 1, 1, 0);

			compSprite
				.DOColor(new Color(1, 1, 1, 1), 0.6f)
				.SetEase(Ease.OutSine)
				.SetUpdate(true);
		}

		public Tween Hide()
		{
			return compSprite
				.DOColor(new Color(1, 1, 1, 0), 0.3f)
				.SetEase(Ease.InSine)
				.SetUpdate(true);
		}

		public void AddScore()
		{
			curScore++;
			UpdateSprite();

			compSprite.transform
				.DOPunchPosition(new Vector3(0, 0.75f, 0), 0.3f)
				.SetUpdate(true);
		}

		private void UpdateSprite()
		{
			compSprite.SetSprite(curScore.ToString());
		}
	}
}
