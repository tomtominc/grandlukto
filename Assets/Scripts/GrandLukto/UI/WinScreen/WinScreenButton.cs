﻿using UnityEngine;

namespace GrandLukto.UI.WinScreen
{
	public class WinScreenButton : MonoBehaviour
	{
		private WinScreenButtonDefinition buttonDefinition;

		public void Initialize(WinScreenButtonDefinition buttonDefinition)
		{
			this.buttonDefinition = buttonDefinition;

			var sprite = GetComponent<tk2dSprite>();
			sprite.SetSprite(buttonDefinition.SpriteName);
		}

		public void ExecuteButtonAction()
		{
			buttonDefinition.OnSelect();
		}
	}
}