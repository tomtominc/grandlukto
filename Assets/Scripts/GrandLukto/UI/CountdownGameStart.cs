﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.UI
{
	public class CountdownGameStart : CountdownBase
	{
		public GameObject centerObject;
		private tk2dSprite centerObjectSprite;

		public float angle = 9.5f;

		[Range(0, 1)]
		public float animationDuration = 0.75f;

		public float offsetToCenter = 1;
		public float offsetBetween = 0.2f;

		public float animIntensity = 0.2f;

		private List<GameObject> bars;

		private Quaternion animationDirection;

		public override void Start()
		{
			centerObjectSprite = centerObject.GetComponent<tk2dSprite>();

			animationDirection = Quaternion.Euler(0, 0, angle);

			base.Start();
		}

		private void CreateBars()
		{
			bars = new List<GameObject>();

			for(var i = 0; i < 6; i++)
			{
				GameObject bar = (GameObject)Instantiate(Resources.Load<GameObject>("UI/Countdown/Bar"), transform, false);
				bar.transform.position += new Vector3(0, 0, 0.5f); // behind center obj
				bars.Add(bar);

				tk2dSprite barSprite = bar.GetComponent<tk2dSprite>();
				barSprite.color = new Color(1, 1, 1, 0);

				barSprite
					.DOColor(new Color(1, 1, 1, 1), animationDuration * timePerNumber / 2)
					.SetDelay(timePerNumber / 2);

				if (i < 3)
				{
					// left
					bar.transform.position = transform.position + animationDirection * new Vector3(offsetToCenter - offsetBetween, 0, 0);

					barSprite.transform
						.DOMove(transform.position + animationDirection * new Vector3(offsetToCenter + offsetBetween * i, 0, 0), animationDuration * timePerNumber / 2)
						.SetDelay(timePerNumber / 2)
						.SetEase(Ease.OutSine);
				}
				else
				{
					bar.transform.position = transform.position - animationDirection * new Vector3(offsetToCenter - offsetBetween, 0, 0);

					barSprite.transform
						.DOMove(transform.position - animationDirection * new Vector3(offsetToCenter + offsetBetween * (i - 3), 0, 0), animationDuration * timePerNumber / 2)
						.SetDelay(timePerNumber / 2)
						.SetEase(Ease.OutSine);
				}
			}
		}

		protected override void OnChangeValue(int newValue)
		{
			base.OnChangeValue(newValue);

			switch(newValue)
			{
				case 4:
					centerObjectSprite.SetSprite("countdown-blank");

					var offset = new Vector3(0, 0.25f, 0);
					centerObject.transform.position = centerObject.transform.position + offset;

					centerObjectSprite.color = new Color(1, 1, 1, 0);

					DOTween.Sequence()
						.SetDelay(timePerNumber / 4)
						.Append(centerObject.transform
							.DOMove(centerObject.transform.position - offset, timePerNumber / 4)
							.SetEase(Ease.OutQuint))
						.Join(centerObjectSprite
							.DOColor(new Color(1, 1, 1, 1), timePerNumber / 4)
							.SetEase(Ease.OutQuint))
						.OnComplete(() => CreateBars());

					break;

				case 3:
					centerObjectSprite.SetSprite("countdown-3");
					break;

				case 2:
					centerObjectSprite.SetSprite("countdown-2");

					AnimateBar(2);
					AnimateBar(5);
					break;
				case 1:
					centerObjectSprite.SetSprite("countdown-1");

					AnimateBar(1);
					AnimateBar(4);
					break;
				case 0:
					centerObjectSprite.SetSprite("countdown-go");

					AnimateBar(0, true);
					AnimateBar(3, true);

					DOTween.Sequence()
						.SetDelay(timePerNumber / 4 * 3)
						//.Append(centerObject.transform
						//	.DOMove(centerObject.transform.position - new Vector3(0, 0.25f, 0), animationDuration * timePerNumber)
						//	.SetEase(Ease.OutQuint))
						.Append(centerObjectSprite
							.DOColor(new Color(1, 1, 1, 0), timePerNumber / 4)
							.SetEase(Ease.OutQuint));
					break;
			}
		}

		private void AnimateBar(int barIndex, bool fadeOut = false)
		{
			var sign = barIndex < 3 ? 1 : -1;
			var bar = bars[barIndex];

			DOTween.Sequence()
				.Append(bar.transform
					.DOMove(bar.transform.position + animationDirection * new Vector3(animIntensity, 0, 0) * sign, animationDuration * timePerNumber / 4)
					.SetEase(Ease.OutSine))
				.Append(bar.transform
					.DOMove(bar.transform.position - animationDirection * new Vector3(offsetBetween, 0, 0) * sign, animationDuration * timePerNumber / 4 * 3)
					.SetEase(Ease.InSine))
				.OnComplete(() => bar.SetActive(false));

			if(fadeOut)
			{
				var barSprite = bar.GetComponent<tk2dSprite>();

				barSprite
					.DOColor(new Color(1, 1, 1, 0), animationDuration * timePerNumber / 4 * 3)
					.SetDelay(animationDuration * timePerNumber / 4)
					.SetEase(Ease.OutSine);
			}
		}
	}
}
