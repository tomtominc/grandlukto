﻿using UnityEngine;
using DG.Tweening;

namespace GrandLukto.UI
{
	public class UIBackgroundAnimation : MonoBehaviour
	{
		public float duration = 0.4f;

		private Renderer compRenderer;

		public void Awake()
		{
			compRenderer = GetComponent<Renderer>();
		}

		public void Update()
		{
			// we need to use unscaled time here since this is used on the pause sceen
			compRenderer.material.SetFloat("_UnscaledTime", Time.unscaledTime);
		}

		public Tween Show()
		{
			compRenderer.material.SetFloat("_FirstToSecondTex", 1);
			compRenderer.material.SetFloat("_FadeTex", 0);

			return DOTween.Sequence()
				.Append(compRenderer.material.DOFloat(1, "_FadeTex", duration / 2))
				.Append(compRenderer.material.DOFloat(0, "_FirstToSecondTex", duration / 2))
				.SetUpdate(true);
		}

		public Tween Hide()
		{
			compRenderer.material.SetFloat("_FirstToSecondTex", 0);
			compRenderer.material.SetFloat("_FadeTex", 1);

			return DOTween.Sequence()
				.Append(compRenderer.material.DOFloat(1, "_FirstToSecondTex", duration / 2))
				.Append(compRenderer.material.DOFloat(0, "_FadeTex", duration / 2))
				.SetUpdate(true);
		}
	}
}
