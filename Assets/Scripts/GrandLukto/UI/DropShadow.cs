﻿using UnityEngine;

namespace GrandLukto.UI
{
	public class DropShadow : MonoBehaviour
	{
		public Color shadowColor;
		public Vector3 shadowOffset;

		private tk2dSprite compSprite;

		private tk2dTextMesh compTextMesh;
		private tk2dTextMesh compTextMeshSource;

		private GameObject gameObjShadow;

		public void Start()
		{
			gameObjShadow = Instantiate(gameObject, transform, true);

			// remove all components except those required for rendering
			foreach(var component in gameObjShadow.GetComponents<Component>())
			{
				if (component is MeshFilter || component is MeshRenderer || component is tk2dSprite || component is Transform || component is tk2dTextMesh) continue;
				Destroy(component);
			}

			compSprite = gameObjShadow.GetComponent<tk2dSprite>();
			compTextMesh = gameObjShadow.GetComponent<tk2dTextMesh>();

			compTextMeshSource = GetComponent<tk2dTextMesh>();
		}

		public void OnEnable()
		{
			if(gameObjShadow != null) gameObjShadow.SetActive(true);
		}

		public void OnDisable()
		{
			if (gameObjShadow != null) gameObjShadow.SetActive(false);
		}

		public void Update()
		{
			UpdateValues();
		}

		public void UpdateValues()
		{
			if (gameObjShadow == null) return;

			gameObjShadow.transform.localPosition = shadowOffset;

			if (compSprite != null)
			{
				compSprite.color = shadowColor;
			}

			if (compTextMesh != null)
			{
				compTextMesh.color = shadowColor;

				compTextMesh.font = compTextMeshSource.font;
				compTextMesh.maxChars = compTextMeshSource.maxChars;
				compTextMesh.text = compTextMeshSource.text;
				compTextMesh.anchor = compTextMeshSource.anchor;
			}
		}
	}
}
