﻿using System;
using DG.Tweening;
using GrandLukto.Characters;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.UI
{
	public class PlayerIcon : MonoBehaviour
	{
		public float tweenDuration = 1f;
		public float animationOffset = 0.5f;
		public Ease easing = Ease.OutSine;

		private tk2dSprite compSprite;

		public void Awake()
		{
			compSprite = GetComponent<tk2dSprite>();
		}

		public void Start()
		{
			StartAnimation();
		}

		private void StartAnimation()
		{
			compSprite.color = new Color(1, 1, 1, 0);
			DOTween.Sequence()
				.Append(compSprite.transform
					.DOMove(new Vector3(0, animationOffset, 0), tweenDuration)
					.From(true)
					.SetEase(easing))
				.Join(compSprite
					.DOColor(new Color(1, 1, 1, 1), tweenDuration)
					.SetEase(easing))
				.AppendInterval(1.25f)
				.Append(compSprite
					.DOColor(new Color(1, 1, 1, 0), 0.5f)
					.SetEase(easing))
				.OnComplete(() => Destroy(gameObject));
		}

		public void SetupIconForPlayer(PlayerSetting playerSetting)
		{
			compSprite.SetSprite("player-icon-" + (playerSetting.playerId + 1));

			transform.localPosition = new Vector3(0, 0.7f, 0);
		}
	}
}
