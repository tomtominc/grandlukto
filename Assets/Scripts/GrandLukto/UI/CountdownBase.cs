﻿using GrandLukto.Database;
using System.Collections;
using UnityCave.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto.UI
{
	public class CountdownBase : MonoBehaviour
	{
		public float timePerNumber = 1f;

		public UnityEvent OnCountdownFinished;

		public int maxValue = 4;
		private int curValue = 0;
		private float curTime = 0;

		public bool startImmediate = false;
		public bool setGameObjectActiveInactive = false;
		public bool triggerFinishEvent1BeforeLast = false;

		public DataSoundSetting soundCount;
		public DataSoundSetting soundFinished;

		private AudioSource compAudioSource;

		public float CurValueTimeRel
		{
			get
			{
				return curTime / timePerNumber;
			}
		}

		public virtual void Start()
		{
			compAudioSource = GetComponent<AudioSource>();

			if (startImmediate)
			{
				StartCountdown();
				return;
			}

			if (setGameObjectActiveInactive) gameObject.SetActive(false);
		}

		public virtual void Update()
		{
			curTime += Time.deltaTime;

			if (curTime < timePerNumber)
			{
				return;
			}

			// next value
			curValue--;
			curTime = 0;

			if(curValue == 0 && triggerFinishEvent1BeforeLast)
			{
				OnCountdownFinished.Invoke();
			}

			if (curValue < 0)
			{
				curValue = 0;

				// no more words left
				compAudioSource.TryPlayOneShot(this, soundFinished);
				if (!triggerFinishEvent1BeforeLast) OnCountdownFinished.Invoke();
				if (setGameObjectActiveInactive) gameObject.SetActive(false);
				return;
			}

			// change
			compAudioSource.TryPlayOneShot(this, soundCount);
			OnChangeValue(curValue);
		}

		protected virtual void OnChangeValue(int newValue)
		{
		}

		public void StartCountdown()
		{
			if (setGameObjectActiveInactive) gameObject.SetActive(true);

			curValue = maxValue - 1;
			curTime = 0;

			OnChangeValue(curValue);
		}

		[ContextMenu("StartCountdownDelayed")]
		public void StartCountdownDelayed()
		{
			StartCoroutine(DoStartCountdownDelayed());
		}

		private IEnumerator DoStartCountdownDelayed()
		{
			yield return new WaitForSeconds(1f);
			StartCountdown();
		}
	}
}