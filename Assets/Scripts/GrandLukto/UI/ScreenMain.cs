﻿using DG.Tweening;
using GrandLukto.Database;
using Rewired;
using Spine.Unity;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI
{
	public enum ScreenMainAction
	{
		None,
		StorySingle,
		StoryCoop,
		LocalMultiplayer,
		Options,
		Exit
	}

	public class ScreenMain : UIScreenBase
	{
		public float enterAnimDuration;
		public float enterAnimDelay;

		public SkeletonAnimation logo;
		public LogoIdleAnimation logoIdleAnim;

		public GameObject bannerOptions;
		public GameObject bannerMultiplayer;
		public GameObject bannerCampaign;

		public DataSoundSetting soundIn;
		public DataSoundSetting soundOut;

		private AudioSource audioSource;

		private ScreenMainAction selectedAction = ScreenMainAction.None;
		private Sequence activeSequence;

		public void Awake()
		{
			audioSource = gameObject.GetOrAddComponent<AudioSource>();
		}

		public void OnEnable()
		{
			PlayAnimation();

			GetComponent<UIGroup>().AssignToPlayer(ReInput.players.AllPlayers.ToList());
		}
		
		public Sequence PlayAnimation(bool playBackwards = false)
		{
			if(activeSequence != null)
			{
				activeSequence.Kill();
			}

			// set initial positions
			bannerOptions.GetComponent<RememberStartPosition>().ResetToStartPosition();
			bannerMultiplayer.GetComponent<RememberStartPosition>().ResetToStartPosition();
			bannerCampaign.GetComponent<RememberStartPosition>().ResetToStartPosition();

			var easing = playBackwards ? Ease.InExpo : Ease.OutExpo;

			if (playBackwards)
			{
				logoIdleAnim.Stop();

				logo.transform
					.DOMoveY(Camera.main.ViewportToWorldPoint(Vector2.up).y + 4, enterAnimDuration)
					.SetEase(Ease.InBack);
			}
			else
			{
				logoIdleAnim.GetComponent<RememberStartPosition>().ResetToStartPosition();

				// logo in animation
				logo.AnimationState.ClearTracks();
				logo.AnimationState.AddAnimation(0, "title", false, 0);
				logo.Update(0);
			}

			var targetY = 10.8f;

			var sequence = DOTween.Sequence()
				.Join(bannerOptions.transform
					.DOLocalMoveY(targetY, enterAnimDuration)
					.From(true)
					.SetEase(easing))
				.Insert(enterAnimDelay, bannerMultiplayer.transform
					.DOLocalMoveY(targetY, enterAnimDuration)
					.From(true)
					.SetEase(easing))
				.Insert(enterAnimDelay * 2, bannerCampaign.transform
					.DOLocalMoveY(targetY, enterAnimDuration)
					.From(true)
					.SetEase(easing)
				);

			if (playBackwards)
			{
				sequence.SetAutoKill(false);
				sequence.Complete();
				sequence.PlayBackwards();

				soundOut.Play(this, audioSource);
			}
			else
			{
				sequence.AppendCallback(logoIdleAnim.Start);

				soundIn.Play(this, audioSource);
			}

			activeSequence = sequence;
			return sequence;
		}

		public void SelectAction(string action)
		{
			SelectAction(action.ToEnum(ScreenMainAction.None));
		}

		public void SelectAction(ScreenMainAction action)
		{
			if (action == ScreenMainAction.None) return;
			if (selectedAction != ScreenMainAction.None) return; // action already queued

			selectedAction = action;

			if (selectedAction == ScreenMainAction.Exit)
			{
				Application.Quit();
				return;
			}

			PlayAnimation(true)
				.OnRewind(HandleSelectedAction);
		}

		private void HandleSelectedAction()
		{

			switch(selectedAction)
			{
				case ScreenMainAction.StorySingle:
				case ScreenMainAction.StoryCoop:
					GameSceneManager.LoadOverworld();
					break;

				case ScreenMainAction.LocalMultiplayer:
					var characterSelection = Manager.GetScreenByType<ScreenCharacterSelectionNew>();
					characterSelection.Reset();
					Manager.ChangeScreen(characterSelection);
					break;

				default:
					//StartGame();
					break;
			}

			selectedAction = ScreenMainAction.None;
		}

		private void StartGame()
		{
			if (!IsEnabled) return;
			Manager.ChangeScreen<ScreenCharacterSelection>();
		}
	}
}