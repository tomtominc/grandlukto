﻿using GrandLukto.Arenas;
using System;
using System.Linq;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI
{
	// todo: reduce to one: ArenaSelector, TeamSelector, LevelSelector
	public class ArenaSelector : MonoBehaviour
	{
		public tk2dTextMesh label;

		public Arena SelectedValue
		{
			get
			{
				return possibleValues[selectedIndex];
			}
		}

		private Arena[] possibleValues = Enum.GetValues(typeof(Arena)).Cast<Arena>().Where(arena => arena != Arena.None).ToArray();
		private int selectedIndex = 0;

		public void Start()
		{
			UpdateSelectedValue();
		}

		public void ChangeSelectedTeam(int change)
		{
			selectedIndex = MathUtil.CycleThrough(selectedIndex, change, 0, possibleValues.Length);
			UpdateSelectedValue();
		}

		private void UpdateSelectedValue()
		{
			label.text = possibleValues[selectedIndex].ToString();
		}
	}
}