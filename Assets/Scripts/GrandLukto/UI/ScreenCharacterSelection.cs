﻿using GrandLukto.UI.CharacterSelection;
using UnityCave.UI;

namespace GrandLukto.UI
{
	public class ScreenCharacterSelection : UIScreenBase
	{
		public void Play()
		{
			if (!IsEnabled) return;
			CreateGameScene();
		}

		private void CreateGameScene()
		{
			var playerSettings = GetComponentInChildren<CharacterSelectionSlotManager>().GetPlayerSettings();
			var selectedArena = GetComponentInChildren<ArenaSelector>().SelectedValue;
			var selectedLevel = GetComponentInChildren<LevelSelector>().SelectedValue;

			var dataArena = DatabaseManager.Instance.dbArena.GetAll().Find(arena => arena.arenaName.ToLower() == selectedArena.ToString().ToLower());

			GameSceneManager.LoadGame(dataArena, selectedLevel, playerSettings);
		}
	}
}