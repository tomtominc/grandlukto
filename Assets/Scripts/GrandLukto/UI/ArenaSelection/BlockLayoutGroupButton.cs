﻿using DG.Tweening;
using GrandLukto.Database;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.UI.ArenaSelection
{
	public class BlockLayoutGroupButton : MonoBehaviour
	{
		public DirectionalOffsetToParent contentOffset;
		public tk2dTextMesh text;
		public tk2dSprite background;

		public float yOffset = 1.89f;
		public float xOffset = 1;
		public float xOffsetOut = 6.4f;

		public float animDuration = 0.3f;

		private DataBlockLayoutGroup data;
		private bool isSelected = false;

		public void ApplyData(DataBlockLayoutGroup data)
		{
			this.data = data;

			text.text = data.blockLayoutGroupName;
			UpdateSprite();
		}

		public void ApplyPosition(int indexOffsetToSelected, bool fadedOut = false)
		{
			transform.localPosition = CalcLocalPosition(indexOffsetToSelected);
			contentOffset.offset = CalcContentOffset(indexOffsetToSelected);

			var color = fadedOut ? Color.white.MakeTransparent() : CalcColor(indexOffsetToSelected);
			text.color = color;
			background.color = color;

			UpdateSelected(indexOffsetToSelected == 0);
		}

		public Tween TweenTo(int indexOffsetToSelected, bool tweenOut = false)
		{
			var color = tweenOut ? Color.white.MakeTransparent() : CalcColor(indexOffsetToSelected);

			var sequence = DOTween.Sequence();

			// change image back to normal if not selected
			sequence.AppendCallback(() =>
			{
				UpdateSelected(indexOffsetToSelected == 0);
			});

			sequence
				.Join(transform
					.DOLocalMove(CalcLocalPosition(indexOffsetToSelected), animDuration)
					.SetEase(Ease.OutExpo))
				.Join(contentOffset
					.DOOffset(CalcContentOffset(indexOffsetToSelected), animDuration)
					.SetEase(Ease.OutExpo))
				.Join(text
					.DOColor(color, animDuration)
					.SetEase(Ease.InOutSine))
				.Join(background
					.DOColor(color, animDuration)
					.SetEase(Ease.InOutSine));
			
			if (tweenOut)
			{
				sequence.AppendCallback(() =>
				{
					gameObject.SetActive(false);
				});
			}

			return sequence;
		}

		public Tween TweenShowHide(int indexOffsetToSelected, bool doShow, bool useDelay = true)
		{
			Vector3 toPos;
			Vector3 outPos = new Vector3(xOffsetOut, 0, 0);
			float delay = 0;
			float duration;

			if (doShow)
			{
				toPos = CalcContentOffset(indexOffsetToSelected);
				contentOffset.offset = outPos;

				if (useDelay) delay = Mathf.Abs(indexOffsetToSelected) * 0.15f;
				duration = 0.4f;
			}
			else
			{
				toPos = outPos;

				if (useDelay) delay = indexOffsetToSelected == 0 ? 0.3f : 0;
				duration = 0.5f;
			}

			return DOTween.Sequence()
				.AppendInterval(delay)
				.Append(contentOffset
					.DOOffset(toPos, duration)
					.SetEase(Ease.OutExpo));
		}

		private Vector3 CalcLocalPosition(int indexOffsetToSelected)
		{
			return new Vector3(0, -indexOffsetToSelected * yOffset);
		}

		private Vector3 CalcContentOffset(int indexOffsetToSelected)
		{
			var absIndexOffset = Mathf.Abs(indexOffsetToSelected);
			return new Vector3(absIndexOffset == 0 ? 0 : 1 + (absIndexOffset - 1) * xOffset, 0, 0);
		}

		private Color CalcColor(int indexOffsetToSelected)
		{
			var absIndexOffset = Mathf.Abs(indexOffsetToSelected);
			return Color.white.MakeTransparent(absIndexOffset == 0 ? 1 : 1 - 0.1f - 0.2f * absIndexOffset);
		}

		private void UpdateSelected(bool isSelected)
		{
			this.isSelected = isSelected;
			UpdateSprite();
		}

		private void UpdateSprite()
		{
			var postFix = isSelected ? " selected" : " not selected";
			background.SetSprite(data.backgroundColor.ToString().ToLower() + postFix);
		}
	}
}