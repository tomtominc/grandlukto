﻿using DG.Tweening;
using GrandLukto.Database;
using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto.UI.ArenaSelection
{
	public class BlockLayoutGroupSelection : MonoBehaviour
	{
		private enum PressedButton
		{
			Up,
			Down,
			Enter,
			Back
		}

		public int maxButtons = 5;
		public int buttonIndexCenter;
		public int numButtons;

		private List<DataBlockLayoutGroup> data;
		private int curIndex = 0;

		private List<BlockLayoutGroupButton> visibleButtons;
		private BlockLayoutGroupButton invisibleButton;

		private bool isInitialized = false;

		private UIGroup compUIGroup;

		private bool isTweenActive = false;

		private Queue<PressedButton> buttonQueue = new Queue<PressedButton>();

		public UnityEvent EnterPressed;
		public UnityEvent BackPressed;

		public void Awake()
		{
			compUIGroup = GetComponent<UIGroup>();
		}

		private void Initialize()
		{
			if (isInitialized) return;

			data = DatabaseManager.Instance.dbBlockLayoutGroup.GetAll();
			CreateButtons();

			isInitialized = true;
		}

		private void CreateButtons()
		{
			visibleButtons = new List<BlockLayoutGroupButton>();

			numButtons = Mathf.Min(maxButtons, data.Count);
			if (numButtons % 2 == 0) numButtons++; // only allow odd numbers

			buttonIndexCenter = Mathf.Min(numButtons / 2);

			for(var i = 0; i < numButtons; i++)
			{
				var button = CreateButton();
				visibleButtons.Add(button);

				var indexOffsetToCenter = CalcIndexOffsetToCenter(i);
				button.ApplyData(GetDataAtIndex(curIndex + indexOffsetToCenter)); // center = curIndex
				button.ApplyPosition(indexOffsetToCenter);
			}

			invisibleButton = CreateButton();
			invisibleButton.gameObject.SetActive(false);
		}

		private BlockLayoutGroupButton CreateButton()
		{
			var gameObj = Instantiate(Resources.Load<GameObject>("ArenaSelection/BlockLayoutGroup"), transform, false);
			return gameObj.GetComponent<BlockLayoutGroupButton>();
		}

		private DataBlockLayoutGroup GetDataAtIndex(int index)
		{
			while (index < 0) index += data.Count;
			while (index >= data.Count) index -= data.Count;

			return data[index];
		}

		private int CalcIndexOffsetToCenter(int index)
		{
			return index - buttonIndexCenter;
		}

		public void Update()
		{
			UpdateInputQueue();

			if (isTweenActive) return;
			if (!buttonQueue.HasItems()) return; // no input in queue

			var button = buttonQueue.Dequeue();

			switch(button)
			{
				case PressedButton.Down:
				case PressedButton.Up:
					UIManager.Instance.PlaySound(compUIGroup.SoundMove);

					MoveCarousel(button == PressedButton.Down);
					break;

				case PressedButton.Enter:
				case PressedButton.Back:
					UIManager.Instance.PlaySound(compUIGroup.SoundSelect);

					buttonQueue.Clear(); // clear queue to avoid additional moves

					var handler = button == PressedButton.Enter ? EnterPressed : BackPressed;
					handler.Invoke();
					break;
			}
		}

		private void UpdateInputQueue()
		{
			if (!compUIGroup.IsSelected) return; // not selected
			var uiConfig = UIManager.Instance.config;

			if (compUIGroup.ParentControllers.GetButtonDown(uiConfig.verticalAxisName))
			{
				buttonQueue.Enqueue(PressedButton.Up);
				return;
			}

			if (compUIGroup.ParentControllers.GetNegativeButtonDown(uiConfig.verticalAxisName))
			{
				buttonQueue.Enqueue(PressedButton.Down);
				return;
			}

			if (compUIGroup.ParentControllers.GetButtonDown(uiConfig.buttonEnterNames))
			{
				buttonQueue.Enqueue(PressedButton.Enter);
				return;
			}

			if (compUIGroup.ParentControllers.GetButtonDown(uiConfig.buttonBackNames))
			{
				buttonQueue.Enqueue(PressedButton.Back);
				return;
			}
		}

		private void MoveCarousel(bool moveUp)
		{
			isTweenActive = true;

			var sequence = DOTween.Sequence();

			if (moveUp) curIndex++;
			else curIndex--;

			var removeIndex = moveUp ? 0 : numButtons - 1;
			var removedButton = visibleButtons[removeIndex];
			visibleButtons.RemoveAt(removeIndex);

			sequence.Join(removedButton.TweenTo(CalcIndexOffsetToCenter(removeIndex + (moveUp ? -1 : 1)), true));

			for (var i = 0; i < visibleButtons.Count; i++)
			{
				sequence.Join(visibleButtons[i].TweenTo(CalcIndexOffsetToCenter(moveUp ? i : i + 1))); // i + 1 since we removed the last element and need to push all forward
			}

			invisibleButton.gameObject.SetActive(true);
			var newElementIndexDir = (moveUp ? 1 : -1);
			invisibleButton.ApplyData(GetDataAtIndex(curIndex + buttonIndexCenter * newElementIndexDir)); // next possible element
			invisibleButton.ApplyPosition((buttonIndexCenter + 1) * newElementIndexDir);
			sequence.Join(invisibleButton.TweenTo(buttonIndexCenter * newElementIndexDir));

			// add now visible button
			if (moveUp) visibleButtons.Add(invisibleButton);
			else visibleButtons.Insert(0, invisibleButton);

			// apply invisibleButton
			invisibleButton = removedButton;

			sequence.AppendCallback(OnEndTween);
		}

		public Tween Show(Player controllingPlayer)
		{
			Initialize();

			isTweenActive = true;

			var sequence = DOTween.Sequence();

			for (var i = 0; i < visibleButtons.Count; i++)
			{
				var button = visibleButtons[i];
				var indexOffsetToCenter = CalcIndexOffsetToCenter(i);
				sequence.Join(button.TweenShowHide(indexOffsetToCenter, true));
			}

			return sequence.OnComplete(() =>
			{
				compUIGroup.AssignToPlayer(controllingPlayer);
				OnEndTween();
			});
		}

		public Tween Hide(bool delayOnSelected = false)
		{
			isTweenActive = true;

			var sequence = DOTween.Sequence();

			for (var i = 0; i < visibleButtons.Count; i++)
			{
				var button = visibleButtons[i];
				var indexOffsetToCenter = CalcIndexOffsetToCenter(i);
				sequence.Join(button.TweenShowHide(indexOffsetToCenter, false, delayOnSelected));
			}

			return sequence.OnComplete(OnEndTween);
		}

		private void OnEndTween()
		{
			isTweenActive = false;
		}

		public DataBlockLayoutGroup GetSelectedItem()
		{
			return GetDataAtIndex(curIndex);
		}
	}
}