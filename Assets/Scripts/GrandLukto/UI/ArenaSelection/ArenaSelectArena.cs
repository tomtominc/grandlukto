﻿using GrandLukto.Database;
using UnityCave.Helpers;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.ArenaSelection
{
	public class ArenaSelectArena : MonoBehaviour
	{
		public tk2dSprite image;

		public RememberStartPosition RememberStartPosition { get; private set; }

		private UIElement compUIElement;

		public DataArena Data { get; private set; }

		public void Awake()
		{
			RememberStartPosition = GetComponent<RememberStartPosition>();
			compUIElement = GetComponent<UIElement>();
		}

		public void SetData(DataArena data)
		{
			if (data == null) return; // show random image

			Data = data;

			image.SetSprite("arena-" + data.arenaName.ToLower()); // update image
		}

		public void SetUICoords(int selectX, int selectY)
		{
			compUIElement.selectionX = selectX;
			compUIElement.selectionY = selectY;
		}

		public void UpdateFrameColor(Color c)
		{
			var changeColorComponents = GetComponentsInChildren<UISelectChangeColor>();

			foreach (var component in changeColorComponents)
			{
				component.colorSelected = c;
			}
		}
	}
}
