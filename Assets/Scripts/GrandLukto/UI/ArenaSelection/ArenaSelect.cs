﻿using DG.Tweening;
using GrandLukto.Database;
using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.UI.ArenaSelection
{
	public class ArenaSelect : MonoBehaviour
	{
		public Vector2 positionOffset;
		public float animDuration = 0.4f;
		public float animDelay = 0.03f;
		private Color frameColor;

		public Ease easeHide;

		private List<ArenaSelectArena> arenaButtons;

		public UIGroup CompUIGroup { get; set; }

		private bool isInitialized = false;

		public void Awake()
		{
			CompUIGroup = GetComponent<UIGroup>();
		}
		
		private IEnumerable<DataArena> GetAvailableArenas()
		{
			return DatabaseManager.Instance.dbArena.GetAll().Where(arena => arena.isAvailableInLocalMultiplayer);
		}

		private void CreateArenaButtons()
		{
			arenaButtons = new List<ArenaSelectArena>();

			foreach(var arena in GetAvailableArenas())
			{
				arenaButtons.Add(CreateArenaButton(arena));
			}

			arenaButtons.Add(CreateArenaButton()); // random arena
		}

		public void SetFrameColor(Color playerColor)
		{
			frameColor = playerColor;

			if (!isInitialized) return; // will set framecolor in initialize method

			foreach(var button in arenaButtons)
			{
				button.UpdateFrameColor(playerColor);
			}
		}

		private ArenaSelectArena CreateArenaButton(DataArena arena = null)
		{
			var gameObj = Instantiate(Resources.Load<GameObject>("ArenaSelection/Arena"), transform, false);

			var button = gameObj.GetComponent<ArenaSelectArena>();
			button.UpdateFrameColor(frameColor);
			button.SetData(arena);

			return button;
		}

		//public void Update()
		//{
		//	if(Input.GetMouseButtonDown(0))
		//	{
		//		Show();
		//	}

		//	if(Input.GetMouseButtonDown(2))
		//	{
		//		Hide();
		//	}
		//}

		[ContextMenu("Reposition Buttons")]
		private void PositionArenaButtons()
		{
			var ctWidth = 0;
			var ctHeight = 0;

			for(var i = 0; i < arenaButtons.Count; i++)
			{
				var curButton = arenaButtons[i];

				curButton.transform.localPosition = new Vector2(positionOffset.x * ctWidth, positionOffset.y * ctHeight);
				curButton.SetUICoords(ctWidth, -ctHeight);

				curButton.RememberStartPosition.SetCurrentPositionAsStartPosition();

				ctWidth++;
				if(ctWidth >= 3)
				{
					// next row
					ctHeight++;
					ctWidth = 0;
				}
			}
		}

		public Tween Show(Player controllingPlayer)
		{
			Initialize();

			var sequence = DOTween.Sequence();

			for (var i = arenaButtons.Count - 1; i >= 0; i--)
			{
				var curButton = arenaButtons[i];

				curButton.transform.position = curButton.transform.position.Clone(x: Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x - 2f);

				sequence.Join(curButton.transform
					.DOMove(curButton.RememberStartPosition.StartPosition, animDuration)
					.SetDelay((arenaButtons.Count - 1 - i) * animDelay)
					.SetEase(Ease.OutExpo));
			}

			sequence.AppendCallback(() =>
			{
				CompUIGroup.AssignToPlayer(controllingPlayer);
			});

			return sequence;
		}

		public Tween Hide()
		{
			var sequence = DOTween.Sequence();

			sequence.AppendCallback(() =>
			{
				CompUIGroup.Unassign();
			});

			for (var i = 0; i < arenaButtons.Count; i++)
			{
				var curButton = arenaButtons[i];
				sequence.Join(curButton.transform
					.DOMove(curButton.transform.position.Clone(x: Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x - 2f), animDuration)
					.SetDelay(i * animDelay)
					.SetEase(easeHide));
			}

			return sequence;
		}

		private void Initialize()
		{
			if (isInitialized) return;

			CreateArenaButtons();
			PositionArenaButtons();

			isInitialized = true;
		}

		public DataArena GetSelectedArena()
		{
			var arena = CompUIGroup.SelectedChild.GetComponent<ArenaSelectArena>().Data;

			if(arena == null)
			{
				arena = GetAvailableArenas().SelectRandom(); // fallback to random arena (??? button)
			}

			return arena;
		}
	}
}
