﻿using DG.Tweening;
using System.Linq;
using UnityEngine;

namespace GrandLukto.UI
{
	public class RosterMenu : MonoBehaviour
	{
		public GameObject button;
		public RosterMenuCharacters menu;

		public bool IsOpen { get; private set; }
		private bool? IsAnimatingTo = null;

		public void Show()
		{
			if (IsOpen) return;

			IsOpen = true;

			var saveData = OverworldManager.Instance.SaveData;
			var selectedCharName = saveData.GetSelectedCharacterByPlayerID(0);

			menu.Show(selectedCharName);
		}

		public void Hide()
		{
			if (!IsOpen || (IsAnimatingTo.HasValue && !IsAnimatingTo.Value)) return;

			IsAnimatingTo = false;

			DOTween.Sequence()
				.Append(menu.Hide())
				.AppendCallback(() =>
				{
					IsAnimatingTo = null;
					IsOpen = false;
				});
		}

		// TODO: Catch escape from pause menu and only close this menu?
		public void ResetAndClose()
		{
			Debug.Log("ResetAndClose");
		}

		public void SaveAndClose()
		{
			menu.SetCurrentButtonAsSelected();

			var selectedCharacter = menu.GetSelectedCharacter();

			var saveData = OverworldManager.Instance.SaveData;
			saveData.SetSelectedCharacterByPlayerID(0, selectedCharacter.characterName);

			saveData.Save();

			Hide();
		}
	}
}