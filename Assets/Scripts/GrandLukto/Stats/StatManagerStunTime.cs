﻿using UnityCave;
using UnityEngine;

namespace GrandLukto.Stats
{
	public class StatManagerStunTime : StatManager
	{
		public void Update()
		{
			if (IsEmpty) return;

			ChangeValue(-Time.deltaTime);
		}
	}
}