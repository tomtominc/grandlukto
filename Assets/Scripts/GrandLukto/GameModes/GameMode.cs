﻿using GrandLukto.UI;
using UnityEngine;

namespace GrandLukto.GameModes
{
	[System.Serializable]
	public class GameMode
	{
		public GameModeType modeType;

		public float duration;

		public bool IsHeartWallTopDestroyable()
		{
			return modeType != GameModeType.Survive && modeType != GameModeType.DestroyTargets;
		}

		public void OnEnterGame()
		{
			switch(modeType)
			{
				case GameModeType.Survive:
					var countdownGameObj = Object.Instantiate(Resources.Load<GameObject>("UI/SurviveCountdown"), GameManager.Instance.uiContainer.transform);
					var surviveCountdown = countdownGameObj.GetComponent<UISurviveCountdown>();
					surviveCountdown.Init(duration, Team.Bottom);
					break;

				case GameModeType.DestroyTargets:
					Object.Instantiate(Resources.Load<GameObject>("UI/TargetsCounter"), GameManager.Instance.uiContainer.transform);

					PracticeTargetsManager.Instance.OnTargetsChanged += OnPracticeTargetsChanged;
					break;
			}
		}

		private void OnPracticeTargetsChanged(object sender, PracticeTargetsManager.TargetsChangedArgs e)
		{
			if (e.AliveTargets > 0 || e.SpawnedTargets == 0) return; // targets alive or no targets spawned

			// no more targets left -> win the game
			GameManager.Instance.WinGame(Team.Bottom, e.HitPosition);
		}
	}
}