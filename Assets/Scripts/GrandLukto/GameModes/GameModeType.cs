﻿namespace GrandLukto.GameModes
{
	public enum GameModeType
	{
		Versus,
		Survive,
		Challenge,
		DestroyTargets
	}
}