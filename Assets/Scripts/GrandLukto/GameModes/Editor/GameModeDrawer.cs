﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.GameModes
{
	[CustomPropertyDrawer(typeof(GameMode))]
	public class GameModeDrawer : PropertyDrawer
	{
		private EditorHeightCalculator heightCalculator = new EditorHeightCalculator();
		private EditorColumn colMain = new EditorColumn();

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			heightCalculator.Reset();

			var propModeType = property.FindPropertyRelative("modeType");
			heightCalculator.AddProperty(propModeType);

			switch (propModeType.GetEnum<GameModeType>())
			{
				case GameModeType.Survive:
					heightCalculator.AddProperty(property.FindPropertyRelative("duration"));
					break;
			}

			return heightCalculator.Height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			colMain.Reset(position);

			var propModeType = property.FindPropertyRelative("modeType");
			colMain.DrawPropertyField(propModeType);

			switch(propModeType.GetEnum<GameModeType>())
			{
				case GameModeType.Survive:
					var propDuration = property.FindPropertyRelative("duration");
					propDuration.floatValue = EditorGUI.IntSlider(colMain.GetLineRect(), propDuration.displayName, Mathf.FloorToInt(propDuration.floatValue), 30, 240);
					break;
			}
		}
	}
}