﻿using GrandLukto.AIDataCollectors;
using Moq;

namespace GrandLukto.Editor.Mocks.AIDataCollectors
{
	public class IAIDataCollectorMock : Mock<IAIDataCollector>
	{
		public Mock<IGameDataCollector> GameMock { get; private set; }

		public IAIDataCollectorMock()
		{
			GameMock = new Mock<IGameDataCollector>();
			Setup(o => o.Game).Returns(GameMock.Object);
		}
	}
}