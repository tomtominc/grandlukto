﻿using GrandLukto.AIBehaviors.Expressions;
using GrandLukto.AIDataCollectors;
using GrandLukto.Characters;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Expressions
{
	[TestFixture]
	public class ConditionIsBehindLastDefenseLineTest
	{
		private ConditionIsBehindLastDefenseLine condition;
		private AI ai;

		private Mock<ICharacterDataCollector> mockCharacterDataCollector;
		private Mock<IPositionDataCollector> mockPositionDataCollector;

		[SetUp]
		public void SetUp()
		{
			condition = new ConditionIsBehindLastDefenseLine();

			ai = new AI();

			Mock<IAIDataCollector> mockDataCollector = new Mock<IAIDataCollector>();
			condition.DataCollector = mockDataCollector.Object;

			mockCharacterDataCollector = new Mock<ICharacterDataCollector>();
			mockDataCollector.Setup(o => o.Character).Returns(mockCharacterDataCollector.Object);

			mockPositionDataCollector = new Mock<IPositionDataCollector>();
			mockDataCollector.Setup(o => o.Position).Returns(mockPositionDataCollector.Object);
		}
		
		[TestCase(5f, FaceDirection.Down, 5.1f, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		[TestCase(5f, FaceDirection.Down, 4.99f, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		[TestCase(5f, FaceDirection.Up, 5.1f, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		[TestCase(5f, FaceDirection.Up, 4.99f, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		public RAINAction.ActionResult Execute(float lastDefenseLine, FaceDirection faceDirection, float checkPosition)
		{
			condition.positionVariable = new Expression();
			condition.positionVariable.SetVariable("TargetPosition");

			ai.WorkingMemory.SetItem("TargetPosition", new Vector2(0, checkPosition));

			mockCharacterDataCollector.Setup(o => o.FaceVector).Returns(faceDirection.ToDirectionVector());

			mockPositionDataCollector.Setup(o => o.GetLastDefenseLine()).Returns(lastDefenseLine);

			condition.Start(ai);
			return condition.Execute(ai);
		}
	}
}