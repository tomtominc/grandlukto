﻿using GrandLukto.AIBehaviors.Expressions;
using GrandLukto.AIDataCollectors;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Expressions
{
	[TestFixture]
	public class ConditionIsInMoveBoundsTest
	{
		private ConditionIsInMoveBounds condition;
		private AI ai;
		private Mock<IPositionDataCollector> mockPositionDataCollector;

		[SetUp]
		public void SetUp()
		{
			condition = new ConditionIsInMoveBounds();

			ai = new AI();

			Mock<IAIDataCollector> mockDataCollector = new Mock<IAIDataCollector>();
			condition.DataCollector = mockDataCollector.Object;

			mockPositionDataCollector = new Mock<IPositionDataCollector>();
			mockDataCollector.Setup(o => o.Position).Returns(mockPositionDataCollector.Object);
		}

		[TestCase(3.5f, 3.5f, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		[TestCase(4.5f, 4.5f, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		public RAINAction.ActionResult Execute(float positionX, float positionY)
		{
			mockPositionDataCollector
				.Setup(o => o.GetMovementBounds())
				.Returns(new Bounds(new Vector3(5, 5, 10), new Vector3(2, 2, 0.5f)));

			condition.positionVariable = new Expression();
			condition.positionVariable.SetVariable("TargetPosition");

			ai.WorkingMemory.SetItem("TargetPosition", new Vector2(positionX, positionY));
			return condition.Execute(ai);
		}
	}
}