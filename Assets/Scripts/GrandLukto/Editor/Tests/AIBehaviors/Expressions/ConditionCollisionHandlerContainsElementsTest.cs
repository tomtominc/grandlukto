﻿using GrandLukto.AIBehaviors.Expressions;
using GrandLukto.AIDataCollectors;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System.Collections.Generic;
using UnityCave.Collision;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Expressions
{
	[TestFixture]
	public class ConditionCollisionHandlerContainsElementsTest
	{
		private ConditionCollisionHandlerContainsElements condition;
		private AI ai;

		private Mock<ICollisionHandlerManager> mockCollisionHandlers;

		[SetUp]
		public void SetUp()
		{
			condition = new ConditionCollisionHandlerContainsElements();

			ai = new AI();

			Mock<IAIDataCollector> mockDataCollector = new Mock<IAIDataCollector>();
			condition.DataCollector = mockDataCollector.Object;

			mockCollisionHandlers = new Mock<ICollisionHandlerManager>();
			mockDataCollector.Setup(o => o.CollisionHandlers).Returns(mockCollisionHandlers.Object);
		}

		[TestCase(null, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		[TestCase(0, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		[TestCase(1, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		public RAINAction.ActionResult Execute_EmptyCollisionHandler(int? minHandlers)
		{
			condition.collisionHandlerName.SetConstant("Handler_NoCollisions");

			if (minHandlers.HasValue)
			{
				condition.minObjects.SetConstant(minHandlers.Value);
			}

			var collisionHandlerMock = new Mock<ICollisionHandler>();
			collisionHandlerMock.Setup(o => o.ActiveTriggerCollisions).Returns(new List<GameObject>());
			mockCollisionHandlers.Setup(o => o.Get("Handler_NoCollisions")).Returns(collisionHandlerMock.Object);

			condition.Start(ai);
			return condition.Execute(ai);
		}

		[TestCase(null, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		[TestCase(4, ExpectedResult = RAINAction.ActionResult.FAILURE)]
		[TestCase(3, ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		public RAINAction.ActionResult Execute_FilledCollisionHandler(int? minHandlers)
		{
			condition.collisionHandlerName.SetConstant("Handler");

			if (minHandlers.HasValue)
			{
				condition.minObjects.SetConstant(minHandlers.Value);
			}

			var collisionHandlerMock = new Mock<ICollisionHandler>();
			collisionHandlerMock.Setup(o => o.ActiveTriggerCollisions).Returns(new List<GameObject>
			{
				new GameObject(),
				new GameObject(),
				new GameObject()
			});

			mockCollisionHandlers.Setup(o => o.Get("Handler")).Returns(collisionHandlerMock.Object);

			condition.Start(ai);
			return condition.Execute(ai);
		}
	}
}