﻿using GrandLukto.AIBehaviors.Expressions.TypeChecks;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Expressions.TypeChecks
{
	[TestFixture]
	public class ObjectIsAnyTypeBaseTest
	{
		private TestClass objectIsOfType;
		private AI ai;

		[SetUp]
		public void SetUp()
		{
			objectIsOfType = new TestClass();

			ai = new AI();
		}

		[Test]
		public void Execute_GameObjectNotSet()
		{
			objectIsOfType.target.SetVariable("InvalidTarget");

			Assert.AreEqual(RAINAction.ActionResult.FAILURE, objectIsOfType.Execute(ai));
		}

		[TestCase(typeof(TestMonoBehaviourAllowed1), ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		[TestCase(typeof(TestMonoBehaviourAllowed2), ExpectedResult = RAINAction.ActionResult.SUCCESS)]
		[TestCase(typeof(TestMonoBehaviourNotAllowed1), ExpectedResult = RAINAction.ActionResult.FAILURE)]
		public RAINAction.ActionResult Execute_ValidGameObject(Type existingType)
		{
			objectIsOfType.target.SetVariable("TargetGameObject");

			var gameObject = new GameObject();
			gameObject.AddComponent(existingType);

			ai.WorkingMemory.SetItem("TargetGameObject", gameObject);

			return objectIsOfType.Execute(ai);
		}

		private class TestClass : ObjectIsAnyTypeBase
		{
			public override Type[] AllowedTypes
			{
				get
				{
					return new[] { typeof(TestMonoBehaviourAllowed1), typeof(TestMonoBehaviourAllowed2) };
				}
			}
		}

		private class TestMonoBehaviourAllowed1 : MonoBehaviour { }
		private class TestMonoBehaviourAllowed2 : MonoBehaviour { }
		private class TestMonoBehaviourNotAllowed1 : MonoBehaviour { }
	}
}
