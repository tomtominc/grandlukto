﻿using GrandLukto.AIBehaviors.Actions.Vectors;
using GrandLukto.Editor.Mocks.AIDataCollectors;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions.Vectors
{
	[TestFixture]
	public class GetClosestEnemyToLineTest
	{
		private const string VariableNameDistance = "Distance";
		private const string VariableNameEnemy = "Enemy";

		private GetClosestEnemyToLine action;
		private IAIDataCollectorMock dataCollectorMock;
		private AI ai;

		[SetUp]
		public void SetUp()
		{
			ai = new AI();
			action = new GetClosestEnemyToLine();

			dataCollectorMock = new IAIDataCollectorMock();
			action.DataCollector = dataCollectorMock.Object;
		}

		[Test]
		public void NoEnemy_Failure()
		{
			action.lineStart.SetConstant(new Vector2());
			action.lineEnd.SetConstant(new Vector2(0, 1));

			action.Start(ai);
			var result = action.Execute(ai);
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, result);
		}

		[Test]
		public void MultipleEnemies_GetClosestEnemy()
		{
			var farEnemy = new GameObject();
			farEnemy.transform.position = new Vector2(1.1f, 0.5f);

			var closeEnemy = new GameObject();
			closeEnemy.transform.position = new Vector2(1, 0);

			dataCollectorMock.GameMock.Setup(o => o.GetEnemies()).Returns(new List<GameObject>
			{
				farEnemy,
				closeEnemy
			});

			action.lineStart.SetConstant(new Vector2());
			action.lineEnd.SetConstant(new Vector2(0, 1));

			action.targetDistance.SetVariable(VariableNameDistance);
			action.targetEnemy.SetVariable(VariableNameEnemy);

			action.Start(ai);
			var result = action.Execute(ai);
			
			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, result);
			Assert.AreEqual(closeEnemy, ai.WorkingMemory.GetItem(VariableNameEnemy));
			Assert.AreEqual(1, ai.WorkingMemory.GetItem(VariableNameDistance));
		}
	}
}