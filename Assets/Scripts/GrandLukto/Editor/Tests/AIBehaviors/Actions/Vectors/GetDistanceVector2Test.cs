﻿using GrandLukto.AIBehaviors.Actions.Vectors;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System.Collections;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions.Vectors
{
	[TestFixture]
	public class GetDistanceVector2Test
	{
		private GetDistanceVector2 action;
		private AI ai;

		[SetUp]
		public void SetUp()
		{
			action = new GetDistanceVector2();
			ai = new AI();
		}
		
		[TestCaseSource("ExecuteCases")]
		public float Execute(Vector2 posA, Vector2 posB)
		{
			action.targetVariable.SetVariable("EvaluatedDistance");

			ai.WorkingMemory.SetItem("ObjectA", posA);
			action.objectA.SetVariable("ObjectA");

			ai.WorkingMemory.SetItem("ObjectB", posB);
			action.objectB.SetVariable("ObjectB");

			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, action.Execute(ai));
			return ai.WorkingMemory.GetItem<float>("EvaluatedDistance");
		}

		public static IEnumerable ExecuteCases
		{
			get
			{
				yield return
					new TestCaseData(new Vector2(0, 0), new Vector2(5, 0))
					.Returns(5f);

				yield return
					new TestCaseData(new Vector2(0, -3), new Vector2(0, 3))
					.Returns(6f);
			}
		}
	}
}