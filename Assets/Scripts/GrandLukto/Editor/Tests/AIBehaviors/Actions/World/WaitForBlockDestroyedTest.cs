﻿using GrandLukto.AIBehaviors.Actions.World;
using GrandLukto.AIDataCollectors;
using GrandLukto.Blocks;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System;
using System.Collections;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions.World
{
	[TestFixture]
	public class WaitForBlockDestroyedTest
	{
		private AI ai;

		private Mock<IAIDataCollector> mockDataCollector;
		private Mock<IBlockDataCollector> mockBlockDataCollector;
		private Mock<IBlockEvents> mockBlockEvents;
		private Mock<ICharacterDataCollector> mockCharacterData;

		[SetUp]
		public void SetUp()
		{
			ai = new AI();

			mockDataCollector = new Mock<IAIDataCollector>();

			mockBlockDataCollector = new Mock<IBlockDataCollector>();
			mockDataCollector.Setup(o => o.Blocks).Returns(mockBlockDataCollector.Object);

			mockBlockEvents = new Mock<IBlockEvents>();
			mockBlockDataCollector.Setup(o => o.Events).Returns(mockBlockEvents.Object);
		}

		[TestCaseSource("Execute_StandardImplementationCases")]
		public void Execute_StandardImplementation(Team team, Type monoBehaviourToAdd, RAINAction.ActionResult result)
		{
			var action = new WaitForBlockDestroyed();
			action.DataCollector = mockDataCollector.Object;

			action.Start(ai);
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(ai));
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(ai));

			var block = new GameObject();
			var component = (BlockBase)block.AddComponent(monoBehaviourToAdd);

			mockBlockEvents.Raise(o => o.BlockDestroyed += null, new BlockDestroyedEventArgs(team, component));
			Assert.AreEqual(result, action.Execute(ai));
			action.Stop(ai);

			action.Start(ai);
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(ai));
		}

		public static IEnumerable Execute_StandardImplementationCases
		{
			get
			{
				yield return new TestCaseData(Team.Top, typeof(BlockBase), RAINAction.ActionResult.SUCCESS);
				yield return new TestCaseData(Team.Bottom, typeof(TestBlock), RAINAction.ActionResult.SUCCESS);
			}
		}

		[Test]
		public void Execute_OverridenIsMatchingEventMethod()
		{
			var action = new WaitForTestBlockDestroyed();
			action.DataCollector = mockDataCollector.Object;

			action.Start(ai);

			var blockBase = new GameObject().AddComponent<BlockBase>();
			mockBlockEvents.Raise(o => o.BlockDestroyed += null, new BlockDestroyedEventArgs(Team.Top, blockBase));
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(ai));

			var testBlock = new GameObject().AddComponent<TestBlock>();
			mockBlockEvents.Raise(o => o.BlockDestroyed += null, new BlockDestroyedEventArgs(Team.Top, testBlock));
			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, action.Execute(ai));
			action.Stop(ai);

			action.Start(ai);
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(ai));
		}

		private class TestBlock : BlockBase
		{
		}

		private class WaitForTestBlockDestroyed : WaitForBlockDestroyed
		{
			protected override bool IsMatchingEvent(object sender, BlockDestroyedEventArgs e)
			{
				return e.Block is TestBlock;
			}
		}
	}
}