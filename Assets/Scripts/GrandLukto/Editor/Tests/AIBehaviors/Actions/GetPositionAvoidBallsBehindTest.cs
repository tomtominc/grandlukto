﻿using GrandLukto.AIBehaviors.Actions;
using GrandLukto.AIDataCollectors;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System.Collections;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions
{
	public class GetPositionAvoidBallsBehindTest
	{
		private GetPositionAvoidBallsBehind action;
		private AI ai;

		private Mock<ICharacterDataCollector> mockCharacterDataCollector;
		private Mock<IPositionDataCollector> mockPositionDataCollector;

		[SetUp]
		public void SetUp()
		{
			action = new GetPositionAvoidBallsBehind();

			ai = new AI();

			Mock<IAIDataCollector> mockDataCollector = new Mock<IAIDataCollector>();
			action.DataCollector = mockDataCollector.Object;

			mockCharacterDataCollector = new Mock<ICharacterDataCollector>();
			mockDataCollector.Setup(o => o.Character).Returns(mockCharacterDataCollector.Object);

			mockPositionDataCollector = new Mock<IPositionDataCollector>();
			mockDataCollector.Setup(o => o.Position).Returns(mockPositionDataCollector.Object);
		}

		public static IEnumerable ExecuteCases
		{
			get
			{
				yield return
					new TestCaseData(new Vector2(2.5f, 0), 0.5f, 0.5f)
					.Returns(new Vector2(7.5f, 5f))
					.SetDescription("Avoid to right");

				yield return
					new TestCaseData(new Vector2(7.5f, 0), 0.5f, 0.5f)
					.Returns(new Vector2(2.5f, 5f))
					.SetDescription("Avoid to left");

				yield return
					new TestCaseData(new Vector2(7.5f, 0), 0.5f, 1f)
					.Returns(new Vector2(2.5f, 10f))
					.SetDescription("Avoid to left - different y value");

				yield return
					new TestCaseData(new Vector2(4f, 0), 1f, 0.5f)
					.Returns(new Vector2(10f, 5f))
					.SetDescription("Avoid to right - do not overshoot x");
			}
		}
		
		[Test, TestCaseSource("ExecuteCases")]
		public Vector2 Execute(Vector2 playerPosition, float avoidXRel, float targetYRel)
		{
			ai.Body = new GameObject();
			ai.Body.transform.position = playerPosition;

			action.avoidXRel.SetConstant(avoidXRel);
			action.targetYRel.SetConstant(targetYRel);

			action.targetVariable.SetVariable("MoveTarget");

			mockPositionDataCollector
				.Setup(o => o.ConvertToRelativePosition(It.IsAny<Vector2>()))
				.Returns<Vector2>((vector) => new Vector2(Mathf.InverseLerp(0, 10, vector.x), Mathf.InverseLerp(0, 10, vector.y)));

			mockPositionDataCollector
				.Setup(o => o.ConvertToAbsolutePosition(It.IsAny<Vector2>()))
				.Returns<Vector2>((vector) => new Vector2(Mathf.Lerp(0, 10, vector.x), Mathf.Lerp(0, 10, vector.y)));

			var result = action.Execute(ai);

			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, result);
			return ai.WorkingMemory.GetItem<Vector2>("MoveTarget");
		}
	}
}