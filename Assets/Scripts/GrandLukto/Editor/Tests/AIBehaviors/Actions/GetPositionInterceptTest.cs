﻿using GrandLukto.AIBehaviors.Actions;
using GrandLukto.AIDataCollectors;
using GrandLukto.Characters;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions
{
	[TestFixture]
	public class GetPositionInterceptTest
	{
		private GetPositionIntercept condition;
		private AI ai;

		private Mock<ICharacterDataCollector> mockCharacterDataCollector;
		private Mock<IPositionDataCollector> mockPositionDataCollector;

		[SetUp]
		public void SetUp()
		{
			condition = new GetPositionIntercept();

			ai = new AI();

			Mock<IAIDataCollector> mockDataCollector = new Mock<IAIDataCollector>();
			condition.DataCollector = mockDataCollector.Object;

			mockCharacterDataCollector = new Mock<ICharacterDataCollector>();
			mockDataCollector.Setup(o => o.Character).Returns(mockCharacterDataCollector.Object);

			mockPositionDataCollector = new Mock<IPositionDataCollector>();
			mockDataCollector.Setup(o => o.Position).Returns(mockPositionDataCollector.Object);
		}
		
		[TestCase(FaceDirection.Up, 10, 10, 5, 12, 0, 0, RAINAction.ActionResult.FAILURE, Description = "Object moves away from intercept line")]
		[TestCase(FaceDirection.Up, 10, 4, 10, 2, 0, 0, RAINAction.ActionResult.FAILURE, Description = "Object behind intercept line")]
		[TestCase(FaceDirection.Up, 10, 10, 18, 0, 14, 4, RAINAction.ActionResult.SUCCESS)]
		[TestCase(FaceDirection.Down, 10, 2, 4, 8, 7, 6, RAINAction.ActionResult.SUCCESS)]
		public void Execute(FaceDirection faceDirection, float x1, float y1, float x2, float y2, float targetX, float targetY, RAINAction.ActionResult expectedResult)
		{
			condition.lineToInterceptPointAVariable.SetConstant(new Vector2(x1, y1));
			condition.lineToInterceptPointBVariable.SetConstant(new Vector2(x2, y2));
			condition.relativeYVariable.SetConstant(0f); // always last defense line for this test (at y = 5)
			condition.targetVariable.SetVariable("MoveTarget");

			mockCharacterDataCollector.Setup(o => o.FaceVector).Returns(faceDirection.ToDirectionVector());
			mockCharacterDataCollector.Setup(o => o.HitOffset).Returns(1);

			mockPositionDataCollector
				.Setup(o => o.GetRelativeHitPosition(It.IsAny<float>(), It.IsAny<float>()))
				.Returns<float, float>((relX, relY) => new Vector2(relX, 5f));

			condition.Start(ai);

			Assert.AreEqual(expectedResult, condition.Execute(ai));

			if (expectedResult == RAINAction.ActionResult.SUCCESS)
			{
				Assert.AreEqual(new Vector2(targetX, targetY), ai.WorkingMemory.GetItem<Vector2>("MoveTarget"));
			}
		}
	}
}