﻿using GrandLukto.AIBehaviors.Actions.PowerUps;
using GrandLukto.Blocks;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions.PowerUps
{
	[TestFixture]
	public class GetPowerUpSpawnerPositionTest
	{
		private const string VariableNamePowerUpSpawner = "powerUpSpawner";
		private const string VariableNameTargetPosition = "targetPosition";

		private GetPowerUpSpawnerPosition action;
		private AI ai;

		public void SetUp()
		{
			action = new GetPowerUpSpawnerPosition();
			action.targetPosition.SetVariable(VariableNameTargetPosition);
			action.powerUpSpawner.SetVariable(VariableNamePowerUpSpawner);

			ai = new AI();
		}

		public void Execute_SpawnerNotSet()
		{
			var result = action.Execute(ai);

			Assert.AreEqual(RAINAction.ActionResult.FAILURE, result);
		}

		public void Execute_SpawnerSet_SetTargetPosition()
		{
			var mockSpawner = new Mock<IPowerUpSpawner>();

			var givenCenterPosition = new Vector2(10, 10);
			mockSpawner.Setup(o => o.CenterPosition).Returns(givenCenterPosition);

			ai.WorkingMemory.SetItem(VariableNamePowerUpSpawner, mockSpawner.Object);

			var result = action.Execute(ai);

			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, result);
			Assert.AreEqual(givenCenterPosition, ai.WorkingMemory.GetItem(VariableNameTargetPosition));
		}
	}
}