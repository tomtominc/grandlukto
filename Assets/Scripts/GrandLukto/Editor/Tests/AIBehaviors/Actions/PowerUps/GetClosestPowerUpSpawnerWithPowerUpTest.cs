﻿using GrandLukto.AIBehaviors.Actions.PowerUps;
using GrandLukto.AIDataCollectors;
using GrandLukto.Blocks;
using Moq;
using NUnit.Framework;
using RAIN.Action;
using RAIN.Core;
using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIBehaviors.Actions.PowerUps
{
	[TestFixture]
	public class GetClosestPowerUpSpawnerWithPowerUpTest
	{
		private GetClosestPowerUpSpawnerWithPowerUp action;
		private Mock<IBlockDataCollector> mockBlockDataCollector;
		private Mock<IPositionDataCollector> mockPositionDataCollector;

		private AI givenAI;

		[SetUp]
		public void SetUp()
		{
			givenAI = new AI();

			var mockAIDataCollector = new Mock<IAIDataCollector>();

			mockBlockDataCollector = new Mock<IBlockDataCollector>();
			mockAIDataCollector.Setup(o => o.Blocks).Returns(mockBlockDataCollector.Object);

			mockPositionDataCollector = new Mock<IPositionDataCollector>();
			mockAIDataCollector.Setup(o => o.Position).Returns(mockPositionDataCollector.Object);

			action = new GetClosestPowerUpSpawnerWithPowerUp();
			action.DataCollector = mockAIDataCollector.Object;
		}

		[TestCase]
		public void NoPowerUpsSpawned_Failure()
		{
			var spawnerA = new Mock<IPowerUpSpawner>();
			spawnerA.Setup(o => o.IsPowerUpSpawned).Returns(false);
			spawnerA.Setup(o => o.CenterPosition).Returns(new Vector2(5, 5));

			var spawnerB = new Mock<IPowerUpSpawner>();
			spawnerB.Setup(o => o.IsPowerUpSpawned).Returns(false);
			spawnerB.Setup(o => o.CenterPosition).Returns(new Vector2(10, 10));

			mockBlockDataCollector
				.Setup(o => o.GetTilePrefabsWithComponent<IPowerUpSpawner>())
				.Returns(new List<IPowerUpSpawner>()
				{
					spawnerA.Object,
					spawnerB.Object
				});

			action.Start(givenAI);
			Assert.AreEqual(RAINAction.ActionResult.FAILURE, action.Execute(givenAI));
		}

		[TestCase]
		public void PowerUpsSpawned_SuccessAndSetClosestSpawner()
		{
			var spawnerA = new Mock<IPowerUpSpawner>();
			spawnerA.Setup(o => o.IsPowerUpSpawned).Returns(false);
			spawnerA.Setup(o => o.CenterPosition).Returns(new Vector2(5, 5));

			var spawnerB = new Mock<IPowerUpSpawner>();
			spawnerB.Setup(o => o.IsPowerUpSpawned).Returns(true);
			spawnerB.Setup(o => o.CenterPosition).Returns(new Vector2(10, 10));

			var spawnerC = new Mock<IPowerUpSpawner>();
			spawnerC.Setup(o => o.IsPowerUpSpawned).Returns(true);
			spawnerC.Setup(o => o.CenterPosition).Returns(new Vector2(-6, -6));

			mockBlockDataCollector
				.Setup(o => o.GetTilePrefabsWithComponent<IPowerUpSpawner>())
				.Returns(new List<IPowerUpSpawner>()
				{
					spawnerA.Object,
					spawnerB.Object,
					spawnerC.Object
				});

			action.Start(givenAI);
			action.targetSpawner.SetVariable("targetSpawner");
			var result = action.Execute(givenAI);

			Assert.AreEqual(RAINAction.ActionResult.SUCCESS, result);
			Assert.AreEqual(spawnerC.Object, givenAI.WorkingMemory.GetItem<IPowerUpSpawner>("targetSpawner"));
		}
	}
}