﻿using GrandLukto.AIDataCollectors;
using GrandLukto.Characters;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine;

namespace GrandLukto.Editor.Tests.AIDataCollectors
{
	[TestFixture]
	public class PositionDataCollectorTest
	{
		private PositionDataCollector dataCollector;

		private Mock<ICharacterDataCollector> mockCharacter;
		private Mock<IAIAreaDescription> mockAreaDescription;
		private GameObject givenBody;

		[SetUp]
		public void SetUp()
		{
			mockCharacter = new Mock<ICharacterDataCollector>();
			mockAreaDescription = new Mock<IAIAreaDescription>();

			givenBody = new GameObject();

			dataCollector = new PositionDataCollector(
				givenBody,
				mockCharacter.Object,
				mockAreaDescription.Object
			);
		}

		[TestCase(Team.Bottom, 0.5f, 0, 5, 2.5f)]
		[TestCase(Team.Top, 0.5f, 0, 5, 7.5f)]
		[TestCase(Team.Bottom, 0.5f, 1, 5, 7.5f)]
		[TestCase(Team.Top, 0.5f, 1, 5, 2.5f)]
		[TestCase(Team.Bottom, 0, 0.5f, 2.5f, 5f)]
		[TestCase(Team.Bottom, 1, 0.5f, 7.5f, 5f)]
		public void GetRelativeMovePosition(Team team, float relX, float relY, float expectedX, float expectedY)
		{
			mockCharacter.Setup(o => o.Team).Returns(team);

			var bounds = new Bounds(new Vector3(5, 5, 0), new Vector3(5, 5, 1));
			mockAreaDescription.Setup(o => o.GetBoundsWithoutBlocks(It.IsAny<float>())).Returns(bounds);

			var result = dataCollector.GetRelativeMovePosition(relX, relY);
			Assert.AreEqual(new Vector2(expectedX, expectedY), result);
		}

		[TestCase(Team.Top, 5, 5, 0.5f, 0.5f)]
		[TestCase(Team.Bottom, 5, 5, 0.5f, 0.5f)]
		[TestCase(Team.Bottom, 2.5f, 2.5f, 0f, 0f)]
		[TestCase(Team.Top, 2.5f, 2.5f, 0f, 1f)]
		[TestCase(Team.Bottom, 7.5f, 7.5f, 1f, 1f)]
		[TestCase(Team.Top, 7.5f, 7.5f, 1f, 0f)]
		public void ConvertBetweenRelativeAndAbsolutePosition(Team team, float posX, float posY, float expectedXRel, float expectedYRel)
		{
			mockCharacter.Setup(o => o.Team).Returns(team);

			var bounds = new Bounds(new Vector3(5, 5, 0), new Vector3(5, 5, 1));
			mockAreaDescription.Setup(o => o.GetBoundsWithoutBlocks(It.IsAny<float>())).Returns(bounds);

			var resultRel = dataCollector.ConvertToRelativePosition(new Vector2(posX, posY));
			Assert.AreEqual(new Vector2(expectedXRel, expectedYRel), resultRel);

			var resultAbs = dataCollector.ConvertToAbsolutePosition(new Vector2(expectedXRel, expectedYRel));
			Assert.AreEqual(new Vector2(posX, posY), resultAbs);
		}

		public static IEnumerable IsMovingTowardsMeSource
		{
			get
			{
				yield return
					new TestCaseData(Team.Top, new Vector2(11, 5), Vector2.up)
					.SetDescription("in front of character; moving towards character")
					.Returns(true);

				yield return
					new TestCaseData(Team.Top, new Vector2(11, 5), Vector2.down)
					.SetDescription("in front of character; moving away from character")
					.Returns(false);

				yield return
					new TestCaseData(Team.Top, new Vector2(3, 15), Vector2.up)
					.SetDescription("behind character; moving away from character")
					.Returns(false);

				yield return
					new TestCaseData(Team.Top, new Vector2(11, 15), Vector2.down)
					.SetDescription("behind character; moving towards character")
					.Returns(false);

				yield return
					new TestCaseData(Team.Bottom, new Vector2(3, 5), new Vector2(1, -1).normalized)
					.SetDescription("different team; in front of character; moving angled towards character")
					.Returns(false);

				yield return
					new TestCaseData(Team.Bottom, new Vector2(3, 5), new Vector2(-1, 1).normalized)
					.SetDescription("different team; in front of character; moving angled away from character")
					.Returns(false);
			}
		}

		[TestCaseSource("IsMovingTowardsMeSource")]
		public bool IsMovingTowardsMe(Team team, Vector2 paramPosition, Vector2 paramDirection)
		{
			mockCharacter.Setup(o => o.Team).Returns(team);
			mockCharacter.Setup(o => o.FaceVector).Returns(team.GetFaceDirection().ToDirectionVector());

			givenBody.transform.position = new Vector2(10, 10);

			return dataCollector.IsMovingTowardsMe(paramPosition, paramDirection);
		}

		public static IEnumerable IsLookingAtMeSource
		{
			get
			{
				yield return new TestCaseData(Team.Top, Vector2.up).Returns(true);
				yield return new TestCaseData(Team.Top, Vector2.down).Returns(false);
				yield return new TestCaseData(Team.Bottom, Vector2.up).Returns(false);
				yield return new TestCaseData(Team.Bottom, Vector2.down).Returns(true);
			}
		}

		[TestCaseSource("IsLookingAtMeSource")]
		public bool IsLookingAtMe(Team team, Vector2 lookDirection)
		{
			mockCharacter.Setup(o => o.Team).Returns(team);
			mockCharacter.Setup(o => o.FaceVector).Returns(team.GetFaceDirection().ToDirectionVector());

			return dataCollector.IsLookingAtMe(lookDirection);
		}
	}
}