﻿using System.Collections;
using UnityCave.Collision;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public interface IAIDataCollector
	{
		IPositionDataCollector Position { get; }
		ICharacterDataCollector Character { get; }
		ICollisionHandlerManager CollisionHandlers { get; }
		IBlockDataCollector Blocks { get; }
		IMovementDataCollector Movement { get; }
		IGameDataCollector Game { get; }

		Coroutine StartCoroutine(IEnumerator routine);
		void StopCoroutine(Coroutine routine);
	}
}