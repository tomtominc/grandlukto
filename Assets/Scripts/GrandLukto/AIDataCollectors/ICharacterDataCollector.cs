﻿using GrandLukto.PowerUps;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public interface ICharacterDataCollector
	{
		/// <summary>
		/// The team which the AI belongs to.
		/// </summary>
		Team Team { get; }

		/// <summary>
		/// Direction in which the character is looking.
		/// </summary>
		Vector2 FaceVector { get; }

		/// <summary>
		/// Returns a value that can be multiplied to a y value so that a positive y value points away from the AI's blocks.
		/// </summary>
		float FaceYMultiplier { get; }

		/// <summary>
		/// Y offset between the AI and the ball so that it can hit the ball.
		/// </summary>
		float HitOffset { get; }

		/// <summary>
		/// Returns the radius of the character's circle collider.
		/// </summary>
		float ColliderSize { get; }

		/// <summary>
		/// Returns true if the character has an empty PowerUp slot.
		/// </summary>
		bool HasEmptyPowerUpSlot { get; }

		/// <summary>
		/// Returns an evaluated random reaction time in seconds.
		/// </summary>
		float GetRandomReactionTime();

		/// <summary>
		/// Tries to get the next available powerup from the first slot.
		/// </summary>
		bool TryGetNextPowerUpInQueue(out PowerUpType powerUp);

		/// <summary>
		/// Tries to use a powerup and returns true if it was used.
		/// </summary>
		bool TryUsePowerUp();

		/// <summary>
		/// Returns true if any currently active effect on the character can be interrupted.
		/// </summary>
		bool HasActiveInterruptableEffect();

		/// <summary>
		/// Returns true if the provided <see cref="minimumTime"/> in seconds has elapsed since the last powerup usage
		/// or no powerup has been used yet, otherwise false.
		/// </summary>
		bool MinimumTimeElapsedSinceLastPowerUpUsage(float minimumTimeSeconds);
	}
}