﻿using System.Collections.Generic;
using System.Linq;
using UnityCave;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public class GameDataCollector : IGameDataCollector
	{
		private readonly EntityManager entityManager;
		private readonly GameManager gameManager;
		private readonly ICharacterDataCollector characterDataCollector;

		public GameDataCollector(
			EntityManager entityManager,
			GameManager gameManager,
			ICharacterDataCollector characterDataCollector)
		{
			this.entityManager = entityManager;
			this.gameManager = gameManager;
			this.characterDataCollector = characterDataCollector;
		}

		public IEnumerable<GameObject> GetEnemies()
		{
			return gameManager
				.PlayersByTeam[characterDataCollector.Team.GetOpponent()]
				.Select(setting => setting.PlayerObject);
		}
		
		public IEnumerable<GameObject> GetEntities<TEntity>()
		{
			return entityManager.GetAll<TEntity>();
		}
	}
}