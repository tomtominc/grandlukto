﻿using GrandLukto.Blocks;
using System.Collections.Generic;

namespace GrandLukto.AIDataCollectors
{
	public interface IBlockDataCollector
	{
		IBlockEvents Events { get; }

		List<TComponent> GetTilePrefabsWithComponent<TComponent>();

		/// <summary>
		/// Fortifies all blocks on the character's field for the given duration.
		/// </summary>
		void FortifyAllBlocks(float totalDuration);
	}
}