﻿using GrandLukto.Characters;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public class PositionDataCollector : IPositionDataCollector
	{
		private readonly GameObject body;
		private readonly ICharacterDataCollector character;
		private readonly IAIAreaDescription areaDescription;

		public int MaxTilesX { get { return 11; } }
		public int MaxTilesY { get { return 12; } }

		public Vector2 Position
		{
			get { return body.transform.position; }
		}

		public PositionDataCollector(GameObject body, ICharacterDataCollector character, IAIAreaDescription areaDescription)
		{
			this.body = body;
			this.character = character;
			this.areaDescription = areaDescription;
		}

		public float GetLastDefenseLine()
		{
			return GetRelativeHitPosition(0.5f, 0).y;
		}

		public Vector2 GetRelativeHitPosition(float relX, float relY)
		{
			var movePosition = GetRelativeMovePosition(relX, relY);
			return movePosition + character.FaceVector * character.HitOffset;
		}

		public Vector2 GetRelativeMovePosition(float relX, float relY)
		{
			if (character.Team == Team.Top)
			{
				relY = 1 - relY; // look in the different direction
			}

			var movementBounds = GetMovementBounds();
			return new Vector2(
				Mathf.Lerp(movementBounds.min.x, movementBounds.max.x, relX),
				Mathf.Lerp(movementBounds.min.y, movementBounds.max.y, relY)
			);
		}

		/// <summary>
		/// Returns the bounds in which the AI can move.
		/// </summary>
		public Bounds GetMovementBounds()
		{
			return areaDescription.GetBoundsWithoutBlocks(character.ColliderSize);
		}
		
		public Vector2 ConvertToRelativePosition(Vector2 position)
		{
			var bounds = GetMovementBounds();
			var positionRel = new Vector2(
				Mathf.InverseLerp(bounds.min.x, bounds.max.x, position.x),
				Mathf.InverseLerp(bounds.min.y, bounds.max.y, position.y));

			if (character.Team == Team.Top)
			{
				positionRel.y = 1 - positionRel.y; // look in the different direction
			}

			return positionRel;
		}

		public Vector2 ConvertToAbsolutePosition(Vector2 positionRel)
		{
			if (character.Team == Team.Top)
			{
				positionRel.y = 1 - positionRel.y; // look in the different direction
			}

			var bounds = GetMovementBounds();
			var position = new Vector2(
				Mathf.Lerp(bounds.min.x, bounds.max.x, positionRel.x),
				Mathf.Lerp(bounds.min.y, bounds.max.y, positionRel.y));

			return position;
		}

		public bool IsLookingAtMe(Vector2 lookDirection)
		{
			return Vector2.Dot(character.FaceVector, lookDirection.normalized) < 0;
		}

		public bool IsMovingTowardsMe(Vector2 position, Vector2 moveDirection)
		{
			var bodyToPosition = position - (Vector2)body.transform.position;

			// check if position is behind body
			if (Vector2.Dot(character.FaceVector, bodyToPosition.normalized) <= 0) return false;

			return IsLookingAtMe(moveDirection);
		}
	}
}