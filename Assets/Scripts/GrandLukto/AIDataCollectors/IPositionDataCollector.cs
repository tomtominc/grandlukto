﻿using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public interface IPositionDataCollector
	{
		int MaxTilesX { get; }
		int MaxTilesY { get; }

		/// <summary>
		/// Returns the AI's world position.
		/// </summary>
		Vector2 Position { get; }

		/// <summary>
		/// Returns the closest y value to the blocks at which the ball must be so that the AI can hit it.
		/// </summary>
		/// <returns>Global y position.</returns>
		float GetLastDefenseLine();

		/// <summary>
		/// Gets a relative position on the field at which the ball must be so that the character can hit the ball.
		/// </summary>
		Vector2 GetRelativeHitPosition(float relX, float relY);

		/// <summary>
		/// Gets a relative position on the field to which the character can move.
		/// </summary>
		/// <param name="relX">Relative X value where 0 is left and 1 is right.</param>
		/// <param name="relY">Relative Y value where 0 is close to the blocks and 1 is the center of the field.</param>
		Vector2 GetRelativeMovePosition(float relX, float relY);

		/// <summary>
		/// Returns the bounds in which the character can move (blocks excluded).
		/// </summary>
		Bounds GetMovementBounds();

		/// <summary>
		/// Converts the given position to a relative position in the movement bounds.
		/// </summary>
		Vector2 ConvertToRelativePosition(Vector2 position);

		/// <summary>
		/// Converts the given relative position to an absolute position in the movement bounds.
		/// </summary>
		Vector2 ConvertToAbsolutePosition(Vector2 positionRel);
		
		/// <summary>
		/// Returns true if the given look direction is looking at the character.
		/// </summary>
		bool IsLookingAtMe(Vector2 lookDirection);

		/// <summary>
		/// Returns true if the object at the provided position is in front of the character and moving towards the character.
		/// </summary>
		bool IsMovingTowardsMe(Vector2 position, Vector2 moveDirection);
	}
}