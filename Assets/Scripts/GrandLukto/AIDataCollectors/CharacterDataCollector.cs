﻿using GrandLukto.Characters;
using GrandLukto.PowerUps;
using System.Linq;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public class CharacterDataCollector : ICharacterDataCollector
	{
		private readonly CharacterBase character;
		private readonly PowerUpManager powerUpManager;
		private readonly PlayerPowerUp playerPowerUp;
		private readonly EffectsManager effectsManager;

		public Vector2 FaceVector { get { return character.FaceDirectionVec; } }

		public float HitOffset { get { return 0.35f; } }

		public float ColliderSize { get { return character.Size; } }

		public float FaceYMultiplier { get; private set; }

		public Team Team { get { return character.Team; } }

		public bool HasEmptyPowerUpSlot { get { return !powerUpManager.IsQueueFull; } }

		public CharacterDataCollector(
			CharacterBase character,
			PowerUpManager powerUpManager,
			PlayerPowerUp playerPowerUp,
			EffectsManager effectsManager)
		{
			this.character = character;
			this.powerUpManager = powerUpManager;
			this.playerPowerUp = playerPowerUp;
			this.effectsManager = effectsManager;

			FaceYMultiplier = character.Team == Team.Top ? -1 : 1;
		}

		public float GetRandomReactionTime()
		{
			return character.PlayerSetting.aiDefinition.randReactionTime;
		}

		public bool TryGetNextPowerUpInQueue(out PowerUpType powerUp)
		{
			powerUp = PowerUpType.None;
			if (powerUpManager.PowerUpQueue.Count < 1) return false;

			powerUp = powerUpManager.PowerUpQueue.Peek();
			return true;
		}

		public bool TryUsePowerUp()
		{
			return playerPowerUp.TryUsePowerUp();
		}

		public bool HasActiveInterruptableEffect()
		{
			return effectsManager.ActiveEffects.SingleOrDefault(effect => effect.CanBeInterrupted()) != null;
		}

		public bool MinimumTimeElapsedSinceLastPowerUpUsage(float minimumTimeSeconds)
		{
			if (!powerUpManager.LastPowerUpUsed.HasValue) return true;

			var timeSinceLastUsage = Time.time - powerUpManager.LastPowerUpUsed;
			return timeSinceLastUsage > minimumTimeSeconds;
		}
	}
}