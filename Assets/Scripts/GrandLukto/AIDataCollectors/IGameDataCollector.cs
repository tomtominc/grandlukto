﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public interface IGameDataCollector
	{
		IEnumerable<GameObject> GetEnemies();

		/// <summary>
		/// Gets all entites of a type.
		/// </summary>
		IEnumerable<GameObject> GetEntities<TEntity>();
	}
}