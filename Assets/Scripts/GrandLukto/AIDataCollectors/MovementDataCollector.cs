﻿using GrandLukto.Characters;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public class MovementDataCollector : IMovementDataCollector
	{
		private readonly NPCInput npcInput;

		public MovementDataCollector(NPCInput npcInput)
		{
			this.npcInput = npcInput;
		}

		public bool MoveTargetReached
		{
			get { return npcInput.MoveTargetReached; }
		}

		public void MoveTo(Vector2 target)
		{
			npcInput.MoveTo(target);
		}
	}
}