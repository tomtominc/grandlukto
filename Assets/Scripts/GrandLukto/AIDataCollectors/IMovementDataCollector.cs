﻿using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	public interface IMovementDataCollector
	{
		/// <summary>
		/// Returns true if the latest set target is reached (with some offset).
		/// </summary>
		bool MoveTargetReached { get; }

		/// <summary>
		/// Tells the character to move to the given target in the next update call.
		/// </summary>
		void MoveTo(Vector2 target);
	}
}