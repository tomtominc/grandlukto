﻿using GrandLukto.Blocks;
using GrandLukto.Blocks.BlockModifiers;
using System.Collections.Generic;

namespace GrandLukto.AIDataCollectors
{
	public class BlockDataCollector : IBlockDataCollector
	{
		private readonly ICharacterDataCollector characterDataCollector;

		public IBlockEvents Events { get; private set; }

		private BlockGroup BlockGroup
		{
			get
			{
				return GameManager.Instance.BlockGroupByTeam[characterDataCollector.Team];
			}
		}

		public BlockDataCollector(ICharacterDataCollector characterDataCollector)
		{
			Events = BlockEvents.Instance;
			this.characterDataCollector = characterDataCollector;
		}

		public List<TComponent> GetTilePrefabsWithComponent<TComponent>()
		{
			return BlockGroup.GetTilePrefabsWithComponent<TComponent>();
		}

		public void FortifyAllBlocks(float totalDuration)
		{
			new Fortify(BlockGroup).FortifyAll(totalDuration);
		}
	}
}