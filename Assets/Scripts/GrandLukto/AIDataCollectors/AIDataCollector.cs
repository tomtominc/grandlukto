﻿using GrandLukto.Characters;
using GrandLukto.PowerUps;
using RAIN.Core;
using System.Collections;
using UnityCave;
using UnityCave.Collision;
using UnityEngine;

namespace GrandLukto.AIDataCollectors
{
	/// <summary>
	/// Groups the AI DataCollectors.
	/// </summary>
	public class AIDataCollector : IAIDataCollector
	{
		private CharacterBase characterBase;

		public IPositionDataCollector Position { get; private set; }
		public ICharacterDataCollector Character { get; private set; }
		public ICollisionHandlerManager CollisionHandlers { get; private set; }
		public IBlockDataCollector Blocks { get; private set; }
		public IMovementDataCollector Movement { get; private set; }
		public IGameDataCollector Game { get; private set; }

		public AIDataCollector(AI ai)
		{
			characterBase = ai.Body.GetComponent<CharacterBase>();

			Character = new CharacterDataCollector(
				characterBase,
				ai.Body.GetComponent<PowerUpManager>(),
				ai.Body.GetComponent<PlayerPowerUp>(),
				ai.Body.GetComponent<EffectsManager>());

			var gameManager = GameManager.Instance;
			var entityManager = EntityManager.Instance;
			var blockGroup = gameManager.BlockGroupByTeam[Character.Team];

			Position = new PositionDataCollector(
				ai.Body,
				Character,
				blockGroup.GetComponentInChildren<IAIAreaDescription>()
			);

			CollisionHandlers = ai.Body.GetComponentInChildren<ICollisionHandlerManager>();

			Blocks = new BlockDataCollector(Character);

			Movement = new MovementDataCollector(ai.Body.GetComponent<NPCInput>());

			Game = new GameDataCollector(entityManager, gameManager, Character);
		}

		public Coroutine StartCoroutine(IEnumerator routine)
		{
			return characterBase.StartCoroutine(routine);
		}

		public void StopCoroutine(Coroutine routine)
		{
			characterBase.StopCoroutine(routine);
		}
	}
}