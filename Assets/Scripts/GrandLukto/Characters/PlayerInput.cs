﻿using Rewired;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerInput : CharacterInputBase
	{
		private CharacterBase compCharacterBase;

		private Player Player
		{
			get
			{
				if (compCharacterBase == null)
				{
					Debug.Log("null 2", gameObject);
				}

				return compCharacterBase.Player;
			}
		}

		public void Awake()
		{
			compCharacterBase = GetComponent<CharacterBase>();
		}

		public override bool ShootStart
		{
			get
			{
				return Player.GetButtonDown(Control.Shoot);
			}
		}

		public override bool ShootCharge
		{
			get
			{
				return Player.GetButton(Control.Shoot);
			}
		}

		public override bool Dash
		{
			get
			{
				return Player.GetButtonDown(Control.Dash);
			}
		}

		public override bool UsePowerUp
		{
			get
			{
				return Player.GetButtonDown(Control.PowerUp);
			}
		}

		protected override Vector3 GetInputMovement()
		{
			return Player.GetAxis2DRaw(Control.MoveHorizontal, Control.MoveVertical);
		}
	}
}