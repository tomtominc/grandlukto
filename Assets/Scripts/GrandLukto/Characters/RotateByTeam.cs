﻿using UnityEngine;

namespace GrandLukto.Characters
{
	public class RotateByTeam : MonoBehaviour
	{
		public void Start()
		{
			var compCharacterBase = GetComponentInParent<CharacterBase>();
			
			switch(compCharacterBase.Team)
			{
				case Team.Top:
					transform.localRotation = Quaternion.Euler(0, 0, 180);
					break;
				case Team.Bottom:
					transform.localRotation = Quaternion.Euler(0, 0, 0);
					break;
			}
		}
	}
}