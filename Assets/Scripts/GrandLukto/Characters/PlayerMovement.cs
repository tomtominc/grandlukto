﻿using GrandLukto.Characters.StateMachine;
using GrandLukto.Stats;
using Spine.Unity.Modules;
using System;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerMovement : MonoBehaviour
	{
		private WindManager windManager;

		private Rigidbody2D compRigidBody;
		private Animator compAnimator;
		private SkeletonGhost compSkeletonGhost;
		private CharacterShoot compCharacterShoot;
		private CharacterStateMachine compStateMachine;
		private CharacterInputBase compInput;
		private StatManagerStunTime compStatStunTime;

		private int hashMoveSpeedX = Animator.StringToHash("moveSpeedX");
		private int hashMoveSpeedY = Animator.StringToHash("moveSpeedY");
		private int hashMoveSpeed = Animator.StringToHash("moveSpeed");
		private int hashIsMovingLeft = Animator.StringToHash("isMovingLeft");

		[Tooltip("The maximum speed that the player can move.")]
		public float maxSpeed = 10;
		
		[Tooltip("The maximum speed that the player can move while attacking.")]
		public float maxSpeedAttack = 1.5f;

		[Tooltip("The maximum speed that the player can move while being stunned.")]
		public float maxSpeedStunned = 1f;

		public float maxSpeedPowerUpModifier = 3.75f;
		public float maxSpeedPowerUpTime = 5;

		public float acceleration;
		public float deceleration;

		private float curSpeed = 0f;

		public float dashSpeed = 5f;
		public float dashDurationMax = 1f;
		private float dashDurationLeft = 0f;
		private Vector3 dashDirection;

		public float dashCooldownMax = 0.5f;
		private float dashCooldownLeft = 0f;

		private float maxSpeedPowerUpTimeLeft = 0;

		public bool IsDashing
		{
			get
			{
				return dashDurationLeft > 0f;
			}
		}

		private bool IsDashOnCooldown
		{
			get
			{
				return dashCooldownLeft > 0f;
			}
		}

		private void Awake()
		{
			windManager = FindObjectOfType<WindManager>();
		}

		public void Start()
		{
			compRigidBody = GetComponent<Rigidbody2D>();
			compCharacterShoot = GetComponent<CharacterShoot>();
			compStateMachine = GetComponent<CharacterStateMachine>();
			compInput = GetComponent<CharacterInputBase>();
			compStatStunTime = GetComponent<StatManagerStunTime>();
			compAnimator = gameObject.GetComponentInChildren<Animator>();
			compSkeletonGhost = gameObject.GetComponentInChildren<SkeletonGhost>();
		}

		public void Update()
		{
			if(maxSpeedPowerUpTimeLeft > 0) maxSpeedPowerUpTimeLeft = Mathf.Max(maxSpeedPowerUpTimeLeft - Time.deltaTime, 0);
			if(dashCooldownLeft > 0) dashCooldownLeft = Mathf.Max(dashCooldownLeft - Time.deltaTime, 0); // reduce dash cooldown

			TryStartDash();
		}

		private void UpdateMove()
		{
			var movementVector = compInput.MovementVec;
			CalculateSpeed(movementVector);

			compRigidBody.velocity = movementVector.normalized * curSpeed;

			if (windManager != null)
			{
				compRigidBody.velocity += new Vector2(windManager.PushPlayerStrength, 0);
			}
		}

		private void UpdateDash()
		{
			dashDurationLeft -= Time.deltaTime;

			compRigidBody.velocity = dashDirection * dashSpeed;

			if (dashDurationLeft <= 0) EndDash();
		}

		public void FixedUpdate()
		{
			if (compStateMachine.ActiveState.CanMove)
			{
				if (dashDurationLeft > 0) UpdateDash();
				else UpdateMove();
			}

			compAnimator.SetFloat(hashMoveSpeedX, compRigidBody.velocity.x);
			compAnimator.SetFloat(hashMoveSpeedY, compRigidBody.velocity.y);
			compAnimator.SetFloat(hashMoveSpeed, curSpeed);

			if (Mathf.Abs(compRigidBody.velocity.x) > 0.01)
			{
				compAnimator.SetBool(hashIsMovingLeft, compRigidBody.velocity.x < 0); // only change direction if player moves left / right
			}
		}

		private void TryStartDash()
		{
			if (!compInput.Dash) return;
			if (!compStateMachine.ActiveState.CanMove) return;
			if (IsDashing || IsDashOnCooldown || compCharacterShoot.IsAttacking) return;

			var curDirection = compInput.MovementVec;
			if (curDirection.magnitude < 0.01f) return; // too less magnitude

			dashDurationLeft = dashDurationMax;
			dashDirection = curDirection.normalized;

			curSpeed = GetMaxSpeed(); // full speed after dash

			compSkeletonGhost.ghostingEnabled = true;
		}

		private void EndDash()
		{
			dashDurationLeft = 0;
			dashDirection = Vector3.zero;

			compSkeletonGhost.ghostingEnabled = false;

			dashCooldownLeft = dashCooldownMax; // put dash on cooldown to avoid dashing all the time
		}

		/// <summary>
		/// Accelerates or decelerates and returns the new speed.
		/// </summary>
		private void CalculateSpeed(Vector3 movementVector)
		{
			var curMaxSpeed = GetMaxSpeed();
			var targetSpeed = movementVector.magnitude * curMaxSpeed;

			if (targetSpeed == curSpeed) return;

			if (targetSpeed < curSpeed)
			{
				// decelerate
				curSpeed -= deceleration * Time.deltaTime;
				curSpeed = Math.Max(targetSpeed, curSpeed); // do not go lower than targetSpeed
				return;
			}

			// accelerate
			curSpeed += acceleration * Time.deltaTime;
			curSpeed = Math.Min(curMaxSpeed, curSpeed);
		}
		
		private float GetMaxSpeed()
		{
			if (!compStatStunTime.IsEmpty) return maxSpeedStunned;
			if (compCharacterShoot.IsCharging) return maxSpeedAttack;

			float modifier = 1;
			if (maxSpeedPowerUpTimeLeft > 0) modifier = maxSpeedPowerUpModifier;

			return maxSpeed * modifier;
		}

		public void UseSpeedPowerUp()
		{
			maxSpeedPowerUpTimeLeft = maxSpeedPowerUpTime;
		}
	}
}