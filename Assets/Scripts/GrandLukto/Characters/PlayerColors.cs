﻿using UnityEngine;

namespace GrandLukto.Characters
{
	[CreateAssetMenu(menuName = "Grand Lukto/PlayerColors")]
	public class PlayerColors : ScriptableObject
	{
		public Color[] colors;
		public Color[] colorsDark;
	}
}