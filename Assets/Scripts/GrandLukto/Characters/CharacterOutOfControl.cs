﻿using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class CharacterOutOfControl : MonoBehaviour
	{
		public float debugDuration = 0.3f;
		public Vector2 debugDirection;
		public float debugStrength;

		private float maxTime;
		private float timeLeft;

		private Vector2 direction;
		private float strength;

		private Rigidbody2D compRigidBody;

		public bool IsFinished { get { return timeLeft <= 0; } }

		public void Awake()
		{
			compRigidBody = GetComponent<Rigidbody2D>();
		}
		
		public void FixedUpdate()
		{
			if (timeLeft <= 0) return;

			var relTime = timeLeft / maxTime;
			relTime = EaseUtil.Quintic.Out(relTime);

			compRigidBody.velocity = direction.normalized * relTime * strength;

			timeLeft -= Time.deltaTime;

			if(timeLeft <= 0)
			{
				compRigidBody.velocity = Vector3.zero;
			}
		}
		
		public void ApplyImpulse(float duration, Vector2 direction, float strength)
		{
			timeLeft = maxTime = duration;
			this.direction = direction;
			this.strength = strength;
		}

		[ContextMenu("Apply debug impulse")]
		public void ApplyDebugImpulse()
		{
			ApplyImpulse(debugDuration, debugDirection, debugStrength);
		}
	}
}