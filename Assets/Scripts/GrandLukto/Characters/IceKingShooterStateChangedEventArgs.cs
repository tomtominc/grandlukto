﻿using System;

namespace GrandLukto.Characters
{
	public class IceKingShooterStateChangedEventArgs : EventArgs
	{
		public IceKingShooter.State State { get; private set; }

		public IceKingShooterStateChangedEventArgs(IceKingShooter.State state)
		{
			State = state;
		}
	}
}