﻿using System.Collections;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Characters
{
	/// <summary>
	/// Lets an ice king shooter shoot out a single shot and hide it afterwards.
	/// </summary>
	public class IceKingShooterSingleShot : MonoBehaviour
	{
		private IceKingShooter shooter;

		public FloatRange shootDelay = new FloatRange(0.5f, 2f);
		public FloatRange afterShootDelay = new FloatRange(0.5f, 2f);

		private void Awake()
		{
			shooter = GetComponent<IceKingShooter>();
		}

		private void OnEnable()
		{
			shooter.StateChanged += WaitForSpawn;
		}

		private void WaitForSpawn(object sender, IceKingShooterStateChangedEventArgs e)
		{
			if (e.State != IceKingShooter.State.Idle) return;

			shooter.StateChanged -= WaitForSpawn;
			StartCoroutine(ShootAfterDelay());
		}

		private IEnumerator ShootAfterDelay()
		{
			yield return new WaitForSeconds(shootDelay);
			shooter.TryShoot();

			shooter.StateChanged += WaitForHide;
		}

		private void WaitForHide(object sender, IceKingShooterStateChangedEventArgs e)
		{
			if (e.State != IceKingShooter.State.Idle) return;

			shooter.StateChanged -= WaitForHide;
			StartCoroutine(HideAfterDelay());
		}

		private IEnumerator HideAfterDelay()
		{
			yield return new WaitForSeconds(afterShootDelay);
			shooter.TryHide();
		}

		private void OnDestroy()
		{
			shooter.StateChanged -= WaitForSpawn;
			shooter.StateChanged -= WaitForHide;
		}
	}
}