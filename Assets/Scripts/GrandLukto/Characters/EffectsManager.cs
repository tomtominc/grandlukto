﻿using GrandLukto.PowerUps.Effects;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Characters
{
	/// <summary>
	/// Manages PowerUp effects for one player.
	/// </summary>
	public class EffectsManager : MonoBehaviour
	{
		/// <summary>
		/// Raised just before an effect gets added. Consumers can cancel adding the effect.
		/// </summary>
		public event EventHandler<AddingEffectEventArgs> AddingEffect;

		/// <summary>
		/// List of all active effects.
		/// </summary>
		public List<EffectBase> ActiveEffects { get; private set; }

		public EffectsManager()
		{
			 ActiveEffects = new List<EffectBase>();
		}

		public void AddEffect(EffectBase effect)
		{
			var addingEffectEventArgs = new AddingEffectEventArgs(effect);
			OnAddingEffect(addingEffectEventArgs);

			if (addingEffectEventArgs.Cancel) return; // event consumer canceled adding the effect

			effect.Finished += Effect_Finished;

			ActiveEffects.Add(effect);
			effect.Activate(this, gameObject);
		}

		private void Effect_Finished(object sender, EventArgs e)
		{
			var effect = (EffectBase)sender;
			effect.Finished -= Effect_Finished;

			// remove effect
			ActiveEffects.Remove(effect);
		}

		/// <summary>
		/// Interrupts all effects that can currently be interrupted.
		/// </summary>
		public void TryInterruptAllEffects()
		{
			foreach(var effect in ActiveEffects)
			{
				effect.TryInterrupt();
			}
		}

		private void OnAddingEffect(AddingEffectEventArgs addingEffectEventArgs)
		{
			if (AddingEffect != null) AddingEffect.Invoke(this, addingEffectEventArgs);
		}
	}
}