﻿using GrandLukto.Balls;
using System;

namespace GrandLukto.Characters
{
	public class PadHitEventArgs : EventArgs
	{
		public PadHitEventArgs(Ball ball)
		{
			Ball = ball;
		}

		private bool isHandled;

		/// <summary>
		/// If true, this event has been handled by a subscriber.
		/// </summary>
		public bool IsHandled { get { return isHandled; } }

		/// <summary>
		/// The ball that will collide with the pad.
		/// </summary>
		public Ball Ball { get; private set; }

		public void SetHandled()
		{
			isHandled = true;
		}
	}
}