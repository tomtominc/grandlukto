﻿using UnityCave.StateMachine;
using UnityEngine;

namespace GrandLukto.Characters.StateMachine
{
	public class CharacterStateMachine : StateMachineBase<CharacterStateMachine, CharacterStateBase>
	{
		public CharacterBase CompCharacterBase { get; private set; }

		public bool IsOutOfControl
		{
			get
			{
				return ActiveState == stateOutOfControl;
			}
		}

		private StateOutOfControl stateOutOfControl = new StateOutOfControl();

		public virtual void Start()
		{
			CompCharacterBase = GetComponent<CharacterBase>();

			ChangeState(typeof(StateWaitForGameStart));
		}

		public virtual void RequestStateChange(StateChangeReason reason)
		{
			switch(reason)
			{
				case StateChangeReason.EndOutOfControl:
				case StateChangeReason.GameStart:
					ChangeState(typeof(StateIdle));
					break;

				default:
					Debug.LogError("StateChangeReason not supported: " + reason);
					break;
			}
		}

		public void ChangeStateOutOfControl(float duration, Vector2? direction = null, float strength = 0)
		{
			stateOutOfControl.Setup(duration, direction, strength);
			ChangeState(stateOutOfControl, true);
		}
	}
}
