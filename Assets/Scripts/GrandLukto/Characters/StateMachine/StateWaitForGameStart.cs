﻿namespace GrandLukto.Characters.StateMachine
{
	public class StateWaitForGameStart : CharacterStateBase
	{
		public StateWaitForGameStart()
		{
			CanMove = false;
			CanAttack = false;
			CanUsePowerUp = false;
		}

		public override void OnEnter()
		{
			base.OnEnter();

			GameManager.Instance.OnStateChanged.AddListener(OnGameStateChanged);
		}

		private void OnGameStateChanged(GameState state)
		{
			if(GameManager.Instance.IsGameRunning)
			{
				Machine.RequestStateChange(StateChangeReason.GameStart);
			}
		}

		public override void OnExit()
		{
			base.OnExit();

			GameManager.Instance.OnStateChanged.RemoveListener(OnGameStateChanged);
		}
	}
}
