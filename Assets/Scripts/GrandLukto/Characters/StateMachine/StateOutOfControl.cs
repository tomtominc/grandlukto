﻿using UnityEngine;

namespace GrandLukto.Characters.StateMachine
{
	public class StateOutOfControl : CharacterStateBase
	{
		private float duration;
		private Vector2? direction;
		private float strength;

		private float timeLeft;

		private CharacterOutOfControl compOutOfControl; 

		public StateOutOfControl()
		{
			CanMove = false;
			CanAttack = false;
		}

		public void Setup(float duration, Vector2? direction = null, float strength = 0)
		{
			this.duration = duration;
			this.direction = direction;
			this.strength = strength;
		}
		
		public override void OnEnter()
		{
			base.OnEnter();

			compOutOfControl = Machine.gameObject.GetComponent<CharacterOutOfControl>();

			// only apply impulse if strength and direction are set, otherwise no impulse necessary
			if (strength > 0 && direction.HasValue && direction != Vector3.zero)
			{
				compOutOfControl.ApplyImpulse(duration, direction.Value, strength);
			}

			timeLeft = Mathf.Max(timeLeft, duration); // do not override duration if it is set longer
		}

		public override void Update()
		{
			base.Update();

			timeLeft -= Time.deltaTime;

			if (compOutOfControl.IsFinished && timeLeft <= 0)
			{
				Machine.RequestStateChange(StateChangeReason.EndOutOfControl);
			}
		}
	}
}