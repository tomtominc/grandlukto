﻿using UnityCave.StateMachine;

namespace GrandLukto.Characters.StateMachine
{
	public class CharacterStateBase : StateBase<CharacterStateMachine, CharacterStateBase>
	{
		public bool CanMove { get; protected set; }
		public bool CanAttack { get; protected set; }
		public bool CanUsePowerUp { get; protected set; }

		public CharacterStateBase()
		{
			CanMove = true;
			CanAttack = true;
			CanUsePowerUp = true;
		}
	}
}