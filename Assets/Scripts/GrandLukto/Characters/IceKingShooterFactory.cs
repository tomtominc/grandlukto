﻿using GrandLukto.Balls;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class IceKingShooterFactory
	{
		public GameObject Spawn()
		{
			return GameObject.Instantiate(Resources.Load<GameObject>("Characters/IceKing Shooter"));
		}

		public GameObject SpawnShootSingleSnowball()
		{
			var gameObject = Spawn();

			gameObject.AddComponent<IceKingShooterSingleShot>();

			var shooter = gameObject.GetComponent<IceKingShooter>();
			shooter.ballSpeedRelative = new UnityCave.CustomDataTypes.FloatRange(0, 0.1f);
			shooter.ballSpawnType = BallType.Snowball;

			return gameObject;
		}
	}
}