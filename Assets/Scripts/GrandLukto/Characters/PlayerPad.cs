﻿using GrandLukto.Balls;
using UnityEngine;

namespace GrandLukto.Characters
{
	/// <summary>
	/// Represents the imaginary pad in front of the player that reflects the ball.
	/// </summary>
	public class PlayerPad : MonoBehaviour
	{
		private CharacterBase characterBase;

		private void Awake()
		{
			characterBase = GetComponentInParent<CharacterBase>();
		}

		public bool TryHandleBallHit(Ball ball)
		{
			var padHitArgs = new PadHitEventArgs(ball);
			characterBase.OnBallHitsPad(this, padHitArgs);
			return padHitArgs.IsHandled;
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			// this trigger collision will always be detected before player collision (if closer to ball)
			// if we hit the ball, we change the balls direction instantly, and so avoid additional player collisions

			var ball = collision.gameObject.GetComponentInParent<Ball>();
			if (!ball) return;

			TryHandleBallHit(ball);
		}
	}
}