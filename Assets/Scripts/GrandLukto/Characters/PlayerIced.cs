﻿using System;
using GrandLukto.Characters.StateMachine;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerIced : MonoBehaviour
	{
		private CharacterBase compCharacterBase;
		private GameObject iceBlock;

		private bool isIceBlockSpawned = false;

		private float timeLeft;

		private void StartIce()
		{
			if (compCharacterBase == null) compCharacterBase = GetComponent<CharacterBase>();

			TogglePlayerColliderAndAnimatorEnabled(false);

			SpawnIceBlock();

			timeLeft = GameManager.Instance.gameConfiguration.powerUpIceShotIceDuration;

			var stateMachine = GetComponent<CharacterStateMachine>();
			stateMachine.ChangeStateOutOfControl(timeLeft);
		}

		public void Update()
		{
			timeLeft -= Time.deltaTime;
			if (timeLeft > 0) return;

			FinishIce();
		}

		private void FinishIce()
		{
			transform.SetParent(null);
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;

			Destroy(iceBlock);
			TogglePlayerColliderAndAnimatorEnabled(true);

			Destroy(this);
		}

		private void TogglePlayerColliderAndAnimatorEnabled(bool isEnabled)
		{
			compCharacterBase.ToggleCollisionEnabled(isEnabled);
			compCharacterBase.visuals.GetComponent<Animator>().enabled = isEnabled;
		}

		private void SpawnIceBlock()
		{
			if (isIceBlockSpawned) return;
			isIceBlockSpawned = true;

			iceBlock = Instantiate(Resources.Load<GameObject>("Ice Block"));
			iceBlock.transform.position = transform.position;

			iceBlock.layer = gameObject.layer; // same collision mask as player

			transform.SetParent(iceBlock.transform, true);
			iceBlock.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity; // move iceblock in move direction
		}

		public static void IcePlayer(GameObject player)
		{
			var playerIced = player.GetOrAddComponent<PlayerIced>();
			playerIced.StartIce();
		}
	}
}