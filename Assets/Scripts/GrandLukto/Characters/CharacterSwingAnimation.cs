﻿using Spine.Unity;
using UnityCave.ExtensionMethods;
using UnityEngine;
using UnityCave.Spine;

namespace GrandLukto.Characters
{
	public class CharacterSwingAnimation : MonoBehaviour
	{
		[SpineAnimation()]
		public string animUp;

		[SpineAnimation()]
		public string animDown;

		private SkeletonAnimation compAnimation;
		private CharacterBase compCharacterBase;
		private Renderer compRenderer;

		public float yOffsetUp = -0.36f;
		public float yOffsetDown = -0.46f;

		public void Awake()
		{
			compAnimation = GetComponent<SkeletonAnimation>();
			compAnimation.AnimationState.ClearTracks();

			compCharacterBase = GetComponentInParent<CharacterBase>();
			compCharacterBase.PlayerSettingRecieved += OnPlayerSettingRecieved;

			compRenderer = GetComponent<Renderer>();
			compRenderer.enabled = false; // disable until first hit to avoid showing swing
		}

		private void OnPlayerSettingRecieved(object sender, PlayerSettingRecievedEventArgs e)
		{
			compCharacterBase.PlayerSettingRecieved -= OnPlayerSettingRecieved;

			compAnimation.AnimationName = null; // reset to avoid errors

			if (!string.IsNullOrEmpty(e.Setting.selectedCharacter.animationSwingNameUp)) animUp = e.Setting.selectedCharacter.animationSwingNameUp;
			if (!string.IsNullOrEmpty(e.Setting.selectedCharacter.animationSwingNameDown)) animDown = e.Setting.selectedCharacter.animationSwingNameDown;

			var swingSkeletonData = e.Setting.selectedCharacter.animationSwingSkeletonData;
			if (swingSkeletonData == null) return; // default swing

			compAnimation.skeletonDataAsset = swingSkeletonData;
			compAnimation.ReloadSkeletonData();
		}

		public void Start()
		{
			// different position for different FaceDirections
			transform.localPosition = transform.localPosition.Clone(y: compCharacterBase.FaceDirection == FaceDirection.Up ? yOffsetUp : yOffsetDown);
		}

		public void PlaySwingAnimation()
		{
			compRenderer.enabled = true;
			compAnimation.AnimationState.SetAnimation(0, compCharacterBase.FaceDirection == FaceDirection.Up ? animUp : animDown, false);
		}
	}
}