﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Characters.Editor
{
	[CustomEditor(typeof(NPCInput))]
	public class NPCInputEditor : EditorBase<NPCInput>
	{
		public void OnSceneGUI()
		{
			if (!Target.MoveTarget.HasValue) return;

			Handles.DrawLine(Target.transform.position, (Vector3)Target.MoveTarget);
		}
	}
}
