﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Characters
{
	[CustomEditor(typeof(AIAreaDescription))]
	public class AIAreaDescriptionEditor : EditorBase<AIAreaDescription>
	{
		public void OnSceneGUI()
		{
			Handles.DrawLine(new Vector3(Target.centerX, Target.transform.position.y - 1000, 0), new Vector3(Target.centerX, Target.transform.position.y + 1000, 0));
		}
	}
}