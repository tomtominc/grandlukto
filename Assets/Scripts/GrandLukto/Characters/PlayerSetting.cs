﻿using GrandLukto.Characters.StateMachine;
using GrandLukto.Database;
using RAIN.Core;
using Rewired;
using UnityCave.Collision;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerSetting
	{
		public int playerId;
		public bool isHuman;
		public Team team;
		public Player controller;
		public DataCharacter selectedCharacter;
		public DataAIDefinition aiDefinition;
		public GameObject ai;
		
		public GameObject PlayerObject { get; private set; }

		public int PlayerNumber
		{
			get
			{
				return playerId + 1;
			}
		}

		public Color PlayerColor
		{
			get
			{
				return DatabaseManager.Instance.playerColors.colors[playerId];
			}
		}

		public void Spawn(Vector2 spawnPos)
		{
			if (isHuman) PlayerObject = CreateHumanPlayer();
			else PlayerObject = CreateAIPlayer();

			PlayerObject.transform.position = spawnPos;

			var compCharacterBase = PlayerObject.GetComponent<CharacterBase>();
			compCharacterBase.Initialize(this);
		}

		private GameObject CreatePlayer()
		{
			return Object.Instantiate(Resources.Load<GameObject>("Characters/Player"));
		}

		private GameObject CreateHumanPlayer()
		{
			var player = CreatePlayer();

			player.AddComponent<CharacterStateMachine>();
			player.AddComponent<PlayerInput>();

			return player;
		}

		// TODO: Factory
		private GameObject CreateAIPlayer()
		{
			var player = CreatePlayer();

			player.AddComponent<NPCInput>();
			player.AddComponent<CharacterStateMachine>();
			
			if (aiDefinition != null)
			{
				aiDefinition.ApplyTo(player);
			}

			// RAIN
			if (ai != null)
			{
				var aiGameObject = Object.Instantiate(ai, player.transform);
				aiGameObject.name = "AI";
				aiGameObject.transform.localPosition = Vector3.zero;

				var aiRig = aiGameObject.GetComponent<AIRig>();
				if (aiRig != null)
				{
					aiRig.AI.Body = player;
				}
			}
			
			// CollisionHandlers
			var collisionHandlersGameObject = player.transform.CreateChildGameObject("CollisionHandlers");
			var collisionHandlerManager = collisionHandlersGameObject.AddComponent<CollisionHandlerManager>();

			foreach(var handlerDefinition in aiDefinition.collisionHandlers)
			{
				collisionHandlerManager.Add(handlerDefinition.name, handlerDefinition.gameObject);
			}

			return player;
		}
	}
}