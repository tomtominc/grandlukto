﻿using GrandLukto.Blocks;
using GrandLukto.UncoveredAreaDetection;
using System.Collections;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class AIAreaDescription : MonoBehaviour, IAIAreaDescription
	{
		public BoxCollider2D movementArea;

		public float centerX;

		public float CenterXWorldPos { get; private set; }
		public float YPosBeforeBlocks { get; private set; }

		private BlockGroup compBlockGroup;

		private Coroutine coroutineUpdateYPosBeforeBlocks;
		
		public UncoveredAreaDetector UncoveredAreaDetector { get; private set; }

		public void Awake()
		{
			compBlockGroup = GetComponentInParent<BlockGroup>();
		}

		public void Start()
		{
			UncoveredAreaDetector = new UncoveredAreaDetector(compBlockGroup.team, GetGridWidthByPlayerCount());

			DoUpdateYPosBeforeBlocks(true); // force update for uncoveredAreaDetector

			CenterXWorldPos = (transform.localToWorldMatrix * new Vector2(centerX, 0)).x;
		}

		private int GetGridWidthByPlayerCount()
		{
			List<PlayerSetting> playerSettingList;
			if (!GameManager.Instance.PlayersByTeam.TryGetValue(compBlockGroup.team, out playerSettingList)) return 1; // no players in team

			var playerCount = playerSettingList.Count;
			switch (playerCount)
			{
				case 2:
					return 6;
				case 3:
					return 7;
			}

			return 1;
		}

		public void OnEnable()
		{
			coroutineUpdateYPosBeforeBlocks = StartCoroutine(UpdateYPosBeforeBlocks());
		}

		public void OnDisable()
		{
			StopCoroutine(coroutineUpdateYPosBeforeBlocks);
		}

		private IEnumerator UpdateYPosBeforeBlocks()
		{
			while (true)
			{
				DoUpdateYPosBeforeBlocks(true);
				yield return new WaitForSeconds(3f);
			}
		}

		private void DoUpdateYPosBeforeBlocks(bool forceUpdateGridBounds = false)
		{
			var oldYPos = YPosBeforeBlocks;

			YPosBeforeBlocks = ComputeYPosBeforeBlocks();

			if(forceUpdateGridBounds || !Mathf.Approximately(oldYPos, YPosBeforeBlocks))
			{
				if(UncoveredAreaDetector != null) UncoveredAreaDetector.UpdateBounds(GetBoundsWithoutBlocks());
			}
		}

		private float ComputeYPosBeforeBlocks()
		{
			var bounds = GetBounds();

			var curPos = new Vector2(bounds.min.x, bounds.center.y); // start from center Y since blocks should not go further
			Vector2 faceDirection = compBlockGroup.team.GetFaceDirection().ToDirectionVector();
			var offset = -faceDirection * GameManager.Instance.blockOffset;
			var raycastDist = bounds.size.x;

			do
			{
				if (Physics2D.Raycast(curPos, Vector2.right, raycastDist, GameManager.Instance.layerMaskBlocks))
				{
					curPos -= offset; // go one step back
					break;
				}

				curPos += offset;
			}
			while (curPos.y > bounds.min.y && curPos.y < bounds.max.y);

			return curPos.y;
		}

		public Bounds GetBounds(float padding = 0)
		{
			return movementArea.bounds.AddOnEachSide(-padding);
		}

		public Bounds GetBoundsWithoutBlocks(float padding = 0)
		{
			var bounds = movementArea.bounds;

			if (compBlockGroup.team == Team.Top)
			{
				bounds.max = bounds.max.Clone(y: Mathf.Min(YPosBeforeBlocks, bounds.max.y));
			}
			else
			{
				bounds.min = bounds.min.Clone(y: Mathf.Max(YPosBeforeBlocks, bounds.min.y));
			}

			return bounds.AddOnEachSide(-padding);
		}
	}
}