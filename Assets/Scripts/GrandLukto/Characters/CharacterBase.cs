﻿using GrandLukto.UI;
using Rewired;
using System;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerSettingRecievedEventArgs : EventArgs
	{
		public PlayerSetting Setting { get; private set; }

		public PlayerSettingRecievedEventArgs(PlayerSetting setting)
		{
			Setting = setting;
		}
	}

	// todo: allow fake data for playerSetting
	public class CharacterBase : MonoBehaviour
	{
		public GameObject visuals;

		public PlayerSetting PlayerSetting { get; private set; }

		public FaceDirection FaceDirection { get; private set; }
		public Vector3 FaceDirectionVec { get; private set; }

		/// <summary>
		/// Radius of the Character's circle collider.
		/// </summary>
		public float Size { get; private set; }

		public event EventHandler<RecievePowerUpEventArgs> RecievePowerUp;
		public event EventHandler<PlayerSettingRecievedEventArgs> PlayerSettingRecieved;
		public event EventHandler<PadHitEventArgs> BallHitsPad;

		public Player Player
		{
			get
			{
				return PlayerSetting.controller;
			}
		}

		public Team Team
		{
			get
			{
				return PlayerSetting.team;
			}
		}

		public Color PlayerColor
		{
			get
			{
				return PlayerSetting.PlayerColor;
			}
		}

		public void Awake()
		{
			Size = GetComponent<CircleCollider2D>().radius;
		}
		
		public void Initialize(PlayerSetting setting)
		{
			GameManager.Instance.OnStateChanged.AddListener(OnGameStateChanged);

			PlayerSetting = setting;

			FaceDirection = Team.GetFaceDirection();
			FaceDirectionVec = FaceDirection.ToDirectionVector();
			gameObject.layer = LayerMask.NameToLayer("Player" + Team.ToString());

			PlayerSetting.selectedCharacter.ApplyAnimationToGameObject(visuals, FaceDirection);

			if (PlayerSetting.selectedCharacter.shadowSize != Vector2.zero)
			{
				var shadowTransform = transform.Find("Shadow");
				var shadowSprite = shadowTransform.GetComponent<tk2dSprite>();
				shadowSprite.scale = PlayerSetting.selectedCharacter.shadowSize;
			}

			var colliderSize = PlayerSetting.selectedCharacter.colliderSize;
			if (colliderSize.HasValue)
			{
				GetComponent<CircleCollider2D>().radius = colliderSize.Value;
				Size = colliderSize.Value;
			}

			OnPlayerSettingRecieved();
		}

		private void OnPlayerSettingRecieved()
		{
			if (PlayerSettingRecieved != null) PlayerSettingRecieved(this, new PlayerSettingRecievedEventArgs(PlayerSetting));
		}

		private void OnGameStateChanged(GameState state)
		{
			switch(state)
			{
				case GameState.Countdown:
					ShowPlayerIcon();
					break;
			}
		}

		private void ShowPlayerIcon()
		{
			if (!PlayerSetting.selectedCharacter.showPlayerIcon) return;

			var playerIcon = (GameObject)Instantiate(Resources.Load("UI/PlayerIcon"), transform);
			playerIcon.GetComponent<PlayerIcon>().SetupIconForPlayer(PlayerSetting);
		}

		public void ToggleCollisionEnabled(bool enabled)
		{
			GetComponent<Rigidbody2D>().simulated = enabled;
		}

		public void OnBallHitsPad(object sender, PadHitEventArgs args)
		{
			if (BallHitsPad != null) BallHitsPad(sender, args);
		}

		public void OnRecievePowerUp(object sender, RecievePowerUpEventArgs e)
		{
			if (RecievePowerUp != null) RecievePowerUp(sender, e);
		}
	}
}