﻿using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using GrandLukto.Blocks;
using Spine;
using Spine.Unity;
using System;
using System.Collections;
using UnityCave;
using UnityCave.CustomDataTypes;
using UnityCave.StateMachine;
using UnityEngine;

namespace GrandLukto.Characters
{
	// TODO: Spawn only if area is not occupied by balls, other characters etc. to avoid collisions
	public class IceKingShooter : MonoBehaviour
	{
		public enum State
		{
			None,
			PoppingUp,
			Idle,
			Shooting,

			/// <summary>
			/// Fading with a white flash when being destroyed.
			/// </summary>
			Fading,

			/// <summary>
			/// Hiding without being destroyed.
			/// </summary>
			Hiding
		}

		private readonly BallFactory ballFactory = new BallFactory();
		private EntityManager entityManager;

		public GameObject visuals;
		private SkeletonAnimation spineAnimation;
		private FadeBlockAnimation fadeAnimation;
		private Collider2D[] colliders;

		public GameObject ballSpawnPosition;
		public BallType ballSpawnType = BallType.Rock;
		public FloatRange ballSpeedRelative;

		[Tooltip("Shoot delay in frames")]
		public int fireAnimationShootDelay = 8;
		
		private SimpleStateMachine<State> stateMachine = new SimpleStateMachine<State>(State.None);

		[SpineAnimation] public string animationPopUp;
		[SpineAnimation] public string animationIdle;
		[SpineAnimation] public string animationFire;
		[SpineAnimation] public string animationHide;

		public event EventHandler<IceKingShooterStateChangedEventArgs> StateChanged;

		/// <summary>
		/// Raised after the shooter spawned a ball.
		/// </summary>
		public event EventHandler BallSpawned;

		private void Awake()
		{
			entityManager = EntityManager.Instance;
			spineAnimation = visuals.GetComponent<SkeletonAnimation>();
			fadeAnimation = visuals.GetComponent<FadeBlockAnimation>();

			colliders = GetComponents<Collider2D>();

			stateMachine.OnExitState = OnExitState;
			stateMachine.OnEnterState = OnEnterState;

			entityManager.Add<IceKingShooter>(gameObject);
		}

		private void Start()
		{
			ToggleEnabled(false);
			StartCoroutine(SpawnAfterDelay());
		}

		private IEnumerator SpawnAfterDelay()
		{
			yield return new WaitForSeconds(0.5f);
			stateMachine.ChangeState(State.PoppingUp);
			ToggleEnabled(true);
		}
		
		private void OnExitState(State oldState)
		{
			StopAllCoroutines();

			switch (oldState)
			{
				case State.PoppingUp:
					spineAnimation.AnimationState.Complete -= PopUpAnimationComplete;
					break;

				case State.Shooting:
					spineAnimation.AnimationState.Complete -= ShootAnimationComplete;
					break;

				case State.Hiding:
					spineAnimation.AnimationState.Complete -= HideAnimationComplete;
					break;
			}
		}

		private void OnEnterState(State state)
		{
			switch (state)
			{
				case State.PoppingUp:
					spineAnimation.AnimationState.SetAnimation(0, animationPopUp, false);
					spineAnimation.AnimationState.Complete += PopUpAnimationComplete;
					break;

				case State.Idle:
					spineAnimation.AnimationState.SetAnimation(0, animationIdle, true);
					break;

				case State.Shooting:
					spineAnimation.AnimationState.SetAnimation(0, animationFire, false);
					spineAnimation.AnimationState.Complete += ShootAnimationComplete;
					StartCoroutine(ShootRoutine());
					break;

				case State.Fading:
					fadeAnimation.OnAnimationComplete.AddListener(FadeAnimationComplete);
					fadeAnimation.StartFade();
					break;

				case State.Hiding:
					spineAnimation.AnimationState.SetAnimation(0, animationHide, false);
					spineAnimation.AnimationState.Complete += HideAnimationComplete;
					break;
			}

			if (StateChanged != null) StateChanged(this, new IceKingShooterStateChangedEventArgs(state));
		}

		private IEnumerator ShootRoutine()
		{
			yield return new WaitForSeconds(fireAnimationShootDelay / 30f);

			var ball = ballFactory.CreateBall(ballSpawnType);
			ball.InitializePosition(ballSpawnPosition.transform.position);
			ball.ApplyForce(ball.Configuration.GetRelativeSpeed(ballSpeedRelative), Vector2.down);

			if (BallSpawned != null) BallSpawned(this, EventArgs.Empty);
		}

		private void PopUpAnimationComplete(TrackEntry trackEntry)
		{
			stateMachine.ChangeState(State.Idle);
		}

		private void ShootAnimationComplete(TrackEntry trackEntry)
		{
			stateMachine.ChangeState(State.Idle);
		}

		private void HideAnimationComplete(TrackEntry trackEntry)
		{
			RemoveFromGame();
		}

		private void OnCollisionEnter2D(Collision2D collision)
		{
			if (stateMachine.CurState <= State.PoppingUp ||
				stateMachine.CurState >= State.Fading) return;

			if (collision.gameObject.tag != Tag.Ball) return; // we are only interested in ball collisions

			stateMachine.ChangeState(State.Fading);
		}

		private void FadeAnimationComplete()
		{
			fadeAnimation.OnAnimationComplete.RemoveListener(FadeAnimationComplete);
			RemoveFromGame();
		}

		private void RemoveFromGame()
		{
			entityManager.Remove<IceKingShooter>(gameObject);
			Destroy(gameObject);
		}

		public bool TryHide()
		{
			if (stateMachine.CurState >= State.Fading) return false;

			stateMachine.ChangeState(State.Hiding);
			return true;
		}

		public bool TryShoot()
		{
			if (stateMachine.CurState != State.Idle) return false;

			stateMachine.ChangeState(State.Shooting);
			return true;
		}

		private void ToggleEnabled(bool isEnabled)
		{
			foreach (var collider in colliders)
			{
				collider.enabled = isEnabled;
			}

			visuals.SetActive(isEnabled);
		}
	}
}