﻿using UnityEngine;

namespace GrandLukto.Characters
{
	public abstract class CharacterInputBase : MonoBehaviour
	{
		public abstract bool ShootStart { get; }
		public abstract bool ShootCharge { get; }

		public abstract bool UsePowerUp { get; }

		public abstract bool Dash { get; }

		public Vector3 MovementVec
		{
			get
			{
				var moveVec = GetInputMovement();
				if (moveVec.magnitude > 1) moveVec.Normalize(); // important for keyboard when pressing e.g. up and right
				return moveVec;
			}
		}

		/// <summary>
		/// Gets the Input movement vector (magnitude not larger than 1 -> checked in containing method).
		/// </summary>
		protected abstract Vector3 GetInputMovement();
	}
}
