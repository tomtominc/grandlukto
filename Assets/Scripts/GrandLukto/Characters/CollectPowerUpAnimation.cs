﻿using DG.Tweening;
using GrandLukto.PowerUps;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class CollectPowerUpAnimation : MonoBehaviour
	{
		public Vector3 moveOffset;
		public float moveDuration;
		public Ease moveEase;

		public float alphaDuration = 0.25f;
		public Ease alphaEase;

		public GameObject visuals;

		private tk2dSprite compSprite;

		public void Awake()
		{
			compSprite = visuals.GetComponent<tk2dSprite>();
		}

		public void Start()
		{
			StartAnim();
		}

		public void Initialize(PowerUpType type)
		{
			compSprite.SetSprite(type.GetIconName() + "_icon_small");
		}

		[ContextMenu("StartAnim")]
		private void StartAnim()
		{
			DOTween.Sequence()
				.Append(visuals.transform
					.DOLocalMove(moveOffset, moveDuration)
					.SetRelative()
					.SetEase(moveEase))
				.Append(DOTween
					.To(() => compSprite.color, value => compSprite.color = value, compSprite.color * new Color(1, 1, 1, 0), alphaDuration)
					.SetEase(alphaEase))
				.AppendCallback(() => Destroy(gameObject));
		}
	}
}