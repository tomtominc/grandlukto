﻿using GrandLukto.PowerUps;
using System;

namespace GrandLukto.Characters
{
	public class RecievePowerUpEventArgs : EventArgs
	{
		public PowerUpType PowerUpType { get; private set; }

		private bool isCollected = false;
		public bool IsCollected { get { return isCollected; } }

		public RecievePowerUpEventArgs(PowerUpType type)
		{
			PowerUpType = type;
		}

		public void Collect()
		{
			isCollected = true;
		}
	}
}