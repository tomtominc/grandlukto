﻿using System;
using UnityEngine;

namespace GrandLukto.Characters
{
	public enum FaceDirection
	{
		Left,
		Right,
		Up,
		Down
	}

	public static class FaceDirectionExtensions
	{
		public static FaceDirection GetOpposite(this FaceDirection faceDirection)
		{
			switch(faceDirection)
			{
				case FaceDirection.Up: return FaceDirection.Down;
				case FaceDirection.Down: return FaceDirection.Up;
				case FaceDirection.Left: return FaceDirection.Right;
				case FaceDirection.Right: return FaceDirection.Left;
			}

			throw new NotImplementedException();
		}

		public static Vector3 ToDirectionVector(this FaceDirection faceDirection)
		{
			switch(faceDirection)
			{
				case FaceDirection.Up: return Vector2.up;
				case FaceDirection.Down: return Vector2.down;
				case FaceDirection.Left: return Vector2.left;
				case FaceDirection.Right: return Vector2.right;
			}

			throw new NotImplementedException();
		}
	}
}
