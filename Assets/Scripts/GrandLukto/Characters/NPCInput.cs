﻿using UnityEngine;

namespace GrandLukto.Characters
{
	public class NPCInput : CharacterInputBase
	{
		private enum ChargeState
		{
			None,
			StartCharge,
			Charge,
			Shoot,
			QuickShot
		}

		public Vector2? MoveTarget { get; private set; }
		
		private ChargeState curChargeState = ChargeState.None;
		private ChargeState lastChargeState = ChargeState.None;

		private bool usePowerUp = false;

		public bool MoveTargetReached
		{
			get
			{
				return MoveTarget.HasValue ? Vector2.Distance(MoveTarget.Value, transform.position) < 0.3f : true;
			}
		}

		public override bool Dash
		{
			get
			{
				return false;
			}
		}

		public override bool ShootCharge
		{
			get
			{
				return curChargeState == ChargeState.StartCharge || curChargeState == ChargeState.Charge;
			}
		}

		public override bool ShootStart
		{
			get
			{
				return curChargeState == ChargeState.StartCharge || curChargeState == ChargeState.QuickShot;
			}
		}

		public override bool UsePowerUp
		{
			get
			{
				var powerUpUsed = usePowerUp;
				usePowerUp = false;
				return powerUpUsed;
			}
		}

		protected override Vector3 GetInputMovement()
		{
			if (!MoveTarget.HasValue) return Vector3.zero;

			return (Vector3)MoveTarget - transform.position;
		}

		public void MoveTo(Vector2 moveTarget)
		{
			MoveTarget = moveTarget;
			if (MoveTargetReached) MoveTarget = null; // we are already at this position
		}

		public void StartCharge()
		{
			if (curChargeState != ChargeState.None) return;
			curChargeState = ChargeState.StartCharge;
		}

		public void EndShot()
		{
			if(curChargeState != ChargeState.None)
			{
				curChargeState = ChargeState.Shoot;
			}
		}

		public void Shoot()
		{
			Debug.Log("Shoot: " + curChargeState);

			if(curChargeState == ChargeState.None)
			{
				curChargeState = ChargeState.QuickShot;
				return;
			}

			if (curChargeState != ChargeState.Charge) return; // ???
			curChargeState = ChargeState.Shoot;
			//if (curChargeState != ChargeState.None || curChargeState) return;
		}

		public void LateUpdate()
		{
			if (lastChargeState != curChargeState)
			{
				lastChargeState = curChargeState; // keep at least for one frame
				return;
			}

			lastChargeState = curChargeState;

			switch(curChargeState)
			{
				case ChargeState.StartCharge:
					curChargeState = ChargeState.Charge;
					break;

				case ChargeState.Shoot:
				case ChargeState.QuickShot:
					curChargeState = ChargeState.None;
					break;
			}

			if (MoveTargetReached) MoveTarget = null; // clear move target
		}

		public void DoUsePowerUp()
		{
			usePowerUp = true;
		}
	}
}
