﻿using GrandLukto.PowerUps.Effects;
using System.ComponentModel;

namespace GrandLukto.Characters
{
	public class AddingEffectEventArgs : CancelEventArgs
	{
		/// <summary>
		/// The effect that is about to get added.
		/// </summary>
		public EffectBase Effect { get; private set; }

		public AddingEffectEventArgs(EffectBase effect)
		{
			Effect = effect;
		}
	}
}