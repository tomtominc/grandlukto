﻿using GrandLukto.Balls;
using GrandLukto.Characters.StateMachine;
using GrandLukto.PowerUps;
using GrandLukto.Stats;
using UnityCave.Audio;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto.Characters
{
	public class CharacterShoot : MonoBehaviour
	{
		private enum AttackState
		{
			None,
			AttackInQueue,
			Charge,
			AnimationPlaying,
			WaitForAnimationEnd
		}

		public UnityEvent onPlayAttackAnimation;

		[Range(0, 90)]
		public float hitAngle1 = 45f;

		[Range(0, 90)]
		public float hitAngle2 = 60f;

		private CharacterBase compCharacterBase;
		private Animator compAnimator;
		private PlayerMovement compPlayerMovement;
		private StatManagerChargeShoot statChargeShoot;
		private Rigidbody2D compRigidBody;
		private CharacterStateMachine compStateMachine;
		private PowerUpManager compPowerUpManager;
		private CharacterInputBase compInput;
		private AudioSourcePool compAudioSourcePool;

		private int hashDoAttack = Animator.StringToHash("doAttack");
		private int hashAttack = Animator.StringToHash("Attack");
		private int hashIsCharging = Animator.StringToHash("isCharging");
		private int hashChargeAmountRel = Animator.StringToHash("chargeAmountRel");

		private AttackState curState = AttackState.None;

		[Tooltip("The min amount the player needs to charge before it counts as a charged attack.")]
		public float chargeMinValue = 0.1f;

		/// <summary>
		/// Relative charge value for the next shot (0-1).
		/// </summary>
		private float chargeValueRel;

		public bool IsAttackInQueue
		{
			get
			{
				return curState == AttackState.AttackInQueue;
			}
		}

		public bool IsAttacking
		{
			get
			{
				return curState == AttackState.Charge || curState == AttackState.AnimationPlaying || curState == AttackState.WaitForAnimationEnd;
			}
		}

		public bool IsCharging
		{
			get
			{
				return curState == AttackState.Charge && statChargeShoot.ValueRel > chargeMinValue;
			}
		}

		private bool CanAttack
		{
			get
			{
				return !compPlayerMovement.IsDashing && compStateMachine.ActiveState.CanAttack;
			}
		}
		
		public void Start()
		{
			compCharacterBase = GetComponent<CharacterBase>();
			compPlayerMovement = GetComponent<PlayerMovement>();
			compAnimator = GetComponentInChildren<Animator>();
			statChargeShoot = GetComponent<StatManagerChargeShoot>();
			compRigidBody = GetComponent<Rigidbody2D>();
			compStateMachine = GetComponent<CharacterStateMachine>();
			compPowerUpManager = GetComponent<PowerUpManager>();
			compInput = GetComponent<CharacterInputBase>();
			compAudioSourcePool = gameObject.AddComponent<AudioSourcePool>();

			compCharacterBase.BallHitsPad += CharacterPadHit;
		}

		private void CharacterPadHit(object sender, PadHitEventArgs e)
		{
			// TODO: Maybe allow hitting multiple balls by allowing to hit the first few frames of the attack anim

			if (!CanAttack) return;

			var ball = e.Ball;
			if (ball.IsMoving && !BallStatic.IsMovingTowardsPlayer(compCharacterBase, ball)) return; // ignore balls that are moving away

			EndCharge(true);
			e.SetHandled();

			HitBall(ball);
		}

		private void HitBall(Ball ball)
		{
			compAudioSourcePool.PlaySound(GameManager.Instance.gameConfiguration.soundShot, Random.Range(0.8f, 1.4f));

			var hitDirection = GetHitDirection(ball.MoveDirection, ball.CurSpeed);
			ball.GetHitByCharacter(compCharacterBase, hitDirection, chargeValueRel, GetCurveValueRel());

			TryUseOnHitPowerUpOnBall(ball);
		}

		public void FixedUpdate()
		{
			switch(curState)
			{
				case AttackState.AnimationPlaying:
					// wait for animation to perform attack

					compAnimator.SetBool(hashDoAttack, false); // end animation
					

					curState = AttackState.WaitForAnimationEnd;
					break;
				case AttackState.WaitForAnimationEnd:
					if (compAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == hashAttack) break; // still animating
					
					// end attack
					curState = AttackState.None;
					break;
			}
		}

		public void Update()
		{
			UpdateAttack();
		}

		private void UpdateAttack()
		{
			AttackState lastState;

			do
			{
				lastState = curState;
				HandleAttackState(); // update as often as the state changes -> increases responsiveness
			}
			while (lastState != curState);
		}

		private void HandleAttackState()
		{
			switch (curState)
			{
				case AttackState.None:
					// wait for attack
					if (compInput.ShootStart) curState = AttackState.AttackInQueue; // put the attack in the queue; we can only attack if e.g. player is not dashing
					break;

				case AttackState.AttackInQueue:
					if (!CanAttack) break; // do not allow attack while dashing etc.

					curState = AttackState.Charge;
					break;

				case AttackState.Charge:
					compAnimator.SetFloat(hashChargeAmountRel, statChargeShoot.ValueRel);

					if (!compInput.ShootCharge)
					{
						EndCharge(false);
						break;
					}

					if (IsCharging)
					{
						// above min charge value
						compAnimator.SetBool(hashIsCharging, true);
					}

					// update charge bar
					statChargeShoot.ChangeValue(Time.deltaTime);
					break;
			}
		}

		private void EndCharge(bool doAttack)
		{
			// not pressed anymore -> start attack
			compAnimator.SetBool(hashIsCharging, false);
			compAnimator.SetBool(hashDoAttack, doAttack);

			chargeValueRel = statChargeShoot.ValueRel;
			statChargeShoot.Empty();

			if (!doAttack)
			{
				curState = AttackState.None;
				return;
			}

			compAudioSourcePool.PlaySound(GameManager.Instance.gameConfiguration.soundSwing);

			onPlayAttackAnimation.Invoke();
			curState = AttackState.AnimationPlaying;
		}

		public Vector3 GetHitDirection(Vector2 objMoveDirection, float objCurSpeed)
		{
			var moveVec = compInput.MovementVec;
			var moveVecLength = compInput.MovementVec.magnitude;

			var faceVec = compCharacterBase.FaceDirectionVec;

			if (moveVecLength < 0.25)
			{
				if(Vector2.Dot(objMoveDirection, faceVec) > 0)
				{
					// ball already moves away from player -> do nothing
					return objMoveDirection;
				}

				// standing still -> try to reflect ball
				if (objCurSpeed <= 0)
				{
					// ball is not moving -> move in player's direction
					return faceVec;
				}

				return Vector2.Reflect(objMoveDirection, faceVec);
			}

			// get angle from movement
			var angleSigned = Vector2Util.AngleSigned(faceVec, moveVec);

			// 0 <= aimStep >= 4 (180/45 = 4)
			var aimStep = Mathf.RoundToInt(angleSigned / 45);

			float hitAngle = 0;

			switch(Mathf.Abs(aimStep))
			{
				case 0:
				case 4:
					hitAngle = 0;
					break;
				case 1:
				case 3:
					hitAngle = hitAngle1;
					break;
				case 2:
					hitAngle = hitAngle2;
					break;
			}

			hitAngle *= Mathf.Sign(aimStep);

			return Quaternion.Euler(0, 0, hitAngle) * faceVec;
		}

		private void TryUseOnHitPowerUpOnBall(Ball ball)
		{
			var ballController = ball.GetComponent<IBallController>();
			if (!ballController.CanTransformBall) return; // can not use power up on ball

			PowerUpType onHitPowerUp;
			if (!compPowerUpManager.TryUseOnHitPowerUp(out onHitPowerUp)) return; // no powerup available

			PowerUpStatic.UseOnHitPowerUpOnBall(ball, onHitPowerUp);
		}

		private float GetCurveValueRel()
		{
			if(compRigidBody.velocity.x < -0.1) return -1;
			if(compRigidBody.velocity.x > 0.1) return 1;
			return 0;
		}
	}
}