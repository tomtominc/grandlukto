﻿using UnityEngine;

namespace GrandLukto.Characters
{
	public interface IAIAreaDescription
	{
		/// <summary>
		/// Returns the bounds of the avilable space in the area without the given padding value.
		/// </summary>
		Bounds GetBounds(float padding = 0);

		/// <summary>
		/// Returns the movementArea's bounds without the part where the blocks are located at.
		/// </summary>
		Bounds GetBoundsWithoutBlocks(float padding = 0);
	}
}