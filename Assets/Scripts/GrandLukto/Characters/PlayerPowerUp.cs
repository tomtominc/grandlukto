﻿using GrandLukto.Characters.StateMachine;
using GrandLukto.PowerUps;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Characters
{
	public class PlayerPowerUp : MonoBehaviour
	{
		private PowerUpManager compPowerUpManager;
		private CharacterStateMachine compStateMachine;
		private CharacterInputBase compInput;
		private CharacterBase compCharacterBase;
		private AudioSource compAudioSource;

		private PowerUpFactory powerUpFactory = new PowerUpFactory();

		public void Start()
		{
			compPowerUpManager = GetComponent<PowerUpManager>();
			compStateMachine = GetComponent<CharacterStateMachine>();
			compInput = GetComponent<CharacterInputBase>();
			compCharacterBase = GetComponent<CharacterBase>();
			compAudioSource = gameObject.GetOrAddComponent<AudioSource>();
		}

		public void Update()
		{
			// use powerup after powerup key press (non AI)
			if (!compInput.UsePowerUp) return;
			TryUsePowerUp();
		}

		public bool TryUsePowerUp()
		{
			if (!compStateMachine.ActiveState.CanUsePowerUp) return false;
			
			PowerUpType powerUpType;
			if (!compPowerUpManager.TryUsePowerUp(out powerUpType)) return false;
			compAudioSource.TryPlayOneShot(this, GameManager.Instance.gameConfiguration.soundUsePowerUp);

			if (compPowerUpManager.OnHitPowerUpActivated) return true; // no instant effect

			var powerUp = powerUpFactory.Create(powerUpType);
			powerUp.UsePowerUp(compCharacterBase);
			return true;
		}
	}
}