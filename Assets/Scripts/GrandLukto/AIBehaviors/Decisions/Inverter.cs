﻿using RAIN.Action;
using RAIN.Core;

namespace Assets.Scripts.GrandLukto.AIBehaviors.Decisions
{
	[RAINDecision("Inverter")]
	public class Inverter : RAINDecision
	{
		private int lastRunning = 0;

		public override void Start(AI ai)
		{
			base.Start(ai);

			lastRunning = 0;
		}

		public override ActionResult Execute(AI ai)
		{
			var result = ActionResult.SUCCESS;

			for (; lastRunning < _children.Count; lastRunning++)
			{
				result = _children[lastRunning].Run(ai);
				if (result != ActionResult.SUCCESS)
				{
					break;
				}
			}

			switch (result)
			{
				case ActionResult.SUCCESS: return ActionResult.FAILURE;
				case ActionResult.FAILURE: return ActionResult.SUCCESS;
				default: return result;
			}
		}
	}
}