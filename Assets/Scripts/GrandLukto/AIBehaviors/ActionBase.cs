﻿using GrandLukto.AIDataCollectors;
using RAIN.Action;
using RAIN.Core;
using System;

namespace GrandLukto.AIBehaviors
{
	public class ActionBase : RAINAction
	{
		private bool isInitialized = false;

		private IAIDataCollector dataCollector;
		public IAIDataCollector DataCollector
		{
			get
			{
				if (dataCollector == null) throw new Exception("DataCollector not loaded! Did you forget to call LoadDataCollector?");
				return dataCollector;
			}
			set
			{
				dataCollector = value;
			}
		}

		public override void Start(AI ai)
		{
			if (!isInitialized)
			{
				isInitialized = true;
				Initialize(ai);
			}

			base.Start(ai);
		}

		/// <summary>
		/// Called the first time the Start method is called.
		/// </summary>
		protected virtual void Initialize(AI ai)
		{
			LoadDataCollector(ai);
		}

		private void LoadDataCollector(AI ai)
		{
			if (dataCollector != null) return; // already loaded

			// try to get from WorkingMemory
			dataCollector = ai.WorkingMemory.GetItem<AIDataCollector>("DataCollector");
			if (dataCollector != null) return;

			// not found -> create new one
			dataCollector = new AIDataCollector(ai);
			ai.WorkingMemory.SetItem("DataCollector", dataCollector);
		}
	}
}