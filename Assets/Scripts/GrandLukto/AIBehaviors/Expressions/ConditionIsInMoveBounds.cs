﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Expressions
{
	/// <summary>
	/// Returns SUCCESS if the Vector2 is in the AI's movement bounds.
	/// </summary>
	[RAINAction]
	public class ConditionIsInMoveBounds : ActionBase
	{
		/// <summary>
		/// The position to check.
		/// </summary>
		public Expression positionVariable;
		
		public override ActionResult Execute(AI ai)
		{
			var position = positionVariable.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);
			var movementBounds = DataCollector.Position.GetMovementBounds();

			return movementBounds.Contains(new Vector3(position.x, position.y, movementBounds.center.z)) ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}