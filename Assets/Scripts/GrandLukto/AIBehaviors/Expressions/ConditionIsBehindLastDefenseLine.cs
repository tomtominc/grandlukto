﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Expressions
{
	/// <summary>
	/// Returns SUCCESS if the Vector2 in the provided variable is behind the AI's last defense line.
	/// </summary>
	[RAINAction]
	public class ConditionIsBehindLastDefenseLine : ActionBase
	{
		/// <summary>
		/// The position to check.
		/// </summary>
		public Expression positionVariable;
		
		public override ActionResult Execute(AI ai)
		{
			var position = positionVariable.Evaluate<Vector2>(ai.DeltaTime, ai.WorkingMemory);

			var lastDefenseLineToPosition = new Vector2(position.x, DataCollector.Position.GetLastDefenseLine()) - position;
			lastDefenseLineToPosition.Normalize();

			return Vector2.Dot(DataCollector.Character.FaceVector, lastDefenseLineToPosition) > 0 ? ActionResult.SUCCESS : ActionResult.FAILURE; 
		}
	}
}