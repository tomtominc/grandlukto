﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace GrandLukto.AIBehaviors.Expressions.PowerUps
{
	/// <summary>
	/// Succeeds if the character has an active effect that can be interrupted, fails otherwise.
	/// </summary>
	[RAINAction("Minimum time elapsed since last powerup usage")]
	public class MinTimeElapsedSinceLastPowerUpUsage : ActionBase
	{
		/// <summary>
		/// Minimum time since last power up usage in seconds.
		/// </summary>
		public Expression minimumTime;

		public override ActionResult Execute(AI ai)
		{
			var minimumTimeFloat = minimumTime.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
			return DataCollector.Character.MinimumTimeElapsedSinceLastPowerUpUsage(minimumTimeFloat) ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}