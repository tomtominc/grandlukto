﻿using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Expressions.PowerUps
{
	/// <summary>
	/// Succeeds if the character has an active effect that can be interrupted, fails otherwise.
	/// </summary>
	[RAINAction]
	public class HasInterruptableEffect : ActionBase
	{
		public override ActionResult Execute(AI ai)
		{
			return DataCollector.Character.HasActiveInterruptableEffect() ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}