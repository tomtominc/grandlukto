﻿using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Expressions.PowerUps
{
	/// <summary>
	/// Returns SUCCESS if the character has an empty PowerUp slot, otherwise FAILURE.
	/// </summary>
	[RAINAction("Has empty PowerUp slot")]
	public class HasEmptyPowerUpSlot : ActionBase
	{
		public override ActionResult Execute(AI ai)
		{
			return DataCollector.Character.HasEmptyPowerUpSlot ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}