﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace GrandLukto.AIBehaviors.Expressions
{
	[RAINAction("Is Ball Moving")]
	public class ConditionBallIsMoving : RAINAction
	{
		/// <summary>
		/// The ball to check.
		/// </summary>
		public Expression ballVariable;

		public override ActionResult Execute(AI ai)
		{
			var ball = ballVariable.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			return ball != null && ball.IsMoving ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}