﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using System.Linq;
using UnityCave.Collision;

namespace GrandLukto.AIBehaviors.Expressions
{
	/// <summary>
	/// Returns SUCCESS if any ball in the AI's view area is moving towards the AI, otherwise FAILURE.
	/// </summary>
	[RAINAction("Is any ball moving towards me")]
	public class ConditionIsAnyBallMovingTowardsMe : ActionBase
	{
		private ICollisionHandler fieldOfView;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			fieldOfView = DataCollector.CollisionHandlers.Get("FieldOfView");
		}

		public override ActionResult Execute(AI ai)
		{
			var incomingBall = fieldOfView.ActiveTriggerCollisions
				.Where(BallStatic.IsBallValid)
				.Select(gameObject => gameObject.GetComponent<Ball>())
				.FirstOrDefault(ball => ball && DataCollector.Position.IsMovingTowardsMe(ball.transform.position, ball.MoveDirection));

			return incomingBall ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}