﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.Collision;

namespace GrandLukto.AIBehaviors.Expressions
{
	[RAINAction("CollisionHandler contains elements")]
	public class ConditionCollisionHandlerContainsElements : ActionBase
	{
		/// <summary>
		/// The name of the CollisionHandler to check.
		/// </summary>
		public Expression collisionHandlerName = new Expression();

		/// <summary>
		/// The minimum number of objects that the must collide with the CollisionHandler so that this Condition returns SUCCESS.
		/// </summary>
		public Expression minObjects = new Expression();

		private ICollisionHandler collisionHandler;
		private int minObjectsEvaluated;
		
		public override void Start(AI ai)
		{
			base.Start(ai);

			collisionHandler = DataCollector.CollisionHandlers.Get(collisionHandlerName.Evaluate<string>(ai.DeltaTime, ai.WorkingMemory));
			minObjectsEvaluated = !minObjects.IsValid ? 1 : minObjects.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);
		}

		public override ActionResult Execute(AI ai)
		{
			return collisionHandler.ActiveTriggerCollisions.Count >= minObjectsEvaluated ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}