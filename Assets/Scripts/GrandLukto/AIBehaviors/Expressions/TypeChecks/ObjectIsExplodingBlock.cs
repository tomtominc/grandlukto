﻿using GrandLukto.Blocks;
using RAIN.Action;
using System;

namespace GrandLukto.AIBehaviors.Expressions.TypeChecks
{
	/// <summary>
	/// Returns SUCCESS if the provided target is an Exploding block.
	/// </summary>
	[RAINAction]
	public class ObjectIsExplodingBlock : ObjectIsAnyTypeBase
	{
		public override Type[] AllowedTypes
		{
			get
			{
				return new[] { typeof(BlockExplode) };
			}
		}
	}
}