﻿using RAIN.Core;
using RAIN.Representation;
using System;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Expressions.TypeChecks
{
	/// <summary>
	/// Base class for checking if a GameObject has a specific MonoBehaviour attached.
	/// </summary>
	public abstract class ObjectIsAnyTypeBase : ActionBase
	{
		public Expression target = new Expression();
		
		public abstract Type[] AllowedTypes { get; }

		public override ActionResult Execute(AI ai)
		{
			var gameObject = target.Evaluate<GameObject>(ai.DeltaTime, ai.WorkingMemory);
			if (gameObject == null) return ActionResult.FAILURE;

			foreach(var type in AllowedTypes)
			{
				if (gameObject.GetComponent(type) != null) return ActionResult.SUCCESS;
			}

			return ActionResult.FAILURE;
		}
	}
}