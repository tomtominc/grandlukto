﻿using GrandLukto.Blocks;
using RAIN.Action;
using System;

namespace GrandLukto.AIBehaviors.Expressions.TypeChecks
{
	/// <summary>
	/// Returns SUCCESS if the provided target is the heart wall.
	/// </summary>
	[RAINAction]
	public class ObjectIsHeartWall : ObjectIsAnyTypeBase
	{
		public override Type[] AllowedTypes
		{
			get
			{
				return new[] { typeof(BlockHeartWall) };
			}
		}
	}
}