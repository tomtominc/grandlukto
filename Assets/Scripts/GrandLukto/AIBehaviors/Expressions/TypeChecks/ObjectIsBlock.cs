﻿using GrandLukto.Blocks;
using RAIN.Action;
using System;

namespace GrandLukto.AIBehaviors.Expressions.TypeChecks
{
	/// <summary>
	/// Returns SUCCESS if the provided target is any block.
	/// </summary>
	[RAINAction]
	public class ObjectIsBlock : ObjectIsAnyTypeBase
	{
		public override Type[] AllowedTypes
		{
			get
			{
				return new[] { typeof(BlockBase) };
			}
		}
	}
}