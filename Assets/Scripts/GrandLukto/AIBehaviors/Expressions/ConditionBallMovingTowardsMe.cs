﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace GrandLukto.AIBehaviors.Expressions
{
	[RAINAction("Ball moving towards me")]
	public class ConditionBallMovingTowardsMe : ActionBase
	{
		/// <summary>
		/// The ball to check.
		/// </summary>
		public Expression ballVariable;
		
		public override ActionResult Execute(AI ai)
		{
			var ball = ballVariable.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			return DataCollector.Position.IsMovingTowardsMe(ball.transform.position, ball.MoveDirection) ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}