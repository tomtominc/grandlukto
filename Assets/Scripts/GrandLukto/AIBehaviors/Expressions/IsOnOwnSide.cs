﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.RAIN;

namespace GrandLukto.AIBehaviors.Expressions
{
	/// <summary>
	/// Returns SUCCESS if the provided target is in the AI's own half, otherwise FAILURE.
	/// </summary>
	[RAINAction("Is on own side")]
	public class IsOnOwnSide : ActionBase
	{
		public Expression target;

		public override ActionResult Execute(AI ai)
		{
			var targetPosition = target.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			var relativePosition = DataCollector.Position.ConvertToRelativePosition(targetPosition);
			return relativePosition.y < 1 ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}