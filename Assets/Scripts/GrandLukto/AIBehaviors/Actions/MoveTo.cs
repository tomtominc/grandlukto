﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.RAIN;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction]
	public class MoveTo : ActionBase
	{
		/// <summary>
		/// The target to which the AI should move. Can be a GameObject, MonoBehavior or Vector2.
		/// </summary>
		public Expression moveTarget = new Expression();

		public float targetReachedOffset = 0.2f;

		public override void Start(AI ai)
		{
			base.Start(ai);
		}

		public override ActionResult Execute(AI ai)
		{
			if (moveTarget.IsNull) return ActionResult.RUNNING;

			var target = moveTarget.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			DataCollector.Movement.MoveTo(target);

			return DataCollector.Movement.MoveTargetReached ? ActionResult.SUCCESS : ActionResult.RUNNING;
		}
	}
}