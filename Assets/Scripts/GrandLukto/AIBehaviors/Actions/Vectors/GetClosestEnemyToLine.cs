﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.ExtensionMethods.RAIN;
using UnityCave.RAIN;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.Vectors
{
	[RAINAction]
	public class GetClosestEnemyToLine : ActionBase
	{
		public Expression lineStart = new Expression();
		public Expression lineEnd = new Expression();

		public Expression targetDistance = new Expression();
		public Expression targetEnemy = new Expression();

		private IEnumerable<GameObject> enemies;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			enemies = DataCollector.Game.GetEnemies();
		}

		public override ActionResult Execute(AI ai)
		{
			if (!enemies.HasItems()) return ActionResult.FAILURE;

			var start = lineStart.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);
			var end = lineEnd.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			var closestDist = 0.0f;
			GameObject closestEnemy = null;

			foreach(var enemy in enemies)
			{
				var closestPointOnLine = Vector2Util.GetClosestPointOnLineSegment(start, end, enemy.transform.position);
				var distToClosestPoint = Vector2.Distance(closestPointOnLine, enemy.transform.position);

				if (closestEnemy == null || distToClosestPoint < closestDist)
				{
					closestEnemy = enemy;
					closestDist = distToClosestPoint;
				}
			}

			targetDistance.TrySetVariable(ai.WorkingMemory, closestDist);
			targetEnemy.TrySetVariable(ai.WorkingMemory, closestEnemy);
			return ActionResult.SUCCESS;
		}
	}
}