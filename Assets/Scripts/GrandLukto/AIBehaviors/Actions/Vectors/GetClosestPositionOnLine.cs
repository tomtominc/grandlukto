﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.ExtensionMethods.RAIN;
using UnityCave.RAIN;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.Vectors
{
	/// <summary>
	/// Gets the closest point on the line segment defined by <see cref="lineStart"/> and <see cref="lineEnd"/> and stores
	/// the closest position in the <see cref="targetPosition"/> variable and the distance to the distance to the closest
	/// position in the <see cref="targetDistance"/> variable.
	/// </summary>
	[RAINAction("Get closest position on line")]
	public class GetClosestPositionOnLine : ActionBase
	{
		public Expression lineStart = new Expression();
		public Expression lineEnd = new Expression();

		public Expression targetDistance = new Expression();
		public Expression targetPosition = new Expression();
		
		public override ActionResult Execute(AI ai)
		{
			var start = lineStart.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);
			var end = lineEnd.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			var closestPointOnLine = Vector2Util.GetClosestPointOnLineSegment(start, end, ai.Body.transform.position);
			var distToClosestPoint = Vector2.Distance(closestPointOnLine, ai.Body.transform.position);

			targetDistance.TrySetVariable(ai.WorkingMemory, distToClosestPoint);
			targetPosition.TrySetVariable(ai.WorkingMemory, closestPointOnLine);
			return ActionResult.SUCCESS;
		}
	}
}