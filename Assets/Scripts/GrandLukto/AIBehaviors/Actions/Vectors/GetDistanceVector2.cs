﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.Vectors
{
	/// <summary>
	/// Returns the distance between two Vector2 objects.
	/// </summary>
	[RAINAction]
	public class GetDistanceVector2 : ActionBase
	{
		public Expression objectA = new Expression();
		public Expression objectB = new Expression();

		public Expression targetVariable = new Expression();

		public override ActionResult Execute(AI ai)
		{
			var objectAPos = objectA.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);
			var objectBPos = objectB.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			ai.WorkingMemory.SetItem(targetVariable.VariableName, Vector2.Distance(objectAPos, objectBPos));
			return ActionResult.SUCCESS;
		}
	}
}