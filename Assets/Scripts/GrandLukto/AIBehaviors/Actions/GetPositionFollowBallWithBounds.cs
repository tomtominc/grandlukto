﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction]
	public class GetPositionFollowBallWithBounds : ActionBase
	{
		/// <summary>
		/// The currently targeted ball.
		/// </summary>
		public Expression targetBall;
		
		/// <summary>
		/// The minimum x value relative to the play area to which the NPC can move.
		/// </summary>
		public Expression minRelativeX;

		/// <summary>
		/// The minimum x value relative to the play area to which the NPC can move.
		/// </summary>
		public Expression maxRelativeX;

		/// <summary>
		/// Variable to which the evaluated position will be saved.
		/// </summary>
		public Expression targetVariable;
		
		public override ActionResult Execute(AI ai)
		{
			Vector2 position;
			if (!TryEvaluatePosition(ai, out position)) return ActionResult.FAILURE;

			ai.WorkingMemory.SetItem(targetVariable.VariableName, position);
			return ActionResult.SUCCESS;
		}

		private bool TryEvaluatePosition(AI ai, out Vector2 position)
		{
			var bounds = DataCollector.Position.GetMovementBounds();

			var minX = bounds.GetRelativeXPosition(minRelativeX.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory));
			var maxX = bounds.GetRelativeXPosition(maxRelativeX.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory));

			var ball = targetBall.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			if (ball == null)
			{
				position = Vector2.zero;
				return false;
			}

			// ball found -> move to ball x at the last defense line
			position = new Vector2(Mathf.Clamp(ball.transform.position.x, minX, maxX), DataCollector.Position.GetRelativeMovePosition(0.5f, 0).y);
			return true;
		}
	}
}