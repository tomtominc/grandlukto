﻿using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Waits for the NPCs ReactionTime.
	/// </summary>
	[RAINAction]
	public class WaitForThinkTime : ActionBase
	{
		private float timeLeft;

		public override void Start(AI ai)
		{
			base.Start(ai);

			timeLeft = DataCollector.Character.GetRandomReactionTime();
		}

		public override ActionResult Execute(AI ai)
		{
			timeLeft -= ai.DeltaTime;
			return timeLeft > 0 ? ActionResult.RUNNING : ActionResult.SUCCESS;
		}
	}
}