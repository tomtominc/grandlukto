﻿using GrandLukto.Balls;
using GrandLukto.Characters.StateMachine;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Returns the x position of the ball's next predicted hit position and an evaluated Expression as y value.
	/// </summary>
	[RAINAction]
	public class GetPositionPredictedBallHit : ActionBase
	{
		/// <summary>
		/// The currently targeted ball.
		/// </summary>
		public Expression targetBall;

		/// <summary>
		/// Y offset to the back line (e.g. "random(0, 1)").
		/// </summary>
		public Expression yOffset;

		/// <summary>
		/// Variable in which the evaluated move target will be stored.
		/// </summary>
		public Expression movePositionVariable;

		private Vector2? lastPredictedHitPoint;
		private Vector2 lastCalculatedMoveTarget;
		
		public override ActionResult Execute(AI ai)
		{
			var ball = targetBall.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			if (ball == null) return ActionResult.FAILURE;

			var nextHit = ball.PredictNextHitPosition();
			if (nextHit.collider == null) return ActionResult.RUNNING; // no hit predicted

			var movePosition = GetMovePosition(ai, ball, nextHit);
			ai.WorkingMemory.SetItem(movePositionVariable.VariableName, movePosition);
			return ActionResult.SUCCESS;
		}

		private Vector2 GetMovePosition(AI ai, Ball ball, RaycastHit2D nextHit)
		{
			// check if we calculated for the same raycast before, and return last movePosition
			if (lastPredictedHitPoint.HasValue && Vector2.Distance(nextHit.point, lastPredictedHitPoint.Value) < 0.1f) return lastCalculatedMoveTarget;
			lastPredictedHitPoint = nextHit.point;

			// recalculate moveTarget
			var target = new Vector2(lastPredictedHitPoint.Value.x, DataCollector.Position.GetRelativeMovePosition(0.5f, 0).y);

			// add yOffset
			target += DataCollector.Character.FaceVector * yOffset.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
			lastCalculatedMoveTarget = target;
			return target;
		}
	}
}