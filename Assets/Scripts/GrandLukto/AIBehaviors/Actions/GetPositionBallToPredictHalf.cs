﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Returns the x position between the ball and the ball's next predicted hit position and a relative y position.
	/// </summary>
	[RAINAction]
	public class GetPositionBallToPredictHalf : ActionBase
	{
		/// <summary>
		/// The ball to hit.
		/// </summary>
		public Expression targetBall;

		/// <summary>
		/// Y offset to the back line (e.g. "random(0, 1)").
		/// </summary>
		public Expression yOffset;

		/// <summary>
		/// Variable in which the evaluated move target will be stored.
		/// </summary>
		public Expression movePositionVariable;

		private Vector2? lastPredictedHitPoint;
		private Vector2 moveTarget;
		
		public override ActionResult Execute(AI ai)
		{
			var ball = targetBall.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			if (ball == null) return ActionResult.FAILURE;

			var nextHit = ball.PredictNextHitPosition();
			if (nextHit.collider == null) return ActionResult.RUNNING;

			var movePosition = GetMovePosition(ai, ball, nextHit);
			ai.WorkingMemory.SetItem(movePositionVariable.VariableName, movePosition);
			return ActionResult.SUCCESS;
		}

		private Vector2 GetMovePosition(AI ai, Ball ball, RaycastHit2D nextHit)
		{
			// always update x pos
			var halfBallPath = ball.LastHitPosition.Value + (nextHit.point - ball.LastHitPosition.Value) / 2;
			moveTarget.x = halfBallPath.x;

			// use same y pos for same prediction
			if (lastPredictedHitPoint.HasValue && Vector2.Distance(nextHit.point, lastPredictedHitPoint.Value) < 0.1f) return moveTarget;
			lastPredictedHitPoint = nextHit.point;

			// recalculate y
			moveTarget.y = DataCollector.Position.GetRelativeMovePosition(0.5f, 0).y;
			moveTarget += DataCollector.Character.FaceVector * yOffset.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
			return moveTarget;
		}
	}
}