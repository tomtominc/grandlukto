﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Gets the position at a relative y position at which the AI can intercept the ball.
	/// </summary>
	[RAINAction]
	public class GetPositionIntercept : ActionBase
	{
		/// <summary>
		/// Position of the object to intercept.
		/// </summary>
		public Expression lineToInterceptPointAVariable = new Expression();

		/// <summary>
		/// Predicted position to which the object to intercept will move.
		/// </summary>
		public Expression lineToInterceptPointBVariable = new Expression();

		/// <summary>
		/// Relative y in the move bounds at which the AI should defend.
		/// </summary>
		public Expression relativeYVariable = new Expression();

		/// <summary>
		/// Target to which the AI must move to intercept the object.
		/// </summary>
		public Expression targetVariable = new Expression();
		
		public override ActionResult Execute(AI ai)
		{
			var faceVector = DataCollector.Character.FaceVector;

			var pointA = lineToInterceptPointAVariable.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);
			var pointB = lineToInterceptPointBVariable.EvaluatePosition2D(ai.DeltaTime, ai.WorkingMemory);

			var lineAToB = pointB - pointA;

			// check if objects is moving to the intercept line
			if (Vector2.Dot(faceVector, lineAToB.normalized) > 0) return ActionResult.FAILURE;

			// check if ball is in front of interception line
			var interceptY = DataCollector.Position.GetRelativeHitPosition(0, relativeYVariable.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory)).y;
			if (Vector2.Dot(faceVector, (pointA - new Vector2(pointA.x, interceptY)).normalized) < 0) return ActionResult.FAILURE; // object already behind intercept line

			var xPerY = lineAToB.x / lineAToB.y;
			var interceptX = pointA.x + xPerY * (interceptY - pointA.y);

			var moveTarget = new Vector2(interceptX, interceptY);

			// convert the ball position to the move position
			moveTarget -= DataCollector.Character.FaceVector * DataCollector.Character.HitOffset;

			ai.WorkingMemory.SetItem(targetVariable.VariableName, moveTarget);
			return ActionResult.SUCCESS;
		}
	}
}
