﻿using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction("Wait for game start")]
	public class WaitForGameStart : ActionBase
	{
		private GameManager gameManager;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			gameManager = GameManager.Instance;
		}

		public override ActionResult Execute(AI ai)
		{
			return GameManager.Instance.IsGameRunning ? ActionResult.SUCCESS : ActionResult.RUNNING;
		}
	}
}