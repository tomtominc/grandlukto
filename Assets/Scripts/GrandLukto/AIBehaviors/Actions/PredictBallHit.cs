﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Predicts the next ball hit and assigns the hit position as Vector2 to the target variable.
	/// Returns SUCCESS if a hit was predicted, otherwise false.
	/// </summary>
	[RAINAction]
	public class PredictBallHit : ActionBase
	{
		public Expression targetBall;

		public Expression targetVariable;
		public Expression collidingGameObjectVariable;
		
		public override ActionResult Execute(AI ai)
		{
			var ball = targetBall.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);

			var nextHit = ball.PredictNextHitPosition();
			if (nextHit.collider == null)
			{
				return ActionResult.FAILURE;
			}

			
			if (targetVariable.IsValid) ai.WorkingMemory.SetItem(targetVariable.VariableName, (Vector2)nextHit.transform.position);
			if (collidingGameObjectVariable.IsValid) ai.WorkingMemory.SetItem(collidingGameObjectVariable.VariableName, nextHit.collider.gameObject);
			return ActionResult.SUCCESS;
		}
	}
}