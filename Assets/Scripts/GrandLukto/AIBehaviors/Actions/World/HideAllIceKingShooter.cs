﻿using GrandLukto.Characters;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCave.CustomDataTypes;
using UnityCave.ExtensionMethods;
using UnityCave.ExtensionMethods.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Hide all IceKing Shooter")]
	public class HideAllIceKingShooter : ActionBase
	{
		private List<GameObject> shootersToHide;
		private bool allHidden;
		private int hiddenShooters;

		private Coroutine activeRoutine;

		private FloatRange waitBetweenHides = new FloatRange(0.01f, 0.2f);

		public Expression outHiddenShooters = new Expression();

		public override void Start(AI ai)
		{
			base.Start(ai);

			shootersToHide = DataCollector.Game.GetEntities<IceKingShooter>().ToList();

			hiddenShooters = 0;
			allHidden = false;

			activeRoutine = DataCollector.StartCoroutine(HideRoutine());
		}

		private IEnumerator HideRoutine()
		{
			while(shootersToHide.Count > 0)
			{
				var shooter = shootersToHide.SelectRandom();
				shootersToHide.Remove(shooter);

				if (shooter == null) continue;

				yield return new WaitForSeconds(waitBetweenHides);

				if (shooter != null && shooter.GetComponent<IceKingShooter>().TryHide())
				{
					hiddenShooters++;
				}
			}

			allHidden = true;
		}

		public override ActionResult Execute(AI ai)
		{
			if (!allHidden)
			{
				return ActionResult.RUNNING;
			}

			outHiddenShooters.TrySetVariable(ai.WorkingMemory, hiddenShooters);
			return ActionResult.SUCCESS;
		}

		public override void Stop(AI ai)
		{
			base.Stop(ai);

			if (activeRoutine != null)
			{
				DataCollector.StopCoroutine(activeRoutine);
			}
		}
	}
}