﻿using GrandLukto.Blocks;
using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Wait for own ExplodingBlock destroyed.")]
	public class WaitForOwnExplodingBlockDestroyed : WaitForBlockDestroyed
	{
		protected override bool IsMatchingEvent(object sender, BlockDestroyedEventArgs e)
		{
			return e.Team == DataCollector.Character.Team && e.Block is BlockExplode;
		}
	}
}