﻿using GrandLukto.Characters;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Spawn IceKing Shooter - Shoot single snowball")]
	public class SpawnIceKingShooterSingleSnowball : ActionBase
	{
		public Expression spawnTileX = new Expression();
		public Expression spawnTileY = new Expression();

		private IceKingShooterFactory shooterFactory;
		private IceKingShooter spawnedShooter;
		private bool ballSpawned;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			shooterFactory = new IceKingShooterFactory();
		}

		public override void Start(AI ai)
		{
			base.Start(ai);

			ballSpawned = false;
		}

		public override ActionResult Execute(AI ai)
		{
			if (!spawnedShooter)
			{
				SpawnShooter(ai);
			}

			// wait until the ball is spawned
			return ballSpawned ? ActionResult.SUCCESS : ActionResult.RUNNING;
		}

		private void SpawnShooter(AI ai)
		{
			var intSpawnTileX = spawnTileX.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);
			var intSpawnTileY = spawnTileY.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);

			var spawnPosition = DataCollector.Position.ConvertToAbsolutePosition(new Vector2(
				(intSpawnTileX + 0.5f) / DataCollector.Position.MaxTilesX,
				(intSpawnTileY + 0.5f) / DataCollector.Position.MaxTilesY
			));

			var shooterGameObject = shooterFactory.SpawnShootSingleSnowball();
			shooterGameObject.transform.position = spawnPosition;

			spawnedShooter = shooterGameObject.GetComponent<IceKingShooter>();
			spawnedShooter.BallSpawned += SpawnedShooter_BallSpawned;
		}

		private void SpawnedShooter_BallSpawned(object sender, System.EventArgs e)
		{
			ballSpawned = true;
		}

		public override void Stop(AI ai)
		{
			base.Stop(ai);

			if (spawnedShooter)
			{
				spawnedShooter.BallSpawned -= SpawnedShooter_BallSpawned;
				spawnedShooter = null;
			}
		}
	}
}