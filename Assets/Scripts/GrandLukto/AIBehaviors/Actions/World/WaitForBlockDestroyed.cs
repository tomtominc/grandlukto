﻿using GrandLukto.Blocks;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Actions.World
{
	public class WaitForBlockDestroyed : ActionBase
	{
		private bool blockDestroyed = false;
		
		public override void Start(AI ai)
		{
			blockDestroyed = false; // reset

			base.Start(ai);

			DataCollector.Blocks.Events.BlockDestroyed += BlockDestroyed;
		}

		private void BlockDestroyed(object sender, BlockDestroyedEventArgs e)
		{
			if (!IsMatchingEvent(sender, e)) return; // no match

			blockDestroyed = true;
		}

		protected virtual bool IsMatchingEvent(object sender, BlockDestroyedEventArgs e)
		{
			return true;
		}

		public override ActionResult Execute(AI ai)
		{
			return blockDestroyed ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}

		public override void Stop(AI ai)
		{
			base.Stop(ai);

			DataCollector.Blocks.Events.BlockDestroyed -= BlockDestroyed;
		}
	}
}