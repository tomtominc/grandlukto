﻿using GrandLukto.Characters;
using RAIN.Action;
using RAIN.Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCave.CustomDataTypes;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Shoot all IceKing Shooters")]
	public class ShootAllIceKingShooters : ActionBase
	{
		private List<GameObject> activeShooters;
		private Coroutine activeRoutine;

		private FloatRange waitBetweenShots = new FloatRange(0f, 0.2f);

		public override void Start(AI ai)
		{
			base.Start(ai);

			activeShooters = DataCollector.Game.GetEntities<IceKingShooter>().ToList();

			activeRoutine = DataCollector.StartCoroutine(ShootRoutine());
		}

		private IEnumerator ShootRoutine()
		{
			while(activeShooters.Count > 0)
			{
				var shooter = activeShooters.SelectRandom();
				activeShooters.Remove(shooter);

				if (shooter == null) continue;

				yield return new WaitForSeconds(waitBetweenShots);

				if (shooter != null) shooter.GetComponent<IceKingShooter>().TryShoot();
			}

			activeRoutine = null;
		}

		public override ActionResult Execute(AI ai)
		{
			return activeRoutine != null ? ActionResult.RUNNING : ActionResult.SUCCESS;
		}

		public override void Stop(AI ai)
		{
			base.Stop(ai);

			if (activeRoutine != null)
			{
				DataCollector.StopCoroutine(activeRoutine);
			}
		}
	}
}