﻿using GrandLukto.Characters;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityCave.Tiles;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Spawn IceKing Shooter")]
	public class SpawnIceKingShooter : ActionBase
	{
		private const int MinTileY = 8;

		/// <summary>
		/// The amount of shooters to spawn.
		/// </summary>
		public Expression numShooters = new Expression();

		private IceKingShooterFactory factory = new IceKingShooterFactory();

		private TilePatternPositionerConfig spawnConfig2;
		private TilePatternPositionerConfig spawnConfig3;
		private TilePatternPositionerConfig spawnConfig4;

		private int MaxTilesX { get { return DataCollector.Position.MaxTilesX; } }
		private int MaxTilesY { get { return DataCollector.Position.MaxTilesY; } }

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			var patternConfigParser = new TilePatternPositionerConfigParser();
			var gameManager = GameManager.Instance;

			spawnConfig2 = patternConfigParser.ParseConfig(gameManager.gameConfiguration.spawnPatternConfig2.text);
			spawnConfig3 = patternConfigParser.ParseConfig(gameManager.gameConfiguration.spawnPatternConfig3.text);
			spawnConfig4 = patternConfigParser.ParseConfig(gameManager.gameConfiguration.spawnPatternConfig4.text);
		}

		public override ActionResult Execute(AI ai)
		{
			var numShootersInt = numShooters.Evaluate<int>(ai.DeltaTime, ai.WorkingMemory);
			numShootersInt = Mathf.Max(numShootersInt, 1);

			switch (numShootersInt)
			{
				case 1:
					SpawnShooter(Random.Range(0, MaxTilesX), Random.Range(MinTileY, MaxTilesY));
					break;

				case 2:
					SpawnWithRandomPattern(spawnConfig2);
					break;

				case 3:
					SpawnWithRandomPattern(spawnConfig3);
					break;

				case 4:
					SpawnWithRandomPattern(spawnConfig4);
					break;
			}
			
			return ActionResult.SUCCESS;
		}

		private void SpawnWithRandomPattern(TilePatternPositionerConfig config)
		{
			var pattern = config.GetRandomPattern(0, 0, MaxTilesX, MaxTilesY - MinTileY);

			var offset = new IntVector2(
				Random.Range(0, MaxTilesX - pattern.Width),
				Random.Range(MinTileY, MaxTilesY - pattern.Height));

			foreach(var spawnPosition in pattern.SpawnPositions)
			{
				SpawnShooter(offset + spawnPosition);
			}
		}

		private void SpawnShooter(IntVector2 position)
		{
			SpawnShooter(position.x, position.y);
		}

		private void SpawnShooter(int tileX, int tileY)
		{
			var newShooter = factory.Spawn();
			newShooter.transform.position = DataCollector.Position.ConvertToAbsolutePosition(new Vector2(
				(tileX + 0.5f) / MaxTilesX,
				(tileY + 0.5f) / MaxTilesY
			));
		}
	}
}