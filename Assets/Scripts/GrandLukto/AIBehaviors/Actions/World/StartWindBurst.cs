﻿using RAIN.Action;
using RAIN.Core;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.World
{
	[RAINAction("Start wind burst")]
	public class StartWindBurst : ActionBase
	{
		private WindManager windManager;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			windManager = Object.FindObjectOfType<WindManager>();
		}

		public override ActionResult Execute(AI ai)
		{
			windManager.StartBurstTween();
			return ActionResult.SUCCESS;
		}
	}
}