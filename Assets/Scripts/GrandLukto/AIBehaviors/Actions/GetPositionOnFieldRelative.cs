﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	/// <summary>
	/// Returns a relative position on the field.
	/// X: 0 - left, 1 - right
	/// Y: 0 - position before blocks; 1 - middle line
	/// </summary>
	[RAINAction]
	public class GetPositionOnFieldRelative : ActionBase
	{
		/// <summary>
		/// The relative x position to evaluate.
		/// </summary>
		public Expression relativeX;

		/// <summary>
		/// The relative y position to evaluate.
		/// </summary>
		public Expression relativeY;

		/// <summary>
		/// Variable to which the calculated position is assigned.
		/// </summary>
		public Expression targetVariable;
		
		public override ActionResult Execute(AI ai)
		{
			var bounds = DataCollector.Position.GetMovementBounds();

			var evaluatedPosition = new Vector2();
			evaluatedPosition.x = Mathf.Lerp(bounds.min.x, bounds.max.x, relativeX.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory));
			evaluatedPosition.y = Mathf.Lerp(bounds.min.y, bounds.max.y, relativeY.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory));

			ai.WorkingMemory.SetItem(targetVariable.VariableName, evaluatedPosition);
			return ActionResult.SUCCESS;
		}
	}
}