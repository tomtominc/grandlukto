﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction]
	public class MoveToHitBallDirect : ActionBase
	{
		/// <summary>
		/// The ball to hit.
		/// </summary>
		public Expression targetBall = new Expression();
		
		public override ActionResult Execute(AI ai)
		{
			var ball = targetBall.Evaluate<Ball>(ai.DeltaTime, ai.WorkingMemory);
			if (ball == null) return ActionResult.FAILURE;

			var moveTarget = (Vector2)ball.transform.position - DataCollector.Character.FaceVector * (DataCollector.Character.ColliderSize + 0.05f);
			DataCollector.Movement.MoveTo(moveTarget);

			return ActionResult.RUNNING;
		}
	}
}