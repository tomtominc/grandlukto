﻿using GrandLukto.Balls.Controllers;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using System.Linq;
using UnityCave.ExtensionMethods.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.Balls
{
	[RAINAction("Count all balls")]
	public class CountAllBalls : ActionBase
	{
		public Expression outBallsCount = new Expression();

		public override ActionResult Execute(AI ai)
		{
			if (outBallsCount.IsVariable)
			{
				var ballsCount = Object.FindObjectsOfType<BallController>().Count();
				outBallsCount.TrySetVariable(ai.WorkingMemory, ballsCount);
			}

			return ActionResult.SUCCESS;
		}
	}
}