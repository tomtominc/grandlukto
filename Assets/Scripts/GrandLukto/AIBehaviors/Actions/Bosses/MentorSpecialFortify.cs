﻿using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using GrandLukto.Blocks;
using RAIN.Action;
using RAIN.Core;
using System.Collections;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.Bosses
{
	[RAINAction]
	public class MentorSpecialFortify : ActionBase
	{
		private float totalDuration = 30f;

		private List<BallShooter> ballShooters;
		private BallFactory ballFactory;

		private Coroutine spawnBallsRoutine;

		public override void Start(AI ai)
		{
			base.Start(ai);

			ballFactory = new BallFactory();

			ballShooters = DataCollector.Blocks.GetTilePrefabsWithComponent<BallShooter>();

			DataCollector.Blocks.FortifyAllBlocks(totalDuration);

			spawnBallsRoutine = DataCollector.StartCoroutine(SpawnBallsRoutine());

			// TODO: Make ball disappear on first execution?
		}

		private IEnumerator SpawnBallsRoutine()
		{
			yield return new WaitForSeconds(10);
			SpawnRandomBall();
			yield return new WaitForSeconds(5);
			SpawnRandomBall();
			yield return new WaitForSeconds(5);
			SpawnRandomBall();
			yield return new WaitForSeconds(2);
			SpawnRandomBall();
			yield return new WaitForSeconds(2);
			SpawnRandomBall();
			yield return new WaitForSeconds(1);
			spawnBallsRoutine = null;
		}

		private void SpawnRandomBall()
		{
			var ballShooter = ballShooters.SelectRandom();
			ballShooter.Shoot(0, Random.Range(0f, 0.25f), CreateBallShooterBall);
		}

		private IBallController CreateBallShooterBall()
		{
			return ballFactory.CreateBall(BallType.Rock);
		}

		public override ActionResult Execute(AI ai)
		{
			return spawnBallsRoutine != null ? ActionResult.RUNNING : ActionResult.SUCCESS;
		}

		public override void Stop(AI ai)
		{
			if (spawnBallsRoutine != null)
			{
				DataCollector.StopCoroutine(spawnBallsRoutine);
			}

			base.Stop(ai);
		}
	}
}