﻿using GrandLukto.Balls;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using System;
using System.Linq;
using UnityCave.Collision;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction("DetectBall")]
	public class DetectBall : ActionBase
	{
		/// <summary>
		/// Variable to which the result ball will be saved.
		/// </summary>
		public Expression ballVariable = new Expression();

		private ICollisionHandler fieldOfView;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			fieldOfView = DataCollector.CollisionHandlers.Get("FieldOfView");
		}
		
		public override ActionResult Execute(AI ai)
		{
			if (!ballVariable.IsVariable) throw new ArgumentException("ballVariable must be a variable");

			var incomingBallsByDistance = fieldOfView.ActiveTriggerCollisions
				.Where(BallStatic.IsBallValid)
				.Select(gameObject => gameObject.GetComponent<Ball>())
				.Where(ball => !ball.IsMoving || DataCollector.Position.IsMovingTowardsMe(ball.transform.position, ball.MoveDirection))
				// we could also compare to the distance of the ball's next predicted position.
				.OrderBy(ball => Vector2.Distance(ai.Body.transform.position, ball.transform.position));

			if (incomingBallsByDistance.HasItems())
			{
				ai.WorkingMemory.SetItem(ballVariable.VariableName, incomingBallsByDistance.First());
			}

			return ActionResult.SUCCESS;
		}
	}
}