﻿using GrandLukto.Blocks;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace GrandLukto.AIBehaviors.Actions.PowerUps
{
	/// <summary>
	/// Saves the provided PowerUpSpawner's position into the targetPosition variable.
	/// </summary>
	[RAINAction("Get powerUp spawner position")]
	public class GetPowerUpSpawnerPosition : ActionBase
	{
		public Expression powerUpSpawner = new Expression();
		public Expression targetPosition = new Expression();

		public override ActionResult Execute(AI ai)
		{
			var spawner = powerUpSpawner.Evaluate<IPowerUpSpawner>(ai.DeltaTime, ai.WorkingMemory);
			if (spawner == null) return ActionResult.FAILURE;

			ai.WorkingMemory.SetItem(targetPosition.VariableName, spawner);
			return ActionResult.SUCCESS;
		}
	}
}