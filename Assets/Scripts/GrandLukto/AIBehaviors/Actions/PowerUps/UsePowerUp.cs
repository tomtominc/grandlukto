﻿using RAIN.Action;
using RAIN.Core;

namespace GrandLukto.AIBehaviors.Actions.PowerUps
{
	/// <summary>
	/// Tries to use a powerup and only succeeds if it was used.
	/// Otherwise the action fails.
	/// </summary>
	[RAINAction("Use next powerup")]
	public class UsePowerUp : ActionBase
	{
		public override ActionResult Execute(AI ai)
		{
			return DataCollector.Character.TryUsePowerUp() ? ActionResult.SUCCESS : ActionResult.FAILURE;
		}
	}
}