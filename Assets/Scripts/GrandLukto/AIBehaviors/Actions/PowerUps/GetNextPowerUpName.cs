﻿using GrandLukto.PowerUps;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace GrandLukto.AIBehaviors.Actions.PowerUps
{
	/// <summary>
	/// Gets the name of the next PowerUp and saves it into a variable.
	/// </summary>
	[RAINAction("Get next powerup name")]
	public class GetNextPowerUpName : ActionBase
	{
		public Expression targetVariable = new Expression();

		public override ActionResult Execute(AI ai)
		{
			PowerUpType nextPowerUp;
			if (!DataCollector.Character.TryGetNextPowerUpInQueue(out nextPowerUp))
			{
				return ActionResult.FAILURE;
			}

			ai.WorkingMemory.SetItem(targetVariable.VariableName, nextPowerUp.ToString());
			return ActionResult.SUCCESS;
		}
	}
}