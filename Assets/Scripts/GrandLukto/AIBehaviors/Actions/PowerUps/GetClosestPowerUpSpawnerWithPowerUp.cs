﻿using GrandLukto.Blocks;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.ExtensionMethods.RAIN;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions.PowerUps
{
	/// <summary>
	/// Finds the closest spawner that has a PowerUp spawned and sets this IPowerUpSpawner instance as value of the targetSpawner variable.
	/// Returns SUCCESS if a spawner was found, otherwise FAILURE.
	/// </summary>
	[RAINAction("Get closest PowerUpSpawner with PowerUp")]
	public class GetClosestPowerUpSpawnerWithPowerUp : ActionBase
	{
		public Expression targetSpawner = new Expression();

		private List<IPowerUpSpawner> powerUpSpawners;

		protected override void Initialize(AI ai)
		{
			base.Initialize(ai);

			// get power up spawners and cache them since they won't change
			powerUpSpawners = DataCollector.Blocks.GetTilePrefabsWithComponent<IPowerUpSpawner>();
		}

		public override ActionResult Execute(AI ai)
		{
			if (!powerUpSpawners.HasItems()) return ActionResult.FAILURE;

			var activePowerUpSpawners = powerUpSpawners.Where(spawner => spawner.IsPowerUpSpawned);
			if (!activePowerUpSpawners.HasItems()) return ActionResult.FAILURE;

			var playerPos = DataCollector.Position.Position;

			var closestActiveSpawner = activePowerUpSpawners
				.OrderBy(spawner => Vector2.Distance(playerPos, spawner.CenterPosition))
				.First();

			targetSpawner.TrySetVariable(ai.WorkingMemory, closestActiveSpawner);
			return ActionResult.SUCCESS;
		}
	}
}