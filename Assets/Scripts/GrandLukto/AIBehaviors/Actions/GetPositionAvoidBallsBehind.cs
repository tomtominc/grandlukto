﻿using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;

namespace GrandLukto.AIBehaviors.Actions
{
	[RAINAction("Get position avoid balls behind")]
	public class GetPositionAvoidBallsBehind : ActionBase
	{
		/// <summary>
		/// How far the character will move on the X axis to avoid the balls.
		/// </summary>
		public Expression avoidXRel = new Expression();

		/// <summary>
		/// Relative y position to which the character will move when avoiding.
		/// </summary>
		public Expression targetYRel = new Expression();

		/// <summary>
		/// Variable in which the target position will be stored.
		/// </summary>
		public Expression targetVariable = new Expression();
		
		public override ActionResult Execute(AI ai)
		{
			var avoidXRelEvaluated = avoidXRel.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
			var targetYRelEvaluated = targetYRel.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);

			var currentPosition = DataCollector.Position.ConvertToRelativePosition(ai.Body.transform.position);

			var targetPositionRel = currentPosition;
			if (targetPositionRel.x < 0.5) targetPositionRel.x += avoidXRelEvaluated;
			else targetPositionRel.x -= avoidXRelEvaluated;

			targetPositionRel.x = Mathf.Clamp01(targetPositionRel.x);
			targetPositionRel.y = targetYRelEvaluated;

			var targetPositionAbs = DataCollector.Position.ConvertToAbsolutePosition(targetPositionRel);

			ai.WorkingMemory.SetItem(targetVariable.VariableName, targetPositionAbs);

			return ActionResult.SUCCESS;
		}
	}
}