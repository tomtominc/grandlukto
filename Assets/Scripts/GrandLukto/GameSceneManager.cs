﻿using GrandLukto.Characters;
using GrandLukto.Database;
using GrandLukto.GameData;
using System.Collections.Generic;
using UnityCave;
using UnityCave.UI;
using UnityEngine.SceneManagement;

namespace GrandLukto
{
	public static class GameSceneManager
	{
		// TODO: Maybe find a better way to store those static variables
		public static string LoadOverworldUnlockedNodeID { get; private set; }

		private static DataArena loadGameArena;
		private static DataLevel loadGameLevel;
		private static List<PlayerSetting> loadGamePlayerSettings;
		private static GameDataBase loadGameData;

		public static void LoadMenu()
		{
			SceneManager.sceneLoaded += OnMenuLoaded;
			SceneManager.LoadSceneAsync(SceneName.Menu.ToString(), LoadSceneMode.Single);
		}

		private static void OnMenuLoaded(Scene scene, LoadSceneMode mode)
		{
			SceneManager.sceneLoaded -= OnMenuLoaded;

			TimeManager.Instance.Reset();
		}

		public static void LoadGame(DataArena arena, DataLevel level, List<PlayerSetting> playerSettings, GameDataBase gameData = null)
		{
			loadGameArena = arena;
			loadGameLevel = level;
			loadGamePlayerSettings = playerSettings;
			loadGameData = gameData ?? new GameDataBase();

			SceneManager.sceneLoaded += OnGameSceneLoaded;
			SceneManager.LoadSceneAsync(SceneName.TestStefan.ToString(), LoadSceneMode.Single);
		}

		private static void OnGameSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			SceneManager.sceneLoaded -= OnGameSceneLoaded;

			TimeManager.Instance.Reset();
			GameManager.Instance.InitializeGame(loadGameArena, loadGameLevel, loadGamePlayerSettings, loadGameData);

			UIManager.Instance.PlaySound(GameManager.Instance.gameConfiguration.soundGameIn);
		}

		public static void LoadOverworld(string unlockedNodeID = null)
		{
			LoadOverworldUnlockedNodeID = unlockedNodeID;

			SceneManager.sceneLoaded += OnOverworldLoaded;

			SceneManager.LoadSceneAsync(SceneName.Overworld.ToString(), LoadSceneMode.Single);
		}

		private static void OnOverworldLoaded(Scene scene, LoadSceneMode mode)
		{
			SceneManager.sceneLoaded -= OnOverworldLoaded;

			TimeManager.Instance.Reset();
		}
	}
}