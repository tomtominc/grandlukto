﻿using GrandLukto.Blocks;
using System.Collections.Generic;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class PracticeTargetsManager : MonoBehaviour
	{
		public class TargetsChangedArgs : System.EventArgs
		{
			public int AliveTargets { get; private set; }
			public int DeadTargets { get; private set; }
			public int SpawnedTargets { get; private set; }
			public Vector2? HitPosition { get; private set; }

			public TargetsChangedArgs(int spawnedTargets, int aliveTargets, int deadTargets, Vector2? hitPosition)
			{
				SpawnedTargets = spawnedTargets;
				AliveTargets = aliveTargets;
				DeadTargets = deadTargets;
				HitPosition = hitPosition;
			}
		}

		public static PracticeTargetsManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<PracticeTargetsManager>();
			}
		}

		private List<PracticeTarget> spawnedTargets;
		private List<PracticeTarget> aliveTargets;

		public System.EventHandler<TargetsChangedArgs> OnTargetsChanged;

		public int AliveTargetsCount
		{
			get
			{
				return aliveTargets.Count;
			}
		}

		private void Awake()
		{
			ResetTargets();
		}

		public void ResetTargets()
		{
			aliveTargets = new List<PracticeTarget>();
			spawnedTargets = new List<PracticeTarget>();
		}

		public void RegisterTarget(PracticeTarget target)
		{
			if (aliveTargets.Contains(target)) return; // if it is alive it must have spawned

			if (!spawnedTargets.Contains(target))
			{
				spawnedTargets.Add(target);
			}

			aliveTargets.Add(target);

			FireTargetsChanged();
		}

		public void DestroyTarget(PracticeTarget target, Vector2? hitPosition = null)
		{
			if (!aliveTargets.Remove(target)) return; // target was not part of the list
			FireTargetsChanged(hitPosition);
		}

		private void FireTargetsChanged(Vector2? hitPosition = null)
		{
			if (OnTargetsChanged == null) return;

			OnTargetsChanged(this, new TargetsChangedArgs(
				spawnedTargets.Count,
				AliveTargetsCount,
				spawnedTargets.Count - AliveTargetsCount,
				hitPosition));
		}
	}
}