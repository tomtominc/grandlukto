﻿using System;
using UnityEngine;

namespace GrandLukto.AIData
{
	[Serializable]
	public class CollisionHandlerDefinition
	{
		public string name;
		public GameObject gameObject;
	}
}