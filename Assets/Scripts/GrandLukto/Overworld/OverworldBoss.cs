﻿using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldBoss : OverworldObjectBase
	{
		public GameObject goBoss;
		private Renderer bossRenderer;

		public float nodeOffsetEllipsisX;
		public float nodeOffsetEllipsisY;

		public float dissolveDuration = 1f;

		private bool isBossShown;
		private float curDissolveValue = 0;

		public override void Awake()
		{
			base.Awake();

			bossRenderer = goBoss.GetComponent<Renderer>();
		}

		public override void Start()
		{
			base.Start();

			if (IsUnlocked)
			{
				isBossShown = true;
				curDissolveValue = 0;
			}
			else
			{
				curDissolveValue = 1;
			}
		}
		
		public override Vector3 GetClosestNodePosition(OverworldObjectBase to)
		{
			var angle = Vector2Util.AngleSigned(Vector2.right, to.transform.position - transform.position);
			var angleRad = angle * Mathf.Deg2Rad;

			return transform.position + new Vector3(nodeOffsetEllipsisX * Mathf.Cos(angleRad), nodeOffsetEllipsisY * Mathf.Sin(angleRad));
		}

		public void Update()
		{
			if (curDissolveValue <= 0) return;

			if (isBossShown)
			{
				curDissolveValue -= 1 / dissolveDuration * Time.deltaTime;
				curDissolveValue = Mathf.Clamp01(curDissolveValue);
			}

			bossRenderer.material.SetFloat("_DissolveAmount", curDissolveValue); // do in update because assigned material can change
		}

		public override void OnPlayerJumpsOnNode()
		{
			base.OnPlayerJumpsOnNode();

			isBossShown = true;
		}
	}
}