﻿using UnityCave.CustomDataTypes;
using UnityCave.ExtensionMethods;
using UnityCave.GraphicUtils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldClouds : FullScreenGraphic
	{
		public float cloudsOffsetXY = 7;
		public float parallax = 1.5f;
		public float minCloudSize = 0.33f;
		public FloatRange cloudScaleVelocity;
		public FloatRange cloudScaleChange;
		public FloatRange cloudScaleTime;

		private Vector2? lastPosition;

		public Vector2? FrameOffset { get; private set; }
		public Bounds CloudBounds { get; private set; }

		private int lastCloudsX = 0;
		private int lastCloudsY = 0;
		
		protected override void UpdateGraphic(float width, float height, float halfWidth, float halfHeight)
		{
			CreateClouds();
		}

		private void CreateClouds()
		{
			var screenExtends = Camera.main.GetScreenExtendsWorld();

			// add offset to avoid hiding clouds before they are off screen
			screenExtends.x += 2;
			screenExtends.y += 2;

			var cloudsX = Mathf.CeilToInt(screenExtends.x / cloudsOffsetXY);
			var cloudsY = Mathf.CeilToInt(screenExtends.y / cloudsOffsetXY);

			if (cloudsX == lastCloudsX && cloudsY == lastCloudsY) return; // we already have the amount of clouds we need

			transform.RemoveAllChildren();

			var curY = -(cloudsY * cloudsOffsetXY) / 2 + cloudsOffsetXY / 2;
			for(var ctY = 0; ctY < cloudsY; ctY++)
			{
				var curX = -(cloudsX * cloudsOffsetXY) / 2 + cloudsOffsetXY / 2;
				for (var ctX = 0; ctX < cloudsX; ctX++)
				{
					CreateClouds(curX, curY);

					curX += cloudsOffsetXY;
				}

				curY += cloudsOffsetXY;
			}

			CloudBounds = new Bounds(Vector3.zero, new Vector3(cloudsX * cloudsOffsetXY, cloudsY * cloudsOffsetXY, 1));

			lastCloudsX = cloudsX;
			lastCloudsY = cloudsY;
		}

		private void CreateClouds(float x, float y)
		{
			var cloudsContainer = Instantiate(Resources.Load<GameObject>("Overworld/Clouds"), transform);
			cloudsContainer.transform.localPosition = new Vector3(x, y, 0);

			while(cloudsContainer.transform.childCount > 0)
			{
				var cloud = cloudsContainer.transform.GetChild(0);

				cloud.SetParent(transform, true); // remove from container and add as direct child

				cloud.gameObject.AddComponent<OverworldCloud>(); // add cloud component here since it is not in the prefab
			}

			// remove the container
			Destroy(cloudsContainer);
		}

		public void LateUpdate()
		{
			if (lastPosition.HasValue)
			{
				FrameOffset = (Vector2)transform.position - lastPosition.Value;
				FrameOffset *= parallax;
			}

			lastPosition = transform.position;
		}
	}
}