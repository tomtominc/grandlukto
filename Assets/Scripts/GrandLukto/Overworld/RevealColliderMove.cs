﻿using DG.Tweening;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class RevealColliderMove : MonoBehaviour
	{
		public float speed = 5;

		public void Init(OverworldObjectBase from, OverworldObjectBase to, TweenCallback onComplete)
		{
			transform.SetParent(to.transform);

			var duration = Vector2.Distance(from.transform.position, to.transform.position) / speed;

			transform.position = from.transform.position;

			transform
				.DOMove(to.transform.position, duration)
				.SetEase(Ease.Linear)
				.OnComplete(onComplete);
		}
	}
}