﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldCloud : MonoBehaviour
	{
		private tk2dSprite sprite;
		private OverworldClouds cloudsContainer;

		private const float DIST_MIN = 3.5f;
		private const float DIST_MAX = 4.5f;
		
		private Vector3 scaleTarget = new Vector3(1, 1, 1);
		private Tween scaleTween;

		private float scaleVelocity;

		public void Awake()
		{
			// randomize z position (has no use at the moment since clouds are white)
			transform.localPosition = transform.localPosition.Clone(z: Random.value);

			sprite = GetComponent<tk2dSprite>();
			cloudsContainer = GetComponentInParent<OverworldClouds>();

			scaleTween = transform
				.DOScale(cloudsContainer.cloudScaleChange, cloudsContainer.cloudScaleTime)
				.SetEase(Ease.InOutSine)
				.SetLoops(-1, LoopType.Yoyo);

			scaleTween.Goto(cloudsContainer.cloudScaleTime, true); // randomize start point

			scaleVelocity = cloudsContainer.cloudScaleVelocity; // random value
		}

		public void Start()
		{
			ApplyTargetScale();
			sprite.scale = scaleTarget; // instant scale to the value we want
		}

		public void OnEnable()
		{
			scaleTween.Play();
		}

		public void OnDisable()
		{
			scaleTween.Pause();
		}

		public void LateUpdate()
		{
			ApplyTargetScale();

			UpdateScrolling();
			UpdateSize();
		}

		private void UpdateScrolling()
		{
			if (!cloudsContainer.FrameOffset.HasValue || cloudsContainer.FrameOffset == Vector2.zero) return; // no need to update position

			transform.localPosition = transform.localPosition + (Vector3)cloudsContainer.FrameOffset;

			if (cloudsContainer.FrameOffset.Value.x > 0 && transform.localPosition.x > cloudsContainer.CloudBounds.max.x)
			{
				transform.localPosition = transform.localPosition.Clone(x: transform.localPosition.x - cloudsContainer.CloudBounds.extents.x * 2);
			}

			if (cloudsContainer.FrameOffset.Value.x < 0 && transform.localPosition.x < cloudsContainer.CloudBounds.min.x)
			{
				transform.localPosition = transform.localPosition.Clone(x: transform.localPosition.x + cloudsContainer.CloudBounds.extents.x * 2);
			}

			if (cloudsContainer.FrameOffset.Value.y > 0 && transform.localPosition.y > cloudsContainer.CloudBounds.max.y)
			{
				transform.localPosition = transform.localPosition.Clone(y: transform.localPosition.y - cloudsContainer.CloudBounds.extents.y * 2);
			}

			if (cloudsContainer.FrameOffset.Value.y < 0 && transform.localPosition.y < cloudsContainer.CloudBounds.min.y)
			{
				transform.localPosition = transform.localPosition.Clone(y: transform.localPosition.y + cloudsContainer.CloudBounds.extents.y * 2);
			}
		}

		private void UpdateSize()
		{
			sprite.scale = Vector3.MoveTowards(sprite.scale, scaleTarget, scaleVelocity * Time.deltaTime);
		}
		
		private void ApplyTargetScale()
		{
			var targetValue = GetTargetScale();
			scaleTarget = new Vector3(targetValue, targetValue, targetValue);
		}

		private float GetTargetScale()
		{
			// TODO: consider using NonAlloc
			var colliders = Physics2D.OverlapPointAll(transform.position);
			if (colliders.Length == 0)
			{
				return 1;
			}

			float lowestValue = 1;

			for (var i = 0; i < colliders.Length; i++)
			{
				var curCollider = colliders[i];
				var dist = Vector2.Distance(transform.position, curCollider.transform.position);

				if (dist < DIST_MIN)
				{
					return 0;
				}

				var value = Mathf.Clamp01(Mathf.InverseLerp(DIST_MIN, DIST_MAX, dist));
				lowestValue = Mathf.Min(lowestValue, value);
			}

			if(lowestValue < cloudsContainer.minCloudSize)
			{
				return 0; // below minimum scale
			}

			return lowestValue;
		}
	}
}