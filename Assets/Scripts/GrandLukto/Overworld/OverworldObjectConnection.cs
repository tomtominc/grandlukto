﻿using DG.Tweening;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldObjectConnection : MonoBehaviour
	{
		private const float NODE_REVEAL_SPEED = 0.2f;

		private const float NODE_OFFSET = 0.8f;
		private const int MIN_NODES = 2;

		public OverworldObjectBase DrawnFrom { get; private set; }
		public OverworldObjectBase DrawnTo { get; private set; }
		private List<GameObject> drawnNodes = new List<GameObject>();

		private void Draw(OverworldObjectBase from, OverworldObjectBase to, bool animate = false)
		{
			DrawnFrom = from;
			DrawnTo = to;

			var fromPos = from.GetClosestNodePosition(to);
			var toPos = to.GetClosestNodePosition(from);

			var dist = Vector3.Distance(fromPos, toPos);
			int nodeCount = Mathf.RoundToInt(dist / NODE_OFFSET); // round to nearest;
			nodeCount = Mathf.Max(MIN_NODES - 1, nodeCount); // verify min amount of nodes

			var curPos = fromPos;
			var offset = (toPos - fromPos) / nodeCount;

			var targetColor = GetNodeColor();

			for (var i = 0; i <= nodeCount; i++)
			{
				var node = CreateNode();
				node.transform.position = curPos;

				if (targetColor != Color.white)
				{
					node.GetComponent<tk2dSprite>().color = targetColor;
				}

				drawnNodes.Add(node);

				curPos += offset;
			}

			if (animate) AnimateNodes();
		}

		public Tween AnimateNodes(OverworldObjectBase startObject = null, bool startTransparent = true)
		{
			if (startObject != null && startObject != DrawnFrom && startObject != DrawnTo)
			{
				Debug.LogError("Invalid startobject for OverworldObjectConnection given.", this);
				return DOTween.Sequence(); // empty sequence to avoid errors
			}

			var playInversed = startObject != null && startObject == DrawnTo;

			var sequence = DOTween.Sequence();
			var transparentWhite = Color.white.MakeTransparent();
			var targetColor = GetNodeColor();

			for (var i = 0; i < transform.childCount; i++)
			{
				var child = transform.GetChild(playInversed ? transform.childCount - 1 - i : i);
				var sprite = child.GetComponent<tk2dSprite>();

				if (startTransparent) sprite.color = transparentWhite;

				sequence.Append(sprite
					.DOColor(targetColor, NODE_REVEAL_SPEED)
					.SetEase(Ease.OutExpo));
			}

			return sequence;
		}

		private GameObject CreateNode()
		{
			return Instantiate(Resources.Load<GameObject>("Overworld/Node"), transform);
		}

		private Color GetNodeColor()
		{
			if (!DrawnFrom.IsUnlocked || !DrawnTo.IsUnlocked)
			{
				return Color.white.MakeTransparent(0.314f);
			}

			return Color.white;
		}

		/// <summary>
		/// Returns the connected object that is not the provided object.
		/// </summary>
		public OverworldObjectBase GetOtherObject(OverworldObjectBase thisObject)
		{
			return DrawnFrom == thisObject ? DrawnTo : DrawnFrom;
		}

		public static OverworldObjectConnection Create(OverworldObjectBase from, OverworldObjectBase to, bool animate = false)
		{
			var gameObj = new GameObject("Connection");
			gameObj.transform.parent = from.transform.parent;

			// place at center between from and to
			gameObj.transform.position = from.transform.position + (to.transform.position - from.transform.position) / 2;

			var connection = gameObj.AddComponent<OverworldObjectConnection>();
			connection.Draw(from, to, animate);

			return connection;
		}
	}
}