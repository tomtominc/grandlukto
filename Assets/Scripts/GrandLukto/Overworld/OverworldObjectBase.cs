﻿using DG.Tweening;
using GrandLukto.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public abstract class OverworldObjectBase : MonoBehaviour, ISerializationCallbackReceiver
	{
		public event EventHandler<OverworldObjectCompletedEventArgs> Completed;

		[HideInInspector] // only hidden in default inspector
		public string nodeID;
		[HideInInspector]
		public DataCampaignLevel levelData;

		[SerializeField, HideInInspector]
		public List<OverworldObjectBase> connectedObjects;

		[Tooltip("The player is offset by this value when standing on the node.")]
		public Vector2 playerOffset;

		private Dictionary<OverworldObjectBase, OverworldObjectConnection> connections = new Dictionary<OverworldObjectBase, OverworldObjectConnection>();

		public bool IsRevealed { get; private set; }

		/// <summary>
		/// Can be useful for nodes that are revealed before being unlocked.
		/// </summary>
		protected virtual bool AlwaysRevealNeighbours { get { return false; } }

		public bool IsCompleted { get; private set; }
		public bool IsJustCompletedNode
		{
			get { return GameSceneManager.LoadOverworldUnlockedNodeID == nodeID; }
		}

		/// <summary>
		/// A node is unlocked if current node or any of the connected nodes IsCompleted.
		/// </summary>
		public bool IsUnlocked { get; private set; }

		public virtual bool CanMoveTo { get { return true; } }
		public virtual bool CanMoveFrom { get { return true; } }

		public float? MaxConnections { get; protected set; }

		public OverworldObjectBase()
		{
			MaxConnections = null;
		}

		public virtual void Awake()
		{
			gameObject.GetOrAddComponent<OverworldObjectNavigator>();

			IsCompleted = EvaluateIsCompleted();
		}

		public virtual void Start()
		{
			if (IsCompleted)
			{
				OnCompleted(false);
			}
		}

		public virtual bool EvaluateIsCompleted()
		{
			return !IsJustCompletedNode && OverworldManager.Instance.SaveData.CompletedLevelIDs.Contains(nodeID); // load from savegame
		}
		
		/// <summary>
		/// Creates connections from this object to its neighbours and returns a list of all new connected objects.
		/// </summary>
		protected List<OverworldObjectConnection> CreateConnections(bool animate = false, bool force = false)
		{
			var newConnections = new List<OverworldObjectConnection>();

			if (!IsUnlocked && !force) return newConnections;

			for (var i = 0; i < connectedObjects.Count; i++)
			{
				var connectedObj = connectedObjects[i];
				if (connections.ContainsKey(connectedObj)) continue; // connection already created by other obj

				var connection = OverworldObjectConnection.Create(this, connectedObj, animate);

				// add connection to both objects
				connections[connectedObj] = connection;
				connectedObj.connections[this] = connection;

				newConnections.Add(connection);
			}

			return newConnections;
		}

		protected Tween AnimateConnections()
		{
			var sequence = DOTween.Sequence();

			// colorize already created nodes since CanMoveTo changed
			var createConnections = GetCreatedConnections();
			for (var i = 0; i < createConnections.Count; i++)
			{
				var curConnection = createConnections[i];
				sequence.Join(curConnection
					.AnimateNodes(curConnection.DrawnFrom == this ? curConnection.DrawnTo : curConnection.DrawnFrom, false));
			}

			return sequence;
		}

		private void AnimateConnectionsAndRevealNext()
		{
			// colorize already created nodes since CanMoveTo changed
			var createConnections = GetCreatedConnections();
			for (var i = 0; i < createConnections.Count; i++)
			{
				var curConnection = createConnections[i];
				var connectedObject = curConnection.DrawnFrom == this ? curConnection.DrawnTo : curConnection.DrawnFrom;

				DOTween.Sequence()
					.Append(curConnection
						.AnimateNodes(this, false))
					.AppendCallback(() => connectedObject.CreateConnections(true));
			}
		}

		public abstract Vector3 GetClosestNodePosition(OverworldObjectBase to);

		public List<OverworldObjectBase> GetAllConnections()
		{
			if (connectedObjects == null)
			{
				connectedObjects = new List<OverworldObjectBase>();
			}

			return connectedObjects.Where(obj => obj != null).ToList();
		}

		public bool AddConnection(OverworldObjectBase overworldObj)
		{
			if (overworldObj == this) return false;
			if (connectedObjects.Contains(overworldObj)) return false;

			connectedObjects.Add(overworldObj);
			overworldObj.AddConnection(this); // it is save to call AddConnection here because of the Contains check
			return true;
		}

		public bool RemoveConnection(OverworldObjectBase overworldObj)
		{
			overworldObj.connectedObjects.Remove(this); // remove direct, calling RemoveConnection would cause a stack overflow
			return connectedObjects.Remove(overworldObj);
		}

		public void RemoveInvalidConnections()
		{
			connectedObjects.RemoveAll(obj => obj == null);
		}

		public void StartInitializeStartNode()
		{
			IsUnlocked = true; // start node is always unlocked

			InitializeIsUnlocked();
			InitializeCreateConnections();

			OverworldEvents.Instance.OnInitializeNodesCompleted(this);
		}

		/// <summary>
		/// Updates the IsUnlocked state for all nodes that have a Completed node as neighbour.
		/// </summary>
		private void InitializeIsUnlocked(bool isStartNode = true)
		{
			if (IsUnlocked && !isStartNode) return; // already unlocked
			
			// we know that at least one neighbour IsCompleted
			if (!TryUnlockNode(false)) return; // stop initializing this branch since the next node is not unlocked anyways
			
			if (!IsCompleted) return; // node not completed -> neighbours can not be unlocked

			for (var i = 0; i < connectedObjects.Count; i++)
			{
				var curObj = connectedObjects[i];
				curObj.InitializeIsUnlocked(false);
			}
		}

		/// <summary>
		/// Recursively creates the initial connections.
		/// </summary>
		private void InitializeCreateConnections(bool isFirstCall = true)
		{
			var newConnections = CreateConnections(false, isFirstCall);

			for (var i = 0; i < newConnections.Count; i++)
			{
				var curObject = newConnections[i].GetOtherObject(this);
				curObject.InitializeCreateConnections(false);
			}
		}
		
		/// <summary>
		/// Starts the reveal animation from the current node.
		/// </summary>
		public void RevealNodeAndConnectedNodes()
		{
			if (!IsRevealed)
			{
				RevealCurrentNode();
				return;
			}

			RevealConnectedNodes();
		}

		private IEnumerator UnlockNodeAfterDelay()
		{
			yield return new WaitForSeconds(OverworldManager.Instance.unlockNodeDelay);
			SetNodeCompleted();
		}

		private void RevealCurrentNode()
		{
			var revealColliderGO = Instantiate(Resources.Load<GameObject>("Overworld/RevealCollider"));
			var changeSize = revealColliderGO.AddComponent<RevealColliderChangeSize>();
			changeSize.Init(this, OnRevealComplete);
		}

		private void RevealConnectedNodes()
		{
			for (var i = 0; i < connectedObjects.Count; i++)
			{
				var connectedObj = connectedObjects[i];
				if (connectedObj.IsRevealed && !connectedObj.AlwaysRevealNeighbours) continue; // already revealed
				if (!connectedObj.IsUnlocked) continue;

				var revealColliderGO = Instantiate(Resources.Load<GameObject>("Overworld/RevealCollider"));
				var move = revealColliderGO.AddComponent<RevealColliderMove>();
				move.Init(this, connectedObj, connectedObj.OnRevealComplete);
			}
		}

		private void OnRevealComplete()
		{
			IsRevealed = true;

			if (!IsCompleted && IsJustCompletedNode)
			{
				StartCoroutine(UnlockNodeAfterDelay());
			}

			RevealNodeAndConnectedNodes();
		}
		
		public void SetNodeCompleted()
		{
			if (IsCompleted) return; // was already completed

			IsCompleted = true;

			if (!string.IsNullOrEmpty(nodeID))
			{
				var saveData = OverworldManager.Instance.SaveData;
				saveData.SetLevelCompleted(nodeID);
				saveData.Save();
			}

			if (IsUnlocked)
			{
				InitializeIsUnlocked(true); // recursive initialization again since gates can be in the way
			}

			RevealConnectedNodes(); // remove clouds

			AnimateConnectionsAndRevealNext(); // reveal all current connections

			var newConnections = CreateConnections(); // create missing connections
			for (var i = 0; i < newConnections.Count; i++)
			{
				var newConnection = newConnections[i];

				DOTween.Sequence()
					.Append(newConnection.AnimateNodes(this))
					.AppendCallback(() => newConnection.GetOtherObject(this).CreateConnections(true));
			}

			OnNodeCompleted();
			OnCompleted(true);
		}

		/// <summary>
		/// Called after the node has been set to completed for the first time.
		/// </summary>
		protected virtual void OnNodeCompleted() { }

		/// <summary>
		/// Called when the player jumps on the node.
		/// </summary>
		public virtual void OnPlayerJumpsOnNode() { }
		
		public static OverworldObjectBase FindObjectByID(string objID)
		{
			var overworldObjs = FindObjectsOfType<OverworldObjectBase>();
			for (var i = 0; i < overworldObjs.Length; i++)
			{
				var curObj = overworldObjs[i];
				if (curObj.nodeID == objID) return curObj;
			}

			return null;
		}

		protected List<OverworldObjectConnection> GetCreatedConnections()
		{
			return connections.Values.ToList();
		}

		protected bool TryUnlockNode(bool checkAnyNeighbourCompleted = true)
		{
			if (checkAnyNeighbourCompleted && !IsAnyNeighbourComplete()) return IsUnlocked;

			IsUnlocked = CanBeUnlocked();
			return IsUnlocked;
		}

		private bool IsAnyNeighbourComplete()
		{
			for (var i = 0; i < connectedObjects.Count; i++)
			{
				var otherObject = connectedObjects[i];
				if (otherObject.IsCompleted) return true;
			}

			return false;
		}

		protected virtual bool CanBeUnlocked()
		{
			return true;
		}
		
		public void OnBeforeSerialize()
		{
			connectedObjects = connectedObjects.Where(obj => obj != null).ToList(); // only save existing objects
		}

		public void OnAfterDeserialize()
		{
			// swallow
		}

		protected void OnCompleted(bool playAnimation)
		{
			if (Completed != null) Completed.Invoke(this, new OverworldObjectCompletedEventArgs(playAnimation));
		}
	}
}