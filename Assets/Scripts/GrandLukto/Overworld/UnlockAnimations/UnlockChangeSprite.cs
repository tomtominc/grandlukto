﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.Overworld.UnlockAnimations
{
	public class UnlockChangeSprite : MonoBehaviour
	{
		[Tooltip("Target tk2dSprite or null to automatically search for a tk2dSprite."), SerializeField]
		private tk2dSprite targetSprite;

		[HideInInspector]
		public int unlockedSpriteID;
		private int lockedSpriteID;

		private OverworldObjectBase targetObject;

		private int numberOfBlinks = 3;
		private float blinkTime = 0.5f; // total time of a single blink in seconds

		private void Awake()
		{
			targetSprite = GetTargetSprite();
			lockedSpriteID = targetSprite.spriteId;

			targetObject = GetComponentInParent<OverworldObjectBase>();
			Assert.IsNotNull(targetObject, "Only works with OverworldObjectBase as parent.");
		}

		private void OnEnable()
		{
			targetObject.Completed += TargetObject_Completed;
		}

		private void OnDisable()
		{
			targetObject.Completed -= TargetObject_Completed;
		}

		private void TargetObject_Completed(object sender, OverworldObjectCompletedEventArgs e)
		{
			targetObject.Completed -= TargetObject_Completed;
			Destroy(this);

			if (!e.PlayAnimation)
			{
				targetSprite.spriteId = unlockedSpriteID;
				return; // no animation -> simply change sprite
			}

			var sequence = DOTween.Sequence();

			// blink 3 times then stay at unlocked sprite
			for(var i = 0; i < numberOfBlinks; i++)
			{
				sequence
					.AppendCallback(ShowUnlockedSprite)
					.AppendInterval(blinkTime * 1 / 3);

				if (i < numberOfBlinks - 1)
				{
					sequence
						.AppendCallback(ShowLockedSprite)
						.AppendInterval(blinkTime * 2 / 3);
				}
			}
		}

		private void ShowUnlockedSprite()
		{
			targetSprite.spriteId = unlockedSpriteID;
		}

		private void ShowLockedSprite()
		{
			targetSprite.spriteId = lockedSpriteID;
		}

		public tk2dSprite GetTargetSprite()
		{
			return targetSprite == null ? GetComponent<tk2dSprite>() : targetSprite;
		}
	}
}