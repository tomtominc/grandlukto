﻿using UnityCave;
using UnityEditor;

namespace GrandLukto.Overworld.UnlockAnimations
{
	[CustomEditor(typeof(UnlockChangeSprite))]
	public class UnlockChangeSpriteEditor : EditorBase<UnlockChangeSprite>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			var tk2dSprite = Target.GetTargetSprite();

			EditorGUILayout.LabelField("Unlocked Sprite");
			tk2dSpriteGuiUtility.SpriteSelector(tk2dSprite.Collection, Target.unlockedSpriteID, OnSpriteChange, Target);
		}

		private void OnSpriteChange(tk2dSpriteCollectionData spriteCollection, int spriteIndex, object callbackData)
		{
			Target.unlockedSpriteID = spriteIndex;
			EditorUtility.SetDirty(Target);
		}
	}
}