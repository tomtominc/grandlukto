﻿using System;

namespace GrandLukto.Overworld
{
	public class OverworldMoveEventArgs : EventArgs
	{
		public OverworldObjectBase StartNode { get; private set; }
		public OverworldObjectBase TargetNode { get; private set; }

		public OverworldMoveEventArgs(OverworldObjectBase startNode, OverworldObjectBase targetNode)
		{
			StartNode = startNode;
			TargetNode = targetNode;
		}
	}
}