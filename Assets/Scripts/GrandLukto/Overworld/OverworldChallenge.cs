﻿using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldChallenge : OverworldObjectBase
	{
		public float nodeOffsetEllipsisX;
		public float nodeOffsetEllipsisY;

		public override Vector3 GetClosestNodePosition(OverworldObjectBase to)
		{
			var angle = Vector2Util.AngleSigned(Vector2.right, to.transform.position - transform.position);
			var angleRad = angle * Mathf.Deg2Rad;

			return transform.position + new Vector3(nodeOffsetEllipsisX * Mathf.Cos(angleRad), nodeOffsetEllipsisY * Mathf.Sin(angleRad));
		}
	}
}