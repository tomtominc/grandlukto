﻿using DG.Tweening;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldNodeDescription : MonoBehaviour
	{
		public GameObject visualsContainer;
		public tk2dSprite backgroundSprite;
		public tk2dTextMesh txtDescription;

		public float visualsContainerOutYOffset = 2;

		private Tween curTween;

		private void Awake()
		{
			visualsContainer.transform.localPosition = new Vector3(0, visualsContainerOutYOffset, 0);

			var events = OverworldEvents.Instance;
			events.PlayerJumpedOnNode += Events_PlayerJumpedOnNode;
			events.PlayerJumpedOffNode += Events_PlayerJumpedOffNode;
		}

		private void OnDestroy()
		{
			var events = OverworldEvents.Instance;
			events.PlayerJumpedOnNode -= Events_PlayerJumpedOnNode;
			events.PlayerJumpedOffNode -= Events_PlayerJumpedOffNode;
		}

		private void EndCurTween()
		{
			curTween.Kill();
		}

		private void Events_PlayerJumpedOffNode(object sender, OverworldMoveEventArgs e)
		{
			EndCurTween();

			curTween = DOTween.Sequence()
				.Append(visualsContainer.transform
					.DOLocalMoveY(visualsContainerOutYOffset, 0.5f)
					.SetEase(Ease.InBack));
		}

		private void Events_PlayerJumpedOnNode(object sender, OverworldMoveEventArgs e)
		{
			var target = e.TargetNode;

			var nodeDescription = target.levelData.nodeDescription;
			if (string.IsNullOrEmpty(nodeDescription))
			{
				nodeDescription = target.levelData.levelName;

				if (string.IsNullOrEmpty(nodeDescription))
				{
					Debug.LogWarning("Node without description!");
					return;
				}
			}

			txtDescription.text = nodeDescription;

			EndCurTween();

			backgroundSprite.SetSprite("mode-" + target.levelData.gameMode.modeType.ToString().ToLower());

			curTween = DOTween.Sequence()
				.Append(visualsContainer.transform
					.DOLocalMoveY(0, 0.3f)
					.SetEase(Ease.OutQuart));
		}
	}
}