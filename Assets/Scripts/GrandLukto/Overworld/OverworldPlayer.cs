﻿using DG.Tweening;
using GrandLukto.Characters;
using GrandLukto.Database;
using Rewired;
using Spine.Unity;
using System.Linq;
using UnityCave.ExtensionMethods;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.Overworld
{
	[SelectionBase]
	public class OverworldPlayer : MonoBehaviour
	{
		public GameObject visuals;
		public SkeletonAnimation characterAnim;

		public OverworldObjectBase startNode;
		public float speed;
		public Ease moveEase = Ease.InOutSine;

		public Ease cameraEase = Ease.InOutSine;

		public float takeMoveInputDelay = 0.3f;

		[Header("Jump")]
		public float jumpStrength = 1;
		public float jumpMaxDist = 2;
		public Ease jumpEase = Ease.Linear;
		public float jumpWaitBetween = 0.1f;

		private bool isMoving = false;

		public OverworldObjectBase LastNode { get; private set; }
		public OverworldObjectBase CurNode { get; private set; }

		private Tween tweenPlayer;

		private FaceDirection? nextMoveDirection;
		private bool isSelectPressed = false;

		private Player ControllingPlayer
		{
			get
			{
				return ReInput.players.AllPlayers[1]; // TODO: Assign player that plays the campaign or 2 players in coop
			}
		}

		public void Start()
		{
			// initialize nodes form start node to update IsUnlocked
			startNode.StartInitializeStartNode();

			LoadStartNode();

			transform.position = startNode.transform.position + (Vector3)startNode.playerOffset;
			Camera.main.transform.position = transform.position.Clone(z: Camera.main.transform.position.z);

			CurNode = startNode;
			OverworldEvents.Instance.OnPlayerJumpedOnNode(this, new OverworldMoveEventArgs(null, CurNode));

			CurNode.RevealNodeAndConnectedNodes();

			// apply animation
			var saveData = OverworldManager.Instance.SaveData;
			var selectedCharName = saveData.GetSelectedCharacterByPlayerID(0);

			var playableCharacters = DatabaseManager.Instance.dbCharacter.GetPlayable();
			var charToLoad = playableCharacters.FirstOrDefault(dbChar => dbChar.characterName == selectedCharName);

			if (charToLoad == null)
			{
				charToLoad = playableCharacters.First(); // simply take first, should not happen at all
			}

			LoadCharacter(charToLoad);
		}

		private void LoadStartNode()
		{
			// try to find the start node from SaveGame
			var saveData = OverworldManager.Instance.SaveData;
			var startLevelID = saveData.playerPosLevelID;
			if (string.IsNullOrEmpty(startLevelID)) return;

			var overworldObj = OverworldObjectBase.FindObjectByID(startLevelID);
			if (overworldObj == null) return; // object not found
			if (!overworldObj.IsUnlocked) return; // object not unlocked (invalid savegame?)

			startNode = overworldObj;
		}

		public void Update()
		{
			if (Time.timeScale <= 0) return; // avoid checking controls if game is paused
			if (!OverworldManager.Instance.PlayerHasControl) return; // e.g. Roster Menu open

			UpdateGetNextMoveDirection();

			if (OverworldManager.Instance.IsAnimating) return; // player not shown yet

			if (isMoving) return;

			if (isSelectPressed)
			{
				isSelectPressed = false;
				OverworldManager.Instance.StartLevel(CurNode);
				return;
			}

			if(nextMoveDirection.HasValue)
			{
				NavigateInDirection(nextMoveDirection.Value);
				nextMoveDirection = null; // reset
			}
		}

		private void UpdateGetNextMoveDirection()
		{
			if (tweenPlayer != null && tweenPlayer.IsPlaying())
			{
				var moveTimeLeft = tweenPlayer.Duration() - tweenPlayer.Elapsed();

				if (moveTimeLeft > takeMoveInputDelay) return; // too soon
			}

			isSelectPressed = ControllingPlayer.GetButtonDown(Control.Shoot) || ControllingPlayer.GetButtonDown(UIManager.Instance.config.buttonEnterNames);

			var moveDirection = GetMoveDirection();
			if (!moveDirection.HasValue) return; // do not override with null

			nextMoveDirection = moveDirection;
		}

		private FaceDirection? GetMoveDirection()
		{
			var player = ControllingPlayer;

			if (player.GetButton(Control.MoveHorizontal))
			{
				return FaceDirection.Right;
			}

			if (player.GetNegativeButton(Control.MoveHorizontal))
			{
				return FaceDirection.Left;
			}

			if (player.GetButton(Control.MoveVertical))
			{
				return FaceDirection.Up;
			}

			if (player.GetNegativeButton(Control.MoveVertical))
			{
				return FaceDirection.Down;
			}

			return null;
		}
		
		private void NavigateInDirection(FaceDirection direction)
		{
			var target = CurNode.GetComponent<OverworldObjectNavigator>().GetObjectInDirection(direction);
			if (target == null) return; // no linked node in this direction

			if (!CurNode.CanMoveFrom) return; // e.g. animation playing
			if (!CurNode.IsCompleted && !target.IsCompleted) return; // can not move from uncompleted node to uncompleted
			if (!target.CanMoveTo) return; // not allowed yet

			isMoving = true;

			LastNode = CurNode;
			CurNode = target;

			var targetPos = target.transform.position + (Vector3)target.playerOffset;

			var distance = Vector3.Distance(transform.position, targetPos);
			var duration = distance / speed;
			var numJumps = Mathf.CeilToInt(distance / jumpMaxDist);
			var positionOffset = targetPos - transform.position;
			var totalDuration = duration + numJumps * jumpWaitBetween;

			var sequence = DOTween.Sequence();

			// jumps
			for(var i = 0; i < numJumps; i++)
			{
				sequence.Append(visuals.transform
					.DOLocalJump(Vector3.zero, jumpStrength, 1, duration / numJumps)
					.SetEase(jumpEase));

				sequence.Join(transform
					.DOMove(transform.position + positionOffset / numJumps * (i + 1), duration / numJumps)
					.SetEase(moveEase));

				sequence.AppendInterval(jumpWaitBetween); // also wait at the end
			}

			sequence.AppendCallback(OnEndMove);

			tweenPlayer = sequence;

			// smooth camera movement
			Camera.main.transform
				.DOMove(targetPos.Clone(z: Camera.main.transform.position.z), totalDuration)
				.SetEase(cameraEase);

			OverworldEvents.Instance.OnPlayerJumpedOffNode(this, new OverworldMoveEventArgs(LastNode, CurNode));
		}

		private void OnEndMove()
		{
			var saveData = OverworldManager.Instance.SaveData;
			saveData.playerPosLevelID = CurNode.nodeID;
			saveData.Save();

			CurNode.OnPlayerJumpsOnNode();

			isMoving = false;

			OverworldEvents.Instance.OnPlayerJumpedOnNode(this, new OverworldMoveEventArgs(LastNode, CurNode));
		}

		public void LoadCharacter(DataCharacter data)
		{
			data.ApplyAnimationToGameObject(characterAnim, FaceDirection.Down, false);
		}

		public void OnDestroy()
		{
			if (tweenPlayer != null && tweenPlayer.IsPlaying())
			{
				tweenPlayer.Kill();
			}
		}
	}
}