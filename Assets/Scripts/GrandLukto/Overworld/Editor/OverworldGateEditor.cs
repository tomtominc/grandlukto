﻿using UnityCave.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Overworld
{
	// ATTENTION: Do not forget to update OverworldKeyEditor as well!
	[CustomEditor(typeof(OverworldGate))]
	public class OverworldGateEditor : OverworldObjectBaseEditor
	{
		public new OverworldGate Target
		{
			get
			{
				return (OverworldGate)target;
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			EditorGUILayout.LabelField("Additional Config", EditorStyles.boldLabel);
			ShowMatchingKeyField();
		}

		private void ShowMatchingKeyField()
		{
			EditorUtils.ObjectConnectionField("Unlock key", "Change unlock key", Target, gate => gate.matchingKey, (gate, key) => gate.matchingKey = key, (key, gate) => key.targetGate = gate);
		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();

			if (Target.matchingKey == null) return; // no key

			Handles.color = Color.yellow;
			Handles.DrawDottedLine(Target.transform.position, Target.matchingKey.transform.position, 8f);
		}
	}
}