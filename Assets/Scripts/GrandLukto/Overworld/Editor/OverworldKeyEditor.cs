﻿using UnityCave.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Overworld
{
	// ATTENTION: Do not forget to update OverworldGateEditor as well!
	[CustomEditor(typeof(OverworldKey))]
	public class OverworldKeyEditor : OverworldObjectBaseEditor
	{
		public new OverworldKey Target
		{
			get
			{
				return (OverworldKey)target;
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			EditorGUILayout.LabelField("Additional Config", EditorStyles.boldLabel);
			ShowTargetGateField();
		}

		private void ShowTargetGateField()
		{
			EditorUtils.ObjectConnectionField("Target gate", "Change target gate", Target, key => key.targetGate, (key, gate) => key.targetGate = gate, (gate, key) => gate.matchingKey = key);
		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();

			if (Target.targetGate == null) return; // no key

			Handles.color = Color.yellow;
			Handles.DrawDottedLine(Target.transform.position, Target.targetGate.transform.position, 8f);
		}
	}
}