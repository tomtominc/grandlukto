﻿using GrandLukto.Database;
using System.Linq;
using UnityCave;
using UnityCave.Database;
using UnityCave.Utils.Editor;
using UnityEditor;
using UnityEngine;
using System;
using UnityCave.Utils;

namespace GrandLukto.Overworld
{
	[CustomEditor(typeof(OverworldObjectBase), true)]
	public class OverworldObjectBaseEditor : EditorBase<OverworldObjectBase>
	{
		private static DatabaseCampaignLevel dbLevel; // cache DB
		private static DatabaseArena dbArena; // cache DB

		private static string[] arenaNames;

		private OverworldObjectBase[] allNodes;

		private bool isNodeIDUnique = false;

		public void Awake()
		{
			if (dbLevel == null)
			{
				dbLevel = DatabaseEditorUtils.LoadDatabase<DatabaseCampaignLevel>();
			}

			if (dbArena == null)
			{
				dbArena = DatabaseEditorUtils.LoadDatabase<DatabaseArena>();
			}

			var allArenaNames = dbArena.GetArenaNames();
			allArenaNames.Insert(0, "None"); // add default "none" element
			arenaNames = allArenaNames.ToArray();

			CheckLevelNameUnique();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Level Data", EditorStyles.boldLabel);

			if (Target.levelData == null)
			{
				if (GUILayout.Button("Create Level")) CreateNewLevel();
			}
			else
			{
				ShowLevelNameNotUniqueWarning();

				if (GUILayout.Button("Edit Level")) CreateLevelEditor();

				ShowArenaSelection();

				var serializedObj = new SerializedObject(Target.levelData);
				serializedObj.Update();
				EditorGUILayout.PropertyField(serializedObj.FindProperty("nodeDescription"));
				EditorGUILayout.PropertyField(serializedObj.FindProperty("enemies"), true);
				EditorGUILayout.PropertyField(serializedObj.FindProperty("spawnInitialBall"));
				EditorGUILayout.PropertyField(serializedObj.FindProperty("gameMode"));
				EditorGUILayout.PropertyField(serializedObj.FindProperty("dialogBeforeGameStart"));
				serializedObj.ApplyModifiedProperties();

				if (Target.levelData.levelObj == null) return;

				serializedObj = new SerializedObject(Target.levelData.levelObj);
				serializedObj.Update();
				EditorGUILayout.PropertyField(serializedObj.FindProperty("scriptPrefab"), false);
				serializedObj.ApplyModifiedProperties();
			}
		}

		private void ShowLevelNameNotUniqueWarning()
		{
			if (isNodeIDUnique) return;

			var guiStyle = new GUIStyle();
			guiStyle.fontStyle = FontStyle.Bold;
			guiStyle.normal.textColor = Color.red;
			EditorGUILayout.LabelField("Level Name is not unique!", guiStyle);
		}

		private void ShowArenaSelection()
		{
			if (arenaNames == null) return;

			var selectedArena = Target.levelData != null && Target.levelData.arena != null ? Array.IndexOf(arenaNames, Target.levelData.arena.arenaName) : 0;
			var newIndex = EditorGUILayout.Popup("Arena", selectedArena, arenaNames);

			if (newIndex == selectedArena) return; // no change

			DataArena newArena = null;

			if(newIndex > 0)
			{
				var newArenaName = arenaNames[newIndex];
				newArena = dbArena.Find(arena => arena.arenaName == newArenaName).FirstOrDefault();
			}
			
			Undo.RecordObject(Target.levelData, "Change Level arena");

			Target.levelData.arena = newArena;

			EditorUtils.SetDirty(Target.levelData);
		}

		private void CheckLevelNameUnique()
		{
			var otherLevelsWithSameName = dbLevel.Find(level =>
			{
				if (Target.levelData != null && Target.levelData == level) return false; // is current level
				return level.levelName == Target.nodeID; // return true if level name is equal
			});

			isNodeIDUnique = otherLevelsWithSameName.Count() == 0;
		}

		private void CreateNewLevel()
		{
			Undo.SetCurrentGroupName("Create Level");
			int group = Undo.GetCurrentGroup();

			var newLevel = DatabaseEditorUtils.CreateNewDataObject<DatabaseCampaignLevel, DataCampaignLevel>();
			var levelName = ScriptableObjectUtil.GetFileName(newLevel);

			Undo.RecordObject(newLevel, "Modify CampaignLevel properties");

			newLevel.levelName = levelName;

			Undo.RecordObject(target, "Applied CampaignLevel to node");
			Target.levelData = newLevel;
			Target.nodeID = levelName;

			Debug.Log("Created CampaignLevel: " + Target.nodeID);

			// Create Level
			var dataLevel = DatabaseEditorUtils.CreateNewDataObject<DatabaseLevel, DataLevel>();

			Undo.RecordObject(dataLevel, "Modify Level properties");
			dataLevel.levelName = levelName;

			newLevel.levelObj = dataLevel;

			dataLevel.blockLayoutTop = CreateBlockLayout(levelName + " (top)");
			dataLevel.blockLayoutBottom = CreateBlockLayout(levelName + " (bottom)");

			EditorUtils.SetDirty(newLevel);
			EditorUtils.SetDirty(dataLevel);

			Undo.CollapseUndoOperations(group);
		}

		private DataBlockLayout CreateBlockLayout(string dataName)
		{
			var dataBlockLayout = DatabaseEditorUtils.CreateNewDataObject<DatabaseBlockLayout, DataBlockLayout>();

			Undo.RecordObject(dataBlockLayout, "Modify BlockLayout properties");

			dataBlockLayout.customName = dataName;
			dataBlockLayout.width = 11;
			dataBlockLayout.height = 4;

			EditorUtils.SetDirty(dataBlockLayout);

			return dataBlockLayout;
		}

		private void CreateLevelEditor()
		{
			Debug.Log("Creating Level Editor...");

			var gameObj = new GameObject("Level Editor (TODO: DELETE!)");
			gameObj.transform.position = Vector3.zero;
			SceneView.lastActiveSceneView.AlignViewToObject(gameObj.transform);

			var levelTileEditor = gameObj.AddComponent<DataLevelTileEditor>();
			levelTileEditor.Initialize(Target.levelData.levelObj);
			DataBlockLayoutTileMapConnectorEditor.Load(levelTileEditor.BlockLayoutTop);
			DataBlockLayoutTileMapConnectorEditor.Load(levelTileEditor.BlockLayoutBottom);

			EditorUtils.SelectObject(levelTileEditor.BlockLayoutTop);
		}
		
		private void AddConnection(OverworldObjectBase objToConnect)
		{
			Undo.RecordObjects(new[] { Target, objToConnect }, "Added connection");

			Target.AddConnection(objToConnect);

			EditorUtility.SetDirty(Target);
			EditorUtility.SetDirty(objToConnect);
		}
		
		private void DeleteConnection(OverworldObjectBase objToDisconnect)
		{
			Undo.RecordObjects(new[] { Target, objToDisconnect }, "Deleted connection");

			Target.RemoveConnection(objToDisconnect);

			EditorUtility.SetDirty(Target);
			EditorUtility.SetDirty(objToDisconnect);
		}

		public virtual void OnSceneGUI()
		{
			DrawConnections();
			DrawConnectDots();
		}

		private void DrawConnectDots()
		{
			for(var i = 0; i < allNodes.Length; i++)
			{
				var curNode = allNodes[i];

				var isConnected = Target.connectedObjects.Contains(curNode);

				if (Target.MaxConnections.HasValue && !isConnected && Target.connectedObjects.Count >= Target.MaxConnections) continue; // max connections added

				Handles.color = isConnected ? Color.red : Color.green;

				var size = HandleUtility.GetHandleSize(curNode.transform.position) * 0.1f;
				if (!Handles.Button(curNode.transform.position, Quaternion.identity, size, size, Handles.DotHandleCap))
				{
					continue;
				}

				// user clicked the handle
				if(isConnected)
				{
					DeleteConnection(curNode);
				}
				else
				{
					AddConnection(curNode);
				}
			}
		}

		private void DrawConnections()
		{
			Handles.color = Color.cyan;

			var allConnections = Target.GetAllConnections();
			for (var i = 0; i < allConnections.Count; i++)
			{
				var connectedObj = allConnections[i];
				Handles.DrawLine(Target.transform.position, connectedObj.transform.position);
			}
		}

		public void OnEnable()
		{
			allNodes = FindObjectsOfType<OverworldObjectBase>();
		}

		public void OnDestroy()
		{
			if (target != null) return; // target not destroied

			// clear all connections
			var allConnections = Target.GetAllConnections();
			for (var i = 0; i < allConnections.Count; i++)
			{
				var connectedObj = allConnections[i];
				connectedObj.RemoveInvalidConnections();

				EditorUtility.SetDirty(connectedObj);
			}
		}
	}
}