﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class RevealColliderChangeSize : MonoBehaviour
	{
		public float animDuration = 1.5f;

		private float targetRadius;
		private CircleCollider2D circleCollider;

		private OverworldObjectBase targetNode;

		public void Awake()
		{
			circleCollider = GetComponent<CircleCollider2D>();

			targetRadius = circleCollider.radius;
			circleCollider.radius = 0;

			circleCollider.enabled = false;
		}

		public void Init(OverworldObjectBase targetNode, TweenCallback onComplete)
		{
			if(this.targetNode != null)
			{
				Debug.LogError("Already initialized!", this);
				return;
			}

			this.targetNode = targetNode;

			circleCollider.enabled = true;

			transform.parent = targetNode.transform;
			transform.localPosition = Vector3.zero;

			circleCollider
				.DORadius(targetRadius, animDuration)
				.SetEase(Ease.Linear)
				.OnComplete(onComplete);
		}
	}
}