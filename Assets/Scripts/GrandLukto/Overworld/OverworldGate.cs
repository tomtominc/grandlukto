﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	[SelectionBase]
	public class OverworldGate : OverworldObjectBase
	{
		[HideInInspector]
		public OverworldKey matchingKey;

		public float offsetTop;
		public float offsetRight;
		public float offsetBottom;
		public float offsetLeft;

		public Rect offsetRectWorld;

		[Header("Unlock Animation")]
		public GameObject lockedGameObject;
		public GameObject unlockedGameObject;

		public override bool CanMoveTo
		{
			get { return IsCompleted && IsUnlocked; }
		}

		protected override bool AlwaysRevealNeighbours
		{
			get { return true; }
		}

		public OverworldGate()
		{
			MaxConnections = 2;
		}

		public override void Awake()
		{
			base.Awake();

			unlockedGameObject.SetActive(false);

			offsetRectWorld = new Rect(-offsetLeft, -offsetBottom, offsetLeft + offsetRight, offsetBottom + offsetTop);
			offsetRectWorld.center += (Vector2)transform.position; // to world space

			OverworldEvents.Instance.InitializeNodesCompleted += OverworldEvents_InitializeNodesCompleted;
		}

		public override Vector3 GetClosestNodePosition(OverworldObjectBase to)
		{
			// angle between center of rect and to
			var angle = Vector2Util.AngleSigned(Vector2.right, (Vector2)to.transform.position - offsetRectWorld.center);
			return RectUtil.GetClosestPointOnRectInDir(offsetRectWorld, angle * Mathf.Deg2Rad);
		}

		private void OverworldEvents_InitializeNodesCompleted(object sender, System.EventArgs e)
		{
			OverworldEvents.Instance.InitializeNodesCompleted -= OverworldEvents_InitializeNodesCompleted;

			if (IsCompleted)
			{
				UnlockInstant();

				if (!IsUnlocked)
				{
					RevealNodeAndConnectedNodes(); // move clouds away
				}
			}
		}
		
		private void UnlockInstant()
		{
			lockedGameObject.SetActive(false);
			Destroy(lockedGameObject);

			unlockedGameObject.SetActive(true);
		}

		public Tween PlayUnlockAnimation()
		{
			var sequence = DOTween.Sequence();

			// unlock the gate
			var lockedSprite = lockedGameObject.GetComponent<tk2dSprite>();
			var lockedSpriteRenderer = lockedSprite.GetComponent<Renderer>();

			lockedSpriteRenderer.material.shader = Shader.Find("UnityCave/PremulVertexColorColorize");

			lockedSpriteRenderer.material.SetColor("_FlashColor", Color.white);
			lockedSpriteRenderer.material.SetFloat("_FlashAmount", 0);

			// let locked sprite flash, then fade it out and show unlocked sprite
			sequence
				.AppendCallback(() =>
				{
					lockedSpriteRenderer.material.SetFloat("_FlashAmount", 1);
					unlockedGameObject.SetActive(true);
				})
				.Append(lockedSprite
					.DOColor(Color.white.MakeTransparent(), 1f)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					lockedGameObject.SetActive(false);
					Destroy(lockedGameObject);
				});

			// unlock other connections
			sequence.AppendCallback(() =>
			{
				TryUnlockNode();
				SetNodeCompleted();
			});

			return sequence;
		}

		public override bool EvaluateIsCompleted()
		{
			return matchingKey != null && matchingKey.EvaluateIsCompleted();
		}

		protected override bool CanBeUnlocked()
		{
			return matchingKey.IsCompleted;
		}
	}
}