﻿using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	[SelectionBase]
	public class OverworldTutorial : OverworldObjectBase
	{
		public float offsetTop;
		public float offsetRight;
		public float offsetBottom;
		public float offsetLeft;

		public Rect offsetRectWorld;

		public override void Awake()
		{
			base.Awake();

			offsetRectWorld = new Rect(-offsetLeft, -offsetBottom, offsetLeft + offsetRight, offsetBottom + offsetTop);
			offsetRectWorld.center += (Vector2)transform.position; // to world space
		}
		
		public override Vector3 GetClosestNodePosition(OverworldObjectBase to)
		{
			// angle between center of rect and to
			var angle = Vector2Util.AngleSigned(Vector2.right, (Vector2)to.transform.position - offsetRectWorld.center);
			return RectUtil.GetClosestPointOnRectInDir(offsetRectWorld, angle * Mathf.Deg2Rad);
		}
	}
}