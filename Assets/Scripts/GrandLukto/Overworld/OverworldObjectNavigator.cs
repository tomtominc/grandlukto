﻿using GrandLukto.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.Overworld
{
	/// <summary>
	/// Handles a navigation map between overworld objects.
	/// </summary>
	public class OverworldObjectNavigator : MonoBehaviour
	{
		private Dictionary<FaceDirection, OverworldObjectBase> navigationMap = new Dictionary<FaceDirection, OverworldObjectBase>();

		private void Start()
		{
			var overworldObject = GetComponent<OverworldObjectBase>();
			Assert.IsNotNull(overworldObject, "Only works on an OverworldObject.");

			InitializeNavigationMap(overworldObject.connectedObjects);
		}
		
		/// <summary>
		/// Checks which key can be assigned to which connection.
		/// </summary>
		private void InitializeNavigationMap(List<OverworldObjectBase> connectedObjects)
		{
			for (var i = 0; i < connectedObjects.Count; i++)
			{
				var connectedObj = connectedObjects[i];

				var angle = Vector2Util.AngleSigned(Vector2.up, connectedObj.transform.position - transform.position);
				var direction = Mathf.FloorToInt((angle + 45) / 90); // offset by 45 so we have 45° offset in every direction
				var directionAbs = Mathf.Abs(direction);

				FaceDirection faceDir;

				switch (directionAbs)
				{
					case 0:
						faceDir = FaceDirection.Up;
						break;
					case 1:
						if (direction == 1)
						{
							faceDir = FaceDirection.Left;
						}
						else
						{
							faceDir = FaceDirection.Right;
						}
						break;
					case 2:
						faceDir = FaceDirection.Down;
						break;
					default:
						Debug.LogError("Something went wrong. Direction not supported!");
						return;
				}

				AddNavigation(faceDir, connectedObj);
			}
		}

		private void AddNavigation(FaceDirection direction, OverworldObjectBase target)
		{
			if (navigationMap.ContainsKey(direction))
			{
				Debug.LogError("Can connect to 2 nodes in the same direction!", this);
				return;
			}

			navigationMap[direction] = target;
		}

		public OverworldObjectBase GetObjectInDirection(FaceDirection direction)
		{
			OverworldObjectBase target;
			return navigationMap.TryGetValue(direction, out target) ? target : null;
		}
	}
}