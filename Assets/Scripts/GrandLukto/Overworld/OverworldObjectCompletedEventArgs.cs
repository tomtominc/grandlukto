﻿using System;

namespace GrandLukto.Overworld
{
	public class OverworldObjectCompletedEventArgs : EventArgs
	{
		public bool PlayAnimation { get; private set; }

		public OverworldObjectCompletedEventArgs(bool playAnimation)
		{
			PlayAnimation = playAnimation;
		}
	}
}