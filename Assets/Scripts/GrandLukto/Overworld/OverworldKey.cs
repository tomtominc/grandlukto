﻿using DG.Tweening;
using UnityCave.Utils;
using UnityEngine;
using UnityCave.ExtensionMethods;

namespace GrandLukto.Overworld
{
	public class OverworldKey : OverworldObjectBase
	{
		[HideInInspector]
		public OverworldGate targetGate;

		public float nodeOffsetEllipsisX;
		public float nodeOffsetEllipsisY;

		[Header("Key Animation")]
		public GameObject keyVisuals;
		public GameObject keyShadow;

		public float moveKeyOffset = 0.3f;
		public float moveKeyDuration = 1f;

		public float collectKeyOffset = 0.3f;
		public float collectKeyDuration = 1f;

		public float cameraSpeed = 3f;

		private Tween tweenKeyMovement;
		
		public override bool CanMoveFrom
		{
			get
			{
				return !isAnimating;
			}
		}

		private bool isAnimating = false;

		override public void Awake()
		{
			base.Awake();

			if (string.IsNullOrEmpty(nodeID))
			{
				Debug.LogError("Key without nodeID detected!", this);
			}
		}

		public override Vector3 GetClosestNodePosition(OverworldObjectBase to)
		{
			var angle = Vector2Util.AngleSigned(Vector2.right, to.transform.position - transform.position);
			var angleRad = angle * Mathf.Deg2Rad;

			return transform.position + new Vector3(nodeOffsetEllipsisX * Mathf.Cos(angleRad), nodeOffsetEllipsisY * Mathf.Sin(angleRad));
		}

		override public void Start()
		{
			base.Start();

			isAnimating = !IsCompleted && IsJustCompletedNode; // prevent player from moving away

			tweenKeyMovement = keyVisuals.transform
				.DOLocalMoveY(moveKeyOffset, moveKeyDuration)
				.SetRelative()
				.SetEase(Ease.InOutSine)
				.SetLoops(-1, LoopType.Yoyo);

			if (IsCompleted)
			{
				// instant unlock
				Destroy(keyVisuals);
				Destroy(keyShadow);
			}
		}
		
		protected override void OnNodeCompleted()
		{
			base.OnNodeCompleted();

			isAnimating = true; // mainly for the debug key

			if (targetGate == null)
			{
				Debug.LogError("No gate set for the collected key!", this);
				isAnimating = false;
				return;
			}

			tweenKeyMovement.Kill(); // kill active motion

			var visualsSprite = keyVisuals.GetComponent<tk2dSprite>();
			var shadowSprite = keyShadow.GetComponent<tk2dSprite>();

			var cameraMoveDuration = Vector2.Distance(transform.position, targetGate.transform.position) / cameraSpeed;

			DOTween.Sequence()
				// collect key anim
				.Append(visualsSprite.transform
					.DOMoveY(collectKeyOffset, collectKeyDuration)
					.SetRelative()
					.SetEase(Ease.OutExpo))
				.Join(visualsSprite
					.DOColor(Color.white.MakeTransparent(), collectKeyDuration)
					.SetEase(Ease.OutSine))
				.Join(shadowSprite
					.DOColor(Color.white.MakeTransparent(), collectKeyDuration)
					.SetEase(Ease.OutSine))
				// reveal gate
				.AppendCallback(() =>
				{
					targetGate.RevealNodeAndConnectedNodes();
				})
				// move camera to gate
				.Append(Camera.main.transform
					.DOMove(targetGate.transform.position.Clone(z: Camera.main.transform.position.z), cameraMoveDuration)
					.SetEase(Ease.InOutSine))
				// unlock gate
				.Append(targetGate.PlayUnlockAnimation())
				// move camera back to key
				.Append(Camera.main.transform
					.DOMove(transform.position.Clone(z: Camera.main.transform.position.z), cameraMoveDuration)
					.SetEase(Ease.InOutSine))
				.AppendCallback(() =>
				{
					isAnimating = false; // end animation -> key is collected now
				});
		}
	}
}