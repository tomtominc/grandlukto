﻿using System;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Overworld
{
	public class OverworldEvents : MonoBehaviour
	{
		public event EventHandler<OverworldMoveEventArgs> PlayerJumpedOffNode;
		public event EventHandler<OverworldMoveEventArgs> PlayerJumpedOnNode;
		public event EventHandler InitializeNodesCompleted;

		public static OverworldEvents Instance
		{
			get
			{
				return SingletonUtil.GetInstance<OverworldEvents>();
			}
		}

		public void OnPlayerJumpedOffNode(object sender, OverworldMoveEventArgs e)
		{
			if (PlayerJumpedOffNode != null) PlayerJumpedOffNode(sender, e);
		}

		public void OnPlayerJumpedOnNode(object sender, OverworldMoveEventArgs e)
		{
			if (PlayerJumpedOnNode != null) PlayerJumpedOnNode(sender, e);
		}

		public void OnInitializeNodesCompleted(object sender)
		{
			if (InitializeNodesCompleted != null) InitializeNodesCompleted(sender, EventArgs.Empty);
		}
	}
}