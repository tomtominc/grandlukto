﻿using UnityEngine;

namespace GrandLukto.Arenas
{
	public class Snow : MonoBehaviour
	{
		[Tooltip("Radius to the center in which the emitter moves.")]
		public float radius = 12;

		public float maxAngle = 80;

		public WindZone windZone;

		private WindManager windManager;

		private Quaternion initialRotation;

		private void Awake()
		{
			windManager = FindObjectOfType<WindManager>();

			initialRotation = transform.rotation;
		}

		private void Update()
		{
			var strength = windManager.WindStrength;

			// look at the center
			transform.rotation = initialRotation * Quaternion.Euler(0, 0, maxAngle * strength);
			transform.position = Quaternion.Euler(0, 0, maxAngle * strength) * Vector2.up * radius;

			if (strength > 0.5f)
			{
				windZone.gameObject.SetActive(true);
				windZone.windMain = Mathf.InverseLerp(0.5f, 1, strength);
			}
			else
			{
				windZone.gameObject.SetActive(false);
			}
		}
	}
}