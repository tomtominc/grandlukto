﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityCave.CustomDataTypes;

namespace Assets.Scripts.GrandLukto.Arenas
{
	public class ArenaBossRing : MonoBehaviour
	{
		public FloatRange randStrength;
		public IntRange randVibrato;
		public FloatRange randDuration;

		public FloatRange randDelay;

		private Coroutine routineShake;

		public void OnEnable()
		{
			routineShake = StartCoroutine(RoutineShake());
		}

		public void OnDisable()
		{
			StopCoroutine(routineShake);
		}

		private IEnumerator RoutineShake()
		{
			while(true)
			{
				yield return new WaitForSeconds(randDelay);
				DoShake();
			}
		}

		private void DoShake()
		{
			transform.DOShakePosition(randDuration, randStrength, randVibrato, 90);
		}
	}
}
