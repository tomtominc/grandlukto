﻿using Spine.Unity;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class Fisherman : MonoBehaviour
	{
		[SpineAnimation]
		public string animIdle;

		public IntRange animIdleCountRand;
		private int animIdleLeft;

		[SpineAnimation]
		public string animPull;

		public SkeletonAnimation skeletonAnimation;

		private void Start()
		{
			skeletonAnimation.AnimationState.Complete += AnimationState_Complete;

			animIdleLeft = animIdleCountRand;
			PlayNextAnimation();
		}

		private void AnimationState_Complete(Spine.TrackEntry trackEntry)
		{
			PlayNextAnimation();
		}

		private void PlayNextAnimation()
		{
			if (animIdleLeft > 0)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, animIdle, false);
				animIdleLeft--;
				return;
			}

			skeletonAnimation.AnimationState.SetAnimation(0, animPull, false);
			animIdleLeft = animIdleCountRand;
		}

		private void OnDestroy()
		{
			skeletonAnimation.AnimationState.Complete -= AnimationState_Complete;
		}
	}
}