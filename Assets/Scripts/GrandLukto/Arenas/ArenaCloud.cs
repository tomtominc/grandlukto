﻿using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaCloud : MonoBehaviour
	{
		public FloatRange randomSpeed;
		public IntRange nameRandomNum;

		private tk2dSprite compSprite;
		private float curSpeed;

		public void Awake()
		{
			compSprite = GetComponent<tk2dSprite>();
		}

		public void OnEnable()
		{
			compSprite.SetSprite("cloud" + (int)nameRandomNum);
			curSpeed = randomSpeed;
		}

		public void Update()
		{
			transform.position = transform.position + new Vector3(-curSpeed * Time.deltaTime, 0, 0);
		}
	}
}