﻿using System;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public abstract class ArenaIntroBase : MonoBehaviour
	{
		public EventHandler IntroComplete;

		protected void OnIntroComplete()
		{
			IntroComplete(this, EventArgs.Empty);
		}
	}
}
