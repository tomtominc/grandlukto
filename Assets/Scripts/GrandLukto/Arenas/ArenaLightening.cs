﻿using DG.Tweening;
using System.Collections;
using UnityCave.CustomDataTypes;
using UnityCave.ExtensionMethods;
using UnityEngine;
using System;

namespace GrandLukto.Arenas
{
	public class ArenaLightening : MonoBehaviour
	{
		public FloatRange randFlashDuration;
		public Ease flashEase;

		public FloatRange randStartAlpha;
		public FloatRange randInterval;
		public FloatRange randDelayBetweenFlashes;

		public bool autoStart = true;

		private tk2dSprite compSprite;

		private Coroutine routineLightening;
		private Tween lastTween;

		public void Start()
		{
			compSprite = GetComponent<tk2dSprite>();

			if (!autoStart) return;
			StartLighteningInInterval();
		}

		public void StartLighteningInInterval()
		{
			if (routineLightening != null) return;
			routineLightening = StartCoroutine(RoutineLightening());
		}

		private IEnumerator RoutineLightening()
		{
			while(true)
			{
				yield return new WaitForSeconds(randInterval);
				SpawnLightening();
			}
		}

		public void SpawnLightening()
		{
			StartCoroutine(SpawnLighteningRoutine());
		}

		private IEnumerator SpawnLighteningRoutine()
		{
			// always flash 2 times
			StartLighteningTween();
			yield return new WaitForSeconds(randDelayBetweenFlashes);
			StartLighteningTween();
		}

		private void StartLighteningTween()
		{
			if(lastTween != null)
			{
				lastTween.Complete();
			}

			lastTween = DOTween.Sequence()
				.AppendCallback(SetStartFlashAlpha)
				.Append(CreateFlashTween());
		}

		private void SetStartFlashAlpha()
		{
			compSprite.color = new Color(1, 1, 1, randStartAlpha);
		}

		private Tween CreateFlashTween()
		{
			return compSprite
				.DOColor(new Color(1, 1, 1, 0), randFlashDuration)
				.SetEase(flashEase);
		}
	}
}