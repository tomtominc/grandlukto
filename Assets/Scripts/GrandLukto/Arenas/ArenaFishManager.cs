﻿using System.Collections.Generic;
using UnityCave.Utils;
using UnityEngine;
using UnityCave.ExtensionMethods;
using UnityCave.Movement;
using Spine.Unity;
using System.Collections;

namespace GrandLukto.Arenas
{
	public class ArenaFishManager : MonoBehaviour
	{
		public BoxCollider2D areaOutside1;
		public BoxCollider2D areaOutside2;
		public BoxCollider2D areaInside;

		public int minFish = 3;
		public int maxFish = 10;

		public int minWaypoints = 2;
		public int maxWaypoints = 5;

		private GameObject fishContainer;

		public List<GameObject> FishPool { get; private set; }
		public List<GameObject> ActiveFish { get; private set; }

		private List<Vector2> movePath;
		private FollowPath pathFollower;

		private bool animationStarted = false;

		public GameObject Leader
		{
			get
			{
				return pathFollower.gameObject;
			}
		}

		public void Start()
		{
			fishContainer = new GameObject("Container");
			fishContainer.transform.SetParent(transform, false);

			CreateLeader();
			CreateFish();

			StartCoroutine(RestartAnimationDelayed());
		}

		[ContextMenu("Restart Animation")]
		private void RestartAnimation()
		{
			GetActiveFish();
			GenerateNewPath();
		}

		private void CreateLeader()
		{
			var leaderGameObj = (GameObject)Instantiate(Resources.Load<GameObject>("Arenas/Fish"), fishContainer.transform);
			leaderGameObj.name = "Leader";

			Destroy(leaderGameObj.GetComponent<SkeletonAnimation>());
			Destroy(leaderGameObj.GetComponent<MeshFilter>());
			Destroy(leaderGameObj.GetComponent<MeshRenderer>());

			pathFollower = leaderGameObj.AddComponent<FollowPath>();
		}

		private void CreateFish()
		{
			FishPool = new List<GameObject>();

			for (var i = 0; i < maxFish; i++)
			{
				var fishGameObj = (GameObject)Instantiate(Resources.Load<GameObject>("Arenas/Fish"), fishContainer.transform);
				fishGameObj.AddComponent<ArenaFish>();
				fishGameObj.SetActive(false); // animation can start after delay
				FishPool.Add(fishGameObj);
			}
		}

		private void GetActiveFish()
		{
			ActiveFish = new List<GameObject>();

			int numFish = Random.Range(minFish, maxFish);

			for(var i = 0; i < FishPool.Count; i++)
			{
				var curFish = FishPool[i];
				var makeActive = i < numFish;

				if(!makeActive)
				{
					curFish.SetActive(false);
					continue;
				}

				curFish.SetActive(true);
				ActiveFish.Add(curFish);
			}
		}

		private void GenerateNewPath()
		{
			movePath = GenerateWaypoints();

			BoxCollider2D start;
			BoxCollider2D end;

			if(MyRandom.Bool())
			{
				// bottom to top
				start = areaOutside2;
				end = areaOutside1;
			}
			else
			{
				// top to bottom
				start = areaOutside1;
				end = areaOutside2;

				movePath.Reverse();
			}

			movePath.Add(end.bounds.RandomInPercentage());
			DebugDrawPath();

			pathFollower.transform.position = start.bounds.RandomInPercentage();
			pathFollower.path = movePath;
			pathFollower.Restart();

			SpawnFish(start.bounds);
		}

		private void SpawnFish(Bounds area)
		{
			foreach(var fish in ActiveFish)
			{
				fish.transform.position = area.RandomInPercentage();
			}
		}

		private void DebugDrawPath()
		{
			Vector2? lastWaypoint = null;
			foreach (var waypoint in movePath)
			{
				if (lastWaypoint.HasValue)
				{
					Debug.DrawLine(lastWaypoint.Value, waypoint, Color.red, 60);
				}

				lastWaypoint = waypoint;
			}
		}

		private List<Vector2> GenerateWaypoints()
		{
			List<Vector2> waypoints = new List<Vector2>();

			var numWaypoints = Random.Range(minWaypoints, maxWaypoints);
			float numWaypointsInversed = 1f / numWaypoints;

			for (var i = 0; i < numWaypoints; i++)
			{
				waypoints.Add(areaInside.bounds.RandomInPercentage(0, 1, i * numWaypointsInversed, (i + 1) * numWaypointsInversed));
			}

			return waypoints;
		}

		public void FixedUpdate()
		{
			if (!animationStarted) return; // waiting for animation to start after delay
			if (!AllFishTargetReached()) return;

			// all fish reached the target
			StartCoroutine(RestartAnimationDelayed());
		}

		private IEnumerator RestartAnimationDelayed()
		{
			animationStarted = false;
			yield return new WaitForSeconds(Random.value * 5f);
			RestartAnimation();
			animationStarted = true;
		}

		private bool AllFishTargetReached()
		{
			if (!pathFollower.TargetReached) return false;

			foreach(var fish in ActiveFish)
			{
				if (((Vector2)fish.GetComponent<SteeringBehaviour>().Velocity).magnitude > 0.1f)
				{
					return false;
				}
			}

			return true;
		}
	}
}