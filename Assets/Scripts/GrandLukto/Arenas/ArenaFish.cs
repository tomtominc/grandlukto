﻿using Spine.Unity;
using UnityCave.ExtensionMethods;
using UnityCave.Movement;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaFish : MonoBehaviour
	{
		private ArenaFishManager compFishManager;
		private SteeringBehaviour compSteeringBehaviour;
		private SkeletonAnimation compSpineAnim;

		public void Awake()
		{
			compSpineAnim = GetComponent<SkeletonAnimation>();
			compFishManager = GetComponentInParent<ArenaFishManager>();
			compSteeringBehaviour = GetComponent<SteeringBehaviour>();
		}

		public void OnEnable()
		{
			Randomize();
		}

		public void FixedUpdate()
		{
			compSteeringBehaviour.Arrive(compFishManager.Leader.transform.position, 1f);
			compSteeringBehaviour.Separation(compFishManager.ActiveFish);
		}

		public void LateUpdate()
		{
			transform.localPosition = transform.localPosition.Clone(z: 0);
		}

		/// <summary>
		/// Changes the fish's properties to randomize it a bit.
		/// </summary>
		public void Randomize()
		{
			compSteeringBehaviour.separationRadius = Random.Range(0.3f, 0.4f);
			compSteeringBehaviour.speedMax = Random.Range(0.9f, 1.2f);

			var randScale = Random.Range(0.8f, 1.1f);
			transform.localScale = new Vector3(randScale, randScale, randScale);

			compSpineAnim.AnimationState.Update(Random.value * 2); // offset animation
		}
	}
}