﻿using Spine.Unity;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaPalmTree : MonoBehaviour
	{
		public void Start()
		{
			// randomize
			transform.position += new Vector3(Random.Range(-1, 1), Random.Range(-0.1f, 0.1f), 0);

			// scale and flip
			var randScale = Random.Range(0.95f, 1);
			transform.localScale = new Vector3(randScale * (MyRandom.Bool() ? 1 : -1), randScale, randScale);

			var skeletonAnim = GetComponent<SkeletonAnimation>();
			skeletonAnim.AnimationState.Update(Random.value * 3f);
			skeletonAnim.AnimationState.TimeScale = Random.Range(0.9f, 1.1f);
		}
	}
}
