﻿using GrandLukto.Database;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaConfiguration : MonoBehaviour
	{
		private GameManager gameManager;

		public GameObject colliders;
		public Vector2[] ballSpawnPositions;

		public GameObject ArenaVisuals { get; private set; }
		public ArenaIntroBase ArenaIntro { get; private set; }

		private void Awake()
		{
			gameManager = GameManager.Instance;
		}

		public Vector3 GetBallSpawnPosition()
		{
			if (!ballSpawnPositions.HasItems()) return Vector3.zero;
			return ballSpawnPositions.SelectRandom();
		}

		public void LoadArena(DataArena arena)
		{
			if(arena == null)
			{
				Debug.LogError("Arena not set!");
				return;
			}

			ArenaVisuals = Instantiate(arena.arenaPrefab, transform);
			ArenaIntro = ArenaVisuals.GetComponent<ArenaIntroBase>();

			var audioSource = GetComponent<AudioSource>();
			audioSource.clip = arena.soundEnvironment;
			audioSource.Play();

			colliders.transform.position = colliders.transform.position + (Vector3)arena.gameplayOffset;

			foreach (var blockGroup in gameManager.BlockGroupByTeam.Values)
			{
				blockGroup.transform.position = blockGroup.transform.position + (Vector3)arena.gameplayOffset;
			}
		}
	}
}