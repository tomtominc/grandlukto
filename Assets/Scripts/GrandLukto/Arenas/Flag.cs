﻿using System.Collections;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class Flag : MonoBehaviour
	{
		public FloatRange thinkInterval;
		public Animator animator;

		private Coroutine updateCoroutine;

		private WindManager windManager;

		private void Awake()
		{
			windManager = FindObjectOfType<WindManager>();
		}

		private void OnEnable()
		{
			updateCoroutine = StartCoroutine(UpdateWindStrength());
		}

		private IEnumerator UpdateWindStrength()
		{
			while(true)
			{
				yield return new WaitForSeconds(thinkInterval);
				animator.SetFloat("WindStrength", windManager.WindStrength);
			}
		}

		private void OnDisable()
		{
			StopCoroutine(updateCoroutine);
		}
	}
}