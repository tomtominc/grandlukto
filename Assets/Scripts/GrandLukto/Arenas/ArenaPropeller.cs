﻿using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaPropeller : MonoBehaviour
	{
		public float rotationSpeed = 1;
		public bool startRandom = true;

		public void Start()
		{
			if(startRandom)
			{
				transform.localRotation = transform.localRotation * Quaternion.Euler(0, 0, Random.value * 360);
			}
		}

		public void Update()
		{
			transform.localRotation = transform.localRotation * Quaternion.Euler(0, 0, rotationSpeed * Time.deltaTime);
		}
	}
}
