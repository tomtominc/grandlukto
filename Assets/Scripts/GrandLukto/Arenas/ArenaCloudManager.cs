﻿using UnityCave.CustomDataTypes;
using UnityEngine;
using UnityCave.ExtensionMethods;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GrandLukto.Arenas
{
	/// <summary>
	/// Spawns a number of clouds and moves them across the screen. Moves off-screen clouds back to the start and spawns them again.
	/// </summary>
	public class ArenaCloudManager : MonoBehaviour
	{
		[Tooltip("Number of clouds that are visible at the start of the game.")]
		public IntRange numClouds;

		public float checkSpawnCloudInterval = 0.5f;

		public float outOfScreenOffset = 2f;

		private Coroutine routineSpawnCloud;

		private List<GameObject> activeClouds = new List<GameObject>();
		private List<GameObject> inactiveClouds = new List<GameObject>();

		public void Start()
		{
			var cameraExtents = Camera.main.OrthographicBounds();

			int randomCloudNum = numClouds;

			for(var i = 0; i < randomCloudNum; i++)
			{
				var cloudGameObj = CreateCloud();
				cloudGameObj.transform.position = cameraExtents.RandomInPercentage();
				cloudGameObj.transform.localPosition = cloudGameObj.transform.localPosition.Clone(z: GetRandomZValue());
			}
		}

		public void OnEnable()
		{
			routineSpawnCloud = StartCoroutine(RoutineSpawnCloud());
		}

		public void OnDisable()
		{
			StopCoroutine(routineSpawnCloud);
		}

		public IEnumerator RoutineSpawnCloud()
		{
			while(true)
			{
				yield return new WaitForSeconds(checkSpawnCloudInterval);

				var cameraBounds = Camera.main.OrthographicBounds();

				inactiveClouds.AddRange(activeClouds.Where(gameObj => gameObj.transform.position.x < cameraBounds.min.x - outOfScreenOffset));
				inactiveClouds.ForEach(gameObj => gameObj.SetActive(false));

				var cloudGameObj = GetCloudFromPool();
				if (cloudGameObj == null) continue; // no new cloud to spawn

				cloudGameObj.transform.position = (Vector3)cameraBounds.RandomInPercentage(1, 1, 0, 1) + new Vector3(outOfScreenOffset, 0);
				cloudGameObj.transform.localPosition = cloudGameObj.transform.localPosition.Clone(z: GetRandomZValue());
			}
		}

		private GameObject GetCloudFromPool()
		{
			if (!inactiveClouds.HasItems()) return null;

			var selectedCloud = inactiveClouds[0];
			inactiveClouds.RemoveAt(0);
			selectedCloud.SetActive(true); // set active to randomize
			return selectedCloud;
		}

		private GameObject CreateCloud()
		{
			var newCloud = (GameObject)Instantiate(Resources.Load<GameObject>("Arenas/Cloud"), transform);
			activeClouds.Add(newCloud);
			return newCloud;
		}

		private float GetRandomZValue()
		{
			return Random.Range(0, 0.1f);
		}
	}
}