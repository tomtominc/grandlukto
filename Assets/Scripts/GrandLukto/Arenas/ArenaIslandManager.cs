﻿using UnityCave.Utils;
using UnityEngine;
using UnityCave.ExtensionMethods;
using System.Collections.Generic;
using System.Linq;

namespace GrandLukto.Arenas
{
	public class ArenaIslandManager : MonoBehaviour
	{
		public BoxCollider2D spawnAreaA;
		public BoxCollider2D spawnAreaB;

		private List<string> islandNames;

		public void Start()
		{
			islandNames = new List<string>();
			for(var i = 1; i <= 3; i++)
			{
				islandNames.Add("island" + i);
			}

			islandNames = islandNames.Shuffle().ToList(); // shuffle to randomize even more

			if(MyRandom.Bool())
			{
				SpawnOne(spawnAreaA);
				SpawnOne(spawnAreaB);
				return;
			}

			var spawnOne = MyRandom.Bool() ? spawnAreaA : spawnAreaB;
			var spawnTwo = spawnOne == spawnAreaA ? spawnAreaB : spawnAreaA;

			SpawnOne(spawnOne);
			SpawnTwo(spawnTwo);
		}

		private void SpawnOne(BoxCollider2D area)
		{
			var island = CreateIsland();

			island.transform.position = area.bounds.RandomInPercentage();
			RandomizeLocalZ(island);
		}

		private void SpawnTwo(BoxCollider2D area)
		{
			var island = CreateIsland();

			island.transform.position = area.bounds.RandomInPercentage(0, 1, 0, 0.3f);
			RandomizeLocalZ(island);

			island = CreateIsland();

			island.transform.position = area.bounds.RandomInPercentage(0, 1, 0.7f, 1);
			RandomizeLocalZ(island);
		}

		private GameObject CreateIsland()
		{
			var gameObj = (GameObject)Instantiate(Resources.Load<GameObject>("Arenas/Island"), transform);

			var tk2dSprite = gameObj.GetComponent<tk2dSprite>();
			tk2dSprite.SetSprite(islandNames[0]);
			islandNames.RemoveAt(0); // remove the name to get the next

			return gameObj;
		}

		private void RandomizeLocalZ(GameObject obj)
		{
			obj.transform.localPosition = obj.transform.localPosition.Clone(z: Random.Range(0, 0.1f));
		}
	}
}