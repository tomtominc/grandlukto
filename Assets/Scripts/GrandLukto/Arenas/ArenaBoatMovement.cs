﻿using DG.Tweening;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaBoatMovement : MonoBehaviour
	{
		public float tweenDurationMin = 1f;
		public float tweenDurationMax = 2f;

		public float idleBetweenMin = 0f;
		public float idleBetweenMax = 0.5f;

		public float minYOffset = 0.5f;
		public float maxYOffset = 0.5f;

		public float minRotate = 1;
		public float maxRotate = 2;

		private Tween tweenMove;

		private Vector3 startPos;

		public void OnEnable()
		{
			startPos = transform.localPosition;

			StartTweenMove();
		}
		
		private void StartTweenMove()
		{
			var duration = Random.Range(tweenDurationMin, tweenDurationMax);

			tweenMove = DOTween.Sequence()
				.SetDelay(Random.Range(idleBetweenMin, idleBetweenMax))
				.Append(transform.DOLocalMove(startPos + new Vector3(0, Random.Range(minYOffset, maxYOffset)), duration))
				.SetEase(Ease.InOutSine)
				.Join(transform.DORotate(new Vector3(0, 0, Random.Range(minRotate, maxRotate)), duration / 2))
				.SetEase(Ease.InOutSine)
				.Join(transform.DORotate(new Vector3(0, 0, 0), duration / 2).SetDelay(duration / 2))
				.SetEase(Ease.InOutSine)
				.Append(transform.DOLocalMove(startPos, duration))
				.SetEase(Ease.InOutSine)
				.Join(transform.DORotate(new Vector3(0, 0, -Random.Range(minRotate, maxRotate)), duration / 2))
				.SetEase(Ease.InOutSine)
				.Join(transform.DORotate(new Vector3(0, 0, 0), duration / 2).SetDelay(duration / 2))
				.SetEase(Ease.InOutSine)
				.OnComplete(StartTweenMove);
		}
		
		public void OnDisable()
		{
			if (tweenMove != null)
			{
				tweenMove.Goto(0);
				tweenMove.Kill();
			}
		}
	}
}
