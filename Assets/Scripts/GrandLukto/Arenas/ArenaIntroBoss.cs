﻿using DG.Tweening;
using UnityEngine;

namespace GrandLukto.Arenas
{
	public class ArenaIntroBoss : ArenaIntroBase
	{
		public GameObject boss;

		private ArenaLightening compLightening;

		public void Awake()
		{
			compLightening = GetComponentInChildren<ArenaLightening>();
			compLightening.autoStart = false;
		}

		public void Start()
		{
			boss.transform
				.DOMove(boss.transform.position - new Vector3(0, 5, 0), 3f)
				.From()
				.OnComplete(OnBossMoveComplete);
		}

		private void OnBossMoveComplete()
		{
			compLightening.SpawnLightening();
			compLightening.StartLighteningInInterval();

			var renderer = boss.GetComponent<Renderer>();
			renderer.material.SetFloat("_FlashAmount", 0);

			//OnIntroComplete(); // todo: enable after trailer to start countdown
		}
	}
}
