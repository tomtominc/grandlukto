﻿using GrandLukto.Characters;
using GrandLukto.Database;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class DatabaseManager : MonoBehaviour
	{
		public static DatabaseManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<DatabaseManager>("DatabaseManager");
			}
		}

		public DatabaseBlockLayout dbBlockLayout;
		public DatabaseCharacter dbCharacter;
		public DatabaseLevel dbLevel;
		public DatabaseArena dbArena;
		public DatabaseBlockLayoutGroup dbBlockLayoutGroup;
		public PlayerColors playerColors;
	}
}
