// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// unlit, vertex colour, premultiplied alpha blend

Shader "GrandLukto/TiledBackground" 
{
	Properties 
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_SecondTex ("Base (RGB) Trans (A)", 2D) = "white" {}

		_FirstToSecondTex("First to second tex", Range(0, 1)) = 0
		_FadeTex("Fade texture", Range(0, 1)) = 1

		_UnscaledTime("Unscaled time", Float) = 0
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend One OneMinusSrcAlpha
		LOD 110
		
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert_vct
			#pragma fragment frag_mult 
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _SecondTex;
			float4 _SecondTex_ST;

			float _FirstToSecondTex;
			float _FadeTex;
			float _UnscaledTime;

			struct vin_vct 
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f_vct
			{
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 screenpos : TEXCOORD2;
			};

			v2f_vct vert_vct(vin_vct v)
			{
				v2f_vct o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;

				float sinY = sin(radians(8.5f)); // rotate around z axis
				float2x2 rotationMatrix = float2x2(1, 0, sinY, 1);

				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.texcoord.xy = mul(o.texcoord.xy, rotationMatrix);
				o.texcoord.x += -_UnscaledTime / 20 * 25; // move background

				o.texcoord1 = TRANSFORM_TEX(v.texcoord, _SecondTex);
				o.texcoord1.xy = mul(o.texcoord1.xy, rotationMatrix);
				o.texcoord1.x += -_UnscaledTime / 20 * 25; // move background

				// position on screen from 0 (bottom | left) to 1 (top | right)
				o.screenpos = ComputeScreenPos(o.vertex);

				return o;
			}

			fixed4 frag_mult(v2f_vct i) : SV_Target
			{
				fixed4 color = lerp(tex2D(_MainTex, i.texcoord), tex2D(_SecondTex, i.texcoord1), _FirstToSecondTex) * i.color;

				if (1 - i.screenpos.y > _FadeTex)
				{
					discard; // fade from top to bottom
				}

				return color;
			}
		
			ENDCG
		} 
	}
}
