﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.UncoveredAreaDetection
{
	public class UncoveredAreaDetector
	{
		private int gridWidth = 6;
		private int gridHeight = 1;

		private List<List<GridElement>> gridElementsByXY;
		private List<GridElement> gridElements;

		public Team Team { get; private set; }

		public int GridWidth { get { return gridWidth;  } }
		public int GridHeight { get { return gridHeight; } }

		public int UpdateCount { get; private set; }
		public GameObject ExcludedCharacter { get; private set; }

		public UncoveredAreaDetector(Team team, int gridWidth)
		{
			Team = team;

			CreateGridElements();
		}

		private void CreateGridElements()
		{
			gridElements = new List<GridElement>();
			gridElementsByXY = new List<List<GridElement>>();

			for (var ctWidth = 0; ctWidth < gridWidth; ctWidth++)
			{
				var curCol = new List<GridElement>();
				gridElementsByXY.Add(curCol);

				for (var ctHeight = 0; ctHeight < gridHeight; ctHeight++)
				{
					var newGridElement = new GridElement(this, ctWidth, ctHeight);
					curCol.Add(newGridElement);
					gridElements.Add(newGridElement);
				}
			}
		}

		public void UpdateBounds(Bounds bounds)
		{
			var widthPerElement = bounds.size.x / gridWidth;
			var heightPerElement = bounds.size.y / gridHeight;

			var minX = bounds.min.x + widthPerElement / 2;
			var minY = bounds.min.y + heightPerElement / 2;

			foreach (var gridElement in gridElements)
			{
				gridElement.UpdateRealPos(minX, minY, widthPerElement, heightPerElement);
			}
		}

		public Bounds GetUncoveredArea(GameObject excludedCharacter)
		{
			UpdateCount++;
			ExcludedCharacter = excludedCharacter;

			GridElement bestMatch = null;
			float bestMatchDist = 0;

			foreach(var gridElement in gridElements)
			{
				var dist = gridElement.GetElementAndSurroundingMinDists();

				if(bestMatch == null || dist > bestMatchDist)
				{
					bestMatch = gridElement;
					bestMatchDist = dist;
				}
			}

			return bestMatch.Bounds;
		}

		public GridElement GetGridElement(int x, int y)
		{
			if (x < 0 || x >= gridWidth) return null;
			if (y < 0 || y >= gridHeight) return null;

			return gridElementsByXY[x][y];
		}
	}
}
