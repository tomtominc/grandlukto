﻿using System;
using UnityEngine;

namespace GrandLukto.UncoveredAreaDetection
{
	public class GridElement
	{
		private UncoveredAreaDetector parent;

		private int gridX;
		private int gridY;
		
		public Vector2 RealPos { get; private set; }
		public Bounds Bounds { get; private set; }

		private float lowestDist;

		private int lastUpdateCount = -1;

		public GridElement(UncoveredAreaDetector parent, int x, int y)
		{
			this.parent = parent;
			gridX = x;
			gridY = y;
		}

		public void UpdateRealPos(float minX, float minY, float widthPerElement, float heightPerElement)
		{
			RealPos = new Vector2(
				minX + gridX * widthPerElement,
				minY + gridY * heightPerElement);

			Bounds = new Bounds(RealPos, new Vector3(widthPerElement, heightPerElement, 0.1f));

			// DEBUG - draw center
			//var realPos = new Vector3(realX, realY, 0);
			//Debug.DrawLine(realPos, realPos + Vector3.up * 0.1f, Color.blue, 0.5f);
		}

		public float GetElementAndSurroundingMinDists()
		{
			// todo: cache?!

			UpdateLowestDist();

			var totalDist = lowestDist;
			totalDist += GetLowestDistForNeighbour(1, 0);
			totalDist += GetLowestDistForNeighbour(1, 1);
			totalDist += GetLowestDistForNeighbour(0, 1);
			totalDist += GetLowestDistForNeighbour(-1, 1);
			totalDist += GetLowestDistForNeighbour(-1, 0);
			totalDist += GetLowestDistForNeighbour(-1, -1);
			totalDist += GetLowestDistForNeighbour(0, -1);
			totalDist += GetLowestDistForNeighbour(1, -1);
			return totalDist;
		}

		private float GetLowestDistForNeighbour(int xOffset, int yOffset)
		{
			var gridElement = parent.GetGridElement(gridX + xOffset, gridY + yOffset);
			return gridElement == null ? 0 : GetLowestDist();
		}

		private float GetLowestDist()
		{
			UpdateLowestDist();
			return lowestDist;
		}

		private void UpdateLowestDist()
		{
			// will be called multiple times for one GridElement in one update cycle, check updateCount!
			if (lastUpdateCount == parent.UpdateCount) return; // already up to date
			lastUpdateCount = parent.UpdateCount;

			var allyChars = GameManager.Instance.PlayersByTeam[parent.Team];
			float? newLowestDist = null;

			foreach(var allyChar in allyChars)
			{
				if (allyChar.PlayerObject == parent.ExcludedCharacter) continue; // do not take this character into account for the calculation

				var distToCharacter = ((Vector2)allyChar.PlayerObject.transform.position - RealPos).magnitude;

				if (!newLowestDist.HasValue || distToCharacter < newLowestDist)
				{
					newLowestDist = distToCharacter;
				}
			}

			lowestDist = newLowestDist.HasValue ? newLowestDist.Value : 0;
		}
	}
}
