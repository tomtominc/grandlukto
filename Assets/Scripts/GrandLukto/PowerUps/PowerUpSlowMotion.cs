﻿using GrandLukto.Characters;
using UnityCave.CameraEffects;

namespace GrandLukto.PowerUps
{
	public class PowerUpSlowMotion : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var gameManager = GameManager.Instance;
			PostProcessingWaveExplosion.Create(character.transform.position, gameManager.gameConfiguration.powerUpSlowMotionExplosionTargetRadius, gameManager.gameConfiguration.powerUpSlowMotionExplosionAmplitude, gameManager.gameConfiguration.powerUpSlowMotionExplosionTime);
			GameSpeedManager.Instance.UseSlowMotionPowerUp();
		}
	}
}