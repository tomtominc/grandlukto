﻿using System;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public abstract class UIPowerUpSlotBase : MonoBehaviour, IComparable<UIPowerUpSlotBase>
	{
		public int slotNumber = 0;

		public abstract void ShowPowerUp(PowerUpType type, bool isOnHitActivated = false);

		public virtual void ShowOnHitPowerUpActivatedAnimation()
		{
			// implement to show an animation
		}

		public int CompareTo(UIPowerUpSlotBase other)
		{
			return slotNumber.CompareTo(other.slotNumber);
		}
	}
}