﻿using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public class PowerUpSpeedUp : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var playerMovement = character.GetComponent<PlayerMovement>();
			playerMovement.UseSpeedPowerUp();
		}
	}
}