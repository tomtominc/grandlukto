﻿using DG.Tweening;
using UnityCave.Helpers;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class UIPowerUpSlotSecondary : UIPowerUpSlotBase
	{
		public Vector3 tweenFromPosition;
		public float tweenDuration;

		public tk2dSprite iconSprite;

		public RememberStartPosition compRememberStartPos;

		private Tween curTween;

		public void Awake()
		{
			compRememberStartPos = GetComponent<RememberStartPosition>();
		}

		public void Start()
		{
			gameObject.SetActive(false);
			transform.localPosition = tweenFromPosition;
		}

		//public void Update()
		//{
		//	if (Input.GetMouseButtonDown(0))
		//	{
		//		ShowPowerUp(MyRandom.Bool() ? PowerUpType.BombKick : PowerUpType.Fireball, false);
		//	}
		//	else if (Input.GetMouseButtonDown(2))
		//	{
		//		ShowPowerUp(PowerUpType.None, false);
		//	}
		//}

		public override void ShowPowerUp(PowerUpType type, bool isOnHitActivated = false)
		{
			if (curTween != null && curTween.IsActive()) curTween.Kill();

			if(type == PowerUpType.None)
			{
				Hide();
				return;
			}

			iconSprite.SetSprite(type.GetIconName() + "_icon2");
			Show();
		}

		private void Hide()
		{
			curTween = DOTween.Sequence()
				.Append(transform
					.DOLocalMove(tweenFromPosition, tweenDuration)
					.SetEase(Ease.InBack))
				.Join(transform
					.DOScale(0, tweenDuration)
					.SetEase(Ease.InCubic))
				.AppendCallback(() =>
				{
					gameObject.SetActive(false);
				});
		}

		private void Show()
		{
			gameObject.SetActive(true);

			curTween = DOTween.Sequence()
				.Append(transform
					.DOLocalMove(compRememberStartPos.StartPositionLocal, tweenDuration)
					.SetEase(Ease.OutExpo))
				.Join(transform
					.DOScale(1, tweenDuration)
					.SetEase(Ease.OutCubic));
		}
	}
}