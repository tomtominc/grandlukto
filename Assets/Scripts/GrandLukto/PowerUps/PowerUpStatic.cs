﻿using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public static class PowerUpStatic
	{
		private static List<PowerUpType> OnHitPowerUps = new List<PowerUpType> { PowerUpType.BombKick };

		public static bool IsOnHitPowerUp(this PowerUpType type)
		{
			return OnHitPowerUps.Contains(type);
		}

		public static void UseOnHitPowerUpOnBall(Ball ball, PowerUpType type)
		{
			switch (type)
			{
				case PowerUpType.BombKick:
					var bombBall = new BallFactory().CreateBallBomb(ball.GetComponent<IBallController>());
					bombBall.CopyValuesFrom(ball);
					break;

				default:
					Debug.LogError("OnHit PowerUpType not implemented!");
					return;
			}
		}
	}
}