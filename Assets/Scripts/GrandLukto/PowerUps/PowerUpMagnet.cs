﻿using GrandLukto.Balls;
using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public class PowerUpMagnet : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			BallMagnetManager.Instance.AddMagnet(new BallMagnetData(character.transform, character.FaceDirection));
		}
	}
}