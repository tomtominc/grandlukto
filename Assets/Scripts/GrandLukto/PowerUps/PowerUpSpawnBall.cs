﻿using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using GrandLukto.Characters;
using System;

namespace GrandLukto.PowerUps
{
	public class PowerUpSpawnBall : IPowerUp
	{
		private readonly Func<BallFactory, IBallController> createBallFunc;

		public PowerUpSpawnBall(Func<BallFactory, IBallController> createBallFunc)
		{
			this.createBallFunc = createBallFunc;
		}

		public void UsePowerUp(CharacterBase character)
		{
			var ball = createBallFunc(new BallFactory());
			ball.InitializePosition(character.transform.position + character.FaceDirectionVec * GameManager.Instance.gameConfiguration.ballSpawnOffset);
		}
	}
}