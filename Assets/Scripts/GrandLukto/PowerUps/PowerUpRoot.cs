﻿using GrandLukto.Characters;
using GrandLukto.PowerUps.Effects;

namespace GrandLukto.PowerUps
{
	public class PowerUpRoot : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var enemies = GameManager.Instance.PlayersByTeam[character.Team.GetOpponent()];
			enemies.ForEach(enemy => enemy.PlayerObject.GetComponent<EffectsManager>().AddEffect(new RootEffect()));
		}
	}
}