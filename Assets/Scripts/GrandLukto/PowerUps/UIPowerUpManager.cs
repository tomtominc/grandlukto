﻿using GrandLukto.Characters;
using System;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class UIPowerUpManager : MonoBehaviour
	{
		public tk2dSprite playerIcon;

		public GameObject goButton;
		public tk2dTextMesh buttonText;

		public Team preferredTeam;

		private UIPowerUpSlotBase[] slots;
		private PowerUpManager compPowerUpManager;

		public bool IsTaken
		{
			get
			{
				return compPowerUpManager != null;
			}
		}

		public void Awake()
		{
			slots = GetComponentsInChildren<UIPowerUpSlotBase>();
			Array.Sort(slots); // sort by slot num
		}

		public void Start()
		{
			if (!IsTaken)
			{
				gameObject.SetActive(false);
			}
		}
		
		public void OnDisable()
		{
			if (compPowerUpManager == null) return;
			compPowerUpManager.OnQueueChanged.RemoveListener(OnQueueChanged);
			compPowerUpManager.OnSlotChanged.RemoveListener(OnSlotChanged);
			compPowerUpManager.OnActivateOnHitPowerUp.RemoveListener(OnActivateOnHitPowerUp);
		}

		private void OnQueueChanged(PowerUpManager powerUpManager)
		{
			var powerUpsInQueue = powerUpManager.PowerUpQueue.ToArray();

			for(var i = 0; i < slots.Length; i++)
			{
				var curSlot = slots[i];
				curSlot.ShowPowerUp(powerUpsInQueue.Length - 1 < i ? PowerUpType.None : powerUpsInQueue[i], i == 0 && powerUpManager.OnHitPowerUpActivated);
			}
		}

		private void OnSlotChanged(int slotID, PowerUpType powerUpType, bool onHitPowerUpActivated)
		{
			if (slotID < 0 || slotID >= slots.Length) return;
			slots[slotID].ShowPowerUp(powerUpType, onHitPowerUpActivated);
		}

		private void OnActivateOnHitPowerUp()
		{
			slots[0].ShowOnHitPowerUpActivatedAnimation();
		}

		public void AssignTo(CharacterBase character, PowerUpManager manager)
		{
			compPowerUpManager = manager;
			gameObject.SetActive(true);

			playerIcon.SetSprite("p" + character.PlayerSetting.PlayerNumber);

			// show button if player is human
			if (character.PlayerSetting.isHuman)
			{
				buttonText.text = character.Player.controllers.maps.GetFirstElementMapWithAction(Control.PowerUp.ToString(), true).elementIdentifierName;
				buttonText.color = character.PlayerColor;
			}
			else
			{
				goButton.SetActive(false);
			}

			compPowerUpManager.OnQueueChanged.AddListener(OnQueueChanged);
			compPowerUpManager.OnSlotChanged.AddListener(OnSlotChanged);
			compPowerUpManager.OnActivateOnHitPowerUp.AddListener(OnActivateOnHitPowerUp);
		}
	}
}
