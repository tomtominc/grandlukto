﻿using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public class PowerUpFireBall : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var spawner = character.gameObject.AddComponent<FireballSpawner>();
			spawner.destroyAfterSpawn = true;
		}
	}
}