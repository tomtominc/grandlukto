﻿using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public interface IPowerUp
	{
		void UsePowerUp(CharacterBase character);
	}
}