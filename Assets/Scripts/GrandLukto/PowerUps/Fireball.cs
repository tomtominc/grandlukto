﻿using UnityCave.ExtensionMethods;
using DG.Tweening;
using Spine.Unity;
using UnityEngine;
using UnityCave.Collision;
using GrandLukto.Characters.StateMachine;
using GrandLukto.Stats;
using UnityCave.CameraEffects;
using Spine;

namespace GrandLukto.PowerUps
{
	public class Fireball : MonoBehaviour
	{
		private enum AnimationStep
		{
			FadeShadow,
			FadeShadowAnimate,
			MoveDown,
			Impact,
			ImpactAnimation,
			BurnAnimation,
			BurnEnd,
			Finished
		}

		public GameObject visuals;
		public GameObject shadow;
		public GameObject explosion;

		public string explosionAnimImpactName;
		public string explosionAnimBurnName;
		public string explosionAnimFadeName;

		private SkeletonAnimation explosionAnimation;

		public float fadeShadowDuration = 1.5f;
		public float moveSpeed = 30;
		public float spawnBurnOffset = 0.3f;

		[Header("Impact")]
		public CollisionHandler explosionArea;

		[Header("Debug")]
		public bool loopAnimation = false;

		private tk2dSprite shadowSprite;

		private AnimationStep curStep;

		private float targetY;

		private Vector3 shadowScaleTarget;

		public void Awake()
		{
			shadowSprite = shadow.GetComponent<tk2dSprite>();

			explosionAnimation = explosion.GetComponent<SkeletonAnimation>();

			targetY = visuals.transform.localPosition.y;
			shadowScaleTarget = shadowSprite.scale;
		}

		public void Start()
		{
			ResetAnimation();
		}

		public void FixedUpdate()
		{
			switch(curStep)
			{
				case AnimationStep.Impact:
				case AnimationStep.ImpactAnimation:
				case AnimationStep.BurnAnimation:
				case AnimationStep.BurnEnd:
					DealDamage();
					break;
			}
		}

		public void Update()
		{
			switch(curStep)
			{
				case AnimationStep.FadeShadow:
					DOTween
						.To(() => shadowSprite.scale, (value) => shadowSprite.scale = value, shadowScaleTarget, fadeShadowDuration)
						.OnComplete(() => { curStep = AnimationStep.MoveDown; });
					
					curStep = AnimationStep.FadeShadowAnimate;
					break;

				case AnimationStep.MoveDown:
					var curY = visuals.transform.localPosition.y;
					curY = Mathf.MoveTowards(curY, targetY, moveSpeed * Time.deltaTime);
					visuals.transform.localPosition = visuals.transform.localPosition.Clone(y: curY);

					if(Mathf.Abs(curY - targetY) < spawnBurnOffset)
					{
						curStep = AnimationStep.Impact;
					}

					break;

				case AnimationStep.Impact:
					explosion.SetActive(true);
					visuals.SetActive(false);
					shadow.SetActive(false);

					GameManager.Instance.ScreenShake.Shake(new ScreenShakeSetting(0.4f, 0.05f, 10));

					explosionAnimation.Skeleton.SetToSetupPose();
					explosionAnimation.AnimationState.SetAnimation(0, explosionAnimImpactName, false);
					explosionAnimation.AnimationState.Complete += ExplosionAnimationComplete;

					curStep = AnimationStep.ImpactAnimation;
					break;
			}
		}

		private void ExplosionAnimationComplete(TrackEntry trackEntry)
		{
			switch(curStep)
			{
				case AnimationStep.ImpactAnimation:
					// play burn anim
					explosionAnimation.AnimationState.SetAnimation(0, explosionAnimBurnName, false);
					curStep = AnimationStep.BurnAnimation;
					break;

				case AnimationStep.BurnAnimation:
					// play burn anim
					explosionAnimation.AnimationState.SetAnimation(0, explosionAnimFadeName, false);
					curStep = AnimationStep.BurnEnd;
					break;

				case AnimationStep.BurnEnd:
					explosionAnimation.AnimationState.Complete -= ExplosionAnimationComplete;
					explosion.SetActive(false);

					curStep = AnimationStep.Finished;

#if UNITY_EDITOR
					if (Application.isEditor && loopAnimation)
					{
						ResetAnimation();
						break;
					}
#endif

					Destroy(gameObject);
					break;
			}
		}

		[ContextMenu("Reset Animation")]
		private void ResetAnimation()
		{
			curStep = default(AnimationStep);

			explosion.SetActive(false);
			visuals.SetActive(true);
			shadow.SetActive(true);

			shadowSprite.scale = Vector3.zero;
			visuals.transform.localPosition = visuals.transform.localPosition.Clone(y: targetY + 12);
		}

		private void DealDamage()
		{
			foreach(var collision in explosionArea.ActiveTriggerCollisions)
			{
				var stateMachine = collision.GetComponent<CharacterStateMachine>();
				if (stateMachine == null) continue;

				if (stateMachine.IsOutOfControl) continue; // already hit

				stateMachine.ChangeStateOutOfControl(0.15f, collision.transform.position - transform.position, 3);
				collision.GetComponent<StatManagerStunTime>().Fill(); // stun after getting pushed away
			}
		}
	}
}