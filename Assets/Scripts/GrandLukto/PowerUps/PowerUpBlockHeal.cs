﻿using GrandLukto.Blocks.BlockModifiers;
using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public class PowerUpBlockHeal : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var blockGroup = GameManager.Instance.BlockGroupByTeam[character.Team];
			var heal = new Heal(blockGroup);
			heal.HealLine();
		}
	}
}