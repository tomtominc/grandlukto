﻿using GrandLukto.Characters;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class PowerUpIceShot : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var characterShoot = character.GetComponent<CharacterShoot>();
			IceProjectile.Spawn(character, characterShoot.GetHitDirection(Vector2.zero, 0));
		}
	}
}