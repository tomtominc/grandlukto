﻿namespace GrandLukto.PowerUps
{
	public enum PowerUpType
	{
		None,
		Fireball,
		AirBlast,
		BombKick,
		SpeedUp,
		SlowMotion,
		BlockHeal,
		Magnet,
		Fortify,
		IceShot,
		Root,
		Shield,
		SpawnRock
	}

	public static class PowerUpTypeExtensions
	{
		public static string GetIconName(this PowerUpType curPowerUp)
		{
			switch (curPowerUp)
			{
				case PowerUpType.None: return "none";
				case PowerUpType.AirBlast: return "wind";
				case PowerUpType.SpeedUp: return "speedboost";
				case PowerUpType.BombKick: return "bomb";
			}

			return curPowerUp.ToString().ToLower();
		}
	}
}