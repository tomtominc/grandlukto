﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class UIPowerUpSlotPrimary : UIPowerUpSlotBase
	{
		public float tweenDuration;

		public Vector3 tweenFromScale;
		private Vector3 tweenToScale;

		public Color tweenFromColor;
		private Color tweenToColor;

		public tk2dSprite iconSprite;

		private Tween curTween;

		[Header("Shake")]
		public float shakeStartOffset = 0.2f;
		public float shakeDuration = 0.2f;
		public float shakeStrength = 1;
		public int shakeVibrato = 10;
		public float shakeRandomness = 90;

		[Header("Tween Out")]
		public float tweenOutDuration;
		public Vector3 tweenOutScale;
		public Color tweenOutColor;

		private Tween tweenOnHitActivated;

		private bool onHitActivated = false;

		public void Awake()
		{
			tweenToScale = iconSprite.transform.localScale;
			tweenToColor = iconSprite.color;

			tweenOnHitActivated = iconSprite
				.DOColor(new Color(0.66f, 0.66f, 0.66f, 1), 0.2f)
				.SetEase(Ease.InOutQuad)
				.SetLoops(-1, LoopType.Yoyo)
				.Pause();

			iconSprite.gameObject.SetActive(false);
		}
		
		public override void ShowPowerUp(PowerUpType type, bool isOnHitActivated = false)
		{
			onHitActivated = isOnHitActivated;
			tweenOnHitActivated.Pause();

			if (curTween != null && curTween.IsActive()) curTween.Kill();

			if (type == PowerUpType.None)
			{
				Hide();
				return;
			}

			iconSprite.SetSprite(type.GetIconName() + "_icon1");
			Show();
		}

		public override void ShowOnHitPowerUpActivatedAnimation()
		{
			base.ShowOnHitPowerUpActivatedAnimation();

			if (curTween.IsActive()) return; // will start after tween finished

			StartOnHitActivatedTween();
		}

		private void StartOnHitActivatedTween()
		{
			if (tweenOnHitActivated.IsPlaying()) return; // already playing
			tweenOnHitActivated.Restart();
		}

		private void Hide()
		{
			curTween = DOTween.Sequence()
				.Append(iconSprite.transform
					.DOScale(tweenOutScale, tweenOutDuration)
					.SetEase(Ease.OutExpo))
				.Join(iconSprite
					.DOColor(tweenOutColor, tweenOutDuration)
					.SetEase(Ease.OutExpo))
				.AppendCallback(() =>
				{
					iconSprite.gameObject.SetActive(false);
				});
		}

		private void Show()
		{
			iconSprite.gameObject.SetActive(true);

			iconSprite.transform.localScale = tweenFromScale;
			iconSprite.color = tweenFromColor;

			curTween = DOTween.Sequence()
				.Append(iconSprite.transform
					.DOScale(tweenToScale, tweenDuration)
					.SetEase(Ease.OutExpo))
				.Join(iconSprite
					.DOColor(tweenToColor, tweenDuration)
					.SetEase(Ease.OutSine))
				.Insert(shakeStartOffset, transform
					.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato, shakeRandomness))
				.AppendCallback(() =>
				{
					if (onHitActivated)
					{
						StartOnHitActivatedTween(); // on hit is already activated -> animate
					}
				});
		}
	}
}
