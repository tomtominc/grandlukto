﻿using GrandLukto.Characters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto.PowerUps
{
	public class EventPowerUpManagerQueueChanged : UnityEvent<PowerUpManager> { }

	public class EventPowerUpManagerSlotChanged : UnityEvent<int, PowerUpType, bool> { }

	public class PowerUpManager : MonoBehaviour
	{
		public EventPowerUpManagerQueueChanged OnQueueChanged { get; private set; }
		public EventPowerUpManagerSlotChanged OnSlotChanged { get; private set; }
		public UnityEvent OnActivateOnHitPowerUp { get; private set; }

		public Queue<PowerUpType> PowerUpQueue { get; private set; }
		public bool IsQueueFull { get { return PowerUpQueue.Count >= maxPowerUpsInQueue; } }
		public int maxPowerUpsInQueue = 2;

		public bool OnHitPowerUpActivated { get; private set; }

		/// <summary>
		/// Game time at which the last power up has been used or null if no power up has been used yet.
		/// </summary>
		public float? LastPowerUpUsed { get; private set; }

		/// <summary>
		/// Setting this to false will hide the powerup UI for this character.
		/// </summary>
		public bool showUI = true;

		private CharacterBase compCharacterBase;

		public PowerUpType NextPowerUp
		{
			get
			{
				if (PowerUpQueue.Count <= 0) return PowerUpType.None;
				return PowerUpQueue.Peek();
			}
		}

		public void Awake()
		{
			compCharacterBase = GetComponent<CharacterBase>();
		}

		public PowerUpManager()
		{
			PowerUpQueue = new Queue<PowerUpType>();
			OnQueueChanged = new EventPowerUpManagerQueueChanged();
			OnSlotChanged = new EventPowerUpManagerSlotChanged();
			OnActivateOnHitPowerUp = new UnityEvent();
		}

		public void Start()
		{
			if (showUI) LinkUI();
		}

		private void LinkUI()
		{
			UIPowerUpManager bestMatchAnyTeam = null;

			foreach (var manager in GameManager.Instance.PowerUpManagers)
			{
				if (manager.IsTaken) continue;

				if (manager.preferredTeam == compCharacterBase.Team)
				{
					manager.AssignTo(compCharacterBase, this);
					return;
				}

				if (bestMatchAnyTeam == null) bestMatchAnyTeam = manager;
			}

			if(bestMatchAnyTeam == null)
			{
				Debug.LogError("No PowerUPManager found!");
				return;
			}

			bestMatchAnyTeam.AssignTo(compCharacterBase, this);
		}

		public void OnEnable()
		{
			compCharacterBase.RecievePowerUp += CompCharacterBase_RecievePowerUp;
		}

		public void OnDisable()
		{
			compCharacterBase.RecievePowerUp -= CompCharacterBase_RecievePowerUp;
		}

		private void CompCharacterBase_RecievePowerUp(object sender, RecievePowerUpEventArgs e)
		{
			if (e.IsCollected) return; // already collected
			if (!TryAddPowerUpToQueue(e.PowerUpType)) return; // can not collect any more

			e.Collect();
		}
		
		public bool TryAddPowerUpToQueue(PowerUpType type)
		{
			if (IsQueueFull) return false;

			PowerUpQueue.Enqueue(type);
			OnSlotChanged.Invoke(PowerUpQueue.Count - 1, type, PowerUpQueue.Count == 1 && OnHitPowerUpActivated);
			ShowPowerUpCollectAnim(type);
			return true;
		}

		public bool TryUsePowerUp(out PowerUpType usedType)
		{
			usedType = default(PowerUpType);
			if (PowerUpQueue.Count <= 0) return false; // no powerups available
			if (OnHitPowerUpActivated) return false; // on hit powerup already queued

			if(NextPowerUp.IsOnHitPowerUp())
			{
				OnHitPowerUpActivated = true;
				OnActivateOnHitPowerUp.Invoke();
				return true;
			}

			usedType = PowerUpQueue.Dequeue();
			LastPowerUpUsed = Time.time;
			OnQueueChanged.Invoke(this);
			return true;
		}

		public bool TryUseOnHitPowerUp(out PowerUpType usedType)
		{
			usedType = default(PowerUpType);
			if (!OnHitPowerUpActivated) return false;

			// use onhit powerup
			usedType = PowerUpQueue.Dequeue();
			LastPowerUpUsed = Time.time;
			OnHitPowerUpActivated = false;

			OnQueueChanged.Invoke(this);
			return true;
		}

		// todo: maybe move this to somewhere else...
		private void ShowPowerUpCollectAnim(PowerUpType type)
		{
			var gameObj = Instantiate(Resources.Load<GameObject>("UI/CollectPowerUpAnimation"), transform, false);
			var collectAnim = gameObj.GetComponent<CollectPowerUpAnimation>();
			collectAnim.Initialize(type);
		}
	}
}
