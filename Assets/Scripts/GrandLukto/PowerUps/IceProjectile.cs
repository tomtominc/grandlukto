﻿using GrandLukto.Characters;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class IceProjectile : MonoBehaviour
	{
		public float spawnOffset = 1f;
		public float speed;

		private Rigidbody2D compRigidBody;
		private bool didCollide = false;

		private Vector2 shootDirection;

		public void Awake()
		{
			compRigidBody = GetComponent<Rigidbody2D>();
		}

		public void Start()
		{
			compRigidBody.velocity = shootDirection * speed;
		}
		
		public void OnCollisionEnter2D(Collision2D collision)
		{
			if (didCollide) return;

			switch (collision.gameObject.tag)
			{
				case Tag.Block:
					break;

				case Tag.Player:
					didCollide = true;
					PlayerIced.IcePlayer(collision.gameObject);
					break;

				default:
					return;
			}

			// only called for valid types
			didCollide = true;
			Destroy(gameObject);
		}

		public void Init(CharacterBase spawner, Vector2 shootDirection)
		{
			transform.position = spawner.transform.position + spawner.FaceDirectionVec * spawnOffset;
			this.shootDirection = shootDirection;
		}

		public static void Spawn(CharacterBase spawner, Vector2 shootDirection)
		{
			var gameObj = Instantiate(Resources.Load<GameObject>("Ice Projectile"));
			var iceProjectile = gameObj.GetComponent<IceProjectile>();
			iceProjectile.Init(spawner, shootDirection);
		}
	}
}