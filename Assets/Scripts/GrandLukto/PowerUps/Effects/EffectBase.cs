﻿using GrandLukto.Characters;
using System;
using System.Collections;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	public abstract class EffectBase
	{
		/// <summary>
		/// Raised when the effect is completed and removed.
		/// </summary>
		public event EventHandler<EventArgs> Finished;

		protected EffectsManager EffectsManager { get; private set; }
		protected GameObject Target { get; private set; }

		private bool isInterrupted = false;

		public void Activate(EffectsManager effectsManager, GameObject target)
		{
			EffectsManager = effectsManager;
			Target = target;

			Activate(target);
		}

		/// <summary>
		/// Called when the effect gets activated.
		/// </summary>
		/// <param name="target">Target to which the effect should be applied.</param>
		public abstract void Activate(GameObject target);

		/// <summary>
		/// Returns true if the effect can be interrupted in its current state, otherwise false.
		/// </summary>
		protected abstract bool CanInterrupt();

		/// <summary>
		/// Returns true if the effect can be interrupted at the moment of evaluation.
		/// </summary>
		public bool CanBeInterrupted()
		{
			if (isInterrupted) return false; // can only be interrupted once
			if (!CanInterrupt()) return false;

			return true;
		}

		/// <summary>
		/// Tries to interrupt the effect. Returns a boolean value indicating if the effect was interrupted.
		/// </summary>
		public bool TryInterrupt()
		{
			if (!CanBeInterrupted()) return false;

			isInterrupted = true;
			Interrupt();
			return true;
		}

		/// <summary>
		/// Called if the effect should be interrupted, but only if the CanInterrupt method returned false.
		/// Can only be called once.
		/// </summary>
		protected abstract void Interrupt();

		/// <summary>
		/// This method should be called by derived classes when the effect is complete.
		/// </summary>
		protected void Finish()
		{
			OnFinished();
		}

		protected virtual void OnFinished()
		{
			if (Finished != null) Finished.Invoke(this, EventArgs.Empty);
		}

		protected Coroutine StartCoroutine(IEnumerator routine)
		{
			return EffectsManager.StartCoroutine(routine);
		}

		protected void StopCoroutine(Coroutine coroutine)
		{
			EffectsManager.StopCoroutine(coroutine);
		}
	}
}