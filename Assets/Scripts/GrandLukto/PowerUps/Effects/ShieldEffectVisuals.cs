﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	public class ShieldEffectVisuals : AppearDisappearEffectVisuals
	{
		public GameObject visuals;
		private tk2dSprite compSprite;

		private void Awake()
		{
			compSprite = visuals.GetComponent<tk2dSprite>();
			compSprite.color = Color.white.MakeTransparent();
		}
		
		public override Tween CreateAppearTween(float duration)
		{
			return compSprite.DOColor(Color.white, duration);
		}

		public override Tween CreateDisappearTween(float duration)
		{
			return compSprite.DOColor(Color.white.MakeTransparent(), duration);
		}

		public static ShieldEffectVisuals Create(GameObject attachTo)
		{
			var visuals = Instantiate(Resources.Load<GameObject>("PowerUpEffects/Shield Effect"), attachTo.transform, false);
			return visuals.GetComponent<ShieldEffectVisuals>();
		}
	}
}