﻿using DG.Tweening;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	public abstract class AppearDisappearEffectVisuals : MonoBehaviour
	{
		private Tween curTween;
		
		public void Appear(float duration)
		{
			curTween.Kill();
			curTween = CreateAppearTween(duration);
		}

		public abstract Tween CreateAppearTween(float duration);

		public void Disappear(float duration)
		{
			curTween.Kill();
			curTween = CreateDisappearTween(duration);
		}

		public abstract Tween CreateDisappearTween(float duration);
	}
}