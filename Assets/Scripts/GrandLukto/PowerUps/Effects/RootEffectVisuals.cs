﻿using DG.Tweening;
using UnityCave.ExtensionMethods;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	/// <summary>
	/// Visuals that grow and root the player to the ground.
	/// </summary>
	public class RootEffectVisuals : AppearDisappearEffectVisuals
	{
		public tk2dSprite visuals;

		public Vector3 scaleMin;
		public Vector3 scaleMax;

		private void Awake()
		{
			visuals.scale = scaleMin;
		}
		
		public override Tween CreateAppearTween(float duration)
		{
			return visuals.DOScale(scaleMax, duration);
		}

		public override Tween CreateDisappearTween(float duration)
		{
			return DOTween.Sequence()
				.Append(visuals.DOScale(scaleMin, duration))
				.Join(visuals.DOColor(Color.white.MakeTransparent(), duration));
		}

		public static RootEffectVisuals Create(GameObject attachTo)
		{
			var visuals = Instantiate(Resources.Load<GameObject>("PowerUpEffects/Root Effect"), attachTo.transform, false);
			return visuals.GetComponent<RootEffectVisuals>();
		}
	}
}