﻿using GrandLukto.Characters;
using GrandLukto.PowerUps.Effects.Config;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	public class ShieldEffect : AppearDisappearEffect
	{
		public override void Activate(GameObject target)
		{
			base.Activate(target);

			EffectsManager.TryInterruptAllEffects();
			EffectsManager.AddingEffect += EffectsManager_AddingEffect;
		}

		private void EffectsManager_AddingEffect(object sender, AddingEffectEventArgs e)
		{
			if (e.Cancel) return; // already cancelled

			if (e.Effect is IBlockableEffect)
			{
				e.Cancel = true;
			}
		}
		
		protected override bool CanInterrupt()
		{
			return false; 
		}

		protected override void OnFinished()
		{
			base.OnFinished();

			EffectsManager.AddingEffect -= EffectsManager_AddingEffect;
		}

		protected override EffectDurationConfig LoadDurationConfig()
		{
			return GameManager.Instance.gameConfiguration.shieldEffectDuration;
		}

		protected override AppearDisappearEffectVisuals CreateVisuals(GameObject target)
		{
			return ShieldEffectVisuals.Create(target);
		}
	}
}