﻿using GrandLukto.Characters.StateMachine;
using GrandLukto.PowerUps.Effects.Config;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	public class RootEffect : AppearDisappearEffect, IBlockableEffect
	{
		private bool canInterrupt = true;
		
		protected override void OnEffectAppeared()
		{
			base.OnEffectAppeared();
			RootPlayer(Config.effectDuration);
		}

		private void RootPlayer(float rootDuration)
		{
			var stateMachine = Target.GetComponent<CharacterStateMachine>();
			if (stateMachine == null) return;

			// TODO: Keep longest out of control duration and multiply strengths?
			if (stateMachine.IsOutOfControl) return;

			Target.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			stateMachine.ChangeStateOutOfControl(rootDuration);
		}

		protected override void Interrupt()
		{
			base.Interrupt();
			// TODO: Remove OutOfControl
		}

		protected override bool CanInterrupt()
		{
			return canInterrupt;
		}
		
		protected override EffectDurationConfig LoadDurationConfig()
		{
			return GameManager.Instance.gameConfiguration.rootEffectDuration;
		}

		protected override AppearDisappearEffectVisuals CreateVisuals(GameObject target)
		{
			return RootEffectVisuals.Create(target);
		}
	}
}