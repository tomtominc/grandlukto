﻿namespace GrandLukto.PowerUps.Effects.Config
{
	/// <summary>
	/// Marks an effect that can be blocked before it is activated.
	/// </summary>
	public interface IBlockableEffect
	{
	}
}