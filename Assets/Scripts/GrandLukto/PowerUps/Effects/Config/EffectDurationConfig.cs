﻿using System;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects.Config
{
	[Serializable]
	public class EffectDurationConfig
	{
		[Tooltip("Duration of the appear animation in seconds.")]
		public float appearDuration = 0.5f;

		[Tooltip("Duration of the actual effect in seconds.")]
		public float effectDuration = 1f;

		[Tooltip("Duration of the disappear animation in seconds.")]
		public float disappearDuration = 0.2f;
	}
}