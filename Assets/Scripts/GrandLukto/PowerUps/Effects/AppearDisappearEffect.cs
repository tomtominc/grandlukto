﻿using GrandLukto.PowerUps.Effects.Config;
using System.Collections;
using UnityEngine;

namespace GrandLukto.PowerUps.Effects
{
	/// <summary>
	/// Base class for effects that require visuals to appear and disappear.
	/// </summary>
	public abstract class AppearDisappearEffect : EffectBase
	{
		protected EffectDurationConfig Config { get; private set; }
		private AppearDisappearEffectVisuals visuals;
		private Coroutine activeRoutine;

		private bool isDisappearing = false;

		public override void Activate(GameObject target)
		{
			Config = LoadDurationConfig();
			visuals = CreateVisuals(target);

			activeRoutine = StartCoroutine(Routine());
		}

		private IEnumerator Routine()
		{
			visuals.Appear(Config.appearDuration);
			yield return new WaitForSeconds(Config.appearDuration);
			OnEffectAppeared();

			yield return new WaitForSeconds(Config.effectDuration);

			StartDisappear();
		}

		/// <summary>
		/// Called after the effect appeared.
		/// </summary>
		protected virtual void OnEffectAppeared() { }

		private void StartDisappear()
		{
			if (isDisappearing) return;
			isDisappearing = true;

			StartCoroutine(DisappearRoutine());
		}

		private IEnumerator DisappearRoutine()
		{
			OnDisappear();
			visuals.Disappear(Config.disappearDuration);
			yield return new WaitForSeconds(Config.disappearDuration);

			Finish();
		}

		/// <summary>
		/// Called just before the effect starts to disappear.
		/// This method is also called when the effect gets interrupted.
		/// </summary>
		private void OnDisappear() { }

		protected override void Interrupt()
		{
			if (activeRoutine != null)
			{
				StopCoroutine(activeRoutine);
				activeRoutine = null;
			}

			StartDisappear();
		}
		
		protected override void OnFinished()
		{
			base.OnFinished();

			Object.Destroy(visuals.gameObject);
		}

		protected abstract EffectDurationConfig LoadDurationConfig();

		/// <summary>
		/// Override this method to create an <see cref="AppearDisappearEffectVisuals"/> object.
		/// When the effect is finished the GameObject containing the visuals gets destroyed.
		/// </summary>
		/// <param name="target">The target on which the effect gets applied.</param>
		protected abstract AppearDisappearEffectVisuals CreateVisuals(GameObject target);
	}
}