﻿using GrandLukto.Characters;
using GrandLukto.PowerUps.Effects;

namespace GrandLukto.PowerUps
{
	public class PowerUpShield : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			character.GetComponent<EffectsManager>().AddEffect(new ShieldEffect());
		}
	}
}