﻿using GrandLukto.Blocks.BlockModifiers;
using GrandLukto.Characters;

namespace GrandLukto.PowerUps
{
	public class PowerUpFortify : IPowerUp
	{
		public void UsePowerUp(CharacterBase character)
		{
			var blockGroup = GameManager.Instance.BlockGroupByTeam[character.Team];
			var fortify = new Fortify(blockGroup);
			fortify.FortifyAll(GameManager.Instance.gameConfiguration.powerUpFortifyDuration);
		}
	}
}