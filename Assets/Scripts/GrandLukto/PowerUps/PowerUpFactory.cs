﻿using GrandLukto.Balls;
using System;

namespace GrandLukto.PowerUps
{
	public class PowerUpFactory
	{
		public IPowerUp Create(PowerUpType powerUpType)
		{
			switch(powerUpType)
			{
				case PowerUpType.Fireball: return new PowerUpFireBall();
				case PowerUpType.BlockHeal: return new PowerUpBlockHeal();
				case PowerUpType.Fortify: return new PowerUpFortify();
				case PowerUpType.IceShot: return new PowerUpIceShot();
				case PowerUpType.Magnet: return new PowerUpMagnet();
				case PowerUpType.SlowMotion: return new PowerUpSlowMotion();
				case PowerUpType.SpeedUp: return new PowerUpSpeedUp();
				case PowerUpType.Root: return new PowerUpRoot();
				case PowerUpType.Shield: return new PowerUpShield();
				case PowerUpType.SpawnRock: return new PowerUpSpawnBall(factory => factory.CreateBall(BallType.Rock));
				default: throw new NotImplementedException();
			}
		}
	}
}