﻿using System.Collections.Generic;
using UnityCave.StateMachine;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class FireballScreenColorOverlay : MonoBehaviour
	{
		private enum State
		{
			Inactive,
			FadeIn,
			Active,
			FadeOut
		}

		private List<GameObject> activeOwners = new List<GameObject>();

		private Renderer compRenderer;
		private tk2dSprite compSprite;

		private float curAlpha = 0;

		private SimpleStateMachine<State> stateMachine = new SimpleStateMachine<State>(State.Inactive);

		public float fadeSpeed = 3;

		public void Awake()
		{
			curAlpha = 0;

			compRenderer = GetComponent<Renderer>();
			compSprite = GetComponent<tk2dSprite>();

			stateMachine.OnEnterState = OnEnterState;
		}

		public void Start()
		{
			compRenderer.enabled = false;
		}

		public void StartRain(GameObject owner)
		{
			if (activeOwners.Contains(owner)) return;

			activeOwners.Add(owner);

			if (stateMachine.CurState == State.Inactive || stateMachine.CurState == State.FadeOut)
			{
				stateMachine.ChangeState(State.FadeIn);
			}
		}

		public void StopRain(GameObject owner)
		{
			if (!activeOwners.Contains(owner)) return;

			activeOwners.Remove(owner);

			if(activeOwners.Count == 0)
			{
				stateMachine.ChangeState(State.FadeOut);
			}
		}
		
		public void Update()
		{
			switch (stateMachine.CurState)
			{
				case State.FadeIn:
					curAlpha += fadeSpeed * Time.deltaTime;

					if(curAlpha >= 1)
					{
						curAlpha = 1;
						stateMachine.ChangeState(State.Active);
					}

					break;

				case State.FadeOut:
					curAlpha -= fadeSpeed * Time.deltaTime;

					if (curAlpha <= 0)
					{
						curAlpha = 0;
						stateMachine.ChangeState(State.Inactive);
					}

					break;

				default:
					return; // do not update color
			}

			UpdateSpriteColor();
		}

		private void OnEnterState(State newState)
		{
			switch(newState)
			{
				case State.FadeIn:
					compRenderer.enabled = true;
					break;

				case State.Inactive:
					compRenderer.enabled = false;
					curAlpha = 0;
					break;
			}
		}

		private void UpdateSpriteColor()
		{
			compSprite.color = new Color(1, 1, 1, curAlpha);
		}
	}
}
