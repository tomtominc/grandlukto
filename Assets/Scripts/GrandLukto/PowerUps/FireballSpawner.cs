﻿using GrandLukto.Blocks;
using GrandLukto.Characters;
using System.Collections;
using UnityCave.UI;
using UnityEngine;

namespace GrandLukto.PowerUps
{
	public class FireballSpawner : MonoBehaviour
	{
		public int spawnAmount = 3;

		public float spawnOffsetMin = 0.1f;
		public float spawnOffsetMax = 0.4f;

		public bool destroyAfterSpawn = false;
		public bool endlessSpawn = false;

		private CharacterBase compCharacterBase;
		private BlockGroup enemyBlockGroup;

		public void Awake()
		{
			compCharacterBase = GetComponent<CharacterBase>();

			var opponentTeam = compCharacterBase.Team.GetOpponent();
			enemyBlockGroup = GameManager.Instance.BlockGroupByTeam[opponentTeam];
		}

		[ContextMenu("Restart")]
		public void Start()
		{
			StartCoroutine(SpawnRoutine());
			UIManager.Instance.PlaySound(GameManager.Instance.gameConfiguration.soundFireballSpawn);
		}

		private IEnumerator SpawnRoutine()
		{
			GameManager.Instance.FireballScreenOverlay.StartRain(gameObject);

			for (var i = 0; i < spawnAmount; i++)
			{
				if (endlessSpawn) i = 0;

				SpawnFireball();

				if (i < spawnAmount - 1)
				{
					yield return new WaitForSeconds(Random.Range(spawnOffsetMin, spawnOffsetMax));
				}
			}

			yield return new WaitForSeconds(Random.Range(1, 2)); // wait until we disable the orange sky

			GameManager.Instance.FireballScreenOverlay.StopRain(gameObject);

			if (destroyAfterSpawn) Destroy(this); // remove from gameobject
		}

		private void SpawnFireball()
		{
			var spawnPosition = enemyBlockGroup.GetRandomPosition(0.5f);
			Instantiate(Resources.Load<GameObject>("Fireball"), spawnPosition, Quaternion.identity);
		}
	}
}