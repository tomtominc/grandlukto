﻿using UnityEngine;

namespace GrandLukto.Database
{
	public class DataLevelTileEditor : MonoBehaviour
	{
		public DataBlockLayoutTileMapConnector BlockLayoutTop { get; private set; }
		public DataBlockLayoutTileMapConnector BlockLayoutBottom { get; private set; }

		public void Awake()
		{
			Debug.LogError("DataLevelTileEditor instance found! Delete it after finishing editing a level!");
			Destroy(this);
		}

		public void Initialize(DataLevel level)
		{
			BlockLayoutTop = CreateBlockLayoutEditor(level.blockLayoutTop, Team.Top);
			BlockLayoutBottom = CreateBlockLayoutEditor(level.blockLayoutBottom, Team.Bottom);
		}

		private DataBlockLayoutTileMapConnector CreateBlockLayoutEditor(DataBlockLayout blockLayout, Team team)
		{
			var gameObj = new GameObject("Block Layout " + team.ToString());
			var tileMapConnector = gameObj.AddComponent<DataBlockLayoutTileMapConnector>();

			gameObj.transform.SetParent(transform);
			gameObj.transform.localPosition = new Vector2(0, team == Team.Top ? 2.5f : -2.5f);

			tileMapConnector.dataObject = blockLayout;
			tileMapConnector.ShowMirrored = team == Team.Bottom;

			return tileMapConnector;
		}
	}
}