﻿using UnityEngine;

namespace GrandLukto.Database
{
	public class DataLevel : ScriptableObject
	{
		public string levelName;

		public DataBlockLayout blockLayoutTop;
		public DataBlockLayout blockLayoutBottom;

		public GameObject scriptPrefab;
	}
}