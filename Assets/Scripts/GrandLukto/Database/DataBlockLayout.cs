﻿using GrandLukto.Blocks;
using tk2dRuntime.TileMap;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DataBlockLayout : ScriptableObject
	{
		[HideInInspector]
		public Layer[] layers;

		public string customName;

		public int width;
		public int height;

		public BallShooterConfig ballShooterConfig;

		[Tooltip("PowerUp spawner routines ordered by index from top left to bottom right.")]
		public PowerUpSpawnerRoutineBase[] powerUpSpawnerRoutines;

		public DataBlockLayout()
		{
		}
	}
}