﻿using GrandLukto.PowerUps.Effects.Config;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/GameConfiguration")]
	public class GameConfiguration : ScriptableObject
	{
		[Tooltip("Relative game speed for debugging purposes.")]
		public float gameSpeed;

		public FloatRange ballSpawnDelay;

		[Header("Power Ups: General")]
		public float ballSpawnOffset = 1f;

		[Header("Power Up: Slow Motion")]
		public float powerUpSlowMotionTime = 1f;
		public float powerUpSlowMotionTimeScale = 0.5f;

		public float powerUpSlowMotionExplosionTargetRadius = 1f;
		public float powerUpSlowMotionExplosionAmplitude = 0.01f;
		public float powerUpSlowMotionExplosionTime = 2f;

		[Header("Power Up: Block Heal")]
		public GameObject powerUpBlockHealBlock;

		[Header("Power Up: Magnet")]
		public float powerUpMagnetActiveTime = 2;
		public float powerUpMagnetRange = 4;
		public float powerUpMagnetStrength = 1;

		[Header("Power Up: Fortify")]
		public float powerUpFortifySpawnDelay = 0.05f;
		public float powerUpFortifyDuration = 2f;

		[Header("Power Up: Ice Shot")]
		public float powerUpIceShotIceDuration = 3f;

		[Header("PowerUp Effects")]
		public EffectDurationConfig rootEffectDuration;
		public EffectDurationConfig shieldEffectDuration;

		[Header("Sounds (Ball)")]
		public DataSoundSetting soundOnBallHitBlock;
		public DataSoundSetting soundOnBallHitWall;
		public DataSoundSetting soundOnBallSpawnAnim;

		[Header("Sounds (Player)")]
		public DataSoundSetting soundSwing;
		public DataSoundSetting soundShot;
		public DataSoundSetting soundUsePowerUp;

		[Header("Sounds (PowerUps)")]
		public DataSoundSetting soundFireballSpawn;
		public DataSoundSetting soundBombExplode;

		public DataSoundSetting soundGameIn;

		[Header("Fade Colors")]
		public Color fadeColorExplode;

		[Header("Block Explode")]
		public float blockExplodeDestroyDelay = 0.2f;

		[Header("Boss: Ice King Shooter")]
		public TextAsset spawnPatternConfig2;
		public TextAsset spawnPatternConfig3;
		public TextAsset spawnPatternConfig4;
	}
}