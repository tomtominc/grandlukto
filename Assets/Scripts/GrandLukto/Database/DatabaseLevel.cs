﻿using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_Level")]
	public class DatabaseLevel : DatabaseBase<DataLevel>
	{

	}
}