﻿using GrandLukto.Characters;
using GrandLukto.UI.Dialog;
using Spine.Unity;
using System;
using UnityCave.CustomDataTypes;
using UnityCave.Spine;
using UnityCave.tk2d;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DataCharacter : ScriptableObject
	{
		public string characterName;
		public string displayName;

		[Header("Description")]
		[TextArea(3, 10)]
		public string descriptionRoster;

		[Header("Animation (down angle)")]
		public RuntimeAnimatorController animationDownController;
		public SkeletonDataAsset animationDownSkeletonData;

		[Header("Animation (up angle)")]
		public RuntimeAnimatorController animationUpController;
		public SkeletonDataAsset animationUpSkeletonData;

		[Header("Swing Animation Override")]
		public SkeletonDataAsset animationSwingSkeletonData;

		public string animationSwingNameUp;
		public string animationSwingNameDown;

		public Vector2 shadowSize;
		public NullableFloat colliderSize;

		public bool showPlayerIcon = true;

		public bool canPlayerSelectCharacter = false;

		[Header("Character Selection")]
		public string characterSelectionSpriteName;
		public Vector2 characterSelectionOffset;
		public Vector2 characterSelectionShadowOffset;

		[Header("Dialog Avatars")]
		public DialogAvatarData avatarLeft;
		public DialogAvatarData avatarRight;

		public void ApplyAnimationToGameObject(SkeletonAnimation target, FaceDirection faceDirection, bool doOverride = true)
		{
			var newSkeletonData = GetSkeletonData(faceDirection);
			if (!doOverride && target.SkeletonDataAsset == newSkeletonData)
			{
				return; // do not override
			}

			target.skeletonDataAsset = newSkeletonData;
			target.ReloadSkeletonData();
		}

		public void ApplyAnimationToGameObject(GameObject target, FaceDirection faceDirection)
		{
			var skeletonAnimator = target.GetComponentInChildren<SkeletonAnimator>();
			skeletonAnimator.skeletonDataAsset = GetSkeletonData(faceDirection);
			skeletonAnimator.ReloadSkeletonData();

			var animator = target.GetComponentInChildren<Animator>();
			animator.runtimeAnimatorController = faceDirection == FaceDirection.Up ? animationUpController : animationDownController;
			animator.Rebind();
		}

		private SkeletonDataAsset GetSkeletonData(FaceDirection faceDir)
		{
			return faceDir == FaceDirection.Up ? animationUpSkeletonData : animationDownSkeletonData; ;
		}

		public DialogAvatarData GetDialogAvatar(DialogAvatarPosition avatarPosition)
		{
			switch (avatarPosition)
			{
				case DialogAvatarPosition.Left: return avatarLeft;
				case DialogAvatarPosition.Right: return avatarRight;
				default: throw new NotImplementedException();
			}
		}
	}
}