﻿using System.Collections.Generic;
using System.Linq;
using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_Character")]
	public class DatabaseCharacter : DatabaseBase<DataCharacter>
	{
		/// <summary>
		/// Returns all characters that the player can play in the game.
		/// </summary>
		public IEnumerable<DataCharacter> GetPlayable()
		{
			return Data.Where(character => character.canPlayerSelectCharacter);
		}
	}
}
