﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Database
{
	public enum DataBlockLayoutGroupBackgroundColor
	{
		Blue,
		Green,
		Red,
		Yellow
	}

	public class DataBlockLayoutGroup : ScriptableObject
	{
		public string blockLayoutGroupName;
		public DataBlockLayoutGroupBackgroundColor backgroundColor;

		[HideInInspector]
		public List<DataLevel> assignedLevels;
	}
}
