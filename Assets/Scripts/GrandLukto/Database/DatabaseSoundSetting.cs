﻿using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_SoundSetting")]
	public class DatabaseSoundSetting : DatabaseBase<DataSoundSetting>
	{
	}
}