﻿using UnityCave.tk2d;
using UnityEngine;

namespace GrandLukto.Database
{
	[RequireComponent(typeof(tk2dTileMap))]
	public class DataBlockLayoutTileMapConnector : MonoBehaviour
	{
		public DataBlockLayout dataObject;
		public bool ShowMirrored { get; set; }

		public void Load()
		{
			var compTileMap = GetComponent<tk2dTileMap>();

#if UNITY_EDITOR
			compTileMap.BeginEditMode();
#endif

			compTileMap.width = Mathf.Max(dataObject.width, 1);
			compTileMap.height = Mathf.Max(dataObject.height, 1);

			tk2dTileMapUtil.CleanLayers(compTileMap);
			compTileMap.Layers = tk2dTileMapUtil.CopyLayers(dataObject.layers, compTileMap.width, compTileMap.height, compTileMap.partitionSizeX, compTileMap.partitionSizeY);

			if (ShowMirrored && compTileMap.Layers != null)
			{
				tk2dTileMapUtil.MirrorTilesXYAxis(compTileMap);
			}

			compTileMap.EndEditMode();

#if UNITY_EDITOR
			compTileMap.BeginEditMode();
#endif
		}

		public void Save()
		{
			var compTileMap = GetComponent<tk2dTileMap>();

			if (ShowMirrored)
			{
				// mirror back before saving
				tk2dTileMapUtil.MirrorTilesXYAxis(compTileMap);
			}

			// commit the tilemap
			if (compTileMap.AllowEdit)
			{
				compTileMap.EndEditMode();
			}

			dataObject.width = compTileMap.width;
			dataObject.height = compTileMap.height;
			dataObject.layers = tk2dTileMapUtil.CopyLayers(compTileMap.Layers, compTileMap.width, compTileMap.height, compTileMap.partitionSizeX, compTileMap.partitionSizeY);

			if (ShowMirrored)
			{
				// mirror after saving to still show it mirrored
				tk2dTileMapUtil.MirrorTilesXYAxis(compTileMap);
			}

			// reload
#if UNITY_EDITOR
			compTileMap.BeginEditMode();
#endif
		}

		public void Mirror()
		{
			var compTileMap = GetComponent<tk2dTileMap>();
			tk2dTileMapUtil.MirrorTilesXYAxis(compTileMap);
		}
	}
}