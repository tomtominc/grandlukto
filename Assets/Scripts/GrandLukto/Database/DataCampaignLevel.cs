﻿using GrandLukto.Characters;
using GrandLukto.GameModes;
using GrandLukto.UI.Dialog;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DataCampaignLevel : ScriptableObject
	{
		public string levelName;

		public DataLevel levelObj;

		[TextArea(2, 2)]
		public string nodeDescription;

		public DataArena arena;

		public DataCampaignLevelEnemy[] enemies;

		public GameMode gameMode;

		public bool spawnInitialBall = true;

		public DialogDataWrapper dialogBeforeGameStart;

		public List<PlayerSetting> GetPlayerSettings(PlayerSetting player)
		{
			var playerSettings = new List<PlayerSetting> { player };

			// propagate playerSettings with enemies
			for(var i = 0; i < enemies.Length; i++)
			{
				var curEnemy = enemies[i];

				playerSettings.Add(new PlayerSetting
				{
					isHuman = false,
					playerId = playerSettings.Count,
					team = Team.Top,
					selectedCharacter = curEnemy.character,
					aiDefinition = curEnemy.aiDefinition,
					ai = curEnemy.ai
				});
			}

			return playerSettings;
		}
	}

	[Serializable]
	public class DataCampaignLevelEnemy
	{
		public DataCharacter character;
		public DataAIDefinition aiDefinition;

		[Tooltip("GameObject that holdes an AI Rig.")]
		public GameObject ai;
	}
}