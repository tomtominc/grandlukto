﻿using UnityEngine;

namespace GrandLukto.Database
{
	public class DataArena : ScriptableObject
	{
		public string arenaName;

		public GameObject arenaPrefab;
		public Vector2 gameplayOffset;

		public bool isAvailableInLocalMultiplayer = false;

		public AudioClip soundEnvironment;
	}
}
