﻿using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_CampaignLevel")]
	public class DatabaseCampaignLevel : DatabaseBase<DataCampaignLevel>
	{
	}
}