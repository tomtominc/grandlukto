﻿using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_BlockLayoutGroup")]
	public class DatabaseBlockLayoutGroup : DatabaseBase<DataBlockLayoutGroup>
	{
	}
}
