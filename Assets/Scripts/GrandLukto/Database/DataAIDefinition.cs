﻿using GrandLukto.AIData;
using GrandLukto.Characters;
using GrandLukto.PowerUps;
using System.Collections.Generic;
using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/AI Definition")]
	public class DataAIDefinition : ScriptableObject
	{
		public FloatRange randReactionTime = new FloatRange(0.25f, 0.4f);

		public float moveSpeedMax = 4;
		public List<CollisionHandlerDefinition> collisionHandlers;

		public bool showAbilityUI = true;
		
		public void ApplyTo(GameObject player)
		{
			player.GetComponent<PlayerMovement>().maxSpeed = moveSpeedMax;

			if (!showAbilityUI)
			{
				var powerUpManager = player.GetComponent<PowerUpManager>();
				powerUpManager.showUI = false;
			}
		}
	}
}