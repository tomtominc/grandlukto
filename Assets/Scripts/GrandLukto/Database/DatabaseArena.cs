﻿using System.Collections.Generic;
using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_Arena")]
	public class DatabaseArena : DatabaseBase<DataArena>
	{
		public List<string> GetArenaNames()
		{
			return Data.ConvertAll(data => data.arenaName);
		}
	}
}
