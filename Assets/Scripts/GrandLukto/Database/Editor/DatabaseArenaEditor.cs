﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseArenaEditor : DatabaseBaseEditor<DatabaseArena, DataArena>
	{
		[MenuItem("Database/Arenas")]
		public static void Init()
		{
			DatabaseArenaEditor window = GetWindow<DatabaseArenaEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseArenaEditor() : base("Assets/Data/DB_Arena", "Arena") { }

		protected override string GetElementName(DataArena curElement)
		{
			return string.IsNullOrEmpty(curElement.arenaName) ? base.GetElementName(curElement) : curElement.arenaName;
		}
	}
}