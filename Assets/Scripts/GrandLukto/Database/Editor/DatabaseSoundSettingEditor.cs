﻿using System.Linq;
using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseSoundSettingEditor : DatabaseBaseEditor<DatabaseSoundSetting, DataSoundSetting>
	{
		[MenuItem("Database/SoundSettings")]
		public static void Init()
		{
			DatabaseSoundSettingEditor window = GetWindow<DatabaseSoundSettingEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseSoundSettingEditor() : base("Assets/Data/DB_SoundSetting", "SoundSetting") { }

		protected override string GetElementName(DataSoundSetting curElement)
		{
			if (curElement.audioClips != null)
			{
				var audioClip = curElement.audioClips.FirstOrDefault();
				if (audioClip != null)
				{
					return audioClip.name;
				}
			}

			return base.GetElementName(curElement);
		}
	}
}