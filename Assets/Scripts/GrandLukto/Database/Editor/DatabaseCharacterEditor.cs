﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseCharacterEditor : DatabaseBaseEditor<DatabaseCharacter, DataCharacter>
	{
		[MenuItem("Database/Characters")]
		public static void Init()
		{
			DatabaseCharacterEditor window = GetWindow<DatabaseCharacterEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseCharacterEditor() : base("Assets/Data/DB_Character", "Character") { }

		protected override string GetElementName(DataCharacter curElement)
		{
			return string.IsNullOrEmpty(curElement.characterName) ? base.GetElementName(curElement) : curElement.characterName;
		}
	}
}