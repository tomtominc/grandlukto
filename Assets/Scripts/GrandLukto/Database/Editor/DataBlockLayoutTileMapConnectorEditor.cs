﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	[CustomEditor(typeof(DataBlockLayoutTileMapConnector))]
	public class DataBlockLayoutTileMapConnectorEditor : EditorBase<DataBlockLayoutTileMapConnector>
	{
		private UnityEditor.Editor cachedEditor;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (Target.dataObject == null) return;

			if (GUILayout.Button("Load"))
			{
				Load(Target);
			}

			if (GUILayout.Button("Save"))
			{
				Target.Save();
				EditorUtility.SetDirty(Target.dataObject);
			}

			if (GUILayout.Button("Mirror"))
			{
				Target.Mirror();
			}

			EditorGUILayout.Space();
			EditorGUILayout.LabelField("BlockLayout Config", EditorStyles.boldLabel);

			CreateCachedEditor(Target.dataObject, typeof(DataBlockLayoutEditor), ref cachedEditor);
			cachedEditor.OnInspectorGUI();
		}

		public static void Load(DataBlockLayoutTileMapConnector target)
		{
			var tileMap = target.GetComponent<tk2dTileMap>();
			tileMap.Editor__SpriteCollection = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/gfx/Blocks/BlocksTiles Data/BlocksTiles.prefab").GetComponent<tk2dSpriteCollectionData>();
			tileMap.data = AssetDatabase.LoadAssetAtPath<tk2dTileMapData>("Assets/BlockLayouts/tileMapData.asset");
			tileMap.editorDataGUID = AssetDatabase.AssetPathToGUID("Assets/BlockLayouts/tileMapEditorData.asset");

			target.Load();
		}
	}
}