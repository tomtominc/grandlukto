﻿using UnityCave;
using UnityEditor;

namespace GrandLukto.Database
{
	[CustomEditor(typeof(DataBlockLayout))]
	public class DataBlockLayoutEditor : EditorBase<DataBlockLayout>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}
	}
}