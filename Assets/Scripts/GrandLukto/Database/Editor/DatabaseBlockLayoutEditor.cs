﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseBlockLayoutEditor : DatabaseBaseEditor<DatabaseBlockLayout, DataBlockLayout>
	{
		[MenuItem("Database/Block Layouts")]
		public static void Init()
		{
			DatabaseBlockLayoutEditor window = GetWindow<DatabaseBlockLayoutEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseBlockLayoutEditor() : base("Assets/Data/DB_BlockLayout", "BlockLayout", typeof(DataBlockLayoutEditor)) { }

		protected override string GetElementName(DataBlockLayout curElement)
		{
			return string.IsNullOrEmpty(curElement.customName) ? base.GetElementName(curElement) : curElement.customName;
		}
	}
}
