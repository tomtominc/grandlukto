﻿using System.Collections.Generic;
using UnityCave;
using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	[CustomEditor(typeof(DataLevel))]
	public class DataLevelEditor : EditorBase<DataLevel>
	{
		private const int MAX_BLOCKLAYOUTGROUP_COLS = 4;

		private List<DataBlockLayoutGroup> blockLayoutGroups;

		public void OnEnable()
		{
			var blockLayoutGroupDB = DatabaseEditorUtils.LoadDatabase<DatabaseBlockLayoutGroup>();
			if(blockLayoutGroupDB == null)
			{
				Debug.LogError("BlockLayoutGroup DB not found!");
				return;
			}

			blockLayoutGroups = blockLayoutGroupDB.GetAll();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			DrawBlockLayoutGroupAssignment();
		}

		private void DrawBlockLayoutGroupAssignment()
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("BlockLayoutGroup Assignment", EditorStyles.boldLabel);

			for (var i = 0; i < blockLayoutGroups.Count; i++)
			{
				var curBlockLayoutGroup = blockLayoutGroups[i];

				if (i == 0) EditorGUILayout.BeginHorizontal();
				else if (i % 4 == 0)
				{
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.BeginHorizontal();
				}

				DrawBlockLayoutGroupAssignmentCheckbox(curBlockLayoutGroup);
			}

			EditorGUILayout.EndHorizontal();
		}

		private void DrawBlockLayoutGroupAssignmentCheckbox(DataBlockLayoutGroup blockLayoutGroup)
		{
			var wasAssigned = blockLayoutGroup.assignedLevels.Contains(Target);
			var isAssigned = EditorGUILayout.ToggleLeft(blockLayoutGroup.blockLayoutGroupName, wasAssigned);

			if (wasAssigned == isAssigned) return; // no change

			Undo.RecordObject(blockLayoutGroup, "Change BlockLayoutGroup Assignment");

			if (isAssigned)
			{
				// add to BlockLayoutGroup
				blockLayoutGroup.assignedLevels.Add(Target);
			}
			else
			{
				blockLayoutGroup.assignedLevels.Remove(Target);
			}

			EditorUtility.SetDirty(blockLayoutGroup);
		}
	}
}
