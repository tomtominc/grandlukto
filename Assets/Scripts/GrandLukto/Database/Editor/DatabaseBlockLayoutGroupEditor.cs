﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseBlockLayoutGroupEditor : DatabaseBaseEditor<DatabaseBlockLayoutGroup, DataBlockLayoutGroup>
	{
		[MenuItem("Database/BlockLayoutGroup")]
		public static void Init()
		{
			DatabaseBlockLayoutGroupEditor window = GetWindow<DatabaseBlockLayoutGroupEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseBlockLayoutGroupEditor() : base("Assets/Data/DB_BlockLayoutGroup", "BlockLayoutGroup") { }

		protected override string GetElementName(DataBlockLayoutGroup curElement)
		{
			return string.IsNullOrEmpty(curElement.blockLayoutGroupName) ? base.GetElementName(curElement) : curElement.blockLayoutGroupName;
		}
	}
}