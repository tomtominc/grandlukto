﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseLevelEditor : DatabaseBaseEditor<DatabaseLevel, DataLevel>
	{
		[MenuItem("Database/Level")]
		public static void Init()
		{
			DatabaseLevelEditor window = GetWindow<DatabaseLevelEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseLevelEditor() : base("Assets/Data/DB_Level", "Level", typeof(DataLevelEditor)) { }

		protected override string GetElementName(DataLevel curElement)
		{
			return string.IsNullOrEmpty(curElement.levelName) ? base.GetElementName(curElement) : curElement.levelName;
		}
	}
}