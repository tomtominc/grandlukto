﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	[CustomPropertyDrawer(typeof(DataCharacter))]
	public class DataCharacterDrawer : PropertyDrawer
	{
		private static DatabaseCharacter dbCharacter;

		private static void Init()
		{
			if (dbCharacter == null)
			{
				dbCharacter = DatabaseEditorUtils.LoadDatabase<DatabaseCharacter>();
			}
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			Init();

			EditorGUI.BeginProperty(position, label, property);

			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			var charactersRaw = dbCharacter.GetAll();

			// find the selected index
			var selectedCharacter = property.objectReferenceValue as DataCharacter;
			var selectedIndex = selectedCharacter == null ? -1 : charactersRaw.IndexOf(selectedCharacter);

			if(selectedIndex < 0)
			{
				selectedIndex = 0; // show None
			}
			else
			{
				selectedIndex++; // offset by one because we will add the None option later
			}

			var charactersString = charactersRaw.ConvertAll(character => character.characterName);
			charactersString.Insert(0, "None");

			var newIndex = EditorGUI.Popup(position, selectedIndex, charactersString.ToArray());

			if (newIndex != selectedIndex)
			{
				// value changed -> select new character
				property.objectReferenceValue = newIndex == 0 ? null : charactersRaw[newIndex - 1]; // convert index back
			}

			EditorGUI.EndProperty();
		}
	}
}