﻿using UnityCave;
using UnityEditor;

namespace GrandLukto.Database
{
	[CustomEditor(typeof(DataCampaignLevel))]
	public class DataCampaignLevelEditor : EditorBase<DataCampaignLevel>
	{
	}
}