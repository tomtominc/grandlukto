﻿using UnityCave.Database;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DatabaseCampaignLevelEditor : DatabaseBaseEditor<DatabaseCampaignLevel, DataCampaignLevel>
	{
		[MenuItem("Database/CampaignLevel")]
		public static void Init()
		{
			DatabaseCampaignLevelEditor window = GetWindow<DatabaseCampaignLevelEditor>();
			window.minSize = new Vector2(800, 400);
			window.Show();
		}

		public DatabaseCampaignLevelEditor() : base("Assets/Data/DB_CampaignLevel", "CampaignLevel", typeof(DataCampaignLevelEditor)) { }

		protected override string GetElementName(DataCampaignLevel curElement)
		{
			return string.IsNullOrEmpty(curElement.levelName) ? base.GetElementName(curElement) : curElement.levelName;
		}
	}
}