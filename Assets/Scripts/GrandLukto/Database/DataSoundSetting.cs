﻿using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Database
{
	public class DataSoundSetting : ScriptableObject
	{
		public AudioClip[] audioClips;
		public float volume = 1;
		public float delay;

		public void Play(MonoBehaviour sender, AudioSource audioSource)
		{
			if (audioSource == null) return;

			var clipToPlay = GetRandomAudioClip();
			if (clipToPlay == null) return;

			audioSource.PlayOneShotDelayed(sender, clipToPlay, delay, volume);
		}

		public AudioClip GetRandomAudioClip()
		{
			return audioClips.HasItems() ? audioClips.SelectRandom() : null;
		}
	}
}