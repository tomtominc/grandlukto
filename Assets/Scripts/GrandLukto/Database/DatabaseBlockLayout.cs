﻿using UnityCave.Database;
using UnityEngine;

namespace GrandLukto.Database
{
	[CreateAssetMenu(menuName = "Grand Lukto/DB_BlockLayout")]
	public class DatabaseBlockLayout : DatabaseBase<DataBlockLayout>
	{
	}
}