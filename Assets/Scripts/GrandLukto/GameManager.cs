﻿using DG.Tweening;
using GrandLukto.Arenas;
using GrandLukto.Balls;
using GrandLukto.Balls.Factories;
using GrandLukto.Blocks;
using GrandLukto.Characters;
using GrandLukto.Database;
using GrandLukto.GameData;
using GrandLukto.ParticleSystems;
using GrandLukto.PowerUps;
using GrandLukto.UI;
using GrandLukto.UI.WinScreen;
using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCave.CameraEffects;
using UnityCave.ExtensionMethods;
using UnityCave.StateMachine;
using UnityCave.UI;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace GrandLukto
{
	public enum GameState
	{
		None,
		FadeOutBackgroundStart,
		FadeOutBackground,
		FadeOutBackgroundEnd,
		ArenaIntro,
		Countdown,
		Running,
		RoundEnded,
		WinScreenShown
	}

	public class GameManager : GameManagerBase
	{
		public class EventChangeState : UnityEvent<GameState> { }

		public class GameWonArgs : EventArgs
		{
			public bool WinGame { get; set; }

			public GameWonArgs()
			{
				WinGame = true;
			}
		}

		public static new GameManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<GameManager>();
			}
		}

		public GameState ActiveState
		{
			get
			{
				return stateMachine.CurState;
			}
		}

		private SimpleStateMachine<GameState> stateMachine = new SimpleStateMachine<GameState>(GameState.None);

		public CountdownBase gameStartCountdown;
		public GameObject powerUpManagerContainer;

		public GameConfiguration gameConfiguration;

		[Header("Debug Settings")]
		public DataBlockLayout forceBlockLayout;

		[Header("Layer Masks")]
		public LayerMask npcLayerMaskTop;
		public LayerMask npcLayerMaskBottom;
		public LayerMask layerMaskBlocks;

		[Header("Other")]
		public float blockOffset = 0.37f;

		[HideInInspector()]
		public EventChangeState OnStateChanged = new EventChangeState();

		public EventHandler<GameWonArgs> GameWon;

		public Dictionary<Team, BlockGroup> BlockGroupByTeam { get; private set; }
		public Dictionary<Team, List<PlayerSetting>> PlayersByTeam { get; private set; }
		public ArenaConfiguration ArenaConfiguration { get; private set; }
		public List<UIPowerUpManager> PowerUpManagers { get; private set; }

		public CameraScreenShake ScreenShake { get; private set; }
		public FireballScreenColorOverlay FireballScreenOverlay { get; private set; }

		private DataArena selectedArena;
		private DataLevel selectedLevel;
		public GameDataBase GameData { get; private set; }
		
		private UIBackgroundAnimation curBackgroundAnim;

		public bool IsGameRunning
		{
			get
			{
				return stateMachine.CurState == GameState.Running;
			}
		}

		public override void Start()
		{
			base.Start();

			ScreenShake = Camera.main.GetComponent<CameraScreenShake>();
			FireballScreenOverlay = FindObjectOfType<FireballScreenColorOverlay>();

			BlockGroupByTeam = GetBlockGroupsByTeam();

			ArenaConfiguration = FindObjectOfType<ArenaConfiguration>();
			ArenaConfiguration.LoadArena(selectedArena);

			PlayersByTeam = GetPlayerSettingsByTeam();
			PowerUpManagers = GetOrderedPowerUpManagers();

			SelectBlockLayout();
			SpawnPlayers();

			// create bg to fade out
			curBackgroundAnim = UIStatic.CreateBackgroundAnim(uiContainer.transform, uiCamera);

			stateMachine.OnEnterState = OnEnterState;
			stateMachine.Reset(GameState.FadeOutBackgroundStart);

			GameData.OnEnterGame();
		}

		private void SpawnPlayers()
		{
			foreach(var keyValuePair in PlayersByTeam)
			{
				var playerSettings = keyValuePair.Value;
				var numPlayers = playerSettings.Count;
				var blockGroup = BlockGroupByTeam[keyValuePair.Key];

				if (numPlayers < 1) continue;

				if (numPlayers == 1)
				{
					if (keyValuePair.Key == Team.Bottom && selectedArena.arenaName == Arena.Boss.ToString())
					{
						playerSettings[0].Spawn(new Vector2(0.3f, -4)); // todo: remove hack
						continue;
					}

					playerSettings[0].Spawn(blockGroup.playerSpawnSingle);
					continue;
				}

				// at least two players
				playerSettings[0].Spawn(blockGroup.playerSpawnMultiple1);
				playerSettings[1].Spawn(blockGroup.playerSpawnMultiple2);

				if(numPlayers > 2)
				{
					playerSettings[2].Spawn(blockGroup.playerSpawnSingle);
				}
			}
		}

		private void SelectBlockLayout()
		{
			if (selectedLevel == null) return; // no level selected

			if (selectedLevel.scriptPrefab != null)
			{
				var levelScriptGameObj = Instantiate(selectedLevel.scriptPrefab, transform.parent, false);
				levelScriptGameObj.transform.position = Vector3.zero;
			}

			BlockGroupByTeam[Team.Top].ApplyLayout(selectedLevel.blockLayoutTop);
			BlockGroupByTeam[Team.Bottom].ApplyLayout(selectedLevel.blockLayoutBottom);
		}
		
		private void OnEnterState(GameState state)
		{
			switch(state)
			{
				case GameState.Countdown:
					gameStartCountdown.OnCountdownFinished.AddListener(CountdownOnFinish);
					gameStartCountdown.StartCountdown();
					break;

				case GameState.Running:
					if (GameData.SpawnInitialBall) StartCoroutine(SpawnBall());
					break;
			}

			OnStateChanged.Invoke(state);
		}

		public override void Update()
		{
			base.Update();

			switch(stateMachine.CurState)
			{
				case GameState.FadeOutBackgroundStart:
					curBackgroundAnim
						.Hide()
						.OnComplete(() =>
						{
							Destroy(curBackgroundAnim.gameObject);
							curBackgroundAnim = null;

							stateMachine.ChangeState(GameState.FadeOutBackgroundEnd);
						});

					stateMachine.ChangeState(GameState.FadeOutBackground);
					break;

				case GameState.FadeOutBackgroundEnd:
					stateMachine.ChangeState(GameState.ArenaIntro);

					GameData.OnBeforeCountdown(() =>
					{
						// TODO: Maybe remove arena intro?
						if (ArenaConfiguration.ArenaIntro != null)
						{
							// start countdown after intro
							ArenaConfiguration.ArenaIntro.IntroComplete += OnArenaIntroComplete;
							return;
						}

						stateMachine.ChangeState(GameState.Countdown);
					});
					break;
			}
		}

		private void OnArenaIntroComplete(object sender, EventArgs e)
		{
			ArenaConfiguration.ArenaIntro.IntroComplete -= OnArenaIntroComplete;
			stateMachine.ChangeState(GameState.Countdown);
		}

		protected override void CheckDebugKeys(Keyboard keyboard)
		{
			base.CheckDebugKeys(keyboard);

			if (keyboard.GetKeyDown(KeyCode.F1))
			{
				Camera.main.cullingMask ^= 1 << LayerMask.NameToLayer("Blocks");
			}

			if (keyboard.GetKeyDown(KeyCode.F2))
			{
				Camera.main.cullingMask ^= 1 << LayerMask.NameToLayer("Balls");
			}

			if (keyboard.GetKeyDown(KeyCode.F3))
			{
				// set all cameras inactive except main
				foreach (var camera in Camera.allCameras)
				{
					if (camera == Camera.main) continue;
					camera.gameObject.SetActive(false);
				}
			}

			if (keyboard.GetKeyDown(KeyCode.F5))
			{
				WinGame(keyboard.GetKey(KeyCode.LeftControl) ? Team.Top : Team.Bottom, Vector2.zero);
			}

			if (keyboard.GetKeyDown(KeyCode.F6))
			{
				foreach(var playerSetting in PlayerSettings)
				{
					PlayerIced.IcePlayer(playerSetting.PlayerObject);
				}
			}
		}

		private void CountdownOnFinish()
		{
			gameStartCountdown.OnCountdownFinished.RemoveListener(CountdownOnFinish);
			stateMachine.ChangeState(GameState.Running);
		}

		private Dictionary<Team, BlockGroup> GetBlockGroupsByTeam()
		{
			Dictionary<Team, BlockGroup> dict = new Dictionary<Team, BlockGroup>();

			var blockGroups = FindObjectsOfType<BlockGroup>();
			foreach(var blockGroup in blockGroups)
			{
				if(dict.ContainsKey(blockGroup.team))
				{
					Debug.LogErrorFormat("Multiple BlockGroups for the same team: {0}", blockGroup.team);
					continue;
				}

				dict[blockGroup.team] = blockGroup;
			}

			return dict;
		}

		private Dictionary<Team, List<PlayerSetting>> GetPlayerSettingsByTeam()
		{
			var settingsByTeam = new Dictionary<Team, List<PlayerSetting>>();

			foreach(var setting in PlayerSettings)
			{
				List<PlayerSetting> settingsList;
				if(!settingsByTeam.TryGetValue(setting.team, out settingsList))
				{
					settingsByTeam[setting.team] = settingsList = new List<PlayerSetting>();
				}

				settingsList.Add(setting);
			}

			return settingsByTeam;
		}

		public void WinGame(Team winningTeam, Vector2? shockwavePosition = null)
		{
			if (ActiveState == GameState.RoundEnded || ActiveState == GameState.WinScreenShown) return; // a team already won

			if (shockwavePosition.HasValue)
			{
				ScreenShake.Shake(new ScreenShakeSetting(0.15f, 0.05f, 50));

				PostProcessingWaveExplosion.Create(shockwavePosition.Value, 1f, 0.02f, 0.5f);
			}

			// confetti cannon
			var goConfettiCannon = Instantiate(Resources.Load<GameObject>("Effects/ConfettiCannon"));
			goConfettiCannon.GetComponent<ConfettiCannon>().Shoot(winningTeam);

			var eventResult = OnGameWon();
			if (!eventResult.WinGame) return; // do not show win screen yet

			stateMachine.ChangeState(GameState.RoundEnded);
			GameData.OnWinGame(winningTeam);

			StartCoroutine(ShowWinScreen(winningTeam));
		}

		private IEnumerator ShowWinScreen(Team winningTeam)
		{
			yield return new WaitForSecondsRealtime(1.5f);

			UIStatic.CreateBackgroundAnim(uiContainer.transform, uiCamera).Show();

			var goWinScreen = Instantiate(Resources.Load<GameObject>("UI/WinScreen"), uiContainer.transform, true);
			goWinScreen.transform.position += uiCamera.transform.position;
			goWinScreen.transform.position = goWinScreen.transform.position.Clone(z: -5);
			
			var winScreenHandler = GameData.CreateWinScreenHandler(winningTeam, goWinScreen.transform);
			goWinScreen.GetComponent<WinScreen>().Show(winScreenHandler);

			yield return new WaitForSecondsRealtime(1.5f);

			stateMachine.ChangeState(GameState.WinScreenShown);
		}

		private IEnumerator SpawnBall()
		{
			yield return new WaitForSeconds(gameConfiguration.ballSpawnDelay); // delay ball spawn

			var startBall = new BallFactory().CreateBall(BallType.Standard);
			startBall.InitializeWithSpawnAnimation();
			startBall.InitializePosition(GetSpawnPosition());
		}

		private Vector3 GetSpawnPosition()
		{
			if (ArenaConfiguration == null) return Vector3.zero;
			return ArenaConfiguration.GetBallSpawnPosition();
		}

		private List<UIPowerUpManager> GetOrderedPowerUpManagers()
		{
			var managersUnordered = powerUpManagerContainer.GetComponentsInChildren<UIPowerUpManager>();

			var managersOrdered = new List<UIPowerUpManager>();

			managersOrdered = managersUnordered.ToList();
			managersOrdered.Sort((a, b) => a.transform.position.x.CompareTo(b.transform.position.x)); // sort by x position

			return managersOrdered;
		}

		public void InitializeGame(DataArena arena, DataLevel level, List<PlayerSetting> playerSettings, GameDataBase gameData)
		{
			this.PlayerSettings = playerSettings;
			selectedArena = arena;
			selectedLevel = level;
			GameData = gameData;
		}
		
		public void RestartGame()
		{
			GameSceneManager.LoadGame(selectedArena, selectedLevel, PlayerSettings, GameData);
		}

		public void QuitGame()
		{
			GameData.DoQuitGame();
		}

		private GameWonArgs OnGameWon()
		{
			var gameWonArgs = new GameWonArgs();

			if (GameWon == null) return gameWonArgs;

			GameWon(this, gameWonArgs);
			return gameWonArgs;
		}
	}
}