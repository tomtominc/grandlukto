﻿using GrandLukto.Characters;

namespace GrandLukto
{
	public enum Team
	{
		Top,
		Bottom
	}

	public static class TeamExtensions
	{
		public static Team GetOpponent(this Team team)
		{
			if (team == Team.Top) return Team.Bottom;
			return Team.Top;
		}

		public static FaceDirection GetFaceDirection(this Team team)
		{
			if (team == Team.Top) return FaceDirection.Down;
			return FaceDirection.Up;
		}

		public static string GetTeamColorName(this Team team)
		{
			return team == Team.Top ? "red" : "blue";
		}
	}
}