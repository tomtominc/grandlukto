﻿using GrandLukto.UI.Dialog;
using System.Collections;
using UnityEngine;
using GrandLukto.PowerUps;
using GrandLukto.Balls.Factories;
using GrandLukto.Blocks;

namespace GrandLukto.LevelScripts
{
	public class LevelScriptTutorialFortify : LevelScriptBase
	{
		public DialogDataWrapper dialogAfterBallSpawn;

		private PowerUpManager playerPowerUpManager;

		protected override void Start()
		{
			base.Start();

			// wait for ability collect
			var player = GameManager.Instance.PlayersByTeam[Team.Bottom][0];
			playerPowerUpManager = player.PlayerObject.GetComponent<PowerUpManager>();
			playerPowerUpManager.OnSlotChanged.AddListener(OnPowerUpCollected);
		}

		private void OnPowerUpCollected(int slot, PowerUpType type, bool onHitActivated)
		{
			if (type != PowerUpType.Fortify) return;

			// fortify powerup collected
			playerPowerUpManager.OnSlotChanged.RemoveListener(OnPowerUpCollected);

			StartCoroutine(ActionRoutine());
		}

		private IEnumerator ActionRoutine()
		{
			yield return new WaitForSeconds(1f);

			var ballShooters = GameManager.Instance.BlockGroupByTeam[Team.Top].GetTilePrefabsWithComponent<BallShooter>();
			for (var i = 0; i < ballShooters.Count; i++)
			{
				ballShooters[i].Shoot(0, 0.6f, () => new BallFactory().CreateBallVanish(2f));
			}

			yield return new WaitForSeconds(1f);

			GameManager.Instance.ShowDialog(dialogAfterBallSpawn);
		}
	}
}