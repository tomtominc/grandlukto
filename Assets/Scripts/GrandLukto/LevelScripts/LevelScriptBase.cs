﻿using UnityEngine;

namespace GrandLukto.LevelScripts
{
	public abstract class LevelScriptBase : MonoBehaviour
	{
		protected virtual void Start()
		{
		}
	}
}