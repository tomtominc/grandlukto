﻿using GrandLukto.Database;
using GrandLukto.UI.Dialog;
using System.Collections;
using UnityEngine;
using GrandLukto.Blocks;

namespace GrandLukto.LevelScripts
{
	public class LevelScriptTutorial : LevelScriptBase
	{
		public DataBlockLayout secondLayout;
		public DialogDataWrapper secondLayoutDialog;

		protected override void Start()
		{
			base.Start();

			GameManager.Instance.GameWon += OnGameWon;
		}

		private void OnGameWon(object sender, GameManager.GameWonArgs e)
		{
			e.WinGame = false; // do not win the game yet

			StartCoroutine(AfterFirstWin());
		}

		private IEnumerator AfterFirstWin()
		{
			GameManager.Instance.GameWon -= OnGameWon;

			// fake a win condition
			GameSpeedManager.Instance.AddWinConditionTrigger(gameObject);

			yield return new WaitForSeconds(0.3f);

			GameManager.Instance.ShowDialog(secondLayoutDialog);

			yield return new WaitForSeconds(0.01f); // wait until game is unpaused
			GameSpeedManager.Instance.RemoveWinConditionTrigger(gameObject); // continue in full speed

			PracticeTargetsManager.Instance.ResetTargets();

			yield return new WaitForSeconds(2f);

			// spawn next set of targets
			var spawnedObjects = GameManager.Instance.BlockGroupByTeam[Team.Top].SpawnTiles(secondLayout);
			for (var i = 0; i < spawnedObjects.Count; i++)
			{
				var curObj = spawnedObjects[i];
				var spawnAnim = curObj.AddComponent<SpawnBlockWithImpact>();
				spawnAnim.startDelay = Random.Range(0, 1.5f);
			}
		}
	}
}