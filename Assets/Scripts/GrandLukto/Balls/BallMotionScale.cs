﻿using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallMotionScale : MonoBehaviour
	{
		public FloatRange scaleModifierY = new FloatRange(1.1f, 1.75f);
		public FloatRange scaleModifierX = new FloatRange(1f, 0.9f);
		
		private Ball compBall;
		private BallPhysics compBallPhysics;

		private float speedMin;
		private float speedMax;

		private void Awake()
		{
			compBall = GetComponent<Ball>();
			compBallPhysics = GetComponent<BallPhysics>();
		}

		private void Start()
		{
			speedMin = compBall.config.GetRelativeSpeed(0);
			speedMax = compBall.config.GetRelativeSpeed(1);
		}

		private void Update()
		{
			if (!compBall.IsMoving) return;

			var newRotation = Quaternion.LookRotation(compBallPhysics.Direction, Vector3.forward);
			newRotation.x = 0;
			newRotation.y = 0;
			compBall.visuals.transform.rotation = newRotation;

			var relativeSpeed = Mathf.Clamp01(Mathf.InverseLerp(speedMin, speedMax, compBallPhysics.Speed));
			compBall.visuals.transform.localScale = new Vector3(
				Mathf.Lerp(scaleModifierX.rangeStart, scaleModifierX.rangeEnd, relativeSpeed),
				Mathf.Lerp(scaleModifierY.rangeStart, scaleModifierY.rangeEnd, relativeSpeed),
				1);
		}
	}
}