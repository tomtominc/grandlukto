﻿using System;
using UnityEngine;

namespace GrandLukto.Balls
{
	[RequireComponent(typeof(TrailRenderer))]
	public class BallTrail : MonoBehaviour
	{
		//public float alphaSpeed = 3;
		public float colorSpeed = 3;
		public float lengthSpeed = 1.5f;

		public Color Color
		{
			set
			{
				if (colorTarget == value) return;

				colorTarget = value;

				// instant color change if trail is not visible
				if (curAlpha <= 0)
				{
					colorCur = value;
					return;
				}

				colorOld = colorCur;
				colorChangeTime = 0;
			}

			get
			{
				return colorTarget;
			}
		}

		/// <summary>
		/// The length of the trail.
		/// </summary>
		public float TrailLength = 0.2f;

		private bool renderTrail = false;

		private TrailRenderer compTrailRenderer;
		private BallPhysics compBallPhyisics;
		private Ball compBall;

		private Gradient gradient;

		private float curAlpha = 1;
		//private float curAlpha = 0;

		private float colorChangeTime;
		private Color colorTarget;
		private Color colorCur;
		private Color colorOld;

		private Color? trailColorCur;
		private float? trailAlphaCur;

		public void Start()
		{
			compTrailRenderer = GetComponent<TrailRenderer>();
			compBallPhyisics = GetComponentInParent<BallPhysics>();
			compBall = GetComponentInParent<Ball>();
			
			gradient = new Gradient();

			UpdateGradient();

			compBallPhyisics.SpeedChanged += CompBallPhyisics_ChangeSpeed;
		}

		private void CompBallPhyisics_ChangeSpeed(object sender, EventArgs e)
		{

			renderTrail = compBallPhyisics.Speed > compBall.config.minSpeed;
			if (renderTrail)
			{
				Color = compBall.LastStrongestHitCharacter == null ? Color.white : compBall.LastStrongestHitCharacter.PlayerColor;
				TrailLength = compBall.config.trailTime.Lerp(Mathf.InverseLerp(compBall.config.minSpeed, compBall.config.maxSpeed, compBallPhyisics.Speed));
			}
		}

		public void Update()
		{
			// disabled since we fade out by modifying length
			//if (!renderTrail)
			//{
			//	if (curAlpha <= 0) return;
			//	curAlpha -= alphaSpeed * Time.deltaTime;
			//}
			//else
			//{
			//	curAlpha += alphaSpeed * Time.deltaTime;
			//}

			UpdateColor();
			UpdateLength();

			curAlpha = Mathf.Clamp01(curAlpha);
			UpdateGradient();
		}

		private void UpdateLength()
		{
			var targetLength = renderTrail ? TrailLength : 0;
			compTrailRenderer.time = Mathf.MoveTowards(compTrailRenderer.time, targetLength, lengthSpeed * Time.deltaTime);
		}

		private void UpdateColor()
		{
			if (colorCur == colorTarget) return;

			colorChangeTime += colorSpeed * Time.deltaTime;
			colorChangeTime = Mathf.Clamp01(colorChangeTime);

			colorCur = Color.Lerp(colorOld, colorTarget, colorChangeTime);
		}

		private void UpdateGradient()
		{
			if (trailColorCur.HasValue && trailColorCur == colorCur && trailAlphaCur.HasValue && trailAlphaCur == curAlpha) return; // nothing changed

			trailColorCur = colorCur;
			trailAlphaCur = curAlpha;

			gradient.SetKeys(new[]
			{
				new GradientColorKey(trailColorCur.Value, 0f),
				new GradientColorKey(trailColorCur.Value, 1f)
			},
			new[]
			{
				new GradientAlphaKey(trailAlphaCur.Value, 0),
				new GradientAlphaKey(trailAlphaCur.Value, 0.5f),
				new GradientAlphaKey(0, 1),
			});

			compTrailRenderer.colorGradient = gradient;
		}

		private void OnDestroy()
		{
			compBallPhyisics.SpeedChanged -= CompBallPhyisics_ChangeSpeed;
		}
	}
}