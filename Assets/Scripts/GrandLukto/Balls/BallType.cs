﻿namespace GrandLukto.Balls
{
	public enum BallType
	{
		Standard,
		Bomb,

		/// <summary>
		/// Ball that disappears after the first hit.
		/// </summary>
		Rock,

		/// <summary>
		/// Ball that vanishes after an amount of time.
		/// </summary>
		Vanish,

		Snowball
	}
}
