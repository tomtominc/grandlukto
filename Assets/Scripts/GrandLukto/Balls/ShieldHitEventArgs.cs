﻿using GrandLukto.Characters;
using System;

namespace GrandLukto.Balls
{
	public class ShieldHitEventArgs : EventArgs
	{
		public CharacterBase Character { get; private set; }

		public ShieldHitEventArgs(CharacterBase hitter)
		{
			Character = hitter;
		}
	}
}