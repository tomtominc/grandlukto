﻿using GrandLukto.Blocks;
using System;

namespace GrandLukto.Balls
{
	public class BlockHitEventArgs : EventArgs
	{
		public BlockBase Block { get; private set; }

		public BlockHitEventArgs(BlockBase block)
		{
			Block = block;
		}
	}
}