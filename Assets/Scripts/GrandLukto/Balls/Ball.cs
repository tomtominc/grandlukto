﻿using GrandLukto.Characters;
using GrandLukto.Stats;
using System;
using System.Collections.Generic;
using UnityCave.CameraEffects;
using UnityCave.ExtensionMethods;
using UnityCave.Physics;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class Ball : MonoBehaviour
	{
		public BallConfiguration config;
		public GameObject visuals;
		public GameObject shadow;

		public GameObject prefabEffectOnHit;

		private Rigidbody2D compRigidBody;
		private BallTrail compBallTrail;
		private AudioSource compAudioSource;
		protected BallPhysics CompBallPhysics { get; private set; }
		
		public Vector2? LastHitPosition { get; private set; }
		public bool IsMoving
		{
			get { return CompBallPhysics.IsMoving; }
		}

		public bool CanBeHit
		{
			get { return IsEnabled; }
		}

		public float Radius { get; private set; }

		public bool IsEnabled { get; private set; }

		private Dictionary<Team, CharacterBase> lastHitByTeam = new Dictionary<Team, CharacterBase>();

		public CharacterBase LastStrongestHitCharacter { get; private set; }
		private float lastStrongestHitSpeed = 0;

		public Vector2 MoveDirection
		{
			get { return CompBallPhysics.Direction; }
		}

		public float CurSpeed
		{
			get { return CompBallPhysics.Speed; }
		}

		public Ball()
		{
			IsEnabled = true;
		}
		
		public void Awake()
		{
			compAudioSource = gameObject.GetOrAddComponent<AudioSource>();
			CompBallPhysics = gameObject.GetOrAddComponent<BallPhysics>();
			gameObject.GetOrAddComponent<BallMotionScale>();

			visuals.AddComponent<BallOnHitAnim>();

			compRigidBody = GetComponent<Rigidbody2D>();
		}

		// Use this for initialization
		public virtual void Start()
		{
			if (compBallTrail == null)
			{
				var ballTrailGameObj = Instantiate(Resources.Load<GameObject>("BallTrail"), visuals.transform, false);
				compBallTrail = ballTrailGameObj.GetComponent<BallTrail>();
			}

			Radius = GetComponent<CircleCollider2D>().radius;
			
			CompBallPhysics.BlockHit += CompBallPhysics_BlockHit;
			CompBallPhysics.AnyHit += CompBallPhysics_AnyHit;
			CompBallPhysics.PlayerHit += CompBallPhysics_PlayerHit;
		}

		private void OnDestroy()
		{
			CompBallPhysics.BlockHit -= CompBallPhysics_BlockHit;
			CompBallPhysics.AnyHit -= CompBallPhysics_AnyHit;
			CompBallPhysics.PlayerHit -= CompBallPhysics_PlayerHit;
		}

		public virtual void Update()
		{
		}
		
		public void GetHitByCharacter(CharacterBase sender, Vector3 newDirection, float chargeValueRel = 0, float curveBallRel = 0)
		{
			if (!IsEnabled) return;

			lastHitByTeam[sender.Team] = sender;

			//curCurveDirection = curveBallRel;
			//curCurveSpeed = Mathf.Lerp(config.curveBallSpeedMin, config.curveBallSpeedMax, config.speedCurve.Evaluate(chargeValueRel));

			var wantedSpeed = config.GetRelativeSpeed(config.speedCurve.Evaluate(chargeValueRel));

			// do not allow changing speed too much
			var minimumSpeed = CurSpeed - (config.maxSpeed - config.minSpeed) * config.maxSpeedLoseOnPlayerHit;

			var newSpeed = Mathf.Max(wantedSpeed, minimumSpeed);

			if (newSpeed >= CompBallPhysics.Speed)
			{
				// update variables for trail color if speed is higher or equal than old speed
				LastStrongestHitCharacter = sender;
				lastStrongestHitSpeed = newSpeed;
			}

			CompBallPhysics.ApplyForce(newSpeed, newDirection);
			CompBallPhysics.RegisterShieldHit(sender);
		}
		
		public RaycastHit2D PredictNextHitPosition()
		{
			return Physics2D.Raycast(transform.position, CompBallPhysics.Direction, 10, Physics2D.GetLayerCollisionMask(gameObject.layer)); // (LayerMask)gameObject.layer
		}

		public void CopyInternalValuesFrom(Ball other)
		{
			LastHitPosition = other.LastHitPosition;

			LastStrongestHitCharacter = other.LastStrongestHitCharacter;
			lastStrongestHitSpeed = other.lastStrongestHitSpeed;
			lastHitByTeam = other.lastHitByTeam;
		}

		public void ToggleEnabled(bool enable)
		{
			IsEnabled = enable;
			compRigidBody.isKinematic = !enable;
		}

		/// <summary>
		/// Returns the last hitter of the given team or null if none was found.
		/// </summary>
		public CharacterBase GetLastHitterOfTeam(Team team)
		{
			CharacterBase lastHitter = null;
			return lastHitByTeam.TryGetValue(team, out lastHitter) ? lastHitter : null;
		}

		private void ShowOnHitEffect()
		{
			if (prefabEffectOnHit == null) return;

			var effect = Instantiate(prefabEffectOnHit);
			effect.transform.position = visuals.transform.position;
		}

		private void CompBallPhysics_AnyHit(object sender, EventArgs e)
		{
			// update last hit position
			LastHitPosition = transform.position;

			ShowOnHitEffect();
		}

		private void CompBallPhysics_BlockHit(object sender, BlockHitEventArgs e)
		{
			// TODO: Block should play the sound...
			GameManager.Instance.gameConfiguration.soundOnBallHitBlock.Play(this, compAudioSource);
		}

		private void CompBallPhysics_PlayerHit(object sender, Physics2DHitEventArgs e)
		{
			e.Collision.gameObject.GetComponent<StatManagerStunTime>().Fill();
			GameManager.Instance.ScreenShake.Shake(new ScreenShakeSetting(0.2f, 0.025f, 20));
		}
	}
}