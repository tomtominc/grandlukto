﻿using Rotorz.ReorderableList;
using System;
using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Balls
{
	[CustomPropertyDrawer(typeof(BallCreator))]
	public class BallCreatorDrawer : PropertyDrawer
	{
		private EditorHeightCalculator heightCalculator = new EditorHeightCalculator();
		private EditorColumn colMain = new EditorColumn();

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			heightCalculator.Reset();

			var propertyBallType = property.FindPropertyRelative("ballType");
			heightCalculator.AddProperty(propertyBallType);

			switch (propertyBallType.GetEnum<BallType>())
			{
				case BallType.Vanish:
					heightCalculator.AddProperty(property.FindPropertyRelative("vanishAfterSeconds"));
					break;
			}
			
			return heightCalculator.Height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			colMain.Reset(position);

			var propertyBallType = property.FindPropertyRelative("ballType");
			colMain.DrawPropertyField(propertyBallType);

			switch(propertyBallType.GetEnum<BallType>())
			{
				case BallType.Vanish:
					colMain.DrawPropertyField(property.FindPropertyRelative("vanishAfterSeconds"));
					break;
			}
		}
	}
}