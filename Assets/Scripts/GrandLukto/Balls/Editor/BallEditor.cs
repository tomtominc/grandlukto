﻿using UnityCave;
using UnityEditor;
using UnityEngine;

namespace GrandLukto.Balls
{
	[CustomEditor(typeof(Ball))]
	public class BallEditor : EditorBase<Ball>
	{
		private readonly Color colorHandles = Color.magenta;

		public void OnSceneGUI()
		{
			Handles.color = colorHandles;

			DrawLastHitPosition();
		}

		private void DrawLastHitPosition()
		{
			var lastHitPos = Target.LastHitPosition;
			if (lastHitPos == null) return;

			Handles.DrawWireDisc((Vector3)lastHitPos, Vector3.forward, Target.Radius);
			Handles.DrawLine((Vector3)lastHitPos + new Vector3(0, 0, Target.transform.position.z), Target.transform.position);

			var hit = Target.PredictNextHitPosition();
			if (hit.collider == null) return;

			Handles.color = colorHandles * new Color(1, 1, 1, 0.5f);

			Handles.DrawLine(Target.transform.position, hit.point);
			Handles.DrawWireDisc(hit.point, Vector3.forward, Target.Radius);
		}
	}
}