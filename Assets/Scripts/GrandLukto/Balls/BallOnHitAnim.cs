﻿using System;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallOnHitAnim : MonoBehaviour
	{
		private BallPhysics compBallPhysics;
		private tk2dSprite compSprite;

		private bool playAnim = false;

		private float baseY;

		public float onHitValueSpeed = 100f;
		public float valueDeceleration = 1000;
		public float valueEaseToZeroDuration = 0.25f;

		public float maxChangeY = -0.05f;
		public float maxChangeScale = 0.15f;

		private float curSpeed = 0;
		private float curValue = 0;

		public void Awake()
		{
			compBallPhysics = GetComponentInParent<BallPhysics>();
			compSprite = GetComponent<tk2dSprite>();
		}

		public void Start()
		{
			var spawnAnim = GetComponentInParent<BallSpawnAnimation>();
			if (spawnAnim == null)
			{
				InitForAnimation();
				return;
			}

			spawnAnim.AnimationFinished += (object sender, EventArgs args) => InitForAnimation();
		}

		private void InitForAnimation()
		{
			playAnim = true;

			baseY = transform.localPosition.y;
		}

		public void OnEnable()
		{
			compBallPhysics.AnyHit += OnAnyHit;
		}

		public void OnDisable()
		{
			compBallPhysics.AnyHit -= OnAnyHit;
		}

		private void OnAnyHit(object sender, EventArgs e)
		{
			if (!playAnim) return;

			curSpeed = onHitValueSpeed;
		}

		public void Update()
		{
			if (!playAnim) return;

			UpdateValue();

			curValue = Mathf.Clamp01(curValue);

			transform.localPosition = transform.localPosition.Clone(y: baseY + maxChangeY * curValue);

			var newScale = 1 + curValue * maxChangeScale;

			if (compSprite != null)
			{
				compSprite.scale = new Vector3(newScale, newScale, newScale);
			}
			else
			{
				transform.localScale = new Vector3(newScale, newScale, newScale);
			}
		}

		private void UpdateValue()
		{
			if (curSpeed > 0)
			{
				curValue += curSpeed;

				curSpeed -= Time.deltaTime * valueDeceleration;
				Mathf.Max(curSpeed, 0);
				return;
			}

			// ease back to base
			var yOffset = curValue;

			// approach
			var speed = yOffset * Time.deltaTime / valueEaseToZeroDuration;
			curValue -= speed;
		}
	}
}
