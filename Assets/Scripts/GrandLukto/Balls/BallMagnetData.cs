﻿using GrandLukto.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallMagnetData
	{
		public Transform Transform { get; private set; }

		public bool IsActive
		{
			get
			{
				return activeUntil > Time.time;
			}
		}

		private FaceDirection attractiveSide;
		private float activeUntil;
		private float maxRange;
		private float strength;

		public BallMagnetData(Transform position, FaceDirection attractiveSide)
		{
			var config = GameManager.Instance.gameConfiguration;
			activeUntil = Time.time + config.powerUpMagnetActiveTime;
			maxRange = config.powerUpMagnetRange;
			strength = config.powerUpMagnetStrength;

			Transform = position;
			this.attractiveSide = attractiveSide;
		}

		public Vector3 EvaluateAttraction(Ball ball)
		{
			var posToBall = ball.transform.position - Transform.position;
			var relDist = posToBall.magnitude / maxRange;

			if (relDist > 1) return Vector3.zero; // not in distance of magnet


			if (Vector2.Dot(ball.MoveDirection, attractiveSide.ToDirectionVector()) < 0)
			{
				// ball moves in player's direction -> attract
				return -posToBall.normalized * (relDist * strength);
			}

			return Vector3.zero;
		}
	}
}
