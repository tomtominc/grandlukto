﻿using UnityEngine;

namespace GrandLukto.Balls
{
	public class OldBallReference : MonoBehaviour
	{
		public IBallController oldBall;

		/// <summary>
		/// Saves the given ball as old ball and makes it inactive.
		/// </summary>
		public void SaveBall(IBallController ball)
		{
			oldBall = ball;
			oldBall.SetActive(false);
		}

		public IBallController LoadBall()
		{
			oldBall.SetActive(true);
			Destroy(this); // reference not needed anymore
			return oldBall;
		}
	}
}