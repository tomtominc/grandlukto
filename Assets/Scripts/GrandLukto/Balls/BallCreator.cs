﻿using GrandLukto.Balls.Factories;
using System;

namespace GrandLukto.Balls
{
	[Serializable]
	public class BallCreator
	{
		public BallType ballType;

		public float vanishAfterSeconds = 0;

		public IBallController CreateBall()
		{
			var ballFactory = new BallFactory();

			switch(ballType)
			{
				case BallType.Vanish:
					return ballFactory.CreateBallVanish(vanishAfterSeconds);

				default:
					return ballFactory.CreateBall(ballType);
			}
		}
	}
}