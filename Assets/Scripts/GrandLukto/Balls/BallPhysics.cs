﻿using GrandLukto.Blocks;
using GrandLukto.Characters;
using System;
using UnityCave.Physics;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallPhysics : MonoBehaviour
	{
		public event EventHandler<BallHitEventArgs> AnyHit;
		public event EventHandler<BlockHitEventArgs> BlockHit;
		public event EventHandler<Physics2DHitEventArgs> PlayerHit;
		public event EventHandler<ShieldHitEventArgs> ShieldHit;
		public event EventHandler SpeedChanged;

		private Rigidbody2D compRigidBody;
		private Ball compBall;

		public float minAngle = 15; // ball needs to move at least minAngle degree away from x-axis

		/// <summary>
		/// The minimum raycast distance for manual collision detection.
		/// It seems like 0.035f is Unity's minimum offset between objects.
		/// </summary>
		public float minRaycastDistance = 0.035f;

		private bool didCollideThisFrame = false;

		public float Speed { get; private set; }

		private Vector2 direction;

		/// <summary>
		/// Stores the ball's normalized move direction.
		/// </summary>
		public Vector2 Direction
		{
			get { return direction; }
			private set
			{
				if (value == Vector2.zero) return;
				direction = value.normalized;
			}
		}

		public bool IsMoving
		{
			get { return Speed > 0; }
		}

		private BallConfiguration Config
		{
			get { return compBall.config; }
		}

		private void Awake()
		{
			compRigidBody = GetComponent<Rigidbody2D>();
			compRigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
			compRigidBody.interpolation = RigidbodyInterpolation2D.Interpolate;

			compBall = GetComponent<Ball>();
		}

		public void FixedUpdate()
		{
			didCollideThisFrame = false;

			if (ManualBlockCollisionCheck())
			{
				// stop movement for one frame
				compRigidBody.velocity = Vector2.zero;
				return;
			}

			ForceMinAngle();

			// always move ball at full speed (do not take value from physics)
			Vector3 newVelocity = Direction * Speed;

			// curve ball
			//if (curCurveDirection > 0 || curCurveDirection < 0)
			//{
			//	var curveAngle = curCurveDirection < 0 ? -90 : 90;
			//	newVelocity = newVelocity + (Quaternion.Euler(0, 0, curveAngle) * newVelocity.normalized * curCurveSpeed);
			//}

			newVelocity += BallMagnetManager.Instance.EvaluateAttraction(compBall); // power up magnets

			compRigidBody.velocity = newVelocity;

			UpdateCheckHeartWallHit();
		}

		/// <summary>
		/// Forces the ball's direction to a minimum angle away from the x-axis.
		/// </summary>
		private void ForceMinAngle()
		{
			ForceMinAngle(Vector2.right);
			ForceMinAngle(Vector2.left);
		}

		private void ForceMinAngle(Vector2 reference)
		{
			var angle = Vector2Util.AngleSigned(reference, Direction);
			if (Mathf.Abs(angle) >= minAngle) return; // angle is okay

			Direction = Quaternion.Euler(0, 0, minAngle * Mathf.Sign(angle)) * reference;
		}

		private bool ManualBlockCollisionCheck()
		{
			if (!compBall.IsEnabled) return false;

			// manually cast hit against blocks since we want perfect collision detection and only want to hit 1 block
			// always cast for a minimum distance since Unity seems to always keep that offset between objects.
			var distance = Mathf.Max(minRaycastDistance, Speed * Time.deltaTime);

			var hits = Physics2D.CircleCastAll(transform.position, compBall.Radius, Direction, distance, Config.layerMaskBlocks);
			
			// find a valid hit
			foreach (var hit in hits)
			{
				var dot = Vector2.Dot((hit.point - (Vector2)transform.position).normalized, Direction);
				if (dot < 0)
				{
					continue; // hit behind ball
				}

				// first valid hit, handle collision and return
				HandleManualBlockCollision(hit);
				return true;
			}

			return false;
		}

		private void HandleManualBlockCollision(RaycastHit2D hit)
		{
			transform.position = hit.centroid; // move back so that ball does not intercept
			ApplyForce(null, Vector2.Reflect(Direction.normalized, hit.normal).normalized); // reflect ball speed

			// TODO: Check if speed was already lowered this frame?
			OnHitLowerSpeed();
			OnAnyHit(new BallHitEventArgs(hit.normal));

			var block = hit.collider.gameObject.GetComponent<BlockBase>();
			if (block == null) return;

			OnBlockHit(block);
			block.TryInvokeOnBallHit(compBall);
		}

		private void UpdateCheckHeartWallHit()
		{
			if (WillCollideWithGameGoalObject())
			{
				GameSpeedManager.Instance.AddWinConditionTrigger(gameObject);
			}
			else
			{
				GameSpeedManager.Instance.RemoveWinConditionTrigger(gameObject);
			}
		}

		private bool WillCollideWithGameGoalObject()
		{
			if (!IsMoving) return false;

			// cast a ray in the balls move direction and change game speed to bullet time if it will hit the heart wall
			var result = Physics2D.Raycast(transform.position, Direction, Config.heartWallRaycastLength, Config.layerMaskBlocks);
			if (result.collider == null) return false; // nothing hit

			// check if the ball will hit the heart wall
			var heartWall = result.collider.gameObject.GetComponentInParent<BlockHeartWall>();
			if (heartWall != null) return true;

			// check if the ball will hit the last available target
			if (PracticeTargetsManager.Instance.AliveTargetsCount == 1)
			{
				if (result.collider.gameObject.GetComponent<PracticeTarget>() != null) return true;
			}

			return false;
		}

		public void OnCollisionEnter2D(Collision2D collision)
		{
			if (!compBall.IsEnabled) return;

			if (collision.gameObject.tag == Tag.Block) return; // we handle block hits manually in FixedUpdate

			// reset curve ball
			//curCurveDirection = 0;

			if (!didCollideThisFrame)
			{
				didCollideThisFrame = true;

				// only allow lowering speed once per frame
				OnHitLowerSpeed();
			}
			
			Direction = compRigidBody.velocity;

			var isPlayerHit = collision.gameObject.tag == Tag.Player;
			OnAnyHit(new BallHitEventArgs(collision.contacts[0].normal));

			if (isPlayerHit)
			{
				OnPlayerHit(collision);
			}
		}

		private void OnHitLowerSpeed()
		{
			var newSpeed = Speed - (Config.maxSpeed - Config.minSpeed) * Config.speedLoseOnCollision;
			newSpeed = Mathf.Clamp(newSpeed, Config.minSpeed, Config.maxSpeed);
			ApplyForce(newSpeed, null);
		}

		public void ApplyForce(float? newSpeed, Vector2? newDirection)
		{
			if (newSpeed.HasValue)
			{
				Speed = newSpeed.Value;
				OnSpeedChanged();
			}

			if (newDirection.HasValue)
			{
				Direction = newDirection.Value;
			}
		}

		private void OnBlockHit(BlockBase block)
		{
			if (BlockHit != null) BlockHit(this, new BlockHitEventArgs(block));
			block.TryInvokeOnBallHit(compBall);
		}

		/// <summary>
		/// Executed when a character hits the ball with his shield
		/// </summary>
		public void RegisterShieldHit(CharacterBase sender)
		{
			OnAnyHit(new BallHitEventArgs(null));
			if (ShieldHit != null) ShieldHit(this, new ShieldHitEventArgs(sender));
		}

		private void OnAnyHit(BallHitEventArgs e)
		{
			if (AnyHit != null) AnyHit(this, e);
		}

		private void OnPlayerHit(Collision2D collision)
		{
			if (PlayerHit != null) PlayerHit(this, new Physics2DHitEventArgs(collision));
		}

		private void OnSpeedChanged()
		{
			if (SpeedChanged != null) SpeedChanged(this, EventArgs.Empty);
		}

		private void OnDestroy()
		{
			// in case the ball gets destroyed while moving to the heart wall
			GameSpeedManager.Instance.RemoveWinConditionTrigger(gameObject);
		}
	}
}