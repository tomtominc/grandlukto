﻿using System;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallHitEventArgs : EventArgs
	{
		public Vector2? HitNormal { get; private set; }

		public BallHitEventArgs(Vector2? hitNormal)
		{
			HitNormal = hitNormal;
		}
	}
}