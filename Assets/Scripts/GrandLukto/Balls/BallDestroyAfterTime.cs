﻿using System.Collections.Generic;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallDestroyAfterTime : MonoBehaviour
	{
		private class BlinkSprite
		{
			public tk2dSprite sprite;
			public Color initialColor;
		}

		public float destroyAfter = 10;
		public float blinkTime = 2;

		public int blinks = 3;

		private List<BlinkSprite> blinkSprites;

		public void Start()
		{
			var sprites = GetComponentsInChildren<tk2dSprite>();

			blinkSprites = new List<BlinkSprite>();
			foreach(var sprite in sprites)
			{
				blinkSprites.Add(new BlinkSprite
				{
					sprite = sprite,
					initialColor = sprite.color
				});
			}
		}

		public void Update()
		{
			destroyAfter -= Time.deltaTime;

			if(destroyAfter <= blinkTime)
			{
				UpdateBlink();
			}

			if (destroyAfter <= 0)
			{
				Destroy(gameObject);
			}
		}

		private void UpdateBlink()
		{
			var curTimeRel = 1 - destroyAfter / blinkTime;

			// 0.5 +  sin(3.141592 / 2 + x * 2) * 0.5
			// Makes the object blink "blinks" times between 0 and 1. This is achieved by multiplying by Math.PI
			var alphaMult = 0.6f + Mathf.Sin(Mathf.PI / 2 + Mathf.PI * curTimeRel * 2 * blinks) * 0.4f;
			var alphaMultColor = new Color(1, 1, 1, alphaMult);

			foreach (var blinkSprite in blinkSprites)
			{
				blinkSprite.sprite.color = blinkSprite.initialColor * alphaMultColor;
			}
		}
	}
}
