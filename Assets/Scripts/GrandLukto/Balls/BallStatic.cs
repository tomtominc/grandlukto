﻿using GrandLukto.Characters;
using UnityEngine;

namespace GrandLukto.Balls
{
	public static class BallStatic
	{
		public static bool IsBallValid(Ball ball)
		{
			if (ball == null) return false;
			return IsBallValid(ball.gameObject);
		}

		public static bool IsBallValid(GameObject ball)
		{
			return ball != null && ball.activeInHierarchy;
		}

		public static bool IsMovingTowardsPlayer(Vector3 faceDirectionVec, Ball ball)
		{
			return Vector2.Dot(faceDirectionVec.normalized, ball.MoveDirection.normalized) < 0;
		}

		public static bool IsMovingTowardsPlayer(CharacterBase characterBase, Ball ball)
		{
			return IsMovingTowardsPlayer(characterBase.FaceDirectionVec, ball);
		}
	}
}