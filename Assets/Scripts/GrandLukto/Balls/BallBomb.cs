﻿using Spine.Unity;
using UnityCave.Spine;
using UnityCave.StateMachine;
using UnityEngine;
using GrandLukto.Blocks;
using UnityCave.Collision;
using GrandLukto.UI;
using UnityCave.CameraEffects;
using UnityCave.ExtensionMethods;
using Spine;

namespace GrandLukto.Balls
{
	/// <summary>
	/// Spawns a Bomb that explodes
	/// * on hitting a block
	/// * after a specified time.
	/// Continues as a normal Ball afterwards.
	/// </summary>
	public class BallBomb : Ball
	{
		private enum BallBombState
		{
			Move,
			Explode
		}

		[Header("Bomb")]

		[SpineAnimation(dataField: "compSpineAnimation")]
		public string animUp;

		[SpineAnimation(dataField: "compSpineAnimation")]
		public string animDown;

		[SpineAnimation(dataField: "compSpineAnimation")]
		public string animExplode;

		public float timeToExplode = 5;
		public CollisionHandler explosionHitbox;
		public Countdown countdown;

		private float timeToExplodeLeft;

		private SkeletonAnimation compSpineAnimation;

		private SimpleStateMachine<BallBombState> stateMachine = new SimpleStateMachine<BallBombState>(BallBombState.Move);

		public override void Start()
		{
			base.Start();

			compSpineAnimation = GetComponentInChildren<SkeletonAnimation>();

			countdown.gameObject.SetActive(false);

			timeToExplodeLeft = timeToExplode;

			stateMachine.OnEnterState += OnEnterState;
			stateMachine.Reset();

			CompBallPhysics.BlockHit += CompBallPhysics_BlockHit;
		}

		private void OnEnterState(BallBombState newState)
		{
			switch (newState)
			{
				case BallBombState.Explode:
					var gameConfig = GameManager.Instance.gameConfiguration;

					gameObject.GetOrAddComponent<AudioSource>().TryPlayOneShot(this, gameConfig.soundBombExplode);
					GameManager.Instance.ScreenShake.Shake(new ScreenShakeSetting(0.4f, 0.05f, 10));

					// check if we can spawn an old ball that will move on
					var oldBallReference = GetComponent<OldBallReference>();
					if (oldBallReference != null && oldBallReference.oldBall != null)
					{
						var oldBall = oldBallReference.LoadBall();
						oldBall.InitializePosition(transform.position);
						oldBall.CopyValuesFrom(this);
					}

					compSpineAnimation.AnimationState.Complete += OnExplodeAnimComplete;
					compSpineAnimation.AnimationState.SetAnimation(0, animExplode, false);

					CompBallPhysics.ApplyForce(0, null); // stop movement

					BlockStatic.ExplodeDestroyBlocks(explosionHitbox, gameConfig.fadeColorExplode);

					ToggleEnabled(false); // disable to avoid characters hitting the ball

					// disable hitbox to avoid collisions
					var colliders = GetComponents<Collider2D>();
					for(var i = 0; i < colliders.Length; i++)
					{
						colliders[i].enabled = false;
					}
					break;
			}
		}

		public override void Update()
		{
			base.Update();

			switch (stateMachine.CurState)
			{
				case BallBombState.Move:
					timeToExplodeLeft -= Time.deltaTime;

					if(!countdown.isActiveAndEnabled && timeToExplodeLeft <= 3)
					{
						StartCountdown();
					}

					if (timeToExplodeLeft <= 0)
					{
						stateMachine.ChangeState(BallBombState.Explode);
						break;
					}

					var curAnim = compSpineAnimation.AnimationName;

					if (MoveDirection.y > 0)
					{
						compSpineAnimation.AnimationState.SetAnimationIfNotPlaying(0, animUp, true);
					}
					else
					{
						compSpineAnimation.AnimationState.SetAnimationIfNotPlaying(0, animDown, true);
					}

					break;
			}
		}

		private void StartCountdown()
		{
			countdown.gameObject.SetActive(true);
			countdown.StartCountdown();
		}

		private void CompBallPhysics_BlockHit(object sender, BlockHitEventArgs e)
		{
			CompBallPhysics.BlockHit -= CompBallPhysics_BlockHit;

			stateMachine.ChangeStateIfNotActive(BallBombState.Explode);
		}
		
		private void OnExplodeAnimComplete(TrackEntry trackEntry)
		{
			compSpineAnimation.AnimationState.Complete -= OnExplodeAnimComplete;
			Destroy(gameObject);
		}

		private void OnDestroy()
		{
			CompBallPhysics.BlockHit -= CompBallPhysics_BlockHit;
		}
	}
}