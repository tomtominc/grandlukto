﻿using GrandLukto.Balls.Implementations;
using System;
using UnityEngine;

namespace GrandLukto.Balls.Factories
{
	/// <summary>
	/// Factory to create Balls by their type.
	/// </summary>
	public class BallFactory : IBallFactory
	{
		public IBallController CreateBall(BallType ballType)
		{
			var gameObject = CreateBallGameObject(ballType);
			return FinalizeBall(gameObject, ballType);
		}

		/// <summary>
		/// Creates a ball of type Vanish with special settings.
		/// </summary>
		public IBallController CreateBallVanish(float lifetime)
		{
			var gameObject = CreateBallGameObject(BallType.Vanish);

			var destroyAfterTime = gameObject.GetComponent<BallDestroyAfterTime>();
			destroyAfterTime.destroyAfter = lifetime;

			return FinalizeBall(gameObject, BallType.Vanish);
		}

		/// <summary>
		/// Creates a ball of type Bomb which will continue with the given oldBall after exploding.
		/// </summary>
		public IBallController CreateBallBomb(IBallController oldBall)
		{
			var gameObject = CreateBallGameObject(BallType.Bomb);

			var oldBallReference = gameObject.AddComponent<OldBallReference>();
			oldBallReference.SaveBall(oldBall);

			return FinalizeBall(gameObject, BallType.Vanish);
		}

		private IBallController FinalizeBall(GameObject gameObject, BallType ballType)
		{
			var ball = gameObject.GetComponent<IBallController>();
			if (ball == null) throw new Exception("Created ball did not implement the IBallController interface.");

			ModifyBallController(ballType, ball);

			gameObject.name = string.Format("Ball - {0}", ballType);
			return ball;
		}

		private void ModifyBallController(BallType ballType, IBallController ball)
		{
			switch(ballType)
			{
				case BallType.Standard:
				case BallType.Vanish:
					ball.Initialize(true);
					break;
				default:
					ball.Initialize(false);
					break;
			}
		}

		private GameObject CreateBallGameObject(BallType ballType)
		{
			switch (ballType)
			{
				case BallType.Standard:
					return LoadPrefab("Ball");

				case BallType.Rock:
					var rockBall = CreateBallGameObject(BallType.Standard);
					rockBall.AddComponent<BallRock>();
					return rockBall;

				case BallType.Vanish:
					var vanishBall = CreateBallGameObject(BallType.Standard);
					vanishBall.AddComponent<BallDestroyAfterTime>();
					return vanishBall;

				case BallType.Bomb:
					return LoadPrefab("Ball_Bomb");

				case BallType.Snowball:
					var snowball = CreateBallGameObject(BallType.Standard);
					snowball.AddComponent<BallSnowball>();
					return snowball;

				default: throw new NotImplementedException();
			}
		}

		private GameObject LoadPrefab(string prefabPath)
		{
			return GameObject.Instantiate(Resources.Load<GameObject>(prefabPath));
		}
	}
}