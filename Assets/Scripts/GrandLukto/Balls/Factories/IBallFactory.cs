﻿namespace GrandLukto.Balls.Factories
{
	public interface IBallFactory
	{
		IBallController CreateBall(BallType ballType);
	}
}