﻿using UnityCave.Utils;
using UnityEngine;
using UnityCave.ExtensionMethods;
using System;

namespace GrandLukto.Balls
{
	[RequireComponent(typeof(Ball))]
	public class BallSpawnAnimation : MonoBehaviour
	{
		public event EventHandler AnimationFinished;

		private enum AnimationStep
		{
			FadeShadow,
			MoveDown,
			Spring,
			Finished
		}

		private GameObject visuals;
		private tk2dSprite shadowSprite;

		public float fadeShadowSpeed = 1.5f;

		public float moveDownSpeed = 30;

		public float startSpringOffset = 2f;

		[Range(0, 1)]
		public float zeta = 0.74f;

		[Range(0, 100)]
		public float omega = 13;

		private float targetVisualsY;
		private float curVelocity = 0;

		private AnimationStep curStep;

		private Ball compBall;

		public void Awake()
		{
			compBall = GetComponent<Ball>();
			visuals = compBall.visuals;
			shadowSprite = compBall.shadow.GetComponent<tk2dSprite>();
		}

		public void Start()
		{
			targetVisualsY = visuals.transform.localPosition.y;
			ResetAnimation();
		}

		public void Update()
		{
			switch(curStep)
			{
				case AnimationStep.FadeShadow:
					var curScale = shadowSprite.scale.x;
					var newScale = Mathf.MoveTowards(curScale, 1, fadeShadowSpeed * Time.deltaTime);

					if(newScale >= 1)
					{
						newScale = 1;
						GameManager.Instance.gameConfiguration.soundOnBallSpawnAnim.Play(this, gameObject.GetOrAddComponent<AudioSource>());
						curStep = AnimationStep.MoveDown;
					}

					shadowSprite.scale = new Vector3(newScale, newScale, 1);
					break;

				case AnimationStep.MoveDown:
					curVelocity = moveDownSpeed;
					visuals.transform.localPosition = Vector3.MoveTowards(visuals.transform.localPosition, visuals.transform.localPosition.Clone(y: targetVisualsY), curVelocity * Time.deltaTime);

					if (Mathf.Abs(targetVisualsY - visuals.transform.localPosition.y) <= startSpringOffset)
					{
						curStep = AnimationStep.Spring;
						curVelocity = -curVelocity;
						compBall.ToggleEnabled(true); // enable the ball already to allow hitting sooner
					}
					break;

				case AnimationStep.Spring:
					float curPos = visuals.transform.localPosition.y;

					EaseUtil.Spring(ref curPos, ref curVelocity, targetVisualsY, zeta, omega, Time.deltaTime);

					visuals.transform.localPosition = visuals.transform.localPosition.Clone(y: curPos);

					if (Mathf.Abs(curVelocity) < 0.001 && Mathf.Abs(curPos - targetVisualsY) < 0.001)
					{
						OnAnimationFinished();
						curStep = AnimationStep.Finished;
					}
					break;
			}
		}

		private void OnAnimationFinished()
		{
			if (AnimationFinished != null) AnimationFinished(this, EventArgs.Empty);

			Destroy(this);
		}

		[ContextMenu("Reset Animation")]
		public void ResetAnimation()
		{
			compBall.ToggleEnabled(false);

			SetToAnimationStartPosition();
			shadowSprite.scale = new Vector3(0, 0, 1);

			curStep = default(AnimationStep);
		}

		private void SetToAnimationStartPosition()
		{
			visuals.transform.localPosition = visuals.transform.localPosition.Clone(y: targetVisualsY + 12);
		}
	}
}