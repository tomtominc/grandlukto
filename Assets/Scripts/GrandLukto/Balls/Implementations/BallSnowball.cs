﻿using DG.Tweening;
using GrandLukto.Characters;
using UnityCave.CameraEffects;
using UnityCave.ExtensionMethods;
using UnityCave.Physics;
using UnityCave.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.Balls.Implementations
{
	public class BallSnowball : MonoBehaviour
	{
		private Ball compBall;
		private BallPhysics compBallPhysics;

		private int hitCount = 0;
		private int fullSizeCount = 3;

		private GameObject growContainer;

		public float maxScale = 2;

		private Tweener shakeTween;
		public ScreenShakeSetting shakeSetting = new ScreenShakeSetting(1f, 0.05f, 40);

		private void Awake()
		{
			compBall = GetComponent<Ball>();
			Assert.IsNotNull(compBall);

			// ugly hack to display different visuals...
			compBall.visuals.GetComponent<tk2dSprite>().SetSprite("snowball");
			//compBall.shadow.GetComponent<tk2dSprite>().SetSprite("snowball");

			compBallPhysics = GetComponent<BallPhysics>();
			compBallPhysics.PlayerHit += CompBallPhysics_PlayerHit;
			compBallPhysics.ShieldHit += CompBallPhysics_ShieldHit;
		}

		private void Start()
		{
			growContainer = compBall.visuals.gameObject.Wrap("Snowball Grow");
		}

		private void CompBallPhysics_PlayerHit(object sender, Physics2DHitEventArgs e)
		{
			OnHitCharacter(e.Collision.gameObject);
		}

		private void CompBallPhysics_ShieldHit(object sender, ShieldHitEventArgs e)
		{
			OnHitCharacter(e.Character.gameObject);
		}

		private void OnHitCharacter(GameObject character)
		{
			hitCount++;

			// what if we hit a player from back? still ice him and grow?
			// wind level => defend against balls
			// ai that hits every ball back except if it is disabled
			// wind ability that sends all balls in the other direction?

			growContainer.transform.localScale = Vector3.one * Mathf.Lerp(1, maxScale, EaseUtil.Cubic.In(hitCount / (float)fullSizeCount));


			if (hitCount == fullSizeCount)
			{
				shakeTween = growContainer.transform
					.DOShakePosition(shakeSetting)
					.SetLoops(-1);
			}
			else if (hitCount == fullSizeCount + 1)
			{
				// TODO: delay before destroying ball (check if player can hit ball?)
				// ice the target and destroy the ball
				PlayerIced.IcePlayer(character);
				Destroy(gameObject);
			}
		}

		private void OnDestroy()
		{
			shakeTween.Kill();

			compBallPhysics.PlayerHit -= CompBallPhysics_PlayerHit;
			compBallPhysics.ShieldHit -= CompBallPhysics_ShieldHit;
		}
	}
}