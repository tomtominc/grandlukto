﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace GrandLukto.Balls.Implementations
{
	public class BallRock : MonoBehaviour
	{
		private Ball compBall;
		private BallPhysics compBallPhysics;

		private Vector2 lastDirection;

		/// <summary>
		/// True if the ball did move in at least one fixed update.
		/// </summary>
		private bool didMove = false;
		
		private void Awake()
		{
			compBall = GetComponent<Ball>();
			Assert.IsNotNull(compBall);

			// ugly hack to display different visuals...
			compBall.visuals.GetComponent<tk2dSprite>().SetSprite("rock");
			compBall.shadow.GetComponent<tk2dSprite>().SetSprite("rock");

			compBall.prefabEffectOnHit = null; // disable OnHit effect since we destroy the ball on first hit

			compBallPhysics = GetComponent<BallPhysics>();
			compBallPhysics.AnyHit += BallPhysics_AnyHit;
		}

		private void FixedUpdate()
		{
			lastDirection = compBallPhysics.Direction; // save last direction since ball gets reflected before dispatching AnyHit event
			didMove = didMove || compBallPhysics.IsMoving;
		}
		
		private void BallPhysics_AnyHit(object sender, BallHitEventArgs e)
		{
			if (!didMove) return;

			// destroy the ball on first hit and spawn rock particles
			Destroy(gameObject);

			// spawn rock particles
			// TODO: Cache particles
			var rockParticles = Instantiate(Resources.Load<GameObject>("Balls/RockParticles"));
			rockParticles.transform.position = transform.position;

			if (!didMove)
			{
				var particleSystem = rockParticles.GetComponent<ParticleSystem>();
				var shape = particleSystem.shape;
				shape.shapeType = ParticleSystemShapeType.Cone;
			}
			else
			{
				Vector2 particleDirection;

				if (e.HitNormal.HasValue && Vector2.Dot(lastDirection, e.HitNormal.Value) > 0)
				{
					// hit from behind
					particleDirection = lastDirection;
				}
				else
				{
					particleDirection = -lastDirection;
				}

				// particle system is facing up by default
				rockParticles.transform.rotation = Quaternion.FromToRotation(Vector2.right, particleDirection);
			}
		}

		private void OnDestroy()
		{
			compBallPhysics.AnyHit -= BallPhysics_AnyHit;
		}
	}
}