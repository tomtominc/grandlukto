﻿using System;
using System.Collections.Generic;
using UnityCave.ExtensionMethods;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallHitAnimationPlayer : MonoBehaviour
	{
		private List<BallHitAnimation> poolAnimations = new List<BallHitAnimation>();
		
		public void OnCollisionEnter2D(Collision2D collision)
		{
			if (collision.gameObject.tag != Tag.Ball) return;
			if (collision.contacts.Length < 1) return;

			var contact = collision.contacts[0];

			var anim = GetAnimationFromPoolOrNew();
			anim.gameObject.SetActive(true);
			anim.transform.position = contact.point;
			anim.SetImpactNormal(contact.normal);

			GameManager.Instance.gameConfiguration.soundOnBallHitWall.Play(this, anim.gameObject.GetOrAddComponent<AudioSource>());
		}

		public BallHitAnimation GetAnimationFromPoolOrNew()
		{
			BallHitAnimation anim;

			if(poolAnimations.Count > 0)
			{
				anim = poolAnimations[0];
				poolAnimations.Remove(anim);
				return anim;
			}

			anim = ((GameObject)Instantiate(Resources.Load("Effects/BallImpact"), transform)).GetComponent<BallHitAnimation>();
			anim.AnimationFinished += OnAnimationFinished;
			return anim;
		}

		private void OnAnimationFinished(object sender, EventArgs e)
		{
			poolAnimations.Add((BallHitAnimation)sender);
		}
	}
}
