﻿using UnityEngine;

namespace GrandLukto.Balls.Controllers
{
	public class BallController : MonoBehaviour, IBallController
	{
		private BallPhysics ballPhysics;
		private Ball ball;

		public BallConfiguration Configuration { get { return ball.config; } }

		/// <summary>
		/// If the ball can be transformed to another type by e.g. using an onHit power up.
		/// </summary>
		public bool CanTransformBall { get; private set; }

		private void Awake()
		{
			ball = GetComponent<Ball>();
			ballPhysics = GetComponent<BallPhysics>();
		}

		public void Initialize(bool canTransformBall)
		{
			CanTransformBall = canTransformBall;
		}

		public void ApplyForce(float? newSpeed, Vector2? newDirection)
		{
			ballPhysics.ApplyForce(newSpeed, newDirection);
		}

		public void InitializePosition(Vector3 position)
		{
			transform.position = position;
		}

		public void CopyValuesFrom(Ball ballToClone)
		{
			transform.position = ballToClone.transform.position;

			var newBall = GetComponent<Ball>();
			newBall.CopyInternalValuesFrom(ballToClone);

			var physics = GetComponent<BallPhysics>();
			physics.ApplyForce(ballToClone.CurSpeed, ballToClone.MoveDirection);
		}

		public void InitializeWithSpawnAnimation()
		{
			gameObject.AddComponent<BallSpawnAnimation>();
		}

		public void SetActive(bool isActive)
		{
			gameObject.SetActive(isActive);
		}
	}
}