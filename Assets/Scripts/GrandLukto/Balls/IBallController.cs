﻿using UnityEngine;

namespace GrandLukto.Balls
{
	public interface IBallController
	{
		BallConfiguration Configuration { get; }
		bool CanTransformBall { get; }

		void ApplyForce(float? newSpeed, Vector2? newDirection);
		void SetActive(bool isActive);

		/// <summary>
		/// Clones the values from the given ballToClone to the current ball.
		/// Position, movement, ...
		/// </summary>
		void CopyValuesFrom(Ball ballToClone);

		void Initialize(bool canTransformBall);
		void InitializePosition(Vector3 position);
		void InitializeWithSpawnAnimation();
	}
}