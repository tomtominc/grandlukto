﻿using Spine;
using Spine.Unity;
using System;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallHitAnimation : MonoBehaviour
	{
		public EventHandler AnimationFinished;

		private SkeletonAnimation activeChildSkeletonAnim;
		private GameObject activeChild;

		public void Start()
		{
		}

		public void OnEnable()
		{
			// set one child element to active, others to inactive
			var randomChildNum = UnityEngine.Random.Range(0, transform.childCount);

			for(var i = 0; i < transform.childCount; i++)
			{
				var curChild = transform.GetChild(i).gameObject;
				curChild.SetActive(false);

				if(i == randomChildNum)
				{
					activeChild = curChild;
				}
			}

			activeChild.SetActive(true);


			activeChildSkeletonAnim = activeChild.GetComponent<SkeletonAnimation>();

			activeChildSkeletonAnim.AnimationState.AddAnimation(0, activeChildSkeletonAnim.AnimationName, false, 0);
			activeChildSkeletonAnim.AnimationState.Complete += OnSkeletonAnimComplete;
		}

		private void OnSkeletonAnimComplete(TrackEntry trackEntry)
		{
			activeChildSkeletonAnim.AnimationState.Complete -= OnSkeletonAnimComplete;

			OnAnimationFinished();
			gameObject.SetActive(false);
		}

		private void OnAnimationFinished()
		{
			if (AnimationFinished == null) return;
			AnimationFinished(this, EventArgs.Empty);
		}

		public void SetImpactNormal(Vector3 normal)
		{
			var impactFromLeft = Vector2.Dot(normal, Vector2.right) > 0;
			activeChild.transform.localScale = new Vector3(impactFromLeft ? 1 : -1, 1, 1);
		}
	}
}
