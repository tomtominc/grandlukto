﻿using System.Collections.Generic;
using System.Linq;
using UnityCave;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto.Balls
{
	public class BallMagnetManager : ManagerBase
	{
		private List<BallMagnetData> activeMagnets = new List<BallMagnetData>();

		public static BallMagnetManager Instance
		{
			get
			{
				return SingletonUtil.GetInstance<BallMagnetManager>();
			}
		}

		public void AddMagnet(BallMagnetData magnetData)
		{
			// remove all magnets that act on the same transform (do not allow stacking the power but extending it)
			activeMagnets.RemoveAll(activeMagnetData => activeMagnetData.Transform == magnetData.Transform);

			activeMagnets.Add(magnetData);
		}

		public Vector3 EvaluateAttraction(Ball ball)
		{
			Vector3 totalAttraction = Vector3.zero;

			for(var i = 0; i < activeMagnets.Count; i++)
			{
				var magnetData = activeMagnets[i];
				totalAttraction += magnetData.EvaluateAttraction(ball);
			}

			return totalAttraction;
		}

		public void LateUpdate()
		{
			// clean up inactive magnets
			activeMagnets = activeMagnets
				.Where(magnetData => magnetData.IsActive)
				.ToList();
		}
	}
}