﻿using UnityCave.CustomDataTypes;
using UnityEngine;

namespace GrandLukto.Balls
{
	[CreateAssetMenu(menuName = "Grand Lukto/BallConfiguration")]
	public class BallConfiguration : ScriptableObject
	{
		public float minSpeed = 2.5f;
		public float maxSpeed = 5;
		public AnimationCurve speedCurve;

		public float curveBallSpeedMin = 0.02f;
		public float curveBallSpeedMax = 0.04f;

		[Range(0, 1), Tooltip("The speed in percent (between min and max speed) lost on collision .")]
		public float speedLoseOnCollision = 0.25f;

		[Range(0, 1), Tooltip("The speed in percent (between min and max speed) that the ball can lose on player hit.")]
		public float maxSpeedLoseOnPlayerHit = 0.5f;

		public FloatRange trailTime = new FloatRange() { rangeStart = 0.2f, rangeEnd = 0.35f };

		public float heartWallRaycastLength = 1f;
		public LayerMask layerMaskBlocks;

		public float GetRelativeSpeed(float relativeSpeed)
		{
			return Mathf.Lerp(minSpeed, maxSpeed, Mathf.Clamp01(relativeSpeed));
		}
	}
}
