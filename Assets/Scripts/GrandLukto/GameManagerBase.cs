﻿using GrandLukto.Characters;
using GrandLukto.UI.Dialog;
using Rewired;
using System;
using System.Collections.Generic;
using UnityCave;
using UnityCave.Utils;
using UnityEngine;

namespace GrandLukto
{
	public class GamePausedArgs : EventArgs
	{
		public PlayerSetting Player { get; private set; }

		public GamePausedArgs(PlayerSetting player)
		{
			Player = player;
		}
	}

	public class GameManagerBase : MonoBehaviour
	{
		public Camera uiCamera;
		public GameObject uiContainer;

		public EventHandler<GamePausedArgs> GamePaused;

		protected List<PlayerSetting> PlayerSettings { get; set; }

		private TimeManager timeManager;

		private UIDialog uiDialog;

		public GameManagerBase()
		{
			PlayerSettings = new List<PlayerSetting>();
		}

		public static GameManagerBase Instance
		{
			get
			{
				return SingletonUtil.GetInstance<GameManagerBase>();
			}
		}

		public virtual void Start()
		{
			timeManager = TimeManager.Instance;
		}

		public virtual void Update()
		{
			CheckPause();

#if UNITY_EDITOR
			if (ReInput.controllers == null || ReInput.controllers.Keyboard == null) return;
			CheckDebugKeys(ReInput.controllers.Keyboard);
#endif
		}

		private void CheckPause()
		{
			PlayerSetting playerSetting;
			if (timeManager.IsPaused || !IsButtonPressed(Control.Pause, out playerSetting)) return;

			timeManager.TogglePause(true);
			OnGamePaused(playerSetting);
		}

		protected bool IsButtonPressed(string buttonName, out PlayerSetting player)
		{
			player = null;

			foreach (var playerSetting in PlayerSettings)
			{
				if (!playerSetting.isHuman) continue; // npcs can't toggle pause

				if (playerSetting.controller.GetButtonDown(buttonName))
				{
					player = playerSetting;
					return true;
				}
			}

			return false;
		}

		private void OnGamePaused(PlayerSetting player)
		{
			if (GamePaused == null) return;
			GamePaused.Invoke(this, new GamePausedArgs(player));
		}

		protected virtual void CheckDebugKeys(Keyboard keyboard)
		{
		}

		public void ShowDialog(DialogDataWrapper dialogData, Action onDialogFinished = null)
		{
			if (uiDialog == null)
			{
				var dialogGameObj = Instantiate(Resources.Load<GameObject>("UI/Dialog"), uiContainer.transform, false);
				uiDialog = dialogGameObj.GetComponent<UIDialog>();
			}

			uiDialog.StartConversation(dialogData, onDialogFinished);
		}
	}
}
